# main makefile to download the data package(s), built the tool,
# and run the unittests and coverage scripts

.PHONY: test unittest fulltest clean cleanall

# some settings
CONFIGFILE = Make.config
EXPECTED_SOURCE_TAR_FILE = CLM_dist/src/multiscatter-1.2.11.tar.gz
EXPECTED_MSIF_TEST_SO_FILE = CLM_dist/src/multiscatter_ctypes_interface_test/lib/libmsiftest.so

.PHONY: all
all: $(CONFIGFILE) $(EXPECTED_SOURCE_TAR_FILE) $(EXPECTED_MSIF_TEST_SO_FILE)

$(CONFIGFILE) $(EXPECTED_SOURCE_TAR_FILE):
	@echo "ERROR: sorry, you need to run the configure.py script first"
	@echo "ERROR: before launching make!"
	exit 1

$(DATAPACKAGE_NAME):
	./CLM_dist/lib/download_data_package.py

$(EXPECTED_MSIF_TEST_SO_FILE): $(EXPECTED_SOURCE_TAR_FILE)
	cd CLM_dist; make

test: fulltest

unittest: $(EXPECTED_MSIF_TEST_SO_FILE)
	cd CLM_dist; make unittest

coverage: $(EXPECTED_MSIF_TEST_SO_FILE)
	cd CLM_dist; make coverage

fulltest: $(EXPECTED_MSIF_TEST_SO_FILE)
	cd CLM_dist; make test

pylint:
	cd CLM_dist; ./scripts/run_pylint.py

clean:
	cd CLM_dist; make cleantest
	cd CLM_dist; make clean

cleanall: clean
	cd CLM_dist; make cleanall
	\rm -f $(CARDINAL_Campaign_Tools)*tar.gz
	\rm Make.config

# workaround to manually add activate test data
.PHONY: activate
activate:
	tar xvfz CARDINAL_Campaign_Tools_ACTIVATE_DATA_extras.tar.gz

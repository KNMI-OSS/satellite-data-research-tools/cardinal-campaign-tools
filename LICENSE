This project is dual licensed.
As a user can choose wich license fits your purpose best.
You can choose between:

1) Apache License v2.0
and
2) European Space Agency Public License – v2.4 – Permissive (Type 3)

The full text for both licenses are included below.

See also:
https://www.apache.org/licenses/LICENSE-2.0
and
https://essr.esa.int/license/european-space-agency-public-license-v2-4-permissive-type-3

---------------------------------------------------------------------------
                                Apache License
                          Version 2.0, January 2004
                       http://www.apache.org/licenses/

   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

   1. Definitions.

      "License" shall mean the terms and conditions for use, reproduction,
      and distribution as defined by Sections 1 through 9 of this document.

      "Licensor" shall mean the copyright owner or entity authorized by
      the copyright owner that is granting the License.

      "Legal Entity" shall mean the union of the acting entity and all
      other entities that control, are controlled by, or are under common
      control with that entity. For the purposes of this definition,
      "control" means (i) the power, direct or indirect, to cause the
      direction or management of such entity, whether by contract or
      otherwise, or (ii) ownership of fifty percent (50%) or more of the
      outstanding shares, or (iii) beneficial ownership of such entity.

      "You" (or "Your") shall mean an individual or Legal Entity
      exercising permissions granted by this License.

      "Source" form shall mean the preferred form for making modifications,
      including but not limited to software source code, documentation
      source, and configuration files.

      "Object" form shall mean any form resulting from mechanical
      transformation or translation of a Source form, including but
      not limited to compiled object code, generated documentation,
      and conversions to other media types.

      "Work" shall mean the work of authorship, whether in Source or
      Object form, made available under the License, as indicated by a
      copyright notice that is included in or attached to the work
      (an example is provided in the Appendix below).

      "Derivative Works" shall mean any work, whether in Source or Object
      form, that is based on (or derived from) the Work and for which the
      editorial revisions, annotations, elaborations, or other modifications
      represent, as a whole, an original work of authorship. For the purposes
      of this License, Derivative Works shall not include works that remain
      separable from, or merely link (or bind by name) to the interfaces of,
      the Work and Derivative Works thereof.

      "Contribution" shall mean any work of authorship, including
      the original version of the Work and any modifications or additions
      to that Work or Derivative Works thereof, that is intentionally
      submitted to Licensor for inclusion in the Work by the copyright owner
      or by an individual or Legal Entity authorized to submit on behalf of
      the copyright owner. For the purposes of this definition, "submitted"
      means any form of electronic, verbal, or written communication sent
      to the Licensor or its representatives, including but not limited to
      communication on electronic mailing lists, source code control systems,
      and issue tracking systems that are managed by, or on behalf of, the
      Licensor for the purpose of discussing and improving the Work, but
      excluding communication that is conspicuously marked or otherwise
      designated in writing by the copyright owner as "Not a Contribution."

      "Contributor" shall mean Licensor and any individual or Legal Entity
      on behalf of whom a Contribution has been received by Licensor and
      subsequently incorporated within the Work.

   2. Grant of Copyright License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      copyright license to reproduce, prepare Derivative Works of,
      publicly display, publicly perform, sublicense, and distribute the
      Work and such Derivative Works in Source or Object form.

   3. Grant of Patent License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      (except as stated in this section) patent license to make, have made,
      use, offer to sell, sell, import, and otherwise transfer the Work,
      where such license applies only to those patent claims licensable
      by such Contributor that are necessarily infringed by their
      Contribution(s) alone or by combination of their Contribution(s)
      with the Work to which such Contribution(s) was submitted. If You
      institute patent litigation against any entity (including a
      cross-claim or counterclaim in a lawsuit) alleging that the Work
      or a Contribution incorporated within the Work constitutes direct
      or contributory patent infringement, then any patent licenses
      granted to You under this License for that Work shall terminate
      as of the date such litigation is filed.

   4. Redistribution. You may reproduce and distribute copies of the
      Work or Derivative Works thereof in any medium, with or without
      modifications, and in Source or Object form, provided that You
      meet the following conditions:

      (a) You must give any other recipients of the Work or
          Derivative Works a copy of this License; and

      (b) You must cause any modified files to carry prominent notices
          stating that You changed the files; and

      (c) You must retain, in the Source form of any Derivative Works
          that You distribute, all copyright, patent, trademark, and
          attribution notices from the Source form of the Work,
          excluding those notices that do not pertain to any part of
          the Derivative Works; and

      (d) If the Work includes a "NOTICE" text file as part of its
          distribution, then any Derivative Works that You distribute must
          include a readable copy of the attribution notices contained
          within such NOTICE file, excluding those notices that do not
          pertain to any part of the Derivative Works, in at least one
          of the following places: within a NOTICE text file distributed
          as part of the Derivative Works; within the Source form or
          documentation, if provided along with the Derivative Works; or,
          within a display generated by the Derivative Works, if and
          wherever such third-party notices normally appear. The contents
          of the NOTICE file are for informational purposes only and
          do not modify the License. You may add Your own attribution
          notices within Derivative Works that You distribute, alongside
          or as an addendum to the NOTICE text from the Work, provided
          that such additional attribution notices cannot be construed
          as modifying the License.

      You may add Your own copyright statement to Your modifications and
      may provide additional or different license terms and conditions
      for use, reproduction, or distribution of Your modifications, or
      for any such Derivative Works as a whole, provided Your use,
      reproduction, and distribution of the Work otherwise complies with
      the conditions stated in this License.

   5. Submission of Contributions. Unless You explicitly state otherwise,
      any Contribution intentionally submitted for inclusion in the Work
      by You to the Licensor shall be under the terms and conditions of
      this License, without any additional terms or conditions.
      Notwithstanding the above, nothing herein shall supersede or modify
      the terms of any separate license agreement you may have executed
      with Licensor regarding such Contributions.

   6. Trademarks. This License does not grant permission to use the trade
      names, trademarks, service marks, or product names of the Licensor,
      except as required for reasonable and customary use in describing the
      origin of the Work and reproducing the content of the NOTICE file.

   7. Disclaimer of Warranty. Unless required by applicable law or
      agreed to in writing, Licensor provides the Work (and each
      Contributor provides its Contributions) on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
      implied, including, without limitation, any warranties or conditions
      of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
      PARTICULAR PURPOSE. You are solely responsible for determining the
      appropriateness of using or redistributing the Work and assume any
      risks associated with Your exercise of permissions under this License.

   8. Limitation of Liability. In no event and under no legal theory,
      whether in tort (including negligence), contract, or otherwise,
      unless required by applicable law (such as deliberate and grossly
      negligent acts) or agreed to in writing, shall any Contributor be
      liable to You for damages, including any direct, indirect, special,
      incidental, or consequential damages of any character arising as a
      result of this License or out of the use or inability to use the
      Work (including but not limited to damages for loss of goodwill,
      work stoppage, computer failure or malfunction, or any and all
      other commercial damages or losses), even if such Contributor
      has been advised of the possibility of such damages.

   9. Accepting Warranty or Additional Liability. While redistributing
      the Work or Derivative Works thereof, You may choose to offer,
      and charge a fee for, acceptance of support, warranty, indemnity,
      or other liability obligations and/or rights consistent with this
      License. However, in accepting such obligations, You may act only
      on Your own behalf and on Your sole responsibility, not on behalf
      of any other Contributor, and only if You agree to indemnify,
      defend, and hold each Contributor harmless for any liability
      incurred by, or claims asserted against, such Contributor by reason
      of your accepting any such warranty or additional liability.

   END OF TERMS AND CONDITIONS

   APPENDIX: How to apply the Apache License to your work.

      To apply the Apache License to your work, attach the following
      boilerplate notice, with the fields enclosed by brackets "[]"
      replaced with your own identifying information. (Don't include
      the brackets!)  The text should be enclosed in the appropriate
      comment syntax for the file format. We also recommend that a
      file or class name and description of purpose be included on the
      same "printed page" as the copyright notice for easier
      identification within third-party archives.

   Copyright [yyyy] [name of copyright owner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

---------------------------------------------------------------------------

European Space Agency Public License (ESA-PL) Permissive (Type 3) – v2.4

1 Definitions

1.1 “Contributor” means (a) the individual or legal entity that originally
creates or later modifies the Software and (b) each subsequent individual
or legal entity that creates or contributes to the creation of Modifications.

1.2 “Contributor Version” means the version of the Software on which the
Contributor based its Modifications.

1.3 “Distribution” and “Distribute” means any act of selling, giving, lending,
renting, distributing, communicating, transmitting, or otherwise making
available, physically or electronically or by any other means, copies of
the Software or Modifications.

1.4 “ESA” means the European Space Agency.

1.5 “License” means this document.

1.6 “Licensor” means the individual or legal entity that Distributes the
Software under the License to You.

1.7 “Modification” means any work or software created that is based upon or
derived from the Software (or portions thereof) or a modification of the
Software (or portions thereof). For the avoidance of doubt, linking a
library to the Software results in a Modification.

1.8 “Object Code” means any non-Source Code form of the Software and/or
Modifications.

1.9 “Patent Claims” (of a Contributor) means any patent claim(s), owned at
the time of the Distribution or subsequently acquired, including without
limitation, method, process and apparatus claims, in any patent licensable
by a Contributor which would be infringed by making use of the rights
granted under Sec. 2.1, including but not limited to make, have made,
use, sell, offer for sale or import of the Contributor Version and/or
such Contributor’s Modifications (if any), either alone or in combination
with the Contributor Version. “Licensable” means having the right to grant,
whether at the time of the Distribution or subsequently acquired, the rights
conveyed herein.

1.10 “Software” means the software Distributed under this License by the
Licensor, in Source Code and/or Object Code form.

1.11 “Source Code” means the preferred, usually human readable form of the
Software and/or Modifications in which modifications are made and the
associated documentation included in or with such code.

1.12 “You” means an individual or legal entity exercising rights under this
License (the licensee). 

2 Grant of Rights

2.1 Copyright

The Licensor, and each Contributor in respect of such Contributor’s
Modifications, hereby grants You a world-wide, royalty-free, non-exclusive
license under Copyright, subject to the terms and conditions of this License,
to:
·      use the Software;
·      reproduce the Software by any or all means and in any or all form;
·      Modify the Software and create works based on the Software;
·      communicate to the public, including making available, display or
       perform the Software or copies thereof to the public;
·      Distribute, sublicense, lend and rent the Software.

The license grant is perpetual and irrevocable, unless terminated pursuant
to Sec. 8.

2.2 Patents

Each Contributor in respect of such Contributor’s Modifications, hereby grants
You a world-wide, royalty-free, non-exclusive, sub-licensable license under
Patent Claims to the extent necessary to make use of the rights granted under
Sec. 2.1, including but not limited to make, have made, use, sell, offer for
sale, import, export and Distribute such Contributor’s Modifications and the
combination of such Contributor’s Modifications with the Contributor Version
(collectively called the “Patent Licensed Version” of the Software).

No patent license is granted for claims that are infringed:

·      only as a consequence of further modification of the Patent Licensed
       Version; or
·      by the combination of the Patent Licensed Version with other software
       or other devices or hardware, unless such combination was an intended
       use case of the Patent Licensed Version (e.g. a general purpose library
       is intended to be used with other software, a satellite navigation
       software is intended to be used with appropriate hardware); or
·      by a Modification under Patent Claims in the absence of the
       Contributor’s Modifications or by a combination of the Contributor’s
       Modifications with software other than the Patent Licensed Version or
       Modifications thereof.

2.3 Trademark

This License does not grant permission to use trade names, trademarks,
services marks, logos or names of the Licensor, except as required for
reasonable and customary use in describing the origin of the Software and
as reasonable necessary to comply with the obligations of this License
(e.g. by reproducing the content of the notices). For the avoidance of
doubt, upon Distribution of Modifications You must not use the Licensor’s
or ESA’s trademarks, names or logos in any way that states or implies,
or can be interpreted as stating or implying, that the final product is
endorsed or created by the Licensor or ESA.

3 Distribution

3.1 No Copyleft

You may Distribute the Software and/or Modifications, as Source Code or
Object Code, under any license terms, provided that

(a)  notice is given of the use of the Software and the applicability of
     this License to the Software; and

(b) You make best efforts to ensure that further Distribution of the Software
    and/or Modifications (including further Modifications) is subject to the
    obligations set forth in this Sec. 3.1 (a) and (b).

4 Notices

The following obligations apply in the event of any Distribution of the
Software and/or Modifications as Source Code and/or Object Code:

4.1 You must include a copy of this License and all of the notices set out
in this Sec. 4.

4.2 You may not remove or alter any copyright, patent, trademark and
attribution notices nor any of the notices set out in this Sec. 4,
except as necessary for your compliance with this License or otherwise
permitted by this License, except for those notices that do not pertain
to the Modifications You Distribute.

4.3 Each Contributor must cause its Modification carrying prominent notices
stating that the Software has been modified and the date of modification and
identify itself as the originator of its Modifications in a manner that
reasonably allows identification and contact with the Contributor.
The aforementioned notices must at a minimum be in a text file included
with the Distribution titled “CHANGELOG”.

4.4 The Software may include a "NOTICE" text file containing general notices.
Any Contributor can create such a NOTICE file or add notices to it, alongside
or as an addendum to the original text, provided that such notices cannot be
construed as modifying the License. 

4.5 Each Contributor must identify all of its Patent Claims by providing at
a minimum the patent number and identification and contact information in a
text file included with the Distribution titled "LEGAL".

5 Warranty and Liability 

5.1 Each Contributor warrants and represents that it has sufficient rights
to grant the rights to its Modifications conveyed by this License.

5.2 Except as expressly set forth in this License, the Software is provided
to You on an “as is” basis and without warranties of any kind, including
without limitation merchantability, fitness for a particular purpose,
absence of defects or errors, accuracy or non-infringement of intellectual
property rights. Mandatory statutory warranty claims, e.g. in the event of
wilful deception or fraudulent misrepresentation, shall remain unaffected.

5.3 Except as expressly set forth in this License, neither Licensor nor any
Contributor shall be liable, including, without limitation, for direct,
indirect, incidental, or consequential damages (including without limitation
loss of profit), however caused and on any theory of liability, arising in
any way out of the use or Distribution of the Software or the exercise of
any rights under this License, even if You have been advised of the
possibility of such damages. Mandatory statutory liability claims,
e.g. in the event of wilful misconduct, wilful deception or fraudulent
misrepresentation, shall remain unaffected.

6 Additional Agreements

While Distributing the Software or Modifications, You may choose to conclude
additional agreements, for free or for charge, regarding for example support,
warranty, indemnity, liability or liability obligations and/or rights,
provided such additional agreements are consistent with this License and
do not effectively restrict the recipient’s rights under this License.
However, in accepting such obligations, You may act only on Your own behalf
and on Your sole responsibility, not on behalf of any other Contributor or
Licensor, and only if You agree to indemnify, defend, and hold each
Contributor or Licensor harmless for any liability incurred by, or
claims asserted against, such Contributor or Licensor by reason of
your accepting any such warranty or additional liability.

7 Infringements

You acknowledge that continuing to use the Software knowing that such use
infringes third party rights (e.g. after receiving a third party
notification of infringement) would expose you to the risk of being
considered as intentionally infringing third party rights. In such event
You should acquire the respective rights or modify the Software so that
the Modification is non-infringing.

8 Termination

8.1 This License and the rights granted hereunder will terminate automatically
upon any breach by You with the terms of this License if you fail to cure such
breach within 30 days of becoming aware of the breach.

8.2 If You institute patent litigation against any entity (including a
cross-claim or counterclaim in a lawsuit) alleging that the Software
constitutes direct or contributory patent infringement, then any patent
and copyright licenses granted to You under this License for the Software
shall terminate as of the date such litigation is filed.

8.3 Any licenses validly granted by You under the License prior to
termination shall continue and survive termination.

9 Applicable Law, Arbitration and Compliance

9.1 This License is governed by the laws of the ESA Member State where the
Licensor resides or has his registered office. “Member States” are the
members of the European Space Agency pursuant to Art. 1 of the ESA
Convention[1]. This licence shall be governed by German law if a
dispute arises with the ESA as a Licensor or if the Licensor has no
residence or registered office inside a Member State.

9.2 Any dispute arising out of this License shall be finally settled in
accordance with the Rules of Arbitration of the International Chamber of
Commerce by one or more arbitrators designated in conformity with those
rules. Arbitration proceedings shall take place in Cologne, Germany.
The award shall be final and binding on the parties, no appeal shall lie
against it. The enforcement of the award shall be governed by the rules
of procedure in force in the state/country in which it is to be executed.

9.3 For the avoidance of doubt, You are solely responsible for compliance
with current applicable requirements of national laws. The Software can be
subject to export control laws. If You export the Software it is your
responsibility to comply with all export control laws. This may include
different requirements, as e.g. registering the Software with the local
authorities.

9.4 If it is impossible for You to comply with any of the terms of this
License due to statute, judicial order or regulation You must:

(a)  comply with the terms of this License to the maximum extent possible; and

(b) describe the limitations and the Object Code and/or Source Code they
affect. Such description must be included in the LEGAL notice described
in Section 4. Except to the extent prohibited by statute or regulation,
such description must be sufficiently detailed for an average recipient
to be able to understand it.

10 Miscellaneous

10.1 Only ESA has the right to modify or publish new versions of this License.
ESA may assign this right to other individuals or legal entities.
Each version will be given a distinguishing version number.

10.2 This License represents the complete agreement concerning subject
matter hereof.

10.3 If any provision of this License is held invalid or unenforceable,
the remaining provisions of this License shall not be affected.
The invalid or unenforceable provision shall be construed and/or
reformed to the extent necessary to make it enforceable and valid.

[1] As of January 2020 the Member States are Austria, Belgium, Czech Republic,
Denmark, Estonia, Finland, France, Germany, Greece, Hungary, Ireland, Italy,
Luxembourg, The Netherlands, Norway, Poland, Portugal, Romania, Spain, Sweden,
Switzerland and the United Kingdom.

---------------------------------------------------------------------------

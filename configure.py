#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 03-Nov-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

'''
A simple configure script to configure the software and the test system.
'''
#
#  #]
#  #[ imported modules
import os
import sys
import getopt

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('CLM_dist')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.helpers import check_in_sw_root
from lib.download_data_package import (
    download_data_packages, DATAPACKAGE_NAME, ACTIVATE_DATAPACKAGE)
# pylint: enable=wrong-import-position, import-error

#  #]
#  #[ settings
MAKE_INCLUDE_FILE = 'Make.config'
#  #]

def main():
    #  #[ main script
    ''' handle untarring of the data packages
    and create the Make.config include file'''
    
    # 1: ensure we are in the software root
    check_in_sw_root()

    # check if Make.config is already present
    if os.path.exists(MAKE_INCLUDE_FILE):
        print(f'file {MAKE_INCLUDE_FILE} already present.')
        print('nothing to do ....')
        return

    # check commandline switches
    usage = '''./configure.py  [options]

Explanation of the options:
    -h/--help')
           Display this help message.
    -n/--no_network:
           Do not try to download the datapacks
'''

    short_options = 'hn'
    long_options = ['help', 'no_network']
    args = sys.argv[1:]
    try:
        opts = getopt.getopt(args, short_options, long_options)[0]
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)

    no_network = False
    for opt, arg in opts:
        if opt in ['-h', '--help']:
            print(usage)
            sys.exit(0)
        elif opt in ("-n", "--no_network"):
            no_network = True
        
    # 2: download datapackages
    if not no_network:
        download_data_packages()

    # 3: inspect system
    use_activate = False
    if os.path.exists(ACTIVATE_DATAPACKAGE):
        use_activate = True

    # 4: untar the datapackages
    if os.path.exists(DATAPACKAGE_NAME):
        cmd = f'tar zxvf {DATAPACKAGE_NAME}'
        print(f'Executing: {cmd}')
        os.system(cmd)
    else:
        print(f'ERROR: cannot find required datapack: {DATAPACKAGE_NAME}')
        print('ERROR: please allow downloading (so remove -n/--no_network')
        print('ERROR: option, or provide the file manually before trying')
        print('ERROR: to run this configure script.')
        sys.exit(1)

    if use_activate:
        if os.path.exists(ACTIVATE_DATAPACKAGE):
            cmd = f'tar zxvf {ACTIVATE_DATAPACKAGE}'
            print(f'Executing: {cmd}')
            os.system(cmd)
        else:
            # this package is optional, so it may be missing.
            # of course the activate test can not be run if the
            # data package is not provided
            use_activate = False

    # 5: write make_include_file file
    with open(MAKE_INCLUDE_FILE, 'wt', encoding='ascii') as fd_config:
        if use_activate:
            fd_config.write('ACTIVATE_TEST=test_ACTIVATE\n')
        else:
            fd_config.write('ACTIVATE_TEST=\n')

    print(f'created make_include_file file:: {MAKE_INCLUDE_FILE}')
    #  #]

if __name__ == '__main__':
    main()

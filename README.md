## CARDINAL Campaign Tools

This repository holds the CARDINAL Campaign Tools (CCT) ATLID simulator.
This is a selection of tools to allow to convert ground/airplane based
measurements as well as NWP/LES model outputs
to inputs suitable for the EathCare simulator, and from there
as inputs for the different EarthCare algorithms.
At the moment only the ATLID tool is fully implemented in this
repository. The MSI instrument has its own page:
https://gitlab.com/wew_fub/msi-tool

## Description
The CARDINAL campaign tools ATLID simulator consists of several
tools/scripts that can be used in the following way:

The **lidar_module.py** tool:
* This is the main tool doing the atmospheric simulations.
  It takes user settings from a (reasonably simple) spreadsheet xlsx file
  that is provided using the -i option.
  After installation of the CCT tools and running the standard tests
  some example spreadsheet files
  can be found in the folder: CLM_dist/test/config_xlsx/
* It takes input data from one or more NetCDF or HDF5 files
  that should be present in the Data Dir which is defined in
  the above mentioned spreadsheet.
* It loads and reformats the input atmospheric properties
  and provides them to the multiscatter library using the
  python ctypes interface.
* The simulation results are written to a NetCDF output file
  specified by the -o option.

The **create_config_file.py** tool:
* This tool is provided for convenience.
  It adds the possibility to define the configuration for
  the "lidar_module.py" tool as ascii file (in TOML format) which can be
  converted to the required spreadsheet format. This allows to
  script the generation of input config files in case many
  cases need to be run. (and is used by the software itself to
  generate the sample xlsx files).

The **create_curtain_from_3d_file.py** tool:
* This tool is useful if the ATLID simulator has been applied
  to 3D model fields (see for example the provided GEM test case).
  It defines a ground trajectory through this field and collects
  vertical profiles along this path (using nearest neighbour at the moment,
  no interpolations are done).
  This collection of vertical profiles along a given trajectory is then
  exported to a new NetCDF file holding a 2D "curtain" of data (with
  dimensions time and altitude). This new file is suitable is input
  for the create_earth_care_files.py tool.

The **create_earth_care_files.py** tool:
* The results from the "lidar_module.py" tool can be reformetted
  to the EarthCare product file formats ATL_NOM_1B, AUX_JSG_1D and AUX_MET_1D
  which can be used as inputs for some of the EathCare L2 processors.
  Optionally an overpass plot can be generated as well.

The **atlid_sim_plot.py** tool:
* This is a simple plotting tool, that could serve as an exaple in case
  you plan to create plotting scripts.
  It is also used by the automatic testing system to verify that the
  generated data looks as expected.

## Dependencies

This software is developed and tested on **fedora linux** and will probably
only compile and run on a linux machine (or with some luck on MacOS or
Windows Subsystem for Linux, WSL).
Other options would be to run it in a virtual machine or a container.

The following software needs to be pre-installed before this
software can be used:
* **git**: to allow downloading this software from gitlab.
* **gcc** and **gfortran**: these are needed to compile
  the multiscatter library.
* **python3**: the main language in which this software is written.
* The commandline tool **nccmp**: needed by the test step
  to compare the generated NetCDF file with the expected one.
  (the source code is available for download at this location:
   https://gitlab.com/remikz/nccmp/-/releases).
* The commandline tool **h5diff** needed to compare the generated
  EC HDF5 products with the expected reference products.
  (this is part of the hdf5 siftware package, see:
  https://portal.hdfgroup.org/documentation/index.html)
* the commandline tool **diff**, needed to compare the generated EC HDR
  files with expected reference files.

In addition the following non-standard python3 modules need to be installed:
* **numpy**: to allow fast numerical calculations on arrays.
* **scipy**: to allow easy 1D interpolations.
* **netCDF4**: to read the input data and generate the intermediate
  simulation results.
* **h5py**: to read the input data and to
  format the EarthCare output files.
* **xarray** needed to generate the EarthCARE formatted HDF5 files
* **datatree**: to format the EarthCare output files
  sometimes also known as xarray-datatree.
* **openpyxl**: to read the input configuration specified in a spreadsheet.
* **tomllib** or in older python versions its
  predecessor **tomli** to read the ascii TOML formatted config files.
* **matplotlib**: to plot the results.
* **geopy**: needed for some of the generated plots.
* optional: **cartopy**: this module is used by the create_earth_care_files.py
  tool to generate overpass plots.
  If this overpass plotting is disabled cartopy is not needed.

Note that the python modules can be installed using the linux package manager
of your linux distribution or by using the pip or conda tool.

## Installation

Follow these steps to download and install this software:
* download the software using the git clone command:
```
git clone https://gitlab.com/KNMI-OSS/satellite-data-research-tools/cardinal-campaign-tools.git
```
* [optional] download the most recent version of the data package
  CARDINAL_Campaign_Tools_Data.tar.gz
  and place it in the software root, next to the CLM_dist folder.
  Note that the make script will do this automatically if your
  computer is connected to the internet and can access gitlab.
  * you can download it manually from this location:
    https://gitlab.com/KNMI-OSS/satellite-data-research-tools/cardinal-campaign-tools/-/packages
  * manual unzipping is not needed, this is done automatically when running
    the ./configure.py command.
* run the configure script:
```
./configure.py
```
* compile the multiscatter library using:
```
make
```
* run the available tests using:
```
cd CLM_dist
make unittest
make test
```

## Using a container

If needed the software can also be installed inside a container.
See the [container README page](CLM_dist/container/README.md) for more details.

## Input data preparation

Here are some explanations on the inputs to be used for the simulator:

* From experience by our first users we know that the multiscatter module
  that is the core of this software will not produce sensible results in case
  the input data is very noisy and/or contains negative values for
  the profiles provided to it. It may even provide a run time error.
  This especially holds for the optical properties profiles like
  extinction, lidar ratio and linear depolarisation.
  - Using too noisy data as input will always mean a noisy simulation !
    At the moment we have not implemented denoising or smoothing in this
    simulator tool, In general, we feel that de-noising is best left up
    to the users who know their own data best.
    So please first use your own judgment to de-noise/smooth things,
    before feeding it to the simulator.
    We do not plan to implement a general solution for this
    (if that even would be possible).
  - What we have implemented is a simple clipping quality control,
    which can be used to force the data to be in a predefined range of
    values. This is mainly useful for first functional tests of the simulator
    on your data. It is controlled by QC parameters block in the configuration
    files (the parameters ending with min/max allowed value).
    These QC parameters use the same units as the respective variables that
    they control, and the clipping is only applied after application
    of the scale_factor and shift that may have been applied to the variable.
  - Once you have the software working, please return to the start and
    as mentioned above check if your data would need denoising or smoothing
    so that this clipping is no longer needed.
    Clipping can be completely switched off by setting the max allowed value
    less than the min allowed value.
* There is probably no need for you to modify The "Lidar parameters"
  table in the configuration file.
  - The fov_telescope, div_laser, and laser_wavelength refer to the
    properties of the ATLID lidar on the EarthCARE satellite. 
  - The z1, z2, dz1, z3, and dz2 settings define the vertical grid
    on which the simulation will be performed.
    The default values result in the range -500 up to 40.000 m
    in steps of 100 m.
* We do not plan to support backscatter as input as alternative to extinction.
  - You can calculate extinction yourself by assuming an
    extinction-to-backscatter ratio value or profile.
    Of course this depends on the type of aerosols in the scene that you
    are studying, and you will have a much better idea on this than we.
  - We cannot implement this in a general way in the simulator without
    compromising the quality of the results.

## Usage
Usage examples for the different tools.

To convert a TOML configuration file to a spreedsheet
launch a command like this:
```
cd CLM_dist
python3 scripts/create_config_file.py input_settings.toml input_settings.xlsx
```

To convert the data launch a command like this:
```
cd CLM_dist
python3 lidar_module.py -i input_settings.xlsx -o result_file.nc
```

(only for 3D model cases) To convert a 3D simulated case
to a 2D curtain launch a command like this:
```
cd CLM_dist/test
python3 ../scripts/create_curtain_from_3d_file.py \
        -i output/Test_GEM_data_RT_3d.nc \
	-o output/Test_GEM_data_RT_2d.nc \
	--traj_start 0,10 --traj_stop 99,25 \
	--traj_start_dt 20210918T120000
```
The explanation of the different options can be obtained by running:
```
cd CLM_dist/test
python3 ../scripts/create_curtain_from_3d_file.py -h
```

To generate EarthCare formatted files launch a command like this:
```
cd CLM_dist
python3 scripts/create_earth_care_files.py \
	--lidar_count=1 \
        --index=23 \
	--overpass_time='14:00:00' \
	--templates_folder='..' \
	--instrument_module random \
	--ifile result_file.nc
```
The optional switch --plot_overpass can be added to generate a
plot_overpass figure, but note that in this case cartopy must be
installed as well.

The explanation of the different options can be obtained by running:
```
cd CLM_dist
python3 scripts/create_earth_care_files.py -h
```

To generate some example plots launch a command like this:
```
cd CLM_dist
python3 atlid_sim_plot.py \
        --traj_start=0 \
	--traj_finish=1199 \
	--zlimits=0.0,20.0 \
        --inputfile result_file.nc \
	--outputfile_prefix plot_prefix
```
The explanation of the different options can be obtained by running
```
cd CLM_dist
python3 atlid_sim_plot.py -h
```

Finally, the software provides 3 main test cases that will be
executed when running ```make test```.
These are the GEM, CLOUDNET and EVE test cases.

It is possible to display the specific settings that are used
for the above described tools for a specific test case.
To display them execute the following commands (in which "EVE" can be
replaced by "GEM" or "CLOUDNET" if needed):
```
cd CLM_dist/test
./run_tst.py -t EVE -p
```

## Changes
This list of changes starts with commit eb0785ad dated 22-Sep-2024
which was tagged version v0001 for user convenience.

Changes in v0002:
* The example config files have been modified for these 2 variables:
  - fov_telescope = 0.075 # [mrads] (used to be 0.01)
  - div_laser = 0.05 # [mrads] (used to be 0.005)
 If you already have your own custom configuration files
 you should also apply these changes.
* In file "lidar_module.py":
  - In call to profile settings, Reff needs to be in meters !
    Comments in MS c codes asserting [um] are missleading !
  - msc.setup_instrument_settings needs to be called each loop.
    (it is not clear why).
  - part_reff is initialised to 0.01 in stead of 1
  - Initialise ssa_ray (single scatter albedo) to 0.9999 in stead of 1.
    This prevents absorption from being exactly zero
    which sometimes causes issues in the multiscatter c-code.
  - solve an issue with initialising fov_t
  - Use some explicit copy commands when storing
    atb_mie, atb_ray and atb_ray_ss
* In lib/multiscatter_interface.py
  - Fixed errors in setup_instrument_settings. Passing of rho_receiver
    was not working properly (and the units were wrong) !
  - lidar_range was being passed to MS. It needed to be altitude !
  - Changed default beam type to GAUSSIAN (used to be TOP_HAT)
* In Scripts/create_earth_care_files.py
  - Output was upside down !
  - Introduce a new advect_velocity setting with as default 7 km/s
    which can be set as commandline option if needed.
  - This advect_velocity replaces the hardcoded u_wind and v_wind
    values that were used before to simulate the satellite track.

## Support
Please contact dave.donovan@knmi.nl or jos.de.kloe@knmi.nl
if you have questions.

## Roadmap
We plan to add the following in future releases:
* dimension re-ordering
* defining the instrument noise module
  (for now we use a pre-calculated look up table for this)
* MSI tool interfacing using json output files

## Contributing
Suggestions for code improvement and bug reports are very welcome.
Please use the issue tracker or propose a merge request on gitlab
to contact us.

## Authors and acknowledgment

The Lidar Module was written by Dave Donovan.
It was converted to this gitlab project and rewritten to be more modular
by Jos de Kloe.
It is based on the multiscatter module written by Robin J. Hogan.

## License
This project is licensed according to the Apache License, Version 2.0

## Project status

This project is actively being developed at the moment.

#!/usr/bin/env python3
#
#  #[ documentation
#
#   Created on 23-Mar-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
'''
This module implements testing of the calipso colormap definition
'''
#  #]
#  #[ imported modules
import os
import sys
import unittest
from unittest.mock import patch
import numpy

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.custom_exceptions import ColorMapException
# an explicit import of this next one seems not needed
# for using it in the patch() function.
#import lib.calipso_cm
from lib.calipso_cm import (
    CM_NAME,
    define_and_register_calipso_cm,
    unregister_calipso_cm,
    get_calipso_cm)
# rename this one otherwise pytest thinks it is a test function
# but actually this is part of the calipso_cm module interface.
from lib.calipso_cm import (
    test_calipso_cm_is_registred as tst_calipso_cm_is_registred)
# pylint: enable=wrong-import-position, import-error
#  #]

# pylint: disable=protected-access
class ImportColormap(unittest.TestCase):
    ''' some tests for the CALIPSO colormap handling '''
    def test_calipso_cm(self):
        #  #[ check some values
        ''' a test for the actual calipso colormap content '''
        ccm = get_calipso_cm()

        np_eq = numpy.testing.assert_almost_equal

        self.assertFalse(ccm.monochrome, False)
        self.assertEqual(len(ccm.colors), 257)

        expected_values = numpy.array([0., 0.49803922, 1.,  1.])
        np_eq(ccm.colors[51], expected_values, decimal=6)

        expected_values = numpy.array([1., 0.831373, 0., 1.])
        np_eq(ccm.colors[112], expected_values, decimal=6)

        expected_values = numpy.array([0.352941, 0.352941, 0.352941, 1.])
        np_eq(ccm.colors[163], expected_values, decimal=6)

        expected_values = numpy.array([0.843137, 0.843137, 0.843137, 1.])
        np_eq(ccm.colors[207], expected_values, decimal=6)

        unregister_calipso_cm()
        #  #]
    def test_undefined_1(self):
        #  #[ test tst_calipso_cm_is_registred 1
        ''' a test for the test_calipso_cm_is_registred function '''
        check = tst_calipso_cm_is_registred()
        self.assertEqual(check, False)
        #  #]
    def test_undefined_2(self):
        #  #[ undefined test while patching mpl.colormaps
        ''' a test for the test_calipso_cm_is_registred function
        while disabling mpl.cm.get_cmap
        and providing a dummy for mpl.colormaps
        '''
        with patch('lib.calipso_cm.mpl') as mock_mpl:
            del mock_mpl.cm.get_cmap
            del mock_mpl.cm
            mock_mpl.colormaps = {} # {CM_NAME:'dummy',}
            check = tst_calipso_cm_is_registred()
            self.assertEqual(check, False)
        #  #]
    def test_undefined_3(self):
        #  #[ undefined test while patching mpl.cm
        ''' a test for the test_calipso_cm_is_registred function
        while disabling mpl.colormaps
        and providing a dummy for mpl.cm.get_cmap
        '''
        with patch('lib.calipso_cm.mpl') as mock_mpl:
            def test_get_cmap(cn_name):
                raise ValueError
            del mock_mpl.colormaps
            mock_mpl.cm.get_cmap = test_get_cmap
            check = tst_calipso_cm_is_registred()
            self.assertEqual(check, False)
        #  #]
    def test_defined_1(self):
        #  #[ test tst_calipso_cm_is_registred 1
        ''' a test for the define_and_register_calipso_cm function '''
        define_and_register_calipso_cm()
        check = tst_calipso_cm_is_registred()
        self.assertEqual(check, True)
        unregister_calipso_cm()
        #  #]
    def test_defined_2(self):
        #  #[ test tst_calipso_cm_is_registred_2
        ''' a test for the test_calipso_cm_is_registred function
        while disabling mpl.cm.get_cmap
        and providing a dummy for mpl.colormaps
        '''
        with patch('lib.calipso_cm.mpl') as mock_mpl:
            del mock_mpl.cm.get_cmap
            del mock_mpl.cm
            mock_mpl.colormaps = {CM_NAME:'dummy',}
            check = tst_calipso_cm_is_registred()
            self.assertEqual(check, True)
        #  #]
    def test_defined_3(self):
        #  #[ test tst_calipso_cm_is_registred_3
        ''' a test for the test_calipso_cm_is_registred function
        while disabling mpl.colormaps
        and providing a dummy for mpl.cm.get_cmap
        '''
        with patch('lib.calipso_cm.mpl') as mock_mpl:
            def test_get_cmap(cm_name):
                return 'dummy: '+cm_name
            del mock_mpl.colormaps
            mock_mpl.cm.get_cmap = test_get_cmap
            check = tst_calipso_cm_is_registred()
            self.assertEqual(check, True)
        #  #]
    def test_is_reg_unknown_interface(self):
        #  #[ test tst_calipso_cm_is_registred_3
        ''' a test for the test_calipso_cm_is_registred function
        while disabling mpl.colormaps
        and also disabling mpl.cm.get_cmap
        '''
        with patch('lib.calipso_cm.mpl') as mock_mpl:
            del mock_mpl.colormaps
            del mock_mpl.cm.get_cmap
            del mock_mpl.cm
            self.assertRaises(ColorMapException, tst_calipso_cm_is_registred)
        #  #]
    def test_define_and_register_calipso_cm_1(self):
        #  #[ test use of mpl.colormaps.register
        ''' test registering the colormap (1) '''
        with patch('lib.calipso_cm.mpl') as mock_mpl:
            mock_mpl.colors.ListedColormap = \
              lambda colors_in, cm_name_in: 'dummy_cm1'
            mock_mpl.colormaps.register = lambda cmap_in: 'dummy_reg'
            result = define_and_register_calipso_cm()
            self.assertEqual(result, 'dummy_cm1')
        #  #]
    def test_define_and_register_calipso_cm_2(self):
        #  #[ test use of plt.register_cmap
        ''' test registering the colormap (2) '''
        with patch('lib.calipso_cm.mpl') as mock_mpl:
            with patch('lib.calipso_cm.plt') as mock_plt:
                mock_mpl.colors.ListedColormap = \
                  lambda colors_in, cm_name_in: 'dummy_cm2'
                del mock_mpl.colormaps
                mock_plt.register_cmap = lambda cmap: 'dummy_reg'
                result = define_and_register_calipso_cm()
                self.assertEqual(result, 'dummy_cm2')
        #  #]
    def test_define_and_register_calipso_cm_3(self):
        #  #[ test catching unknown interface
        ''' test registering the colormap (3) '''
        with patch('lib.calipso_cm.mpl') as mock_mpl:
            with patch('lib.calipso_cm.plt') as mock_plt:
                mock_mpl.colors.ListedColormap = \
                  lambda colors_in, cm_name_in: 'dummy_cm'

                del mock_mpl.colormaps
                del mock_plt.register_cmap
                self.assertRaises(ColorMapException,
                                  define_and_register_calipso_cm)
        #  #]
    def test_unregister_calipso_cm_1(self):
        #  #[ test use of mpl.colormaps.unregister
        ''' test unregistering the colormap (1)
        while providing a dummy for mpl.colormaps.unregister
        '''
        with patch('lib.calipso_cm.mpl') as mock_mpl:
            mock_mpl.colormaps.unregister = lambda cm_name: 'dummy'
            result = unregister_calipso_cm()
            self.assertEqual(result, None)
        #  #]
    def test_unregister_calipso_cm_2(self):
        #  #[ test use of mpl.cm.unregister_cmap
        ''' test unregistering the colormap (2)
        while disabling mock_mpl.colormaps and mock_mpl.cm._cmap_registry
        and providing a dummy for mpl.cm.unregister_cmap
        '''
        with patch('lib.calipso_cm.mpl') as mock_mpl:
            del mock_mpl.colormaps
            del mock_mpl.cm._cmap_registry
            mock_mpl.cm.unregister_cmap = lambda cm_name: 'dummy'
            result = unregister_calipso_cm()
            self.assertEqual(result, None)
        #  #]
    def test_unregister_calipso_cm_3(self):
        #  #[ test use of mpl.cm._cmap_registry.pop
        ''' test unregistering the colormap (3)
        while disabling mpl.colormaps and mpl.cm.unregister_cmap
        and providing a dummy for mpl.cm._cmap_registry
        '''
        with patch('lib.calipso_cm.mpl') as mock_mpl:
            del mock_mpl.colormaps
            del mock_mpl.cm.unregister_cmap
            mock_mpl.cm._cmap_registry = {CM_NAME:'dummy',}
            result = unregister_calipso_cm()
            self.assertEqual(result, None)
        #  #]
    def test_unregister_calipso_cm_4(self):
        #  #[ test use of mpl.cm._cmap_registry.pop
        ''' unknown interface test for unregistering the colormap (4)
        while disabling mpl.colormaps and mpl.cm.unregister_cmap
        and mpl.cm._cmap_registry
        '''
        with patch('lib.calipso_cm.mpl') as mock_mpl:
            del mock_mpl.colormaps
            del mock_mpl.cm.unregister_cmap
            del mock_mpl.cm._cmap_registry
            del mock_mpl.cm
            self.assertRaises(ColorMapException, unregister_calipso_cm)
        #  #]
# pylint: enable=protected-access

if __name__ == '__main__':
    unittest.main()

#!/usr/bin/env python3

#  #[ documentation
#   Created on 3-Mar-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module implements testing several non standard python modules
and additional commandline tools needed by this software.
'''
#  #]

import os
import unittest
import warnings

NOT_INSTALLED_MSG = ('ERROR: {0} seems not installed. '+
        'The {0} tool is a requirement to run this software. '+
        'Please install it and try again.')

NOT_INSTALLED_PY_MOD_MSG = ('ERROR: {0} seems not installed. '+
        'The python {0} module is a requirement to run this software. '+
        'Please install it and try again.')

# pylint: disable=unused-import, import-outside-toplevel
class ImportTests(unittest.TestCase):
    ''' tests to see if required tools and modules are available '''
    def test_gcc(self):
        #  #[ check gcc
        ''' ensure gcc is installed '''
        cmd = 'gcc --version'
        result = os.system(cmd)
        if result != 0:
            self.fail(NOT_INSTALLED_MSG.format('gcc'))
        #  #]
    def test_gfortran(self):
        #  #[ check gfortran
        ''' ensure gfortran is installed '''
        cmd = 'gfortran --version'
        result = os.system(cmd)
        if result != 0:
            self.fail(NOT_INSTALLED_MSG.format('gfortran'))
        #  #]
    def test_nccmp(self):
        #  #[ check nccmp
        ''' ensure nccmp is installed '''
        cmd = 'nccmp -V'
        result = os.system(cmd)
        if result != 0:
            self.fail(NOT_INSTALLED_MSG.format('nccmp'))
        #  #]
    def test_numpy(self):
        #  #[ numpy check
        ''' ensure numpy is installed '''
        try:
            import numpy as np
        except ModuleNotFoundError:
            self.fail(NOT_INSTALLED_PY_MOD_MSG.format('numpy'))
        #  #]
    def test_scipy(self):
        #  #[ scipy check
        ''' ensure scipy is installed '''
        try:
            import scipy
        except ModuleNotFoundError:
            self.fail(NOT_INSTALLED_PY_MOD_MSG.format('scipy'))
        #  #]
    def test_netcdf4(self):
        #  #[ netCDF4 check
        ''' ensure netCDF4 is installed '''
        # for now ignore the numpy.ndarray size changed warning while
        # importing NetCDF4.
        # the exact warning is:
        # ========== warnings summary ==========
        # test/test_imports.py::ImportTests::test_netcdf4
        # <frozen importlib._bootstrap>:241: RuntimeWarning:
        # numpy.ndarray size changed, may indicate binary incompatibility.
        # Expected 16 from C header, got 96 from PyObject
        # ======================================
        # It does not occur if this test module is executed manually,
        # it only occurs if it is called by pytest
        # I have no idea what is wrong here and have for now no time
        # to dig deeper in to this.

#        this seems no longer needed in f39
#        warnings.simplefilter('ignore', category=RuntimeWarning)

        try:
            from netCDF4 import Dataset
        except ModuleNotFoundError:
            self.fail(NOT_INSTALLED_PY_MOD_MSG.format('netCDF4'))
        #  #]
    def test_h5py(self):
        #  #[ check h5py
        ''' ensure h5py is installed '''
        try:
            import h5py
        except ModuleNotFoundError:
            self.fail(NOT_INSTALLED_PY_MOD_MSG.format('h5py'))
        #  #]
    def test_openpyxl(self):
        #  #[ openpyxl check
        ''' ensure openpyxl is installed '''
        try:
            import openpyxl as pd
        except ModuleNotFoundError:
            self.fail(NOT_INSTALLED_PY_MOD_MSG.format('openpyxl'))
        #  #]
    def test_matplotlib(self):
        #  #[ matplotlib check
        ''' ensure matplotlib is installed '''
        try:
            import matplotlib
        except ModuleNotFoundError:
            self.fail(NOT_INSTALLED_PY_MOD_MSG.format('matplotlib'))
        #  #]
    def test_tomllib(self):
        #  #[ tomlib check
        ''' ensure tomlib is available '''
        available = False
        try:
            import tomllib
            available = True
        except ImportError:
            pass

        if not available:
            try:
                import tomli as tomllib
                available = True
            except ImportError:
                pass

        if not available:
            self.fail(NOT_INSTALLED_PY_MOD_MSG.format('tomllib or its alternative tomli'))
        #  #]
    def test_datatree(self):
        #  #[ xarray-datatree check
        ''' ensure xarray-datatree is installed '''
        try:
            import datatree
        except ModuleNotFoundError:
            self.fail(NOT_INSTALLED_PY_MOD_MSG.format('xarray-datatree'))
        #  #]
    def test_geopy(self):
        #  #[ geopy check
        try:
            import geopy
        except ModuleNotFoundError:
            self.fail(NOT_INSTALLED_PY_MOD_MSG.format('geopy'))
        #  #]

if __name__ == '__main__':
    unittest.main()

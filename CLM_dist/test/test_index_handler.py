#!/usr/bin/env python3
#
#  #[ documentation
#
#   Created on 14-Apr-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module implements testing of the index_handler module.
'''
#  #]
#  #[ imported modules
import os
import sys
import unittest
import io
from contextlib import redirect_stdout
import numpy

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.index_handler import IndexIterClass, IndexClass, IterationError
# pylint: enable=wrong-import-position, import-error

#  #]

# pylint: disable=invalid-name
class TestIndexClass(unittest.TestCase):
    ''' some tests for the IndexClass '''
    def test_initialize(self):
        #  #[ test init
        ''' test the initialization '''
        idx = IndexClass(123, None)
        self.assertEqual(idx.num_x, 123)
        self.assertEqual(idx.num_y, None)
        self.assertEqual(idx.total_cases, 123)

        idx = IndexClass(45, 67)
        self.assertEqual(idx.num_x, 45)
        self.assertEqual(idx.num_y, 67)
        self.assertEqual(idx.total_cases, 45*67)
        #  #]
    def test_1D_stepping(self):
        #  #[ 1D step test
        ''' test the step_to_next_index_pair method (1D case) '''
        idx = IndexClass(2)

        idx.step_to_next_index_pair()
        self.assertEqual(idx.index_x, 0)
        self.assertEqual(idx.index_y, None)
        idx.step_to_next_index_pair()
        self.assertEqual(idx.index_x, 1)
        self.assertEqual(idx.index_y, None)

        self.assertRaises(StopIteration,
                          idx.step_to_next_index_pair)
        #  #]
    def test_2D_stepping(self):
        #  #[ 2D step test
        ''' test the step_to_next_index_pair method (2D case) '''
        idx = IndexClass(2, 3)

        idx.step_to_next_index_pair()
        self.assertEqual(idx.index_x, 0)
        self.assertEqual(idx.index_y, 0)
        idx.step_to_next_index_pair()
        self.assertEqual(idx.index_x, 0)
        self.assertEqual(idx.index_y, 1)
        idx.step_to_next_index_pair()
        self.assertEqual(idx.index_x, 0)
        self.assertEqual(idx.index_y, 2)

        idx.step_to_next_index_pair()
        self.assertEqual(idx.index_x, 1)
        self.assertEqual(idx.index_y, 0)
        idx.step_to_next_index_pair()
        self.assertEqual(idx.index_x, 1)
        self.assertEqual(idx.index_y, 1)
        idx.step_to_next_index_pair()
        self.assertEqual(idx.index_x, 1)
        self.assertEqual(idx.index_y, 2)

        self.assertRaises(StopIteration,
                          idx.step_to_next_index_pair)
        #  #]
    def test_get_progress_string(self):
        #  #[ test get progress str
        ''' test for the get_progress_string method '''
        idx = IndexClass(2, 3)
        idx.step_to_next_index_pair()
        idx.step_to_next_index_pair()
        idx.step_to_next_index_pair()
        txt = idx.get_progress_string()
        expected_txt = '3 out of 6'
        self.assertEqual(txt, expected_txt)
        #  #]
    def test_get_progress_percentage(self):
        #  #[ test get progress perc.
        ''' test for the get_progress_percentage method '''
        idx = IndexClass(7)
        idx.step_to_next_index_pair()
        idx.step_to_next_index_pair()

        perc = idx.get_progress_percentage()
        expected_perc = 28
        self.assertEqual(perc, expected_perc)
        #  #]
    def test_combined_index(self):
        #  #[ test using combined index
        ''' test for using idx.ci '''
        n_x=5
        n_y=7
        n_z=1
        arr_3D = numpy.arange(n_z*n_x*n_y).reshape(n_z, n_y, n_x)
        idx = IndexClass(n_x, n_y)
        expected_result = [0,5,10,15,20,25,30,1]
        for i in range(8):
            idx.step_to_next_index_pair()
            result = arr_3D[idx.ci]
            self.assertEqual(result, expected_result[i])
        #  #]
    def test_num_x_none(self):
        #  #[ test wrong input for num_x
        ''' test the IndexClass for wrong inputs (2D case) x=None'''
        self.assertRaises(IterationError,
                          IndexClass, None, 3)
        #  #]
    def test_num_y_zero(self):
        #  #[ test wrong input for num_y
        ''' test the IndexClass for wrong inputs (2D case) y=0 '''
        self.assertRaises(IterationError,
                          IndexClass, 3, 0)
        #  #]
    def test_1D_reset_stepping(self):
        #  #[ 1D reset test
        ''' test the reset method (1D case) '''
        idx = IndexClass(2)

        idx.step_to_next_index_pair()
        idx.step_to_next_index_pair()

        idx.reset()
        idx.step_to_next_index_pair()
        self.assertEqual(idx.index_x, 0)
        #  #]

# pylint: enable=invalid-name

class TestIndexIterClass(unittest.TestCase):
    ''' some tests for the IndexIterClass '''
    def test_init(self):
        #  #[ test init and stepping
        ''' test for initialization and stepping '''
        index_iterator = IndexIterClass(3)
        idx = next(index_iterator)
        idx = next(index_iterator)
        self.assertEqual(idx.index_x, 1)
        self.assertEqual(idx.index_y, None)

        idx = next(index_iterator)
        self.assertRaises(StopIteration,
                          next, index_iterator)

        index_iterator = IndexIterClass(2,4)
        idx = next(index_iterator)
        idx = next(index_iterator)
        idx = next(index_iterator)
        idx = next(index_iterator)
        idx = next(index_iterator)
        self.assertEqual(idx.index_x, 1)
        self.assertEqual(idx.index_y, 0)

        idx = next(index_iterator)
        idx = next(index_iterator)
        idx = next(index_iterator)
        self.assertRaises(StopIteration,
                          next, index_iterator)
        #  #]
    def test_display_progress(self):
        #  #[ test display text progress indicator
        ''' test for the display_progress method '''
        index_iterator = IndexIterClass(9)
        next(index_iterator)
        next(index_iterator)
        next(index_iterator)

        with io.StringIO() as buf, redirect_stdout(buf):
            index_iterator.display_progress()
            output = buf.getvalue()

        expected_output = 'progress:3 out of 9 (33%)\n'
        self.assertEqual(output, expected_output)
        #  #]
    def test_display_progress_hashes(self):
        #  #[ test display hashes progress indicator
        ''' test for the display_progress_hashes method '''
        index_iterator = IndexIterClass(9)
        next(index_iterator)
        next(index_iterator)
        next(index_iterator)

        with io.StringIO() as buf, redirect_stdout(buf):
            index_iterator.display_progress_hashes()
            output = buf.getvalue()

        expected_output = '\r[#############                           ]33%'
        self.assertEqual(output, expected_output)
        #  #]
    def test_get_size_def(self):
        #  #[ test the get_size_def method
        ''' test 1d and 2d calls for get_size_def '''
        num_t = 12
        index_iterator = IndexIterClass(num_t)
        num_z = 123
        sizedef = index_iterator.get_size_def(num_z)
        expected_sizedef = [num_z, num_t]
        self.assertEqual(sizedef, expected_sizedef)

        num_x = 7
        num_y = 9
        num_z = 129
        index_iterator = IndexIterClass(num_x, num_y)
        sizedef = index_iterator.get_size_def(num_z)
        expected_sizedef = [num_z, num_y, num_x]
        self.assertEqual(sizedef, expected_sizedef)
        #  #]
    def test_iteration(self):
        #  #[ test index iteration (1d case)
        ''' test index iteration for the 1D case'''
        num_x = 5
        index_iterator = IndexIterClass(num_x)
        expected_idx_ci = [
            (Ellipsis, 0),
            (Ellipsis, 1),
            (Ellipsis, 2),
            (Ellipsis, 3),
            (Ellipsis, 4),
            ]
        for i, idx in enumerate(index_iterator):
            self.assertEqual(idx.ci, expected_idx_ci[i])

        index_iterator.reset()
        next(index_iterator)
        expected_idx_ci = (Ellipsis, 0)
        self.assertEqual(index_iterator.idx.ci, expected_idx_ci)
        #  #]

if __name__ == '__main__':
    unittest.main()

#!/usr/bin/env python3
#
#  #[ documentation
#
#   Created on 23-Mar-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module implements testing of the load functionality
for xlsx files that are used to define the user inputs
for this software.
'''
#  #]

import os
import sys
import unittest

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.rayleigh_scat_util import get_ray_sigma, get_ray_sigma_beta
# pylint: enable=wrong-import-position, import-error

class TestRayleighScatUtil(unittest.TestCase):
    ''' some tests for the rayleigh_scat_util module'''
    def test_get_ray_sigma(self):
        #  #[ tests get_ray_sigma
        ''' test the get_ray_sigma function'''
        wavelenghts = [354.8, 550.1, 872.2] # [nm]
        expected_sigmas = [
            2.758624152848108e-26,
            4.496160516415561e-27,
            6.984528285063183e-28]

        for wavelen, expected_sigma in zip(wavelenghts, expected_sigmas):
            sigma = get_ray_sigma(wavelen)
            self.assertAlmostEqual(sigma, expected_sigma)
        #  #]
    def test_get_ray_sigma_beta(self):
        #  #[ tests get_ray_sigma_beta
        ''' test the get_ray_sigma_beta function'''
        wavelenghts = [354.8, 543.2, 876.5] # [nm]
        expected_sigmas = [
            2.758624152848108e-26,
            4.496160516415561e-27,
            6.984528285063183e-28]
        expected_betas = [
            3.2400329145036146e-22,
            5.638860520439703e-23,
            8.054355535096885e-24]
        for (wavelen, expected_sigma,
            expected_beta) in zip(wavelenghts,
                                   expected_sigmas,
                                   expected_betas):
            result = get_ray_sigma_beta(wavelen)

            self.assertAlmostEqual(result['sigma'], expected_sigma)
            self.assertAlmostEqual(result['beta'], expected_beta)
        #  #]

if __name__ == '__main__':
    unittest.main()

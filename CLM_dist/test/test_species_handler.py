#!/usr/bin/env python3
#
#  #[ documentation
#
#   Created on 2-Feb-2024
#
#   Copyright (C) 2024  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module implements testing of the species_handler module
'''
#  #]
#  #[ imported modules
import os
import sys
import unittest
# import io
# from contextlib import redirect_stdout
import numpy

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.vertical_axis import VerticalAxis
from lib.handle_xlsx import AllInputs
from lib.index_handler import IndexIterClass
from lib.species_handler import SpeciesHandler
# pylint: enable=wrong-import-position, import-error

#  #]class TestIndexClass(unittest.TestCase):

class TestSpeciesHandler(unittest.TestCase):
    ''' some tests for the SpeciesHandler class '''
    def setUp(self):
        #  #[ setup
        ''' generic setup for most tests '''

        # needed inputs:
        self.input_data = {
            'species_1_height':numpy.array([0.,1.,2.,3.,4.,5.]), # km
            #'species_1_latitude':
            #'species_1_longitude':
            #'species_1_time':
            #'species_1_id':
            #'species_1_ids_to_include':
            'species_1_wc':None, # not needed if ext is given
            'species_1_reff':0.5, # not needed if ext is given
            'species_1_extinction':
            numpy.array([0., 4.e-5, 5.e-4, 3.e-3, 2.e-4, 0.]),
            'species_1_s': numpy.array([1., 10., 30., 150., 250., 47.]),
            'species_1_lin_depol':
            numpy.array([0.0, 0.012, 0.2, 0.3, 0.07, -0.05]),
            'species_1_g': 0.98765,
            }

        self.all_inp = AllInputs()
        self.all_inp.dict_of_sp_params = {
            # type must be one of "generic", "water", "ice" or "aerosol"
            'species_1_type':'water', 
            'species_1_name':'dummy',
            }

        self.all_inp.lidar_params.sat_alt = 400 # [km]
        self.all_inp.lidar_params.z1 = 40000 # [m]
        self.all_inp.lidar_params.z2 = 123456 # [m] # this one seems never used
        self.all_inp.lidar_params.dz1 = 500 # [m]
        self.all_inp.lidar_params.z3 = 500 # [m]
        self.all_inp.lidar_params.dz2 = 100 # [m]

        nz_orig = 6
        self.v_ax = VerticalAxis(self.all_inp, nz_orig)

        num_x = 1
        num_z_interp = 23
        index_iterator = IndexIterClass(num_x)
        self.size_def = index_iterator.get_size_def(num_z_interp)
        self.species_index = 1
        #  #]
    def test_initialize(self):
        #  #[ test init
        ''' test the initialization '''
        test_sp = SpeciesHandler(
            self.input_data, self.all_inp,
            self.v_ax, self.size_def, self.species_index)

        #print('test_sp.extinction = ', test_sp.extinction)
        #print('len(test_sp.extinction) = ', len(test_sp.extinction))
        #print('test_sp.key_ext = ', test_sp.key_ext )
        #print('test_sp.species_used = ', test_sp.species_used)

        self.assertEqual(len(test_sp.extinction) , 23)
        self.assertEqual(test_sp.key_ext, 'species_1_extinction')
        self.assertEqual(test_sp.species_used, True)
        #  #]

if __name__ == '__main__':
    unittest.main()

#!/usr/bin/env python3

#  #[ documentation
#
#   Copyright (C) 2022-2023  KNMI
#
#   Author: Dave Donovan <dave.donovan@knmi.nl>
#   Converted from bash to Python by: Jos de Kloe, 26-May-2023
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
A small test script that:
1. Runs lidar_module.py
3. Compares output with a reference output file using nccmp
4. Produces some plots that can be visually compared with
   the plots in test/ref/
5. Compares these plots with reference plots provided in the
   data package, and ensured they are as expected.
   (note that this test uses specially adapted plots without labels
   or text, since different fonts on different machines make it very
   hard to do the comparison if text is present).
'''
#  #]
#  #[ imported modules
import os
import sys
import getopt
import glob

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.helpers import run_shell_command, textcolor
from lib.custom_exceptions import CommandLineInputError, CctException
import lidar_module
from report_test_results import TestResult, define_testresult_filename
import compare_plots
# pylint: enable=wrong-import-position, import-error

#  #]

def run_lidar_module(test_settings, options):
    #  #[ run the simulation
    ''' runs the lidar_module code '''

    config_file = test_settings['config_file']
    out_file_2d = test_settings['out_file_2d']
    out_file_3d = test_settings['out_file_3d']
    testcase    = test_settings['testcase']

    run_simulation      = options['run_simulation']
    only_print_commands = options['only_print_commands']

    test_result = TestResult('bool', 'lidar simulation test run')

    if not run_simulation:
        test_result.set_to_skipped()
        return test_result

    if testcase == 'GEM':
        out_file = out_file_3d
    else:
        out_file = out_file_2d

    if only_print_commands:
        print('='*50)
        print('The command to run the lidar module is:')
        print('='*50)
        print('../lidar_module.py \\')
        print(f'    -i {config_file} \\')
        print(f'    -o {out_file}')
        print('='*50)
        test_result.set_to_skipped()
        return test_result

    test_result.set_to_passed()
    try:
        lidar_module.lidar_module(config_file, out_file)
    except CctException:
        test_result.set_to_failed()

    print('='*50)
    print('Simulation run finished ')
    print('='*50)

    return test_result
    #  #]

def convert_3d_to_2d(test_settings, options):
    #  #[ run the conversion
    '''
    Use a trajectory through the 3D field to construct a 2D
    curtain of data, to be exported to EC formatted files
    and to be used for plotting.
    '''
    out_file_2d = test_settings['out_file_2d']
    out_file_3d = test_settings['out_file_3d']

    traj_start = test_settings['traj_start']
    traj_stop = test_settings['traj_stop']
    traj_start_dt = test_settings['traj_start_dt']

    only_print_commands = options['only_print_commands']
    run_convert_3d_to_2d = options['run_convert_3d_to_2d']

    test_result = TestResult('bool', 'convert 2D to 3D test run')

    if not run_convert_3d_to_2d:
        test_result.set_to_skipped()
        return test_result

    cmd = (
        '../scripts/create_curtain_from_3d_file.py '+
        f' -i {out_file_3d} -o {out_file_2d}'+
        f' --traj_start {traj_start} --traj_stop {traj_stop}'+
        f' --traj_start_dt {traj_start_dt}'
        )

    if only_print_commands:
        print('='*50)
        print('The command to run the 2D to 3D conversion is:')
        print('='*50)
        print('cmd = ', cmd)
        print('='*50)
        test_result.set_to_skipped()
        return test_result

    # execute the conversion
    test_result.set_to_passed()
    print('Convert 3D to 2D file:')
    exit_status = os.system(cmd)
    if exit_status != 0:
        print('ERROR in create_curtain_from_3d_file: '+
              'could not generate 2D NetCDF file.')
        test_result.set_to_failed()

    return test_result
    #  #]

def check_nccmp_installed():
    #  #[ check if nccmp is available
    ''' check if we can use the nccmp tool '''
    cmd = 'nccmp -H'
    result = run_shell_command(cmd)
    exit_status = result[2]

    nccmp_installed = True
    if exit_status != 0:
        nccmp_installed = False

    return nccmp_installed
    #  #]

def compare_nc_files(cmd):
    #  #[ compare 2 nc files
    '''
    compare 2 NetCDF files against each other
    and print relebant stdout/stderr lines in case of problems.
    '''
    stdout, stderr, exit_status = run_shell_command(cmd, verbose=True)
    if exit_status != 0:
        if len(stdout) > 0:
            print(f'first 10 lines out of {len(stdout)} lines of stdout are:')
            for line in stdout[:10]:
                print('nccmp stdout: '+line.strip())

        if len(stderr) > 0:
            print(f'first 10 lines out of {len(stderr)} lines of stderr are:')
            for line in stderr[:10]:
                print('nccmp stderr: '+line.strip())

        print('Test failed: nccmp returned with an error.')
        text = (
            '==================================================\n'+
            '                     FAILED\n'+
            '==================================================\n')
        files_match = False
        color = 'red'
    else:
        text = (
            '==================================================\n'+
            '                     PASSED\n'+
            '==================================================\n')
        files_match = True
        color = 'blue'

    print(textcolor(color, text))

    return files_match
    #  #]

def compare_nc_results(test_settings, options):
    #  #[ compare nc file
    ''' use the nccmp tool to compare the generated
    with the reference file (also for the intermediate 3d
    file that is used for the GEM case) '''

    # this 3d case is only used for the GEM test case
    out_file_3d = test_settings['out_file_3d']
    ref_file_3d = test_settings['ref_file_3d']

    out_file_2d = test_settings['out_file_2d']
    ref_file_2d = test_settings['ref_file_2d']

    run_compare_2d_nc_result = options['run_compare_2d_nc_result']
    run_compare_3d_nc_result = options['run_compare_3d_nc_result']
    testcase = options['testcase']

    only_print_commands = options['only_print_commands']

    test_result_3d = TestResult('bool', '3D nc file test')
    test_result_2d = TestResult('bool', '2D nc file test')

    # check for nccmp
    nccmp_installed = check_nccmp_installed()
    if not nccmp_installed:
        #  #[ print a warning and return
        text = (
            '==================================================\n'+
            '== nccmp is not installed, or not on the PATH   ==\n'+
            '==================================================\n'+
            f'Please manually compare {ref_file_2d} against {out_file_2d}\n'+
            f'and {out_file_3d} against {ref_file_3d}.\n'
            'If you want to install nccmp and it is not in your system repos\n'+
            'see https://gitlab.com/remikz/nccmp\n')
        print(textcolor('red', text))
        text = ('Auto output verification could not be done since\n'+
                'nccmp was not found ! Please do a manual check\n')
        print(textcolor('red', text))

        test_result_3d.set_to_skipped()
        test_result_2d.set_to_skipped()
        return test_result_3d, test_result_2d
        #  #]

    excluded_fields = [
        'File_Name',
        'Creation_Date',
        'productName',
        'processingStartTime',
        'processingStopTime'
        ]
    cmdln_options = '-fdm -T 0.1 --exclude='+(','.join(excluded_fields))

    cmd_3d = f'nccmp {cmdln_options} {ref_file_3d} {out_file_3d}'
    cmd_2d = f'nccmp {cmdln_options} {ref_file_2d} {out_file_2d}'

    if only_print_commands:
        if testcase == 'GEM' and run_compare_3d_nc_result:
            print('='*50)
            print('The command to compare the 3D NetCDF output file is:')
            print('='*50)
            print(cmd_3d)
            print('='*50)

        if run_compare_2d_nc_result:
            print('='*50)
            print('The command to compare the 2D NetCDF output file is:')
            print('='*50)
            print(cmd_2d)
            print('='*50)

        test_result_3d.set_to_skipped()
        test_result_2d.set_to_skipped()
        return test_result_3d, test_result_2d

    if (testcase == 'GEM') and (run_compare_3d_nc_result):
        print('Checking 3D netCDF outputs against reference file')
        files_match = compare_nc_files(cmd_3d)
        if files_match:
            test_result_3d.set_to_passed()
        else:
            test_result_3d.set_to_failed()
    else:
        test_result_3d.set_to_skipped()

    if run_compare_2d_nc_result:
        print('Checking 2D netCDF outputs against reference file')
        files_match = compare_nc_files(cmd_2d)
        if files_match:
            test_result_2d.set_to_passed()
        else:
            test_result_2d.set_to_failed()
    else:
        test_result_2d.set_to_skipped()

    return test_result_3d, test_result_2d
    #  #]

def compare_h5_result(cmd):
    #  #[ compare 2 h5 files
    ''' use the h5diff tool to compare the generated
    with the reference file '''
    print('Checking HDF5 product against HDF5 reference file')

    stdout, stderr, exit_status = run_shell_command(cmd, verbose=True)

    if exit_status != 0:
        # try to remove the difference:
        #
        # h5diff stdout: attribute: <_NCProperties of </>> and <_NCProperties of </>>
        # h5diff stdout: 1 differences found
        # h5diff stdout: --------------------------------
        # h5diff stdout: Some objects are not comparable
        # h5diff stdout: --------------------------------
        # h5diff stdout: Use -c for a list of objects.
        #
        # that can not be avoided
        # since this attribute contains the version of the netcdf library
        # which may differ from one system to the next.
        remaining_probems = 0
        for line in stdout:
            if 'attribute' in line:
                if '_NCProperties' not in line:
                    remaining_probems += 1
            elif line == '--------------------------------':
                pass
            elif line == 'Some objects are not comparable':
                pass
            elif line == 'Use -c for a list of objects.':
                pass
            elif line.strip() == '1 differences found':
                pass
            else:
                remaining_probems += 1

        if remaining_probems == 0:
            exit_status = 0

    if exit_status != 0:
        if len(stdout) > 0:
            print(f'first 10 lines out of {len(stdout)} lines of stdout are:')
            for line in stdout[:10]:
                print('h5diff stdout: '+line.strip())

        if len(stderr) > 0:
            print(f'first 10 lines out of {len(stderr)} lines of stderr are:')
            for line in stderr[:10]:
                print('h5diff stderr: '+line.strip())

        print('Test failed: h5diff returned with an error.')
        text = (
            '==================================================\n'+
            '                     FAILED\n'+
            '==================================================\n')
        files_match = False
        color = 'red'
    else:
        text = (
            '==================================================\n'+
            '                     PASSED\n'+
            '==================================================\n')
        files_match = True
        color = 'blue'

    print(textcolor(color, text))
    return files_match
    #  #]

def compare_xml_result(ref_file, out_file):
    #  #[ compare xml file
    ''' use the diff tool to compare the generated
    with the reference HDR file in xml format, for now
    using a simple diff tool comparison. '''
    print('Checking product HDR against reference HDR file')

    cmd = f'diff {ref_file} {out_file}'
    (stdout, stderr, exit_status) = run_shell_command(cmd, verbose=True)

    if exit_status != 0:
        if len(stdout) > 0:
            print(f'first 10 lines out of {len(stdout)} lines of stdout are:')
            for line in stdout[:10]:
                print('diff stdout: '+line.strip())

        if len(stderr) > 0:
            print(f'first 10 lines out of {len(stderr)} lines of stderr are:')
            for line in stderr[:10]:
                print('diff stderr: '+line.strip())

        print('Test failed: diff returned with an error.')
        text = (
            '==================================================\n'+
            '                     FAILED\n'+
            '==================================================\n')
        test_passed = False
        color = 'red'
    else:
        text = (
            '==================================================\n'+
            '                     PASSED\n'+
            '==================================================\n')
        test_passed = True
        color = 'blue'

    print(textcolor(color, text))
    return test_passed
    #  #]

def generate_plots(test_settings, options):
    #  #[ Generate some plots
    ''' generate several test plots '''

    testcase = test_settings['testcase']
    out_file_2d = test_settings['out_file_2d']
    out_file_3d = test_settings['out_file_3d']
    plot_prefix = test_settings['plot_prefix']

    run_generate_plots = options['run_generate_plots']
    only_print_commands = options['only_print_commands']

    test_result = TestResult('bool', 'plot generation test')

    if not run_generate_plots:
        test_result.set_to_skipped()
        return test_result

    if not only_print_commands:
        # remove possible previous results
        cmd = f'rm -f {plot_prefix}*'
        print(f'Executing [{cmd}]')
        exit_status = os.system(cmd)
        if exit_status != 0:
            print('ERROR in generate_plots: '+
                'could not delete old results named '+
                f'{plot_prefix}*')

            test_result.set_to_failed()
            return test_result

    if testcase=='GEM':
        cmd = (f'../atlid_sim_plot.py -i {out_file_2d} '+
               f'--i3d {out_file_3d} -o {plot_prefix} --zlimits=0.0,20.0')
    if testcase=='CLOUDNET':
        cmd = (f'../atlid_sim_plot.py -i {out_file_2d} -o {plot_prefix} '+
               '--zlimits=0.0,20.0')
    if testcase=='ACTIVATE':
        cmd = (f'../atlid_sim_plot.py -i {out_file_2d} -o {plot_prefix} '+
               '--zlimits=0.0,5.0')
    if testcase=='EVE':
        cmd = (f'../atlid_sim_plot.py -i {out_file_2d} -o {plot_prefix} '+
               ' --zlimits=0.0,15.0')

    if only_print_commands:
        print('='*50)
        print('The command to generate the standard plots is:')
        print('='*50)
        print(cmd)
        print('='*50)
        test_result.set_to_skipped()
        return test_result

    print(f'Executing [{cmd}]')
    exit_status = os.system(cmd)

    if exit_status == 0:
        test_result.set_to_passed()
    else:
        test_result.set_to_failed()

    return test_result
    #  #]

def compare_plots_with_reference(test_settings, options):
    #  #[ Compare generated and reference plots
    ''' compare generated plots pixel by pixel with reference plots '''

    run_compare_plots = options['run_compare_plots']

    test_result = TestResult('count', 'compare plots with reference')
    if not run_compare_plots:
        test_result.set_to_skipped()
        return test_result

    plot_prefix = test_settings['plot_prefix']
    nr_of_passed, nr_of_failed = \
      compare_plots.compare_against_reference(plot_prefix)

    test_result.add_to_passed_count(nr_of_passed)
    test_result.add_to_failed_count(nr_of_failed)

    return test_result
    #  #]

def generate_ec_files(test_settings, options):
    #  #[ Generate EC files
    ''' Generate EarthCARE formatted files from
    the simulator NetCDF output.
    '''
    out_file = test_settings['out_file_2d']
    output_dir_ec_files = test_settings['output_dir_ec_files']
    start_index = test_settings['start_index']
    reproducible_files_day = test_settings['reproducible_files_day']

    run_generate_ec_files = options['run_generate_ec_files']
    only_print_commands = options['only_print_commands']

    test_result = TestResult('bool', 'EC file generation test')

    if not run_generate_ec_files:
        test_result.set_to_skipped()
        return test_result

    if not only_print_commands:
        # remove possible old results
        print('Deleting possible old results')
        cmd = f'rm -rf {output_dir_ec_files}'
        print('Executing command: ', cmd)
        exit_status = os.system(cmd)
        if exit_status != 0:
            print('ERROR in generate_ec_files: '+
                'could not delete old results in '+
                f'{output_dir_ec_files}')
            test_result.set_to_failed()
            return test_result

    # execute the generation
    cmd = (
        '../scripts/create_earth_care_files.py '+
        '--lidar_count=1 '+
        f'--ifile {out_file} '+
        f'--output_dir {output_dir_ec_files} '+
        f'--index={start_index} '+
        "--overpass_time='14:00:00' "+
        f'--reproducible_files_day {reproducible_files_day} '+
        "--templates_folder='../templates' "+
        '--instrument_module random '+
        '--instr_data_folder=./instrument_data'
        )
    if only_print_commands:
        print('='*50)
        print('The command for generating EarthCARE files is:')
        print('='*50)
        print('cmd = ', cmd)
        print('='*50)
        test_result.set_to_skipped()
        return test_result

    print('Generate the EC files:')
    test_result.set_to_passed()
    exit_status = os.system(cmd)
    if exit_status != 0:
        print('ERROR in generate_ec_files: '+
              'could not generate EC files.')
        test_result.set_to_failed()
        return test_result

    return test_result
    #  #]

def compare_ec_files_with_reference(test_settings, options):
    #  #[ check the EC files
    ''' compare the generated EC files with the provided reference files '''

    USE_NCCMP = False
    # if True: use nccmp for comparing
    # if False: use h5diff for comparing

    output_dir_ec_files = test_settings['output_dir_ec_files']
    ref_dir_ec_files = test_settings['ref_dir_ec_files']
    run_compare_ec_files = options['run_compare_ec_files']
    only_print_commands = options['only_print_commands']

    test_result = TestResult('count', 'compare EC files with reference')

    file_type_to_check = ['ATL_NOM_1B', 'AUX_JSG_1D', 'AUX_MET_1D']
    for file_type in file_type_to_check:
        gen_f5_pattern = os.path.join(
            output_dir_ec_files, f'ECA_EXAA_{file_type}_*/*.h5')
        ref_f5_pattern = os.path.join(
            ref_dir_ec_files, f'ECA_EXAA_{file_type}_*/*.h5')

        generated_h5_file = glob.glob(gen_f5_pattern)[0]
        reference_h5_file = glob.glob(ref_f5_pattern)[0]

        # TODO:
        # add here a check to ensure the h5diff tool is available

        if USE_NCCMP:
            # check for nccmp
            nccmp_installed = check_nccmp_installed()
            if not nccmp_installed:
                #  #[ print a warning and return
                text = (
                    '==================================================\n'+
                    '== nccmp is not installed, or not on the PATH   ==\n'+
                    '==================================================\n'+
                    f'Please manually compare {reference_h5_file} '+
                    f'against {generated_h5_file}\n'+
                    'If you want to install nccmp and it is not in your system repos\n'+
                    'see https://gitlab.com/remikz/nccmp\n')
                print(textcolor('red', text))
                text = ('Auto output verification could not be done since\n'+
                    'nccmp was not found ! Please do a manual check\n')
                print(textcolor('red', text))

                test_result.set_to_skipped()
                continue
            #  #]

            #excluded_fields = []
            #cmdln_options = '-fdm -T 0.1 --exclude='+(','.join(excluded_fields))
            cmdln_options = '-fdm -T 0.1 '
            cmd = f'nccmp {cmdln_options} {generated_h5_file} {reference_h5_file}'
        else:
            cmd = f'h5diff {reference_h5_file} {generated_h5_file}'

        if only_print_commands:
            print('='*50)
            print(f'The command to compare the hdf5 {file_type} output file is:')
            print('='*50)
            print(cmd)
            print('='*50)
            test_result.set_to_skipped()

        if not run_compare_ec_files:
            test_result.set_to_skipped()
            continue

        if USE_NCCMP:
            files_match = compare_nc_files(cmd)
        else:
            files_match = compare_h5_result(cmd)

        if files_match:
            test_result.add_to_passed_count(1)
        else:
            test_result.add_to_failed_count(1)

        gen_hdr_pattern = os.path.join(
            output_dir_ec_files, f'ECA_EXAA_{file_type}_*/*.HDR')
        ref_hdr_pattern = gen_hdr_pattern.replace('output/', 'ref/')

        generated_hdr_file = glob.glob(gen_hdr_pattern)[0]
        reference_hdr_file = glob.glob(ref_hdr_pattern)[0]

        hdr_test_passed = compare_xml_result(reference_hdr_file,
                                             generated_hdr_file)
        if hdr_test_passed:
            test_result.add_to_passed_count(1)
        else:
            test_result.add_to_failed_count(1)

    return test_result
    #  #]

def choose_test_case(testcase):
    #  #[ allow different test cases
    ''' choose settings depending on the selected test case '''

    # define some settings for each test case
    test_settings = {'testcase':testcase}
    if testcase == 'ACTIVATE':
        test_settings['config_file'] = \
          'config_xlsx/Test_input_spread_sheet_ACTIVATE_data.xlsx'
        test_settings['out_file_3d'] = None
        test_settings['ref_file_3d'] = None
        test_settings['out_file_2d'] = 'output/Test_ACTIVATE_data_RT.nc'
        test_settings['ref_file_2d'] = 'ref/Test_ACTIVATE_data_RT.nc'
        test_settings['plot_prefix'] = 'output/figures_ACTIVATE/test_ACTIVATE'
        test_settings['reproducible_files_day'] = '20231005'
        test_settings['output_dir_ec_files'] = 'output/EC_files_ACTIVATE/'
        test_settings['ref_dir_ec_files'] = 'ref/EC_files_ACTIVATE/'
        test_settings['start_index'] = '23'
    elif testcase == 'CLOUDNET':
        test_settings['config_file'] = \
          'config_xlsx/Test_input_spread_sheet_CLOUDNET_data.xlsx'
        test_settings['out_file_3d'] = None
        test_settings['ref_file_3d'] = None
        test_settings['out_file_2d'] = 'output/Test_CLOUDNET_data_RT.nc'
        test_settings['ref_file_2d'] = 'ref/Test_CLOUDNET_data_RT.nc'
        test_settings['plot_prefix'] = 'output/figures_CLOUDNET/test_CLOUDNET'
        test_settings['reproducible_files_day'] = '20231006'
        test_settings['output_dir_ec_files'] = 'output/EC_files_CLOUDNET/'
        test_settings['ref_dir_ec_files'] = 'ref/EC_files_CLOUDNET/'
        test_settings['start_index'] = '11'
    elif testcase == 'EVE':
        test_settings['config_file'] = \
          'config_xlsx/Test_input_spread_sheet_EVE_data.xlsx'
        test_settings['out_file_3d'] = None
        test_settings['ref_file_3d'] = None
        test_settings['out_file_2d'] = 'output/Test_EVE_data_RT.nc'
        test_settings['ref_file_2d'] = 'ref/Test_EVE_data_RT.nc'
        test_settings['plot_prefix'] = 'output/figures_EVE/test_EVE'
        test_settings['reproducible_files_day'] = '20231007'
        test_settings['output_dir_ec_files'] = 'output/EC_files_EVE/'
        test_settings['ref_dir_ec_files'] = 'ref/EC_files_EVE/'
        test_settings['start_index'] = '3'
    elif testcase == 'GEM':
        test_settings['config_file'] = \
          'config_xlsx/Test_input_spread_sheet_GEM_data.xlsx'
        test_settings['out_file_3d'] = 'output/Test_GEM_data_RT_3d.nc'
        test_settings['ref_file_3d'] = 'ref/Test_GEM_data_RT_3d.nc'
        test_settings['out_file_2d'] = 'output/Test_GEM_data_RT_2d.nc'
        test_settings['ref_file_2d'] = 'ref/Test_GEM_data_RT_2d.nc'
        test_settings['plot_prefix'] = 'output/figures_GEM/test_GEM'

        # used for the 3D to 2D conversion (trajectory curtain generation)
        test_settings['traj_start'] = '0,10'
        test_settings['traj_stop'] = '99,25'
        test_settings['traj_start_dt'] = '20210918T120000'

        # used for the EC file generation
        test_settings['reproducible_files_day'] = '20231008'
        test_settings['output_dir_ec_files'] = 'output/EC_files_GEM/'
        test_settings['ref_dir_ec_files'] = 'ref/EC_files_GEM/'
        test_settings['start_index'] = '1'
    else:
        errtxt = ('ERROR: input test case must be one of GEM or CLOUDNET '+
            'or ACTIVATE or EVE, '+
            f'but the current value is {testcase}. This is not implemented.')
        raise CommandLineInputError(errtxt)

    return test_settings
    #  #]

def get_commandline_options():
    #  #[ handle options
    ''' handle incoming commandline options '''
    args = sys.argv[1:]

    short_options = 'ho:t:p'
    long_options = ['help', 'only=', 'testcase=', 'only_print_commands']

    try:
        options = getopt.getopt(args, short_options, long_options)[0]
    except getopt.GetoptError:
        print_usage()
        raise

    # defaults
    run_only = None
    testcase = None
    run_simulation = True
    run_convert_3d_to_2d = True # only used for the GEM case
    run_compare_2d_nc_result = True
    run_compare_3d_nc_result = True # only used for the GEM case
    run_generate_plots = True
    run_compare_plots = True
    run_generate_ec_files = True
    run_compare_ec_files = True
    only_print_commands = False

    # handle options
    for opt, value in options:
        if opt in ['-h', '--help']:
            print_usage()
            sys.exit(0)
        if opt in ['-o', '--only']:
            run_only = value
        if opt in ['-t', '--testcase']:
            testcase = value
        if opt in ['-p', '--only_print_commands']:
            only_print_commands = True
            run_compare_plots = False
            run_compare_ec_files = False

    if run_only is not None:
        run_simulation = False
        run_convert_3d_to_2d = False
        run_compare_2d_nc_result = False
        run_compare_3d_nc_result = False
        run_generate_plots = False
        run_compare_plots = False
        run_generate_ec_files = False
        run_compare_ec_files = False
        if run_only == 'simulation':
            run_simulation = True
        elif run_only == 'convert_3d_to_2d':
            run_convert_3d_to_2d = True
        elif run_only == 'compare_2d_nc_result':
            run_compare_2d_nc_result = True
        elif run_only == 'compare_3d_nc_result':
            run_compare_3d_nc_result = True
        elif run_only == 'generate_plots':
            run_generate_plots = True
        elif run_only == 'compare_plots':
            run_compare_plots = True
        elif run_only == 'generate_ec_files':
            run_generate_ec_files = True
        elif run_only == 'compare_ec_files':
            run_compare_ec_files = True
        else:
            error_txt = ('ERROR: unknown value for commandline option '+
                f'run_only: {run_only}')
            raise CommandLineInputError(error_txt)

    options = {
        'run_only':run_only,
        'testcase':testcase,
        'run_simulation':run_simulation,
        'run_convert_3d_to_2d':run_convert_3d_to_2d,
        'run_compare_2d_nc_result':run_compare_2d_nc_result,
        'run_compare_3d_nc_result':run_compare_3d_nc_result,
        'run_generate_plots':run_generate_plots,
        'run_compare_plots':run_compare_plots,
        'run_generate_ec_files':run_generate_ec_files,
        'run_compare_ec_files':run_compare_ec_files,
        'only_print_commands':only_print_commands,
        }

    return options
    #  #]

def print_usage():
    #  #[ print usage
    ''' display an explanation of how to use this script '''
    try:
        testcase = sys.argv[1]
    except IndexError:
        # default
        testcase = 'GEM'

    if testcase in ['-h', '--help']:
        script_name = sys.argv[0]
        print('')
        print(f'USAGE: {script_name} [OPTIONS]')
        print('')
        print('Explanation of the options:')
        print('    -h/--help')
        print('           Display this help message.')
        print('    -o/--only <STEP TO RUN>')
        print('           in which <STEP TO RUN> is one of:')
        print('           simulation')
        print('           convert_3d_to_2d')
        print('           compare_2d_nc_result')
        print('           compare_3d_nc_result')
        print('           generate_plots')
        print('           compare_plots')
        print('           generate_ec_files')
        print('           compare_ec_files')
        print('    -t/--testcase <TESTCASE>')
        print('           select test case to use')
        print('           in which <TESTCASE> is one of:')
        print('           GEM       (3D model data)')
        print('           CLOUDNET  (2D ground lidar/radar data)')
        print('           ACTIVATE  (2D sample airplane data)')
        print('           EVE       (2D ground lidar data)')
        print('    -p/--only_print_commands')
        print('           only print the commands that will be executed')
        print('           for thew selected test case, nbut do not execute')
        print('           the test itself.')
        print('')
        sys.exit(0)
    #  #]

def main():
    #  #[ main program
    ''' run the choosen test and compare the outputs with reference results'''

    # parse commandline options
    options = get_commandline_options()

    # select test case based on commandline input
    testcase = options['testcase']
    only_print_commands = options['only_print_commands']

    # get settings for the selected test case
    test_settings = choose_test_case(testcase)

    test_results = []

    # execute the simulation
    tst = run_lidar_module(test_settings, options)
    test_results.append(tst)

    if testcase == 'GEM':
        # sanity check (useful if steps are skipped)
        out_file_3d = test_settings['out_file_3d']
        out_file_3d_exists = os.path.exists(out_file_3d)
        if not out_file_3d_exists:
            # disable the next compare and conversion calls
            options['run_convert_3d_to_2d'] = False
            options['run_compare_3d_nc_result'] = False

        # convert 3D to 2D
        tst = convert_3d_to_2d(test_settings, options)
        test_results.append(tst)

    # sanity check (useful if steps are skipped)
    out_file_2d = test_settings['out_file_2d']
    out_file_2d_exists = os.path.exists(out_file_2d)
    if not out_file_2d_exists:
        # disable all next steps that need this input
        options['run_compare_2d_nc_result'] = False
        options['run_generate_plots'] = False
        options['run_compare_plots'] = False
        options['run_generate_ec_files'] = False
        options['run_compare_ec_files'] = False

    # compare the generated and reference nc files
    tsts = compare_nc_results(test_settings, options)
    test_results.extend(tsts)

    # generate some plots
    tst = generate_plots(test_settings, options)
    test_results.append(tst)

    # verify the plots
    tst = compare_plots_with_reference(test_settings, options)
    test_results.append(tst)

    # create EarthCARE files
    tst = generate_ec_files(test_settings, options)
    test_results.append(tst)

    # verify the generated EC files
    tst = compare_ec_files_with_reference(test_settings, options)
    test_results.append(tst)

    if only_print_commands:
        return

    # remove a possible earlier test result
    tst_fn = define_testresult_filename(testcase)
    if os.path.exists(tst_fn):
        os.remove(tst_fn)

    # print test results and save them to file
    for tst in test_results:
        tst.print_result()
        tst.write_to_file(testcase)

    #  #]

if __name__ == '__main__':
    main()

#!/usr/bin/env python3

#  #[ documentation
#
#   Copyright (C) 2022-2023  KNMI
#
#   Author: Dave Donovan <dave.donovan@knmi.nl>
#   Converted from bash to Python by: Jos de Kloe, 26-May-2023
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
A small test script that:
1. provides a function to store test results in a small text file
2. provides a main script to load all test results and siaplays
   them as a summary listing.
'''
#  #]
#  #[ imported modules

import os
import sys
import glob

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.helpers import textcolor
from lib.custom_exceptions import ProgrammingError, NotYetImplementedFeature
# pylint: enable=wrong-import-position, import-error
#  #]

class TestResult:
    '''
    A class to easily store test results
    load them to file, read them again, and print them.
    '''
    def __init__(self, test_type, test_name):
        #  #[ init
        '''
        initialize this tes
        test type can be bool or count
        '''

        if test_type in ['bool', 'count']:
            self.test_type = test_type
        else:
            errtxt = ('ERROR in TestResult.__init__(): '+
                f'unknown test type: {test_type}')
            raise NotYetImplementedFeature(errtxt)

        self.test_name = test_name

        # only used for test type 'bool'
        self.passed = False

        # only used for test type 'count'
        self.nr_of_passes = 0
        self.nr_of_failures = 0

        # set if test is skipped
        self.skipped = False

        # keep track of test execution
        self.executed = False
        #  #]
    def set_to_passed(self):
        #  #[ mark as passed
        ''' mark as passed '''
        self.passed  = True
        self.executed = True
        #  #]
    def set_to_failed(self):
        #  #[ mark as failed
        ''' mark as failed '''
        self.passed  = False
        self.executed = True
        #  #]
    def add_to_passed_count(self, count=1):
        #  #[ increase count
        ''' count how many files were succesfully checked '''
        self.nr_of_passes += count
        self.executed = True
        #  #]
    def add_to_failed_count(self, count=1):
        #  #[ increase count
        ''' count how manu file checks failed '''
        self.nr_of_failures += count
        self.executed = True
        #  #]
    def set_to_skipped(self):
        #  #[ mark as skipped
        ''' mark as skipped '''
        self.skipped = True
        #  #]
    def print_result(self):
        #  #[ print the test result
        '''
        prints the test outcome using colored text
        '''
        if self.skipped:
            color = 'grey'
            test_text = 'this test case was skipped.'
            print(textcolor(color, f'{self.test_name}: {test_text}'))
            return

        if not self.executed:
            # this is an error!
            color = 'red'
            test_text = 'the testcase was defined but never set ....'
            print(textcolor(color, f'{self.test_name}: {test_text}'))
            return

        if self.test_type == 'bool':
            if self.passed:
                test_text = 'passed'
                color = 'green'
            else:
                test_text = 'failed'
                color = 'red'
            print(textcolor(color, f'{self.test_name}: {test_text}'))
            return

        if self.test_type == 'count':
            color = 'green'
            if self.nr_of_passes == 0:
                color = 'red'
            print(textcolor(color,
                            f'{self.test_name}: nr of passed files = '+
                            f'{self.nr_of_passes}'))

            color = 'green'
            if self.nr_of_failures > 0:
                color = 'red'
            print(textcolor(color,
                            f'{self.test_name}: nr of failed files = '+
                            f'{self.nr_of_failures}'))
            return
        #  #]
    def write_to_file(self, testcase):
        #  #[ store test result
        ''' writes the test results for a given test case to an ascii file'''
        test_fn = define_testresult_filename(testcase)

        # check that self.test_name does not contain a semi colon
        if ';' in self.test_name:
            errtxt = ('Error in write_to_file: test_name should '+
                      'not contain a semi-colon!!!')
            raise ProgrammingError(errtxt)

        text = (
            f'{self.test_type};'+
            f'{self.test_name};'+
            f'{self.passed};'+
            f'{self.nr_of_passes};'+
            f'{self.nr_of_failures};'+
            f'{self.skipped};'+
            f'{self.executed}'
            )
        with open(test_fn, 'at', encoding='ascii') as tfd:
            tfd.write(f'{text}\n')
        #  #]

def load_tests_from_file(test_fn):
    #  #[ load test result
    '''
    load a stored test result from file
    '''
    test_results = []
    with open(test_fn, 'rt', encoding='ascii') as tfd:
        for line in tfd.readlines():
            parts = line.strip().split(';')
            if len(parts) != 7:
                print('ERROR in load_from_file: line could not be '+
                      f'parsed: [{line.strip()}]')
                print('DEBUG: parts = ', parts)

            tst = TestResult('bool', 'dummy')
            tst.test_type = parts[0]
            tst.test_name = parts[1]

            tst.passed = False
            if parts[2] == 'True':
                tst.passed = True

            tst.nr_of_passes = int(parts[3])
            tst.nr_of_failures = int(parts[4])

            tst.skipped = False
            if parts[5] == 'True':
                tst.skipped = True

            tst.executed = False
            if parts[6] == 'True':
                tst.executed = True

            test_results.append(tst)

    return test_results
    #  #]

def define_testresult_filename(testcase):
    #  #[ define filename
    ''' defines the file name for the test result file of a given testcase '''
    test_fn = f'testresults_{testcase}.txt'
    return test_fn
    #  #]

def load_and_print_unittest_log():
    #  #[ load/parse it
    ''' load and parse a unittest logfile and print
        an overview result with color coding '''
    unittest_logfile = '../unittest_logfile.txt'
    summary_line = ''
    with open(unittest_logfile, 'rt', encoding='ascii') as fd_utl:
        for line in fd_utl.readlines():
            if 'collected' in line:
                summary_line = line

    print('='*50)
    print('='*50)
    print('Unittesting result:')
    unit_testing_failed = False
    color = 'green'
    if 'error' in summary_line.lower():
        unit_testing_failed = True
        color = 'red'

    print(textcolor(color, summary_line.strip()))

    if unit_testing_failed:
        print(textcolor(color, 'unittesting failed!'))
        print(textcolor(color,
                        f'See the log file {unittest_logfile} for details.'))
    else:
        print(textcolor(color, 'unittesting passed'))
    print('='*50)
    #  #]

def report_cov_per_file(line):
    #  #[ report per file
    ''' parse result for one file and print with color'''
    parts = line.split()
    if len(parts) != 4:
        print(line)
        return
    if parts[3] == 'Cover':
        print(line)
        return

    total_statements = int(parts[1])
    missed_statements = int(parts[2])
    covered_statements = total_statements - missed_statements

    percentage = 100.
    if total_statements > 0:
        percentage = 100.*covered_statements/total_statements

    color = 'green'
    if percentage < 75.:
        color = 'yellow'
    if percentage < 50.:
        color = 'red'

    print(textcolor(color, line))
    #  #]

def load_and_print_coverage_log():
    #  #[ load/parse it
    ''' load and parse a coverage logfile and print
        an overview result with color coding '''

    print('Unittest code coverage result:')
    coverage_logfile = '../coverage_logfile.txt'

    # allow missing coverage results
    if not os.path.exists(coverage_logfile):
        return

#    summary_line = ''
    with open(coverage_logfile, 'rt', encoding='ascii') as fd_cl:
        for line in fd_cl.readlines():
            # results per file
            report_cov_per_file(line.strip())

#            if 'TOTAL' in line:
#                summary_line = line

#    parts = summary_line.split()
#    if len(parts) == 4:
#        total_statements = int(parts[1])
#        missed_statements = int(parts[2])
#        covered_statements = total_statements - missed_statements
#        percentage = 100.*covered_statements/total_statements
#        print('covered_statements = ', covered_statements)
#        print('missed_statements  = ', missed_statements)
#        print('total_statements   = ', total_statements)
#        print('percentage         = ', percentage)
    #  #]

def main():
    #  #[ main script
    ''' the main function '''
    print('\n'*5)
    load_and_print_unittest_log()
    load_and_print_coverage_log()

    pattern = 'testresults_*.txt'
    test_reports = sorted(glob.glob(pattern))
    for test_report in test_reports:
        print(65*'=')
        print(f'loading test results from: {test_report}')
        test_results = load_tests_from_file(test_report)
        for tst in test_results:
            tst.print_result()
        print(65*'=')
     #  #]

if __name__ == '__main__':
    main()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  #[ documentation
#
#   Created on 23-Mar-2023
#
#   Copyright (C) 2023  KNMI
#
#   Authors: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module implements testing the loading of user inputs
from the sample config file.
It compares the results from Dave's original implementation
with the new class based implementation added by Jos.
'''
#  #]
#  #[ imported modules
import os
import sys
import unittest
import openpyxl

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.handle_xlsx import (FormatErrorXLSX,
       get_next_value, verify_cell_value, strip_if_possible,
       extract_table_positions,
       check_config_version, extract_file_list, extract_domain,
       extract_simulation_variables, extract_lidar_params, AllInputs)
from lib import handle_xlsx
from lib.config_file_definition import SIMVAR_METEO_DEFINITIONS
#  #]

FN_XLSX = 'config_xlsx/Test_input_spread_sheet_GEM_data.xlsx'
if not os.path.exists(FN_XLSX):
    FN_XLSX = 'test/config_xlsx/Test_input_spread_sheet_GEM_data.xlsx'

#FN_XLSX = 'test.xlsx'

class LoadFromSampleConfig(unittest.TestCase):
    ''' tests for the handle_xlsx module'''
    def setUp(self):
        #  #[ open the spreadsheet
        ''' setup the worksheet as needed by most tests below '''
        wbk = openpyxl.load_workbook(filename=FN_XLSX)
        self.worksheet =  wbk['Sheet1']
        self.tpos = extract_table_positions(self.worksheet)
        #  #]
    def test_get_next_value(self):
        #  #[ test some individual cells
        ''' a test for the get_next_value function '''
        val1 = get_next_value(self.worksheet, 'B13')
        val2 = get_next_value(self.worksheet, 'B26', hor_offset=1)
        val3 = get_next_value(self.worksheet, 'B13', ver_offset=1)
        val4 = get_next_value(self.worksheet, 'A14', hor_offset=1, ver_offset=1)
        self.assertAlmostEqual(val1, 400)
        self.assertEqual(val2, None)
        self.assertAlmostEqual(val3, 40000)
        self.assertEqual(val4, 20000)

        val1 = get_next_value(self.worksheet, 'A19')
        val2 = get_next_value(self.worksheet, 'A19', hor_offset=1)
        val3 = get_next_value(self.worksheet, 'A19', ver_offset=1)
        val4 = get_next_value(self.worksheet, 'A19', hor_offset=1, ver_offset=2)
        self.assertEqual(val1, 'fov_telescope[mrads]')
        self.assertAlmostEqual(val2, 0.075)
        self.assertEqual(val3, 'div_laser[mrads]')
        self.assertEqual(val4, 355)
        #  #]
    def test_verify_cell_value(self):
        #  #[ test cell verification
        ''' a test for the verify_cell_value function '''
        cell_index = 'Z99'
        expected_value = 'should pass'
        actual_value = 'should pass'
        verify_cell_value(cell_index, expected_value, actual_value)

        actual_value = 'should fail'
        self.assertRaises(FormatErrorXLSX,
            verify_cell_value, cell_index, expected_value, actual_value)
        #  #]
    def test_strip_if_possible(self):
        #  #[ test striiping of spaces
        ''' a test for the strip_if_possible function '''

        value = 'test without spaces'
        result = strip_if_possible(value)
        self.assertEqual(value, result)

        value = 1.234
        result = strip_if_possible(value)
        self.assertEqual(value, result)

        value = None
        result = strip_if_possible(value)
        self.assertEqual(value, result)

        value = 'test with some spaces   '
        result = strip_if_possible(value)
        self.assertEqual(value.strip(), result)
        #  #]
    def test_check_config_version(self):
        #  #[ check the config check
        ''' a test for the function check_config_version '''

        # should pass
        check_config_version(self.worksheet)

        # should fail

        # stub a wrong value in the EXPECTED_CONFIG_FORMAT_VERSION constant
        saved_value = handle_xlsx.EXPECTED_CONFIG_FORMAT_VERSION

        handle_xlsx.EXPECTED_CONFIG_FORMAT_VERSION = '-0.999x'
        self.assertRaises(FormatErrorXLSX,
                          check_config_version, self.worksheet)

        # and restore
        handle_xlsx.EXPECTED_CONFIG_FORMAT_VERSION = saved_value
        #  #]
    def test_extract_file_list(self):
        #  #[ read the directory that should hold the data
        ''' a test for the function extract_file_list
        that should provide the directory that holds the data
        and the filenames that will be used.
        '''
        dict_of_file_names, list_of_keys = \
          extract_file_list(self.worksheet, self.tpos)
        data_dir = dict_of_file_names['data_dir']

        print('dict_of_file_names, list_of_keys = ',
              dict_of_file_names, list_of_keys)

        expected_data_dir = './data/GEM_DATA'
        self.assertEqual(data_dir, expected_data_dir)
        #  #]
    def test_extract_domain(self):
        #  #[ read the Domain to process
        ''' a test for the extract_domain function '''
        dict_of_domain_params = extract_domain(self.worksheet, self.tpos)[0]

        domain = (dict_of_domain_params['ix1'],
                  dict_of_domain_params['ix2'],
                  dict_of_domain_params['iy1'],
                  dict_of_domain_params['iy2'],)

        expected_domain = (1, 100, 1, 50)
        self.assertEqual(domain, expected_domain)
        #  #]
    def test_extract_simulation_variables(self):
        #  #[ read the Files and variables to read
        ''' a test for the extract_simulation_variables function '''

        row_defs = SIMVAR_METEO_DEFINITIONS
        ulc_index = self.tpos['meteo_simvar_table']
        dict_of_sv, list_of_keys = \
          extract_simulation_variables(self.worksheet, ulc_index, row_defs)

        expected_list_of_first_4_keys = [
            'met_height', 'met_latitude', 'met_longitude', 'met_time']
        fn1 = 'FILE#1'
        fn2 = 'FILE#2'
        expected_dict_of_inp_file = {
            'met_height':fn1,
            'met_latitude':fn2,
            'met_longitude':fn2,
            'met_time':None}
        expected_dict_of_file_var = {
            'met_height':'height_thermodynamic',
            'met_latitude':'lat',
            'met_longitude':'lon',
            'met_time':None}

        self.assertEqual(expected_list_of_first_4_keys, list_of_keys[:4])

        for key in expected_list_of_first_4_keys:
            inp_file = dict_of_sv[key].input_file
            self.assertEqual(inp_file, expected_dict_of_inp_file[key])
            file_variable = dict_of_sv[key].file_variable
            self.assertEqual(file_variable, expected_dict_of_file_var[key])
        #  #]
    def test_extract_lidar_params(self):
        #  #[ read the lidar params
        ''' a test for the extract_lidar_params function '''
        (dict_of_lidar_params,
         list_of_keys) = extract_lidar_params(self.worksheet, self.tpos)

        expected_list_of_keys = ['sat_alt', 'z1', 'z2', 'dz1', 'z3', 'dz2',
            'fov_telescope', 'div_laser', 'laser_wavelength']

        expected_dict_of_lidar_params = {
            'sat_alt': 400,
            'z1': 40000,
            'z2': 20000,
            'dz1': 500,
            'z3': -500,
            'dz2': 100,
            'fov_telescope': 0.075,
            'div_laser': 0.05,
            'laser_wavelength': 355}

        self.assertEqual(list_of_keys, expected_list_of_keys)

        for key in list_of_keys:
            self.assertEqual(dict_of_lidar_params[key],
                             expected_dict_of_lidar_params[key])
        #  #]
    def test_load_it_all(self):
        #  #[ read all at once
        ''' a test for the load_all_from_worksheet method '''
        all_inp = AllInputs()
        all_inp.load_all_from_worksheet(self.worksheet)

        expected_data_dir = './data/GEM_DATA'
        self.assertEqual(all_inp.data_dir, expected_data_dir)

        expected_num_inp_files = 2
        self.assertEqual(all_inp.dict_of_file_names['num_inp_files'],
                         expected_num_inp_files)

        expected_input_file = 'Cape_Verde_F12_GEM_2021091812_300min_SIPon_trimmed.nc'
        self.assertEqual(all_inp.dict_of_file_names['FILE#1'],
                         expected_input_file)

        expected_input_file = 'FILE#1'
        self.assertEqual(all_inp.simulation_variables.species_3_wc.input_file,
                         expected_input_file)

        expected_z1 = 40000.0
        self.assertAlmostEqual(all_inp.lidar_params.z1, expected_z1)

        expected_g_water = 0.85
        self.assertAlmostEqual(
            all_inp.simulation_variables.species_1_g.fixed_value,
            expected_g_water)
        #  #]

if __name__ == '__main__':
    unittest.main()

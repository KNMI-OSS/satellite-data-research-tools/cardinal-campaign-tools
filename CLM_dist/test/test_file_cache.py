#!/usr/bin/env python3
#
#  #[ documentation
#
#   Created on 12-Dec-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
'''
This module implements testing of the file_cache.py helper module
'''
#  #]
#  #[ imported modules
import os
import sys
import unittest
import io
from contextlib import redirect_stdout

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.file_cache import InputFileCache
# pylint: enable=wrong-import-position, import-error
#  #]

INPUT_FILE_1 = 'dummy_input_file_1'
INPUT_FILE_2 = 'dummy_input_file_2'
NOT_EXISTING_FILE = 'notexistingfile'

class StubNetCdfDataset:
    '''
    a little stub class to mimic the NetCDF Dataset class
    '''
    def __init__(self):
        self.filename = None
        self.data = None
    def dataset(self, filename):
        #  #[ open
        ''' open a sub dataset/file '''
        # mimic file not found
        if filename == NOT_EXISTING_FILE:
            raise FileNotFoundError
        self.filename = filename
        self.data = f'data from file: {filename}'
        return self
        #  #]
    def close(self):
        #  #[ close
        ''' close the stub dataset '''
        self.filename = None
        self.data = None
        #  #]

class StubFileCache(InputFileCache):
    '''
    a little derived stub class to test caching the currently open file
    for reuse, and only open a new one if needed.
    '''
    def open_in_file(self):
        #  #[ open file
        ''' test opening the first file in the file cache '''
        stub_ds = StubNetCdfDataset()
        return stub_ds.dataset(self.in_file)
        #  #]
    def close_in_file(self):
        #  #[ close
        ''' test closing the file cache'''
        self.in_ds.close()
        #  #]

class StubFileCacheWrong(InputFileCache):
    '''
    a little derived stub class without close_in_file method
    to test if this case is caught
    '''
    def open_in_file(self):
        #  #[ open file
        ''' test opening the first file in the file cache '''
        stub_ds = StubNetCdfDataset()
        return stub_ds.dataset(self.in_file)
        #  #]

class TestInputFileCache(unittest.TestCase):
    ''' test the bare class '''
    def test_instantiate(self):
        #  #[ should fail
        ''' check that using the baseclass fails
        because subclassing is required '''
        self.assertRaises(NotImplementedError, InputFileCache, INPUT_FILE_1)
        #  #]
    def test_close(self):
        #  #[ should fail
        ''' check that using the close_in_file method on the class
        StubFileCacheWrong fails
        because subclassing is required '''
        wrong_sfc = StubFileCacheWrong(INPUT_FILE_1)
        self.assertRaises(NotImplementedError, wrong_sfc.close_in_file)
        #  #]

class TestFileCache(unittest.TestCase):
    ''' test a derived class '''
    def setUp(self):
        #  #[ setup
        ''' generic setup for most tests '''
        self.ifc = StubFileCache(INPUT_FILE_1)
        #  #]
    def test_instantiate(self):
        #  #[ instantiate
        ''' test getting an instance of the file cache '''
        self.assertEqual(self.ifc.in_file, INPUT_FILE_1)
        #  #]
    def test_open(self):
        #  #[ test open
        ''' test opening a file '''
        result = self.ifc.open_in_file()
        expected_result = f'data from file: {INPUT_FILE_1}'
        self.assertEqual(result.data, expected_result)
        #  #]
    def test_open_same_file(self):
        #  #[ test open the same file again
        ''' open the same file, which should now come from the cache '''
        self.ifc.update_to_new_fn(INPUT_FILE_1)
        result = self.ifc.in_ds
        expected_result = f'data from file: {INPUT_FILE_1}'
        self.assertEqual(result.data, expected_result)
        #  #]
    def test_open_next_file(self):
        #  #[ test open next
        ''' open a new file and cache it '''
        self.ifc.update_to_new_fn(INPUT_FILE_2)
        result = self.ifc.in_ds
        expected_result = f'data from file: {INPUT_FILE_2}'
        self.assertEqual(result.data, expected_result)
        #  #]
    def test_open_not_existing_file(self):
        #  #[ non existing test 1
        ''' test opening a non existing file '''
        with io.StringIO() as buf, redirect_stdout(buf):
            self.assertRaises(FileNotFoundError,
                              self.ifc.update_to_new_fn, NOT_EXISTING_FILE)
            output = buf.getvalue()

        expected_output = f'Error File Not Found: {NOT_EXISTING_FILE}\n'
        self.assertEqual(output, expected_output)
        #  #]
    def test_instantiate_with_not_existing_file(self):
        #  #[ non existing test 2
        ''' test instantiate using a non existing file '''
        with io.StringIO() as buf, redirect_stdout(buf):
            self.assertRaises(FileNotFoundError,
                              StubFileCache, NOT_EXISTING_FILE)
            output = buf.getvalue()

        expected_output = f'Error File Not Found: {NOT_EXISTING_FILE}\n'
        self.assertEqual(output, expected_output)
        #  #]
    def test_close_file(self):
        #  #[ close file test
        ''' test closing the open file '''
        # pylint: disable=bare-except
        try:
            self.ifc.close_in_file()
        except:
            self.fail('ERROR while calling close_in_file()')
        # pylint: enable=bare-except
        #  #]
    def test_close(self):
        #  #[ close file cache test
        ''' test closing the file cache '''
        # pylint: disable=bare-except
        try:
            self.ifc.close()
        except:
            self.fail('ERROR while calling close_in_file()')
        # pylint: enable=bare-except
        #  #]

if __name__ == '__main__':
    unittest.main()

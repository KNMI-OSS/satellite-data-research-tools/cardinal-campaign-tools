#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 23-Mar-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
'''
This module implements testing of the load functionality
for xlsx files that are used to define the user inputs
for this software.
'''
#
#  #]

import os
import unittest
from openpyxl import Workbook, load_workbook
from openpyxl.worksheet.worksheet import Worksheet

FN_XLSX = 'config_xlsx/Test_input_spread_sheet_GEM_data.xlsx'
if not os.path.exists(FN_XLSX):
    FN_XLSX = 'test/config_xlsx/Test_input_spread_sheet_GEM_data.xlsx'

class LoadExcelTests(unittest.TestCase):
    ''' a simple test to check if the openpyxl module
    does what it promised to do '''
    def test_load_excel(self):
        ''' test the load_workbook function '''

        # open an xlsx file as a workbook
        workbook = load_workbook(filename=FN_XLSX)

        # point to the relevant sheet
        worksheet = workbook['Sheet1']

        self.assertIsInstance(workbook, Workbook)
        self.assertIsInstance(worksheet, Worksheet)

if __name__ == '__main__':
    unittest.main()

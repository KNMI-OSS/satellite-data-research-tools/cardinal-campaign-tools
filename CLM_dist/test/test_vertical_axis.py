#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 10-Nov-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module implements testing of the vertical_axis module.
'''
#  #]

#  #[ imported modules
import os
import sys
import unittest
import numpy as np

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.vertical_axis import VerticalAxis
from lib.handle_xlsx import AllInputs
from lib.index_handler import IndexIterClass #, IndexClass # not yet used
# pylint: enable=wrong-import-position, import-error

#  #]

np_eq = np.testing.assert_almost_equal

class TestVerticalAxisClass(unittest.TestCase):
    ''' test the VerticalAxis class '''
    def setUp(self):
        #  #[ setup
        ''' generic setup for most tests '''

        # note: self.all_inp is used below in test_initialize
        #       therefore it must be an instance variable
        self.all_inp = AllInputs()

        self.all_inp.lidar_params.sat_alt = 400 # [km]
        self.all_inp.lidar_params.z1 = 40000 # [m]
        self.all_inp.lidar_params.z2 = 123456 # [m] # this one seems never used
        self.all_inp.lidar_params.dz1 = 500 # [m]
        self.all_inp.lidar_params.z3 = 500 # [m]
        self.all_inp.lidar_params.dz2 = 100 # [m]

        nz_orig = 75
        self.test_va = VerticalAxis(self.all_inp, nz_orig)
        #  #]
    def test_initialize(self):
        #  #[ test initializing
        ''' test initializing '''
        self.assertEqual(self.all_inp.lidar_params.z1, self.test_va.z1)
        self.assertEqual(self.all_inp.lidar_params.z2, self.test_va.z2)
        #  #]
    def test_define_1d_z_array(self):
        #  #[ test creating a 1D z_array
        ''' test create 1D z_array '''

        # for a 1D height array
        height = np.linspace(25.,0,6)
        idx = None # not used
        z_arr = self.test_va.define_z_array(height, idx)
        #print('z_arr = ', z_arr)

        expected_z_arr = np.array([375000., 380000., 385000.,
                                   390000., 395000., 400000.])
        try:
            np_eq(z_arr, expected_z_arr)
        except AssertionError:
            self.fail("np_eq() raised AssertionError unexpectedly!")
        #  #]
    def test_define_3d_z_array(self):
        #  #[ test creating a 1D z_array
        ''' test create 3D z_array '''

        # for a 3D height array
        z_arr = np.linspace(36.,0.,7)
        height = np.zeros((7,5,5),dtype=float)
        for i in range(5):
            for j in range(5):
                height[:,j,i] = z_arr

        index_iterator = IndexIterClass(6, 5)
        idx = index_iterator.__next__()
        #idx = index_iterator.__next__()
        #idx = index_iterator.__next__()

        z_arr = self.test_va.define_z_array(height, idx)
        #print('z_arr = ', z_arr)
        expected_z_arr = np.array([364000., 370000., 376000.,
                                   382000., 388000., 394000., 400000.])

        # note that the final element of test_va.lid_range[150:320:20]
        # is 391000 which is out of range of z_inp
        # so this should contain the fill value 0.0
        # and not the edge value 15.2
        try:
            np_eq(z_arr, expected_z_arr)
        except AssertionError:
            self.fail("np_eq() raised AssertionError unexpectedly!")
        #  #]
    def test_interpolate_to_lidar_range_incr(self):
        #  #[ test interpolate with increasing z
        ''' test interpolate with increasing z '''

        # test for increasing z values
        z_inp = np.array([0,3,6,9,12,15])*1000+375000
        profile = np.array([4.1,5.1,7.3,9.8,12.5,15.2])
        key = '_extinction'
        prof_interp = self.test_va.interpolate_to_lidar_range(
                                             z_inp, profile, key)
        #print('prof_interp ', prof_interp[150:320:20])

        expected_profile = np.array([4.1, 4.76666667, 5.83333333, 7.3,
                                     8.96666667, 10.7, 12.5, 14.3, 0.0 ])

        # note that the final element of test_va.lid_range[150:320:20]
        # is 391000 which is out of range of z_inp
        # so this should contain the fill value 0.0
        # and not the edge value 15.2
        try:
            np_eq(prof_interp[150:320:20], expected_profile)
        except AssertionError:
            self.fail("np_eq() raised AssertionError unexpectedly!")
        #  #]
    def test_interpolate_to_lidar_range_decr(self):
        #  #[ test interpolate with decreasing z
        ''' test interpolate with decreasing z '''

        # test for decreasing z values
        # but same values as for the increasing z test above
        z_inp = np.array([15,12,9,6,3,0])*1000+375000
        profile = np.array([15.2,12.5,9.8,7.3,5.1,4.1,])

        key = '_extinction'
        prof_interp = self.test_va.interpolate_to_lidar_range(
                                                z_inp, profile, key)
        #print('prof_interp ', prof_interp[150:320:20])

        expected_profile = np.array([4.1, 4.76666667, 5.83333333, 7.3,
                                     8.96666667, 10.7, 12.5, 14.3, 0.0 ])

        # note that the final element of test_va.lid_range[150:320:20]
        # is 391000 which is out of range of z_inp
        # so this should contain the fill value 0.0
        # and not the edge value 15.2
        try:
            np_eq(prof_interp[150:320:20], expected_profile)
        except AssertionError:
            self.fail("np_eq() raised AssertionError unexpectedly!")
        #  #]
    def test_interpolate_pressure_to_lidar_range_incr(self):
        #  #[ test pressure interpolation
        ''' test interpolation with decreasing z_inp '''
        z_inp = np.array([
            382003., 382750., 383497., 384244., 384992.,
            385739., 386486., 387233., 387980., 388727.,
            389474., 390222., 390969., 391716., 392463.,
            393210., 393957., 394704., 395452., 396199.,
            396946., 397693., 398440., 399187., 399934.])

        log10_p_profile = np.array([
            3.899, 3.954, 4.010, 4.065, 4.119,
            4.174, 4.227, 4.278, 4.329, 4.379,
            4.426, 4.473, 4.518, 4.563, 4.606,
            4.649, 4.691, 4.732, 4.773, 4.813,
            4.853, 4.891, 4.928, 4.966, 5.002])

        lid_range = np.linspace(375.e3, 402.e3, 28)

        # replace lid_range by specific array contructed for this test
        self.test_va.lid_range = lid_range

        log10_p_interpolated = self.test_va.interpolate_pressure_to_lidar_range(
                                 z_inp, log10_p_profile) #, create_plot=True)

        expected_log10_p_interpolated = np.array([
            3.4013233, 3.4729171, 3.5445109, 3.6161046, 3.6876984,
            3.7592922, 3.830886 , 3.9024798, 3.9727416, 4.0470348,
            4.119589 , 4.1925181, 4.2620924, 4.3303387, 4.3961767,
            4.4590508, 4.5198675, 4.5793481, 4.63691165, 4.69336011,
            4.7482246, 4.80234404, 4.85574699, 4.90620616, 4.95648728,
            5.002, 5.002, 5.002])
        try:
            np_eq(log10_p_interpolated, expected_log10_p_interpolated)
        except AssertionError:
            self.fail("np_eq() raised AssertionError unexpectedly!")
        #  #]
    def test_interpolate_pressure_to_lidar_range_decr(self):
        #  #[ test pressure interpolation
        ''' test interpolation with decreasing z_inp '''
        z_inp = np.array([
            399934., 399187., 398440., 397693., 396946.,
            396199., 395452., 394704., 393957., 393210.,
            392463., 391716., 390969., 390222., 389474.,
            388727., 387980., 387233., 386486., 385739.,
            384992., 384244., 383497., 382750., 382003.])

        log10_p_profile = np.array([
            5.002, 4.966, 4.928, 4.891, 4.853,
            4.813, 4.773, 4.732, 4.691, 4.649,
            4.606, 4.563, 4.518, 4.473, 4.426,
            4.379, 4.329, 4.278, 4.227, 4.174,
            4.119, 4.065, 4.01,  3.954, 3.899])

        lid_range = np.linspace(375.e3, 402.e3, 28)

        # replace lid_range by specific array contructed for this test
        self.test_va.lid_range = lid_range

        log10_p_interpolated = self.test_va.interpolate_pressure_to_lidar_range(
                                 z_inp, log10_p_profile) #, create_plot=True)

        expected_log10_p_interpolated = np.array([
            3.40132329, 3.47291708, 3.54451086, 3.61610464, 3.68769842,
            3.7592922, 3.83088598, 3.90247977, 3.97274163, 4.04703481,
            4.11958902, 4.19251807, 4.26209237, 4.33033869, 4.39617671,
            4.4590508,  4.51986747, 4.57934806, 4.63691165, 4.69336011,
            4.7482246,  4.80234404, 4.85574699, 4.90620616, 4.95648728,
            5.002, 5.002, 5.002])

        try:
            np_eq(log10_p_interpolated, expected_log10_p_interpolated)
        except AssertionError:
            self.fail("np_eq() raised AssertionError unexpectedly!")
        #  #]

if __name__ == '__main__':
    unittest.main()

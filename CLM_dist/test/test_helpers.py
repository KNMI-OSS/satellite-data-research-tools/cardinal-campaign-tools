#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 26-May-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
'''
This module implements testing of the helpers module
'''
#
#  #]
#  #[ imported modules
import os
import sys
import stat
import shutil
import unittest
from io import StringIO

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.helpers import (
    run_shell_command, textcolor,
    ensure_rx, ensure_r, fix_permissions)
# pylint: enable=wrong-import-position, import-error

#  #]

class HelpersTesting(unittest.TestCase):
    ''' collected tests for the helpers odule '''
    def test_run_shell_command_1(self):
        #  #[ test a command that succeeds
        ''' this test executes an ls command that returns
        a proper result '''
        cmd = 'ls Makefile'
        (stdout, stderr, exit_status) = run_shell_command(cmd)
        expected_stdout = ['Makefile',]
        expected_stderr = []
        expected_exit_status = 0

        self.assertEqual(stdout, expected_stdout)
        self.assertEqual(stderr, expected_stderr)
        self.assertEqual(exit_status, expected_exit_status)
        #  #]
    def test_run_shell_command_2(self):
        #  #[ test a command that fails
        ''' this test executes an ls command that returns
        with an error '''
        cmd = 'ls DoesNotExist.dummy'
        (stdout, stderr, exit_status) = run_shell_command(cmd)

        expected_stdout = []
        expected_stderr = ["ls: cannot access 'DoesNotExist.dummy': No such file or directory",]
        expected_exit_status = 2

        self.assertEqual(stdout, expected_stdout)
        self.assertEqual(stderr, expected_stderr)
        self.assertEqual(exit_status, expected_exit_status)
        #  #]
    def test_run_shell_command_3(self):
        #  #[ test a command that succeeds and prints a debug line
        ''' this test executes an ls command that returns
        a proper result and prints the command to be executed as well '''
        cmd = 'ls Makefile'

        stdout_str_io = StringIO()
        saved_stdout = sys.stdout
        sys.stdout = stdout_str_io

        (stdout, stderr, exit_status) = run_shell_command(cmd, verbose=True)

        captured_text = stdout_str_io.getvalue().strip()
        sys.stdout = saved_stdout

        expected_stdout = ['Makefile',]
        expected_stderr = []
        expected_exit_status = 0
        expected_captured_text = 'Executing command:  ls Makefile'

        self.assertEqual(stdout, expected_stdout)
        self.assertEqual(stderr, expected_stderr)
        self.assertEqual(exit_status, expected_exit_status)
        self.assertEqual(captured_text, expected_captured_text)
        #  #]
    def test_textcolor(self):
        #  #[ test the textcolor function
        ''' test the correct functioning of the textcolor() function '''
        result = textcolor('red', 'some red text')
        expected_result = '\033[31msome red text\033[0m'
        self.assertEqual(result, expected_result)
        #  #]
    def test_ensure_rx(self):
        #  #[ test setting 755 permission
        ''' test the ensure_rx function '''

        # create a test file
        testfile = './test_ensure_rx.dummy'
        new_mode = '444'
        cmd = f'touch {testfile}; chmod {new_mode} {testfile}'
        os.system(cmd)

        # execute the ensure_rx function
        ensure_rx(testfile)

        # test the result
        stt = os.stat(testfile)
        current_mode = stat.S_IMODE(stt.st_mode)
        expected_mode = int('755', 8)

        self.assertEqual(current_mode, expected_mode)

        os.remove(testfile)
        #  #]
    def test_ensure_r(self):
        #  #[ test setting 644 permission
        ''' test the ensure_r function '''

        # create a test file
        testfile = './test_ensure_r.dummy'
        new_mode = '777'
        cmd = f'touch {testfile}; chmod {new_mode} {testfile}'
        os.system(cmd)

        # execute the ensure_rx function
        ensure_r(testfile)

        # test the result
        stt = os.stat(testfile)
        current_mode = stat.S_IMODE(stt.st_mode)
        expected_mode = int('644', 8)

        self.assertEqual(current_mode, expected_mode)

        os.remove(testfile)
        #  #]
    def test_fix_permissions(self):
        #  #[ test setting permissions on a dirtree
        ''' test the fix_permissions function '''

        # create a test dir
        testdir = './test_dummy_dir'
        new_mode = '700'
        cmd = f'mkdir -p {testdir}; chmod {new_mode} {testdir}'
        os.system(cmd)

        # create a test file
        testfile = './test_dummy_dir/test_fix_permission.dummy'
        new_mode = '400'
        cmd = f'touch {testfile}; chmod {new_mode} {testfile}'
        os.system(cmd)

        # execute the fix_permissions function
        fix_permissions(testdir)

        # test the result
        stt = os.stat(testdir)
        current_mode = stat.S_IMODE(stt.st_mode)
        expected_mode = int('755', 8)
        self.assertEqual(current_mode, expected_mode)

        stt = os.stat(testfile)
        current_mode = stat.S_IMODE(stt.st_mode)
        expected_mode = int('644', 8)
        self.assertEqual(current_mode, expected_mode)

        shutil.rmtree(testdir)
        #  #]

if __name__ == '__main__':
    unittest.main()

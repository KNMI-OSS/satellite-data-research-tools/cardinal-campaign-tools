#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 17-Apr-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module implements testing of the multiscatter ctypes
interface as is used for this software.
'''
#  #]
#  #[ imported modules
import os
import sys
import ctypes as c
from ctypes import Structure
import unittest
import numpy as np

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.multiscatter_interface import (
    MS_WIDE_ANGLE_TDTS_FORWARD_LOBE, MS_SMALL_ANGLE_PVC_FAST,
    MS_TOP_HAT)
# pylint: enable=wrong-import-position, import-error

#  #]
#  #[ load shared object file for testing
lib_so_path = os.path.join('../src/multiscatter_ctypes_interface_test/lib/',
                           'libmsiftest.so')
if not os.path.exists(lib_so_path):
    lib_so_path = os.path.join('src/multiscatter_ctypes_interface_test/lib/',
        'libmsiftest.so')

libMStest = c.CDLL(lib_so_path)
#print('loaded lib_so_path: ', lib_so_path)
#  #]

LOG_FILENAME = b"ctypes_interface_test_log_file.log"

def define_test_inputs_for_config(size_n):
    #  #[ input definition
    ''' define config test inputs '''

    # define the config structure
    # pylint: disable=too-few-public-methods
    class MsConfig(Structure):
        ''' defines a struct as needed by the ctypes interface '''
        _fields_ = [
            ("small_angle_algorithm",c.c_int), # ms_small_angle_algorithm
            ("wide_angle_algorithm", c.c_int), # ms_wide_angle_algorithm
            ("options",c.c_int),
            ("max_scattering_order",c.c_int),
            ("max_theta",c.c_double),
            ("first_wide_angle_gate",c.c_int),
            ("coherent_backscatter_enhancement",c.c_double),
            ("small_angle_lag",c.POINTER(c.c_double)),
            ("total_src",c.c_double),
            ("total_reflected",c.c_double),
            ("ss_multiplier",c.c_double)]
    # pylint: enable=too-few-public-methods

    # fill the config structure with data
    small_angle_lag = np.zeros(size_n, dtype=c.c_double)
    small_angle_lag[3] = 333.
    small_angle_lag[6] = 444.
    small_angle_lag[9] = 555.

    c_double_p = c.POINTER(c.c_double)
    small_angle_lag_p = small_angle_lag.ctypes.data_as(c_double_p)

    ms_config_default = MsConfig(
        MS_SMALL_ANGLE_PVC_FAST,      # small angle algo
        MS_WIDE_ANGLE_TDTS_FORWARD_LOBE, # wide angle algo
        c.c_int(1),        # options
        c.c_int(4),        # max scat. order
        c.c_double(0.1),   # max theta
        c.c_int(0),        # first wide angle gate
        c.c_double(1.0),   # coherent backsc. enhancement
        small_angle_lag_p, # small angle lag
        c.c_double(-1.0),  # total src
        c.c_double(-1.0),  # total reflected
        c.c_double(1.0))   # ss multiplier

    return ms_config_default
    #  #]

def define_test_inputs_for_instrument(nfov):
    #  #[ input definition
    ''' defines instrument test inputs '''

    # define the instrument structure
    # pylint: disable=too-few-public-methods
    class MsInstrument(Structure):
        ''' defines the instrument struct for ctypes'''
        _fields_ = [
            ("receiver_type",c.c_int),
            ("altitude",c.c_double),
            ("wavelength",c.c_double),
            ("rho_transmitter",c.c_double),
            ("rho_receiver",c.POINTER(c.c_double)),
            ("nfov",c.c_int)]
    # pylint: enable=too-few-public-methods

    # fill the instrument structure with data
    rho_receiver = np.zeros(nfov, dtype=c.c_double)
    rho_receiver[0] = 1.25
    rho_receiver[1] = 2.36
    rho_receiver[2] = 3.47

    c_double_p = c.POINTER(c.c_double)
    rho_receiver_p = rho_receiver.ctypes.data_as(c_double_p)

    ms_instrument_default = MsInstrument(
        MS_TOP_HAT,        # receiver_type
        c.c_double(400.1), # altitude
        c.c_double(354.8), # wavelength
        c.c_double(1.234), # rho_transmitter
        rho_receiver_p,  # rho_receiver
        c.c_int(nfov))   # nfov

    return ms_instrument_default
    #  #]

def define_test_inputs_for_surface():
    #  #[ input definition
    ''' defines surface test inputs '''

    # define the surface structure
    # pylint: disable=too-few-public-methods
    class MsSurface(Structure):
        ''' define a struct as needed for the ctypes interface '''
        _fields_ = [
            ("sigma0",c.c_double),
            ("diffuse_albedo",c.c_double),
            ("direct_to_diffuse_albedo",c.c_double),
            ("diffuse_to_direct_backscatter",c.c_double),
            ("range",c.c_double)]
    # pylint: enable=too-few-public-methods

    # fill the surface structure with data
    ms_surface_default = MsSurface(
        c.c_double(2.3), # sigma0
        c.c_double(3.4), # diffuse_albedo
        c.c_double(4.5), # direct_to_diffuse_albedo
        c.c_double(5.6), # diffuse_to_direct_backscatter
        c.c_double(6.7)) # range

    return ms_surface_default
    #  #]

# pylint: disable=too-many-locals
def define_10_test_real_input_arrays(size_n):
    #  #[ input definition
    ''' defines inputs for real arrays '''
    test_range                 = np.zeros(size_n, dtype=c.c_double)
    test_radius                = np.zeros(size_n, dtype=c.c_double)
    test_ext                   = np.zeros(size_n, dtype=c.c_double)
    test_ssa                   = np.zeros(size_n, dtype=c.c_double)
    test_g                     = np.zeros(size_n, dtype=c.c_double)
    test_ext_bscat_ratio       = np.zeros(size_n, dtype=c.c_double)
    test_ext_air               = np.zeros(size_n, dtype=c.c_double)
    test_ssa_air               = np.zeros(size_n, dtype=c.c_double)
    test_droplet_fraction      = np.zeros(size_n, dtype=c.c_double)
    test_pristine_ice_fraction = np.zeros(size_n, dtype=c.c_double)

    test_range[:3]                 = [101., 102., 103.]
    test_radius[:3]                = [201., 202., 203.]
    test_ext[:3]                   = [301., 302., 303.]
    test_ssa[:3]                   = [401., 402., 403.]
    test_g[:3]                     = [501., 502., 503.]
    test_ext_bscat_ratio[:3]       = [601., 602., 603.]
    test_ext_air[:3]               = [701., 702., 703.]
    test_ssa_air[:3]               = [801., 802., 803.]
    test_droplet_fraction[:3]      = [901., 902., 903.]
    test_pristine_ice_fraction[:3] = [911., 912., 913.]

    c_double_p = c.POINTER(c.c_double)

    test_range_p  = test_range.ctypes.data_as(c_double_p)
    test_radius_p = test_radius.ctypes.data_as(c_double_p)
    test_ext_p    = test_ext.ctypes.data_as(c_double_p)
    test_ssa_p    = test_ssa.ctypes.data_as(c_double_p)
    test_g_p      = test_g.ctypes.data_as(c_double_p)
    test_ext_bscat_ratio_p  = (test_ext_bscat_ratio.ctypes.
        data_as(c_double_p))
    test_ext_air_p          = test_ext_air.ctypes.data_as(c_double_p)
    test_ssa_air_p          = test_ssa_air.ctypes.data_as(c_double_p)
    test_droplet_fraction_p = (test_droplet_fraction.
        ctypes.data_as(c_double_p))
    test_pristine_ice_fraction_p = (test_pristine_ice_fraction.
        ctypes.data_as(c_double_p))

    return (test_range_p, test_radius_p, test_ext_p, test_ssa_p, test_g_p,
            test_ext_bscat_ratio_p, test_ext_air_p, test_ssa_air_p,
            test_droplet_fraction_p, test_pristine_ice_fraction_p)
    #  #]
# pylint: enable=too-many-locals

class TestCtypesInterface(unittest.TestCase):
    ''' a collection of tests to exercize the ctype calls
    needed to be able to use the multiscatter library code. '''
    def test_2_ints(self):
        #  #[ test case for 2 ints
        '''
        test the call to:
        int test_2_ints(int n, int m);
        '''
        size_n = c.c_int(34)
        size_m = c.c_int(75)

        status = libMStest.open_log_file(LOG_FILENAME)
        self.assertEqual(status, 0)

        result = libMStest.test_2_ints(size_n, size_m)

        status = libMStest.close_log_file()
        self.assertEqual(status, 0)

        expected_result = 109
        self.assertEqual(result, expected_result)

        with open(LOG_FILENAME, 'rt', encoding='ascii') as fd_in:
            text = fd_in.read()

        expected_text = "test_2_ints: n = 34\ntest_2_ints: m = 75\n"
        self.assertEqual(text, expected_text)

        os.remove(LOG_FILENAME)
        #  #]
    def test_2_reals(self):
        #  #[ test case for 2 reals
        '''
        test the call to:
        int test_2_reals(ms_real x, ms_real y);
        '''
        x_coord = c.c_double(765.432)
        y_coord = c.c_double(3.21e2)

        # note: this forces the return type of the c-function to be c_double
        #       it seems this cannot be done in the c-code itself ....
        test_2_reals_func = libMStest.test_2_reals
        test_2_reals_func.restype = c.c_double

        status = libMStest.open_log_file(LOG_FILENAME)
        self.assertEqual(status, 0)

        result = test_2_reals_func(x_coord, y_coord)

        status = libMStest.close_log_file()
        self.assertEqual(status, 0)

        expected_result = 1086.432
        self.assertAlmostEqual(result, expected_result)

        with open(LOG_FILENAME, 'rt', encoding='ascii') as fd_in:
            text = fd_in.read()

        expected_text = ('test_2_reals: x = 765.432000\n'+
                         'test_2_reals: y = 321.000000\n')
        self.assertEqual(text, expected_text)

        os.remove(LOG_FILENAME)
        #  #]
    # pylint: disable=too-many-locals
    def test_10_ms_real_pointers(self):
        #  #[ test case for 10 real arrays
        '''
        test the call to:
        int test_10_ms_real_pointers(
        const ms_real *range,
        const ms_real *radius,
        const ms_real *ext,
        const ms_real *ssa,
        const ms_real *g,
        const ms_real *ext_bscat_ratio,
        const ms_real *ext_air,
        const ms_real *ssa_air,
        const ms_real *droplet_fraction,
        const ms_real *pristine_ice_fraction);
        '''

        size_n = 3
        (test_range_p, test_radius_p,
         test_ext_p, test_ssa_p, test_g_p,
         test_ext_bscat_ratio_p,
         test_ext_air_p, test_ssa_air_p,
         test_droplet_fraction_p,
         test_pristine_ice_fraction_p
         ) = define_10_test_real_input_arrays(size_n)

        # note: this forces the return type of the c-function to be c_double
        #       it seems this cannot be done in the c-code itself ....
        test_10_ms_real_pointers_func = libMStest.test_10_ms_real_pointers
        test_10_ms_real_pointers_func.restype = c.c_double

        status = libMStest.open_log_file(LOG_FILENAME)
        self.assertEqual(status, 0)

        result = test_10_ms_real_pointers_func(
            c.c_int(size_n),
            test_range_p, test_radius_p, test_ext_p, test_ssa_p, test_g_p,
            test_ext_bscat_ratio_p, test_ext_air_p, test_ssa_air_p,
            test_droplet_fraction_p, test_pristine_ice_fraction_p)

        status = libMStest.close_log_file()
        self.assertEqual(status, 0)

        expected_result = 16290.0
        self.assertAlmostEqual(result, expected_result)

        with open(LOG_FILENAME, 'rt', encoding='ascii') as fd_in:
            text = fd_in.read()

        # set this switch if things dont match
        #self.maxDiff = None
        expected_text = """=============
=== i = 0 ===
=============
test_10_ms_reals: range = 101.000000
test_10_ms_reals: radius = 201.000000
test_10_ms_reals: ext = 301.000000
test_10_ms_reals: ssa = 401.000000
test_10_ms_reals: g = 501.000000
test_10_ms_reals: ext_bscat_ratio = 601.000000
test_10_ms_reals: ext_air = 701.000000
test_10_ms_reals: ssa_air = 801.000000
test_10_ms_reals: droplet_fraction = 901.000000
test_10_ms_reals: pristine_ice_fraction = 911.000000
=============
=== i = 1 ===
=============
test_10_ms_reals: range = 102.000000
test_10_ms_reals: radius = 202.000000
test_10_ms_reals: ext = 302.000000
test_10_ms_reals: ssa = 402.000000
test_10_ms_reals: g = 502.000000
test_10_ms_reals: ext_bscat_ratio = 602.000000
test_10_ms_reals: ext_air = 702.000000
test_10_ms_reals: ssa_air = 802.000000
test_10_ms_reals: droplet_fraction = 902.000000
test_10_ms_reals: pristine_ice_fraction = 912.000000
=============
=== i = 2 ===
=============
test_10_ms_reals: range = 103.000000
test_10_ms_reals: radius = 203.000000
test_10_ms_reals: ext = 303.000000
test_10_ms_reals: ssa = 403.000000
test_10_ms_reals: g = 503.000000
test_10_ms_reals: ext_bscat_ratio = 603.000000
test_10_ms_reals: ext_air = 703.000000
test_10_ms_reals: ssa_air = 803.000000
test_10_ms_reals: droplet_fraction = 903.000000
test_10_ms_reals: pristine_ice_fraction = 913.000000
rsum = 16290.000000
"""
        self.assertEqual(text, expected_text)

        os.remove(LOG_FILENAME)
        #  #]
    # pylint: enable=too-many-locals
    def test_ms_config(self):
        #  #[ test case for the config struct interface
        '''
        test the call to:
        int test_ms_config(int n, ms_config *config);
        '''

        size_n = 10
        ms_config_default = define_test_inputs_for_config(size_n)

        # note: this forces the return type of the c-function to be c_double
        #       it seems this cannot be done in the c-code itself ....
        test_ms_config_func = libMStest.test_ms_config
        test_ms_config_func.restype = c.c_double

        status = libMStest.open_log_file(LOG_FILENAME)
        self.assertEqual(status, 0)

        result = test_ms_config_func(c.c_int(size_n),
                                     c.byref(ms_config_default))

        status = libMStest.close_log_file()
        self.assertEqual(status, 0)

        expected_result = 1341.1
        self.assertAlmostEqual(result, expected_result)

        with open(LOG_FILENAME, 'rt', encoding='ascii') as fd_in:
            text = fd_in.read()

        # set this switch if things dont match
        #self.maxDiff = None
        expected_text = """test_ms_config: config->small_angle_algorithm = 3
test_ms_config: config->wide_angle_algorithm = 1
test_ms_config: config->options = 1
test_ms_config: config->max_scattering_order = 4
test_ms_config: config->max_theta = 0.100000
test_ms_config: config->first_wide_angle_gate = 0
test_ms_config: config->coherent_backscatter_enhancement = 1.000000
array size n = 10
test_ms_config: config->small_angle_lag[ 0] = 0.000000
test_ms_config: config->small_angle_lag[ 1] = 0.000000
test_ms_config: config->small_angle_lag[ 2] = 0.000000
test_ms_config: config->small_angle_lag[ 3] = 333.000000
test_ms_config: config->small_angle_lag[ 4] = 0.000000
test_ms_config: config->small_angle_lag[ 5] = 0.000000
test_ms_config: config->small_angle_lag[ 6] = 444.000000
test_ms_config: config->small_angle_lag[ 7] = 0.000000
test_ms_config: config->small_angle_lag[ 8] = 0.000000
test_ms_config: config->small_angle_lag[ 9] = 555.000000
test_ms_config: config->total_src = -1.000000
test_ms_config: config->total_reflected = -1.000000
test_ms_config: config->ss_multiplier = 1.000000
rsum = 1341.100000
"""

        self.assertEqual(text, expected_text)

        os.remove(LOG_FILENAME)
        #  #]
    def test_ms_instrument(self):
        #  #[ test case for the instrument struct interface
        '''
        test the call to:
        int test_ms_instrument(ms_instrument *instrument);
        '''

        nfov = 3
        ms_instrument_default = define_test_inputs_for_instrument(nfov)

        # note: this forces the return type of the c-function to be c_double
        #       it seems this cannot be done in the c-code itself ....
        test_ms_instrument_func = libMStest.test_ms_instrument
        test_ms_instrument_func.restype = c.c_double

        status = libMStest.open_log_file(LOG_FILENAME)
        self.assertEqual(status, 0)

        #status = libMStest.open_log_file("STDOUT") # for debugging
        result = test_ms_instrument_func(c.c_int(nfov), ms_instrument_default)

        status = libMStest.close_log_file()
        self.assertEqual(status, 0)

        expected_result = 766.214
        self.assertAlmostEqual(result, expected_result)

        with open(LOG_FILENAME, 'rt', encoding='ascii') as fd_in:
            text = fd_in.read()

        # set this switch if things dont match
        #self.maxDiff = None
        expected_text = """test_ms_instrument: instrument.receiver_type = 0
test_ms_instrument: instrument.altitude = 400.100000
test_ms_instrument: instrument.wavelength = 354.800000
test_ms_instrument: instrument.rho_transmitter = 1.234000
test_ms_instrument: instrument.rho_receiver[ 0] = 1.250000
test_ms_instrument: instrument.rho_receiver[ 1] = 2.360000
test_ms_instrument: instrument.rho_receiver[ 2] = 3.470000
test_ms_instrument: instrument.nfov = 3
rsum = 766.214000
"""

        self.assertEqual(text, expected_text)

        os.remove(LOG_FILENAME)
        #  #]
    def test_ms_surface(self):
        #  #[ test case for the surface struct interface
        '''
        test the call to:
        int test_ms_surface(ms_surface *surface);
        '''

        ms_surface_default = define_test_inputs_for_surface()

        # note: this forces the return type of the c-function to be c_double
        #       it seems this cannot be done in the c-code itself ....
        test_ms_surface_func = libMStest.test_ms_surface
        test_ms_surface_func.restype = c.c_double

        status = libMStest.open_log_file(LOG_FILENAME)
        self.assertEqual(status, 0)

        result = test_ms_surface_func(ms_surface_default)

        status = libMStest.close_log_file()
        self.assertEqual(status, 0)

        expected_result = 22.5
        self.assertAlmostEqual(result, expected_result)

        with open(LOG_FILENAME, 'rt', encoding='ascii') as fd_in:
            text = fd_in.read()

        expected_text = """test_ms_surface: surface.sigma0 = 2.300000
test_ms_surface: surface.diffuse_albedo = 3.400000
test_ms_surface: surface.direct_to_diffuse_albedo = 4.500000
test_ms_surface: surface.diffuse_to_direct_backscatter = 5.600000
test_ms_surface: surface.range = 6.700000
rsum = 22.500000
"""

        self.assertEqual(text, expected_text)

        os.remove(LOG_FILENAME)
        #  #]
    # pylint: disable=too-many-locals
    def test_output_real_arrays(self):
        #  #[ test case for output of real arrays
        '''
        test the call to:
        int test_output_real_arrays(ms_real *bscat_out,
                                    ms_real *bscat_air_out, int m);
        '''
        size_m = 8
        c_double_p = c.POINTER(c.c_double)

        bscat_out       = np.zeros(size_m, dtype=c.c_double)
        bscat_air_out   = np.zeros(size_m, dtype=c.c_double)
        bscat_out_p     = bscat_out.ctypes.data_as(c_double_p)
        bscat_air_out_p = bscat_air_out.ctypes.data_as(c_double_p)

        # note: this forces the return type of the c-function to be c_double
        #       it seems this cannot be done in the c-code itself ....
        test_output_real_arrays_func = libMStest.test_output_real_arrays
        test_output_real_arrays_func.restype = c.c_double

        status = libMStest.open_log_file(LOG_FILENAME)
        self.assertEqual(status, 0)

        result = test_output_real_arrays_func(
                           bscat_out_p, bscat_air_out_p, c.c_int(size_m))

        status = libMStest.close_log_file()
        self.assertEqual(status, 0)

        expected_result = 71.6632
        self.assertAlmostEqual(result, expected_result)

        expected_bscat_out = np.array(
            [0.78, 3.08, 5.38, 7.68,
             9.98, 12.28, 14.58, 16.88],
            dtype=c.c_double)
        expected_bscat_air_out = np.array(
            [0.0089, 0.0429, 0.0769, 0.1109,
             0.1449, 0.1789, 0.2129, 0.2469],
            dtype=c.c_double)
        for i in range(size_m):
            self.assertAlmostEqual(bscat_out[i], expected_bscat_out[i])
            self.assertAlmostEqual(bscat_air_out[i], expected_bscat_air_out[i])

        with open(LOG_FILENAME, 'rt', encoding='ascii') as fd_in:
            text = fd_in.read()

        # set this switch if things dont match
        #self.maxDiff = None
        expected_text = """test_output_real_arrays: 0.780000
test_output_real_arrays: 0.008900
rsum = 0.788900
test_output_real_arrays: 3.080000
test_output_real_arrays: 0.042900
rsum = 3.911800
test_output_real_arrays: 5.380000
test_output_real_arrays: 0.076900
rsum = 9.368700
test_output_real_arrays: 7.680000
test_output_real_arrays: 0.110900
rsum = 17.159600
test_output_real_arrays: 9.980000
test_output_real_arrays: 0.144900
rsum = 27.284500
test_output_real_arrays: 12.280000
test_output_real_arrays: 0.178900
rsum = 39.743400
test_output_real_arrays: 14.580000
test_output_real_arrays: 0.212900
rsum = 54.536300
test_output_real_arrays: 16.880000
test_output_real_arrays: 0.246900
rsum = 71.663200
"""
        self.assertEqual(text, expected_text)

        os.remove(LOG_FILENAME)
        #  #]
    # pylint: enable=too-many-locals
    # pylint: disable=too-many-locals
    def test_multiscatter(self):
        #  #[ test the full interface to the multiscatter function
        '''
        test the call to:
        int test_multiscatter(
        /* Input data */
        int n,                    /* Number of input gates */
        int m,                    /* Number of output gates (>= n) */
        ms_config *config,        /* Configuration information */
        ms_instrument instrument, /* Structure containing instrument variables */
        ms_surface surface,       /* Surface scattering variables */
        const ms_real *range,     /* Height of each range gate, metres */
        const ms_real *radius,    /* Cloud/aerosol equivalent radius, microns */
        const ms_real *ext,       /* Total extinction coefficient, m-1 */
        const ms_real *ssa,       /* Total single-scatter albedo */
        const ms_real *g,         /* Total asymmetry factor */
        const ms_real *ext_bscat_ratio,/* Cloud/aerosol ext./bscat. ratio, sr */
        const ms_real *ext_air,   /* Air ext. coefft., m-1 (NULL for vacuum) */
        const ms_real *ssa_air,   /* Air single-scatter albedo */
        const ms_real *droplet_fraction,/* Fraction of extinction from droplets */
        const ms_real *pristine_ice_fraction,/* Fraction of ext from pristine ice */
        /* Output data */
        ms_real *bscat_out,       /* Measured backscatter, m-1 sr-1 */
        ms_real *bscat_air_out);   /* Measured backscatter of air, m-1 sr-1 */
        '''

        num_s = 10 # dim for small_angle_lag
        ms_config_default = define_test_inputs_for_config(num_s)

        nfov = 3
        ms_instrument_default = define_test_inputs_for_instrument(nfov)

        ms_surface_default = define_test_inputs_for_surface()

        size_n = 4
        (test_range_p, test_radius_p,
         test_ext_p, test_ssa_p, test_g_p,
         test_ext_bscat_ratio_p,
         test_ext_air_p, test_ssa_air_p,
         test_droplet_fraction_p,
         test_pristine_ice_fraction_p
         ) = define_10_test_real_input_arrays(size_n)

        # define output arrays
        size_m = 12
        c_double_p = c.POINTER(c.c_double)
        bscat_out       = np.zeros(size_m, dtype=c.c_double)
        bscat_air_out   = np.zeros(size_m, dtype=c.c_double)
        bscat_out_p     = bscat_out.ctypes.data_as(c_double_p)
        bscat_air_out_p = bscat_air_out.ctypes.data_as(c_double_p)

        status = libMStest.open_log_file(LOG_FILENAME)
        self.assertEqual(status, 0)

        ms_status =  libMStest.test_multiscatter(
            # Input data
            c.c_int(size_n),
            c.c_int(size_m),
            c.byref(ms_config_default),
            ms_instrument_default,
            ms_surface_default,
            test_range_p, test_radius_p,
            test_ext_p, test_ssa_p, test_g_p, test_ext_bscat_ratio_p,
            test_ext_air_p, test_ssa_air_p, test_droplet_fraction_p,
            test_pristine_ice_fraction_p,
            # Output data
            bscat_out_p, bscat_air_out_p)
        status = libMStest.close_log_file()

        self.assertEqual(ms_status, 0)

        expected_bscat_out = np.array(
            [0.0, 1.23, 2.46, 3.69,
             4.92, 6.15, 7.38, 8.61,
             9.84,11.07,12.30,13.53],
            dtype=c.c_double)
        expected_bscat_air_out = np.array(
            [0.0, 4.56, 9.12, 13.68,
             18.24, 22.80, 27.36, 31.92,
             36.48,41.04,45.60,50.16],
            dtype=c.c_double)
        for i in range(size_m):
            self.assertAlmostEqual(bscat_out[i], expected_bscat_out[i])
            self.assertAlmostEqual(bscat_air_out[i], expected_bscat_air_out[i])

        with open(LOG_FILENAME, 'rt', encoding ='ascii') as fd_in:
            text = fd_in.read()

        # set this switch if things dont match
        # self.maxDiff = None
        expected_text = """after adding config: rsum = 358.100000
after adding instrument: rsum = 1124.314000
after adding surface: rsum = 1146.814000
after adding the 10 arrays: rsum = 17436.814000
"""
        self.assertEqual(text, expected_text)

        os.remove(LOG_FILENAME)
        #  #]
    # pylint: enable=too-many-locals

if __name__ == '__main__':
    unittest.main()

#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 24-May-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
'''
This module implements testing of the get_cloud_extinction() function
as implemented in the calculated_extinction.py library module.
'''
#
#  #]

import os
import sys
import unittest
import numpy as np

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.calc_extinction import (
    get_cloud_extinction, get_ice_ext, get_water_ext)
# pylint: enable=wrong-import-position, import-error

class TestCalcExtinction(unittest.TestCase):
    '''
    a test based on lindenberg inputs and outputs extracted
    from the original get_cloud_extinction_for_vns_channels()
    function by Nils and Sebastian.
    '''
    def test_calc_ext(self):
        ''' test the different extinction calculations '''

        np_eq = np.testing.assert_almost_equal

        # is not needed after all
        #z = np.array(
        #    [3700., 3457., 3223., 2999., 2786.,
        #     2583., 2390., 2208., 2036., 1875.,
        #     1723., 1581., 1449., 1325., 1210.,
        #     1103.]) # this array needs to be one element larger

        iwc = np.array(
            [0.0, 0.0, 0.0, 0.0, 0.0,
             3.02e-05, 2.81e-05, 3.15e-05, 1.79e-05, 4.25e-06,
             0.0, 0.0, 0.0, 0.0, 0.0])

        lwc = np.array(
            [0., 0., 0., 0., 0.,
             0., 0., 0., 0., 0.,
             0., 0., 0., 0., 0.])

        expected_ext_ice = np.array(
            [0.0, 0.0, 0.0, 0.0, 0.0,
             8.236e-4, 7.663e-4, 8.591e-4, 4.882e-4, 1.159e-4,
             0.0, 0.0, 0.0, 0.0, 0.0])

        expected_ext_water = np.array(
            [0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0])

        ext_ice1, ext_water1 = get_cloud_extinction(iwc, lwc) #, z)

        try:
            np_eq(ext_ice1, expected_ext_ice, decimal=6)
            np_eq(ext_water1, expected_ext_water, decimal=6)
        except AssertionError:
            self.fail("np_eq() raised AssertionError unexpectedly!")

        ext_ice2 = get_ice_ext(iwc)
        try:
            np_eq(ext_ice2, expected_ext_ice, decimal=6)
        except AssertionError:
            self.fail("np_eq() raised AssertionError unexpectedly!")

        ext_water2 = get_water_ext(lwc)
        try:
            np_eq(ext_water2, expected_ext_water, decimal=6)
        except AssertionError:
            self.fail("np_eq() raised AssertionError unexpectedly!")

        #self.assertAlmostEqual(...)

if __name__ == '__main__':
    unittest.main()

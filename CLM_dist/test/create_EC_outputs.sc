#!/bin/sh

\rm -rf output_atlid

python3 ../scripts/create_earth_care_files.py \
        --lidar_count=1 \
        --ifile output/Test_ACTIVATE_data_RT.nc \
        --output_dir output/EC_files_ACTIVATE/ \
        --index=23 \
        --overpass_time='14:00:00' \
        --reproducible_files_day 20231005 \
        --templates_folder='../templates' \
        --instrument_module random \
        --instr_data_folder=./instrument_data

python3 ../scripts/create_earth_care_files.py \
        --lidar_count=1 \
        --ifile output/Test_CLOUDNET_data_RT.nc \
        --output_dir output/EC_files_CLOUDNET/ \
        --index=11 \
        --overpass_time='14:00:00' \
        --reproducible_files_day 20231006 \
        --templates_folder='../templates' \
        --instrument_module random \
        --instr_data_folder=./instrument_data

python3 ../scripts/create_earth_care_files.py \
        --lidar_count=1 \
        --ifile output/Test_EVE_data_RT.nc \
        --output_dir output/EC_files_EVE/ \
        --index=3 \
        --overpass_time='14:00:00' \
        --reproducible_files_day 20231007 \
        --templates_folder='../templates' \
        --instrument_module random \
        --instr_data_folder=./instrument_data

# this one still fails, currently with the error
#   KeyError: "Unable to open object (object 'Time' doesn't exist)
# but the problem is more general:
# we need to first define a trajectory through this 3D field
# defining a curtain to be exported to EC format,
# only then it makers sense to define some time to fix this.
#
#python3 ../scripts/create_earth_care_files.py \
#       --lidar_count=1 \
#       --ifile output/Test_GEM_data_RT.nc \
#       --output_dir output/EC_files_GEM/ \
#       --index=3 \
#       --overpass_time='14:00:00' \
#       --reproducible_files_day 20231008 \
#       --templates_folder='../templates' \
#       --instrument_module random \
#       --instr_data_folder=./instrument_data


echo "=============================="

# compare the result with the reference
# the -d option helps in case of noise: h5diff -d 1.e-5

# ACTIVATE case
h5diff output/EC_files_ACTIVATE/ECA_EXAA_ATL_NOM_1B_*_21023D/*.h5 \
          ref/EC_files_ACTIVATE/ECA_EXAA_ATL_NOM_1B_*_21023D/*.h5
h5diff output/EC_files_ACTIVATE/ECA_EXAA_AUX_JSG_1D_*_21023D/*.h5 \
          ref/EC_files_ACTIVATE/ECA_EXAA_AUX_JSG_1D_*_21023D/*.h5
h5diff output/EC_files_ACTIVATE/ECA_EXAA_AUX_MET_1D_*_21023D/*.h5 \
          ref/EC_files_ACTIVATE/ECA_EXAA_AUX_MET_1D_*_21023D/*.h5

# CLOUDNET case
h5diff output/EC_files_CLOUDNET/ECA_EXAA_ATL_NOM_1B_*_21011D/*.h5 \
          ref/EC_files_CLOUDNET/ECA_EXAA_ATL_NOM_1B_*_21011D/*.h5
h5diff output/EC_files_CLOUDNET/ECA_EXAA_AUX_JSG_1D_*_21011D/*.h5 \
          ref/EC_files_CLOUDNET/ECA_EXAA_AUX_JSG_1D_*_21011D/*.h5
h5diff output/EC_files_CLOUDNET/ECA_EXAA_AUX_MET_1D_*_21011D/*.h5 \
          ref/EC_files_CLOUDNET/ECA_EXAA_AUX_MET_1D_*_21011D/*.h5

# EVE case
h5diff output/EC_files_EVE/ECA_EXAA_ATL_NOM_1B_*_21003A/*.h5 \
          ref/EC_files_EVE/ECA_EXAA_ATL_NOM_1B_*_21003A/*.h5
h5diff output/EC_files_EVE/ECA_EXAA_AUX_JSG_1D_*_21003A/*.h5 \
          ref/EC_files_EVE/ECA_EXAA_AUX_JSG_1D_*_21003A/*.h5
h5diff output/EC_files_EVE/ECA_EXAA_AUX_MET_1D_*_21003A/*.h5 \
          ref/EC_files_EVE/ECA_EXAA_AUX_MET_1D_*_21003A/*.h5

diff output/EC_files_EVE/ECA_EXAA_ATL_NOM_1B_*_21003A/*.HDR \
        ref/EC_files_EVE/ECA_EXAA_ATL_NOM_1B_*_21003A/*.HDR
diff output/EC_files_EVE/ECA_EXAA_AUX_JSG_1D_*_21003A/*.HDR \
        ref/EC_files_EVE/ECA_EXAA_AUX_JSG_1D_*_21003A/*.HDR
diff output/EC_files_EVE/ECA_EXAA_AUX_MET_1D_*_21003A/*.HDR \
        ref/EC_files_EVE/ECA_EXAA_AUX_MET_1D_*_21003A/*.HDR

echo "=============================="

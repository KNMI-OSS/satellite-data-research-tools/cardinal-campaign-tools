#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 11-Apr-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
'''
This module implements comparing of the generated reference plots
against the generated plots.
'''
#
#  #]
#  #[ imported modules
import os
import sys
import glob
from matplotlib.testing.compare import compare_images, ImageComparisonFailure

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.helpers import textcolor
# pylint: enable=wrong-import-position, import-error

#  #]
#  #[ settings
OUTPUT_DIR = 'output'
REFERENCE_DIR = 'ref'
#  #]

#  #[ main comparing code
def compare_against_reference(plot_prefix):
    ''' a function to implement the comparisons '''
    nr_of_passed = 0
    nr_of_failed = 0

    output_figure_location = os.path.split(plot_prefix)[0]
    ref_figure_location = (output_figure_location.
                           replace(OUTPUT_DIR, REFERENCE_DIR))

#    print('output_figure_location = ', output_figure_location)
#    print('ref_figure_location = ', ref_figure_location)

    if not os.path.exists(ref_figure_location):
        ## assume we are one level up in the CLM_dist folder
        output_figure_location = os.path.join('test', output_figure_location)
        ref_figure_location = os.path.join('test', ref_figure_location)
    
    list_of_ref_plots = glob.glob(os.path.join(ref_figure_location, '*.png'))
    for ref_plot_fn in sorted(list_of_ref_plots):
        base_fn = os.path.split(ref_plot_fn)[1]
        outp_plot_fn = os.path.join(output_figure_location, base_fn)
        # print('DEBUG: considering outp_plot_fn: ', outp_plot_fn)
        print(f'comparing: {ref_plot_fn} and {outp_plot_fn}:', end='')
        if os.path.exists(outp_plot_fn):
            try:
                err = compare_images(ref_plot_fn, outp_plot_fn, tol=0.001)
            except ImageComparisonFailure as this_err:
                print('error message: ', this_err)
                err = 'failed'
        else:
            err = 'File not found'
        if err is None:
            print(textcolor('blue', ' passed'))
            nr_of_passed += 1
        else:
            print(textcolor('red', ' failed'))
            print(err)
            nr_of_failed += 1

    print('='*25)
    print('Summary:')
    print(textcolor('blue', f'nr_of_passed = {nr_of_passed}'))
    if nr_of_failed == 0:
        color_to_use = 'blue'
    else:
        color_to_use = 'red'
    print(textcolor(color_to_use, f'nr_of_failed = {nr_of_failed}'))
    print('='*25)
    return nr_of_passed, nr_of_failed

if __name__ == '__main__':
    try:
        TESTCASE = sys.argv[1]
    except IndexError:
        # default
        TESTCASE = 'GEM'

    PLOT_PREFIX = f'output/test_{TESTCASE}'
    compare_against_reference(PLOT_PREFIX)
    #  #]

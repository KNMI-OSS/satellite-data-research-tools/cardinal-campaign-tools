#!/usr/bin/env python3

#  #[ documentation
#
#   Copyright (C) 2022-2024  KNMI
#
#   Author: Dave Donovan <dave.donovan@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
'''
This is a script to create plots for the simulation results.

You can run it in this way to request the available options::
python3 atlid_sim_plot.py -h
'''

#  #]
#  #[ imported modules
import os
import sys
import getopt
import datetime

# this import gives an error with pylint, even if I have installed netCDF4
# so disable checking for it.

# pylint: disable=no-name-in-module
from netCDF4 import Dataset
# pylint: enable=no-name-in-module


import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.testing.decorators import remove_ticks_and_titles
from lib.calipso_cm import get_calipso_cm

from lib.helpers import get_cmap_wrapper
from lib.custom_exceptions import PlotException

#  #]
#  #[ settings

def define_list_of_species():
    #  #[ get list of species
    ''' defines the list of possible species, including
    the generic species indicated by their id '''
    list_of_species = ['water', 'rain', 'ice_1', 'ice_2', 'ice_3',
                       'aer_1', 'aer_2', 'aer_3']
    for species_id in range(25):
        generic_sp = f'generic_id_{species_id}'
        list_of_species.append(generic_sp)
    return list_of_species
    #  #]

LIST_OF_SPECIES = define_list_of_species()

parameters_to_load = [
    'altitude_mid', 'Latitude', 'Longitude', 'Time',
    'latitude_of_sensor', 'longitude_of_sensor',
    'ATB_Mie_co', 'ATB_Ray', 'ATB_Mie_cr', 'MS_SS_ratio',]

# parameters to plot
parameters_to_plot_1d = [
    'ATB_Mie_co',
    'ATB_Ray',
    'ATB_Mie_cr',
    'tau',
    ]

parameters_to_plot_2d = [
    'ATB_Mie_co',
    'ATB_Ray',
    'ATB_Mie_cr',
    'MS_SS_ratio',
    'Extinction',
    'Reff',
    ]

parameters_to_plot_ground = [
    'tau',
    ]

for species in LIST_OF_SPECIES:
    par_ext = f'Extinction_{species}'
    par_reff = f'Reff_{species}'
    parameters_to_load.append(par_ext)
    parameters_to_load.append(par_reff)
    parameters_to_plot_2d.append(par_ext)
    parameters_to_plot_2d.append(par_reff)

#  #]

def calc_total_extinction(data):
    #  #[ calc. total extinction
    ''' calculate the total extinction by adding the extinction for
    the separate species'''

    extinction = None
    for spc in LIST_OF_SPECIES:
        name_ext = f'Extinction_{spc}'
        #print('debug: testing: ', name_ext)
        if name_ext in data:
            if data[name_ext] is not None:
                extinction = np.zeros_like(data[name_ext])
                break

    if extinction is None:
        error_txt = 'ERROR: could not find extinction data for plotting'
        raise PlotException(error_txt)

    for spc in LIST_OF_SPECIES:
        name_ext = f'Extinction_{spc}'
        if name_ext in data:
            if data[name_ext] is not None:
                extinction += data[name_ext]

    data['Extinction'] = extinction
    #  #]

def calc_total_reff(data):
    #  #[ calc. total Reff
    ''' calculate the total effective particle radius '''

    reff = None
    for spc in LIST_OF_SPECIES:
        name_reff = f'Reff_{spc}'
        if name_reff in data:
            if data[name_reff] is not None:
                reff = np.zeros_like(data[name_reff])

    for spc in LIST_OF_SPECIES:
        name_ext = f'Extinction_{spc}'
        name_reff = f'Reff_{spc}'
        if name_ext in data:
            if name_reff in data:
                if data[name_ext] is not None:
                    if data[name_reff] is not None:
                        reff += data[name_reff] * data[name_ext]

    # this total extinction is calculated in
    # the above function calc_total_extinction
    extinction = data['Extinction']

    select_pos_ext = np.where(extinction > 0.0)
    reff[select_pos_ext] = reff[select_pos_ext]/extinction[select_pos_ext]

    data['Reff'] = reff
    #  #]

def calc_total_tau(data):
    #  #[ calc. total tau
    ''' calculate the total transmission of the atmosphere'''

    # construct an array of dz values by taking the difference
    # between the levels and adding one item at the start (at 40km)
    res = np.abs(np.diff(data['height'])) # is given in [km]
    res = np.concatenate(([res[0]],res),axis=0)
    # question: why is res calculated in [km] and not in [m] ???
#    res=np.abs(np.diff(altitude_mid)) # is given in [m]

    if data['Time'] is None:
        coords = 'xyz'
    else:
        coords = 'tz'

    arr_shape = data['array_shape']

    if coords == 'xyz':
        tau = np.zeros([arr_shape[1],arr_shape[2]], dtype=float)
        for i in range(np.shape(tau)[0]):
            for j in range(np.shape(tau)[1]):
                for spc in LIST_OF_SPECIES:
                    name_ext = f'Extinction_{spc}'
                    if data[name_ext] is not None:
                        tau[i,j] += np.sum(res * data[name_ext][:,i,j], axis=0)

    if coords == 'tz':
        tau = np.zeros([arr_shape[1],], dtype=float)
        for i in range(np.shape(tau)[0]):
            for spc in LIST_OF_SPECIES:
                name_ext = f'Extinction_{spc}'
                if data[name_ext] is not None:
                    tau[i] += np.sum(res * data[name_ext][:,i], axis=0)

    data['tau'] = tau
    #  #]

def read_nc_file_and_preprocess(inputfile):
    #  #[ read the data, convert to numpy arrays and preprocess
    ''' read the needed data from the input netcdf file,
    convert it to numpy arrays, and allow for certain variables
    to be missing.'''
    print('Reading datafile: ', inputfile)
    try:
        ncf = Dataset(inputfile)
    except FileNotFoundError:
        print('Can not open ', inputfile)
        raise

    data = {}
    units = {}

    for par in parameters_to_load:
        # allow for missing parameters if some species were not provided
        try:
            data[par] = np.array(ncf[par])
            units[par] = ncf[par].units
        except IndexError:
            #print('parameter not available: ', par)
            data[par] = None
            units[par] = None

    # derived parameters
    data['height'] = data['altitude_mid']/1.0e3 # convert [m] to [km]
    units['height'] = 'km'

    array_shape = None
    for spc in LIST_OF_SPECIES:
        name_ext = f'Extinction_{spc}'
        if name_ext in data:
            if data[name_ext] is not None:
                array_shape = np.shape(data[name_ext])
                break

    if array_shape is None:
        error_txt = 'ERROR: could not find extinction array_shape for plotting'
        raise PlotException(error_txt)

    data['array_shape'] = array_shape
    units['array_shape'] = 1

    ncf.close()

    # maybe coords is no longer needed?
    if data['Time'] is None:
        data['coords'] = 'xyz'
    else:
        data['coords'] = 'tz'

    calc_total_extinction(data)
    calc_total_reff(data)
    calc_total_tau(data)

    return data, units
    #  #]

def define_plot_details(param_name):
    #  #[ set plot details
    ''' set plot details depending on parameter name '''
    plot_log10 = True
    cmap = get_cmap_wrapper('jet')
    plot_title = param_name.replace('_',' ')

    if 'ATB' in param_name:
        cmap = get_cmap_wrapper('gist_ncar').copy()
        # ensure the cmap copy is modified, not the original
        cmap.set_under('blue')

        if 'Mie' in param_name:
            cb_title = 'Co-Polar Mie Atten. Backscatter [sr$^{-1}$ m$^{-1}$]'
            trunc_values_below = 1.e-8
            cmap = get_calipso_cm()
        elif 'Ray' in param_name:
            cb_title = 'Ray. Atten. Backscatter [sr$^{-1}$ m$^{-1}$]'
            trunc_values_below = 1.e-9
        else:
            cb_title = 'Cross-Polar Atten. Backscatter [sr$^{-1}$ m$^{-1}$]'
            trunc_values_below = 1.e-8

        start_color = -8. # = np.log10(1.0e-8)
        stop_color = -5. # = np.log10(1.0e-5)

    elif 'MS_SS_ratio' in  param_name:
        cb_title = 'MS_SS_ratio'
        start_color = 1.0 # linear scale
        stop_color = 10.0 # linear scale
        trunc_values_below = None
        plot_log10 = False

    elif 'Extinction' in  param_name:
        cb_title = param_name+' [m$^{-1}$]'
        start_color = -5. # = np.log10(1.0e-5)
        stop_color = 0. # = np.log10(1.0)
        trunc_values_below = 1.e-9

    elif 'Reff' in  param_name:
        cb_title = param_name+' [microns]'
        start_color = -1. # = np.log10(0.1)
        stop_color = 2. # = np.log10(100.0)
        trunc_values_below = 1.e-9

    elif param_name == 'tau':
        cb_title = 'Particulate optical depth'
        start_color = -5. # = np.log10(0.1)
#        start_color = -1. # = np.log10(0.1)
        stop_color = 2. # = np.log10(100.0)
        stop_color = -1. # = np.log10(100.0)
        trunc_values_below = 1.e-9

    else:
        print('Error: undefined plot case for param: ', param_name)
        sys.exit(1)

    if plot_log10:
        cb_title = 'log10 '+cb_title

    return (plot_title, cb_title, start_color, stop_color, trunc_values_below,
            plot_log10, cmap)
    #  #]

def save_plot(param_name, settings, fig, pl_ax, cbar=None, plot_type=None):
    #  #[ save to file
    ''' define plot file name, render it and save it to file,
    in addition also save a copy without text as needed by the automatic
    testing, since font definitions often differ between different
    computer platforms. '''

    outputfile_prefix = settings['outputfile_prefix']
    plot_ext = settings['plot_ext']

    # setting for line plots
    plot_type_text = ''
    if plot_type:
        plot_type_text = f'_{plot_type}'
    fileout = outputfile_prefix+'_'+param_name+plot_type_text+'.'+plot_ext
    fileout_no_text = (outputfile_prefix+'_'+param_name+plot_type_text+
                       '_no_text'+'.'+plot_ext)
    print(f'generating: {fileout}')

    figure_dir = os.path.split(fileout)[0]
    if not os.path.exists(figure_dir):
        os.makedirs(figure_dir)

    # save the figure
    fig.savefig(fileout)

    # and save the figure again, now without text
    # because the differnt fonts on different platforms
    # cause the image comparisons to fail in some cases.
    remove_ticks_and_titles(fig)
    pl_ax.set_xlabel(None)
    pl_ax.set_ylabel(None)
    pl_ax.set_title(None)
    if cbar:
        cbar.ax.set_xlabel(None)
        cbar.ax.set_ylabel(None)

    fig.savefig(fileout_no_text)
    #  #]

def cm2inch(cmr):
    ''' conversion used for figure size '''
    return cmr/2.54

def create_line_plot(data, units, param_name, settings):
    #  #[ create a line plot
    ''' code for plotting lines '''

    if data[param_name] is None:
        # no data available for this parameter
        #print(f'skipping plot for {param_name}: no data available')
        return

    if data[param_name].min() == data[param_name].max():
        # the data seems to have a constant value
        # so plotting this is not very useful
        #print(f'skipping plot for {param_name}: seems a constant value')
        return

    #'ATB_Mie_co',
    #'ATB_Ray_co',
    #'ATB_cr',

    do_plot_vs_time = False
    do_plot_vs_altitude = False

    dt_values = data['Time']
    array = data[param_name]
    altitude = data['altitude_mid']*1.e-3 # convert n to km

    if param_name=='tau':
        do_plot_vs_time = True
    elif len(dt_values) < 10:
        do_plot_vs_altitude = True
    else:
        return

    details = define_plot_details(param_name)
    plot_title = details[0]
    cb_title = details[1]
    trunc_values_below = details[4]
    plot_log10 = details[5]

    if trunc_values_below is not None:
        array[array <= trunc_values_below] = trunc_values_below

    if plot_log10:
        array = np.log10(array)

    if do_plot_vs_time:
        figsize = (cm2inch(30), cm2inch(8.5))
    if do_plot_vs_altitude:
        figsize = (cm2inch(20), cm2inch(30))

    # plot the figure
    dpi = settings['dpi']
    fig = plt.figure(figsize=figsize, dpi=dpi)
    fig.clear() # remove plot details from previous iteration!
    pl_ax = fig.add_subplot(1, 1, 1) # rows, columns, count

    if do_plot_vs_time:
        #  create a 1D line plot
        pl_ax.plot(dt_values, array)
        ref_datetime = units['Time'][12:31]
        xtitle = f'Datetime [seconds since {ref_datetime}]'
        ytitle='10log total extinction'

    if do_plot_vs_altitude:
        for i in range(len(dt_values)):
            pl_ax.plot(array[:,i], altitude)
        xtitle = cb_title # f'10 log {param_name}'
        ytitle = 'altitude [km]'

#        select_below_6km = np.where(altitude < 6.)
#        max_val = array[select_below_6km,:].max()
#        pl_ax.set_xlim(0,max_val)

    pl_ax.set_title(plot_title)
    pl_ax.set_xlabel(xtitle)
    pl_ax.set_ylabel(ytitle)

    # tell matplotlib to adjust the canvas size to ensure
    # all labels are included in the saved figure
    fig.tight_layout()

    save_plot(param_name, settings, fig, pl_ax, plot_type='line')
    #  #]

def create_curtain_plot(data, units, param_name, settings):
    #  #[ code for plotting 2d curtains
    ''' creates a curtain plot '''

    if data[param_name] is None:
        # no data available for this parameter
        #print(f'skipping plot for {param_name}: no data available')
        return

    if data[param_name].min() == data[param_name].max():
        # the data seems to have a constant value
        # so plotting this is not very useful
        #print(f'skipping plot for {param_name}: seems a constant value')
        return

    # define what data along the vertical axis should be included
    # in the select_profiles_along_path() result
    zlimits = settings['zlimits']
    height = data['height']
    selected_z_indices = np.where((height <= zlimits[1]) &
                                  (height >= zlimits[0]))[0]
    t_start = data['Time'][0]
    t_end = data['Time'][-1]
    zlimits = settings['zlimits'] # altitude in km
    plot_extent = (t_start, t_end, zlimits[0], zlimits[1])
    array = data[param_name][selected_z_indices,:]

    (plot_title, cb_title, start_color, stop_color,
     trunc_values_below, plot_log10,
     cmap) = define_plot_details(param_name)

    # define colorbar tickmarks
    cb_ticks = np.linspace(start_color, stop_color,
                           num=int(abs(start_color-stop_color)+1))

    if trunc_values_below is not None:
        array[array <= trunc_values_below] = trunc_values_below

    if plot_log10:
        array = np.log10(array)

    # plot the figure
    dpi = settings['dpi']
    fig = plt.figure(figsize=(cm2inch(30),cm2inch(8.5)), dpi=dpi)
    fig.clear() # remove plot details from previous iteration!
    pl_ax = fig.add_subplot(1, 1, 1) # rows, columns, count
    divider = make_axes_locatable(pl_ax)

    #  plot against time
    img = pl_ax.imshow(array[::-1,:],cmap=cmap,
        vmin=start_color,vmax=stop_color,
        aspect='auto',origin='lower',extent=plot_extent)

    fig.subplots_adjust(bottom=0.15,right=0.82, top=0.9,left=0.06)

    #xtitle='Latitude [$^{\\circ}$N]'

    # time unit is: "hours since 2021-9-18 00:00:00 +00:00" ;
    #                0123456789012345678901234567890123456
    ref_datetime = units['Time'][12:31]
    xtitle = f'Datetime [seconds since {ref_datetime}]'

    pl_ax.set_title(plot_title)
    pl_ax.set_xlabel(xtitle)
    pl_ax.set_ylabel('Height [km]')

    cax = divider.append_axes('right', size=0.3, pad=0.1)
    cbar = fig.colorbar(img, cax=cax, ticks=cb_ticks)
    cbar.ax.set_ylabel(cb_title)

    save_plot(param_name, settings, fig, pl_ax, cbar) #, plot_type='curtain')
    #  #]

def create_ground_plot(data, data3d, param_name, settings):
    #  #[ code for plotting ground surface (lat,lon) plots
    ''' creates a ground (lat,lon) plot '''

    if data[param_name] is None:
        # no data available for this parameter
        #print(f'skipping plot for {param_name}: no data available')
        return

    if data[param_name].min() == data[param_name].max():
        # the data seems to have a constant value
        # so plotting this is not very useful
        #print(f'skipping plot for {param_name}: seems a constant value')
        return

    # path is defined by the lat,lon values in the 2D input_data

    # only used for tau, which is a 2D array in this 3D model output file
    #array_1d = data[param_name]
    lat_1d = data['latitude_of_sensor']
    lon_1d = data['longitude_of_sensor']

    array_2d = data3d[param_name]
    lat_2d = data3d['Latitude']
    lon_2d = data3d['Longitude']

    (plot_title, cb_title, start_color, stop_color,
     trunc_values_below, plot_log10,
     cmap) = define_plot_details(param_name)

    # define colorbar tickmarks
    cb_ticks = np.linspace(start_color, stop_color,
                           num=int(abs(start_color-stop_color)+1))

    if trunc_values_below is not None:
        array_2d[array_2d <= trunc_values_below] = trunc_values_below

    if plot_log10:
        array_2d = np.log10(array_2d)

    # plot the figure
    dpi = settings['dpi']
    fig = plt.figure(figsize=(cm2inch(30),cm2inch(8.5)),dpi=dpi)
    fig.clear() # remove plot details from previous iteration!
    pl_ax = fig.add_subplot(1, 1, 1) # rows, columns, count
    divider = make_axes_locatable(pl_ax)
    extent=(lon_2d.min(), lon_2d.max(), lat_2d.min(), lat_2d.max())
    cmap = get_cmap_wrapper('gist_gray')

    # plot the image
    img = pl_ax.imshow(np.transpose(array_2d[:,:]),cmap=cmap,
        vmin=start_color,vmax=stop_color,aspect='equal',
        origin='lower',extent=extent)

    # overplot the trajectory
    if (lat_1d is not None) and (lat_2d is not None):
        pl_ax.plot(lon_1d, lat_1d,'r--')

    cmap = get_cmap_wrapper('gist_gray').copy()
    # ensure the cmap copy is modified, not the original
    cmap.set_under('black')
    img.set_cmap(cmap)

    fig.subplots_adjust(bottom=0.15,right=0.82, top=0.9,left=0.06)

    xtitle = 'Longitude [$^{\\circ}$W]'
    ytitle = 'Latitude [$^{\\circ}$N]'
    pl_ax.set_title(plot_title)
    pl_ax.set_xlabel(xtitle)
    pl_ax.set_ylabel(ytitle)

    # disable [rinting the laongitudes relative to an offset
    # of 3.341e2
    pl_ax.get_xaxis().get_major_formatter().set_useOffset(False)

    cmap = get_cmap_wrapper('gist_gray').copy()
    # ensure the cmap copy is modified, not the original
    cmap.set_under('black')
    img.set_cmap(cmap)

    cax = divider.append_axes('right', size=0.3, pad=0.1)
    cbar = fig.colorbar(img, cax=cax, ticks=cb_ticks)
    cbar.ax.set_ylabel(cb_title)

    save_plot(param_name, settings, fig, pl_ax, cbar, plot_type='ground')
    #  #]

def atlid_sim_plot(settings):
    #  #[ plot
    ''' generate all required plots '''

    # Read the data file
    data, units = read_nc_file_and_preprocess(settings['inputfile'])

    data3d = None
    if settings['inputfile3d']:
        data3d = read_nc_file_and_preprocess(settings['inputfile3d'])[0]

    for param_name in parameters_to_plot_1d:
        create_line_plot(data, units, param_name, settings)

    for param_name in parameters_to_plot_2d:
        create_curtain_plot(data, units, param_name, settings)

    for param_name in parameters_to_plot_ground:
        if data3d is not None:
            create_ground_plot(data, data3d, param_name, settings)

    #  #]

def print_usage_and_stop(error_txt=None):
    #  #[ print some help
    '''
    print a help message, and if needed an error message,
    then stop the script.
    '''
    script_name = sys.argv[0]
    print('')
    print(f'USAGE: {script_name} [OPTIONS]')
    print('''
Explanation of the options:
    -h/--help
           Display this help message.
    -i/--inputfile <input filename>
           Define the name of the input NetCDF file to use
           for 2D curtain fields (required).
    --i3d/--inputfile3d <input filename>
           Define the name of the input NetCDF file to use
           for 3D model fields (optional)
    -o/--outputfile_prefix <prefix for output plot files>
           Define a prefix (could include a path name)
           to be prepended to the generated plot files.
    --latlimits <lat1,lat2>
           Define start/stop values in degrees for the
           latitude coordinate.
    --lonlimits <lon1,lon2>
           Define the start/stop values in degrees for the
           longitude coordinate.
    --zlimits <z1,z2>
           Define the start/stop values in km for the
           altitude coordinate.
    --dtlimits <dt1,dt2>
           Define the start/stop values for the datetime
           coordinate, using formatting: YYYYMMDDTHHmmss.
    --ilatlimits <ilat1,ilat2>
           Define the start/stop index values for the
           latitude coordinate.
    --ilonlimits <ilon1,ilon2>
           Define the start/stop index values for the
           longitude coordinate.
    --izlimits <iz1,iz2>
           Define the start/stop index values for the
           altitude coordinate.
    --idtlimits <idt1,idt2>
           Define the start/stop index values for the
           datetime coordinate (i.e. the profile index).
    -d/--dpi <dpi value>
           Define the dots-per-inch value to be used
           for generation of the plots.
''')

    if error_txt is not None:
        print('')
        print('ERROR: '+error_txt)
        print('')
        sys.exit(1)
    else:
        sys.exit(0)
    #  #]

def handle_commandline_settings():
    #  #[ handle commandline inputs
    '''
    Process the command line arguments
    '''

    # init to None
    inputfile = None
    inputfile3d = None
    outputfile_prefix = None

    # limits in real units
    latlimits = None # [0., 0.] # latitude in deg
    lonlimits = None # [0., 0.] # longitude in deg
    zlimits = None   # [0., 0.]# altitude in km
    dtlimits = None  # [20220612T0930000, 20220612T1000000] # dt start/stop

    # limits on index values
    ilatlimits = None # [1, 50]   # index values (old xlims)
    ilonlimits = None # [1, 100]  # index values (old ylims)
    izlimits = None   # [1, 300]  # index values
    idtlimits = None  # [0, 1200] # index values

    # plot details
    dpi = 100
    plot_ext = 'png' # default, could also be 'jpg', 'pdf', etc.

    # note: the ':' and '=' options must have a value following it
    short_options = 'hi:o:s:f:d:'
    long_options = ['help',
        'inputfile=', 'i3d=', 'inputfile3d=', 'outputfile_prefix=',
        'latlimits=', 'lonlimitss=', 'zlimits=', 'dtlimits',
        'ilatlimits=', 'ilonlimits=', 'izlimits=', 'idtlimits',
        'dpi=', 'plot_ext=']

    args = sys.argv[1:]
    try:
        options = getopt.getopt(args, short_options, long_options)[0]
    except getopt.GetoptError as err:
        print_usage_and_stop(f'invalid option in argument list: {err}')

    for opt, value in options:
        if opt in ['-h', '--help']:
            print_usage_and_stop()
        elif opt in ('-i', '--inputfile'):
            inputfile = value
        elif opt in ('--i3d', '--inputfile3d'):
            inputfile3d = value
        elif opt in ('-o', '--outputfile_prefix'):
            outputfile_prefix = value
        elif opt == '--latlimits':
            latlimits = [float(val) for val in value.split(',')]
        elif opt == '--lonlimits':
            lonlimits = [float(val) for val in value.split(',')]
        elif opt == '--zlimits':
            zlimits = [float(val) for val in value.split(',')]
        elif opt == '--dtlimits':
            dt_format = '%Y%m%dT%H%M%S'
            dtlimits = [datetime.datetime.strptime(dt, dt_format)
                        for dt in value.split(',')]
        elif opt == '--ilatlimits':
            ilatlimits = [int(val) for val in value.split(',')]
        elif opt == '--ilonlimits':
            ilonlimits = [int(val) for val in value.split(',')]
        elif opt == '--izlimits':
            izlimits = [int(val) for val in value.split(',')]
        elif opt == '--idtlimits':
            idtlimits = [int(val) for val in value.split(',')]
        elif opt in ('-d', '--dpi'):
            dpi = value
        elif opt == '--plot_ext':
            plot_ext = value
        else:
            # this point should never be reached
            # (it would be a programming error if it still gets here)
            print('Unhandled option: ', opt, value)
            sys.exit(1)

    # some sanity checks
    if inputfile is None:
        print_usage_and_stop(error_txt='inputfile not defined')
    if outputfile_prefix is None :
        print_usage_and_stop(error_txt='outputfile_prefix not defined')

    # combine settings in a dict for later use by the program
    settings = {}
    settings['inputfile'] = inputfile
    settings['inputfile3d'] = inputfile3d
    settings['outputfile_prefix'] = outputfile_prefix

    settings['latlimits'] = latlimits
    settings['lonlimits'] = lonlimits
    settings['zlimits'] = zlimits
    settings['dtlimits'] = dtlimits

    settings['ilatlimits'] = ilatlimits
    settings['ilonlimits'] = ilonlimits
    settings['izlimits'] = izlimits
    settings['idtlimits'] = idtlimits

    settings['dpi'] = dpi
    settings['plot_ext'] = plot_ext

    print('Selected settings are:')
    for key, value in settings.items():
        print(f'({key} = {value}')

    return settings
    #  #]

def main():
    #  #[ main program
    ''' main entry point '''
    settings = handle_commandline_settings()
    atlid_sim_plot(settings)
    #  #]

if __name__ == '__main__':
    main()

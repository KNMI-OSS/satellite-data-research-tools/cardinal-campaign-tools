#!/usr/bin/env python3
'''
This is a little example script to demonstrate how the different
tools in this project can be combined to process several eVe
LIDAR profiles.

It takes the following steps:
* modify the toml template configuration filke
* convert this toml configuration to a xlsx configuration file
* run the lidar_module.py tool to do the actual processing
'''

import os
import sys
import glob

#TESTFOLDER = './20210916_met_NWP_data'
#TESTFOLDER = './20210924_met_NWP_data'
#TESTFOLDER = './20220909_met_NWP_data'
TESTFOLDER = './20220911_met_NWP_data'

TOML_TEMPLATE = 'config_EVE_template.toml'
toml_file = os.path.join(TESTFOLDER, 'config_EVE.toml')
xlsx_file = os.path.join(TESTFOLDER, 'config_EVE.xlsx')
nc_path_and_file = glob.glob(os.path.join(TESTFOLDER, '*.nc'))[0]
nc_file = os.path.split(nc_path_and_file)[1]

datetime_code = nc_file[21:34]
#0         1         2         3         4
#012345678901234567890123456789012345678901234567890
#eVe_profiles_mindelo_20210916T2016_20210916T2054UTC.nc
#print('datetime_code: ', datetime_code)

output_file = os.path.join(TESTFOLDER, f'atlid_tool_output_{datetime_code}.nc')

def run(cmd):
    ''' run a shell command and check the exit status '''
    print(f'executing: {cmd}')
    result = os.system(cmd)
    if result == 0:
        print('success')
    else:
        print('command failed')
        sys.exit(1)

# load the toml template
with open(TOML_TEMPLATE, 'rt', encoding='ascii') as fd_in:
    with open(toml_file, 'wt', encoding='ascii') as fd_out:
        for line in fd_in.readlines():
            # modify the toml template
            if 'data_dir' in line:
                line = f'data_dir = \"{TESTFOLDER}\"\n'
            if 'input_file.01.filename' in line:
                line = f'input_file.01.filename = \"{nc_file}\"\n'
            fd_out.write(line)

# convert toml to xlsx
command = f'create_config_file.py {toml_file} {xlsx_file}'
run(command)

# execute the ATLID tool
command = f'./lidar_module.py -i {xlsx_file} -o {output_file}'
run(command)

print('input nc file:', nc_path_and_file)
print('resulting file:', output_file)

## Container example

As an example for users who whish to install the simulator in
a container, an example Docker file is provided that can be
used as starting point.

This container example is based on Fedora-40 and contains specific
commands to install the needed dependencies and the software
itself on this operating system. For other linux versions
the commands will have to be adapted.

This container was developed and tested using podman.
It is likely compatible with the docker software as well
but this has not yet been tested, so any feedback is welcome.

## Build the container

To build the container execute a podman command linke this:
```
podman build ./ -f Dockerfile -t atlid_l1_simulator
```

## Using the container

Before the container can be used the input data
must be provided in a dedicated folder outside the container.

To define the location of the local data folder you can use
the following commands.

csh syntax:
```
setenv LOCALDATAFOLDER /local/path/to/data
```
or bash syntax:
```
export LOCALDATAFOLDER=/local/path/to/data
```
This data folder will be available inside the container
as the path /data

Then the container can be started like this to provide
a bash shell inside the container for testing:

```
podman run -it -v $LOCALDATAFOLDER:/data/:z atlid_l1_simulator /bin/bash
```
Note that the ':z' setting is needed on systems running selinux.
For systems not running selinux it can be omitted.

The different steps and tools of the simulator software can
be executed as follows.

Convert the toml config file to xlsx format:
```
podman run -it -v ${LOCALDATAFOLDER}:/data/:z atlid_l1_simulator \
    /work/CCT/CLM_dist/scripts/create_config_file.py \
        /data/config_toml/config_EVE.toml \
	/data/config_xlsx/config_EVE.xlsx
```

Run the lidar module:
```
podman run -it -v ${LOCALDATAFOLDER}:/data/:z atlid_l1_simulator \
    /work/CCT/CLM_dist/lidar_module.py \
        -i /data/config_xlsx/config_EVE.xlsx \
        -o /data/output_data/Test_EVE_data_RT.nc
```

Compare a generated nc file with a reference nc file:
```
podman run -it -v ${LOCALDATAFOLDER}:/data/:z atlid_l1_simulator \
    nccmp -fdm -T 0.1 \
        --exclude=File_Name,Creation_Date,productName,processingStartTime,processingStopTime \
        /data/reference_data/Test_EVE_data_RT.nc \
        /data/output_data/Test_EVE_data_RT.nc
```

Generate the standard plots:
```
podman run -it -v ${LOCALDATAFOLDER}:/data/:z atlid_l1_simulator \
    /work/CCT/CLM_dist/atlid_sim_plot.py \
        -i /data/output_data/Test_EVE_data_RT.nc \
        -o /data/generated_plots/prefix_ \
        --zlimits=0.0,15.0
```

Generate EarthCARE files:
```
podman run -it -v ${LOCALDATAFOLDER}:/data/:z atlid_l1_simulator \
    /work/CCT/CLM_dist/scripts/create_earth_care_files.py \
        --lidar_count=1 \
        --ifile /data/output_data/Test_EVE_data_RT.nc \
        --output_dir /data/output_data/ \
        --index=3 \
        --overpass_time='14:00:00' \
        --reproducible_files_day 20231007 \
        --templates_folder=/work/CCT/CLM_dist/templates \
        --instrument_module random \
        --instr_data_folder=/work/CCT/CLM_dist/test/instrument_data
```

Compare a generated and an expected EarthCARE file:
```
podman run -it -v ${LOCALDATAFOLDER}:/data/:z atlid_l1_simulator \
    h5diff <file1> <file2>

podman run -it -v ${LOCALDATAFOLDER}:/data/:z atlid_l1_simulator \
    h5diff /data/output_data//ECA_EXAA_ATL_NOM_1B_20221024T211401Z_20231007T000000Z_21003A/ECA_EXAA_ATL_NOM_1B_20221024T211401Z_20231007T000000Z_21003A.h5 \
         /data/reference_data/ECA_EXAA_ATL_NOM_1B_20221024T211401Z_20231007T000000Z_21003A/ECA_EXAA_ATL_NOM_1B_20221024T211401Z_20231007T000000Z_21003A.h5
```

Note that h5diff may complain about a difference in the _NCProperties
This can be ignored. This variable reports the version of the netcdf library
which may differ between different installations.
it has nothing to do with the actual data in the generated files.
Unfortunately the --exclude-attribute of h5diff seems not able
to suppress this usse (at least in the version that I use for testing).

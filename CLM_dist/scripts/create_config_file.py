#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 20-Jun-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

'''
a script to automatically generate different config spreadsheet
files to be able to provide examples for users
and to generate inputs for different tests and unittests.
'''
#
#  #]
#  #[ imported modules
import os
import sys

# note that for python3.11 and earlier tomllib was named tomli
try:
    import tomllib
except ImportError:
    import tomli as tomllib

from openpyxl import Workbook
from openpyxl.styles import Border, Side, Font, Alignment, PatternFill

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.config_file_definition import(
    EXPECTED_CONFIG_FORMAT_VERSION,
    TABLE_POS_HEADERS, TABLE_POS_DEFINITIONS,
    LID_PARS_COL_HEADERS, LID_PARS_ROW_DEFINITIONS,
    DOMAIN_POS_HEADERS, DOMAIN_PARS_ROW_DEFINITIONS,
    QC_PARS_HEADERS, QC_PARS_ROW_DEFINITIONS,
    FILE_LIST_COL_HEADERS, FILE_LIST_ROW_DEFINITIONS,
    SPECIES_COUNT_HEADERS, SPECIES_COUNT_DEFINITIONS,
    SPECIES_TYPE_HEADERS, SPECIES_TYPE_DEFINITIONS, MAIN_COL_HEADERS,
    SIMVAR_METEO_DEFINITIONS, SIMVAR_GENERIC_DEFINITIONS,
    )
from lib.handle_xlsx import (apply_offset_to_cell_index,
                             adapt_column_width_to_cell_content,
                             merge_file_table_cells)
# pylint: enable=wrong-import-position, import-error

#  #]
#  #[ constants
# colors
BLACK = '000000'
RED = 'FF0000'
LIGHT_RED = 'FFCCCC'
GRAY = 'CCCCCC'
#  #]

def format_cell(cell):
    #  #[ generic cell format
    ''' apply formatting needed for all cells '''
    cell.font = Font(color=BLACK)
    side = Side(border_style='thin', color=BLACK)
    cell.border = Border(left=side, right=side, bottom=side, top=side)
    cell.alignment = Alignment(vertical='center')
    #  #]

def format_hdr_cell(cell):
    #  #[ header cell format
    ''' apply formatting specific for header cells '''
    format_cell(cell)
    cell.fill = PatternFill(start_color=LIGHT_RED,
                            end_color=LIGHT_RED,
                            fill_type="solid")
    #  #]

def add_hdr_cell(wsh, hdr_cell_index, hdr_cell_name):
    #  #[ add header cell
    ''' add and format a header cell '''
    wsh[hdr_cell_index] = hdr_cell_name
    format_hdr_cell(wsh[hdr_cell_index])
    #  #]

def add_hor_data_cell_pair(wsh, cell_index_start,
                           val_cell_def, num_data_cells=1):
    #  #[ add data cell
    ''' add and format a data cell pair, i.e. a description cell
    and a data cell right to it. '''
    name_unit = val_cell_def[1]

    wsh[cell_index_start] = name_unit
    format_cell(wsh[cell_index_start])

    for idc in range(num_data_cells):
        cell_index = apply_offset_to_cell_index(cell_index_start,
                                                hor_offset=1+idc)
        format_cell(wsh[cell_index])
    #  #]

def add_ver_data_cell_pair(wsh, cell_index, hdr_cell_text):
    #  #[ add data cell
    ''' add and format a data cell pair, i.e. a description cell
    and a data cell below to it. '''

    wsh[cell_index] = hdr_cell_text
    format_hdr_cell(wsh[cell_index])

    data_cell_index = apply_offset_to_cell_index(cell_index, ver_offset=1)
    format_cell(wsh[data_cell_index])
    #  #]

def get_from_toml(toml_data, toml_path):
    #  #[ get one element
    ''' extract a given element from the toml data structure '''
    # print('DEBUG: getting toml data from path: ', toml_path)
    path_elements = toml_path.split('.')
    try:
        data = toml_data
        for path_elem in path_elements:
            data = data[path_elem]
        return data
    except KeyError:
        # in case an element is not defined return None
        return None
    #  #]

def add_version(wsh):
    #  #[ add the version tag
    ''' add a marker for the config format version '''
    wsh['A1'] = EXPECTED_CONFIG_FORMAT_VERSION[0]
    wsh['B1'] = EXPECTED_CONFIG_FORMAT_VERSION[1]
    wsh['A1'].font = Font(color=RED)
    wsh['B1'].font = Font(color=RED)

    return 'A1', 'B1'
    #  #]

def add_table_positions(wsh, toml_data):
#  #[ add table position cells
    ''' adds the table position cells
        and adds, formats and fills the data cells next to them'''
    for hdr_cell_def in TABLE_POS_HEADERS:
        hdr_cell_index = hdr_cell_def[0]
        hdr_cell_name = hdr_cell_def[1]
        add_hdr_cell(wsh, hdr_cell_index, hdr_cell_name)

    for row_def in TABLE_POS_DEFINITIONS:
        cell_index = row_def[0]
        add_hor_data_cell_pair(wsh, cell_index, row_def[1:3])
        toml_path = row_def[3]
        if toml_path is not None:
            value = get_from_toml(toml_data, toml_path)
            data_cell_index = apply_offset_to_cell_index(cell_index,
                                                         hor_offset=1)
            wsh[data_cell_index] = value

    ulc_index = TABLE_POS_HEADERS[0][0]
    lrc_index = data_cell_index
    return ulc_index, lrc_index
    #  #]

def add_lidar_params(wsh, toml_data):
    #  #[ add lidar params cells
    ''' adds the lidar params cells
        and adds, formats and fills the data cells next to them'''
    toml_path = 'Table_Position_Definitions.lidar_table'
    upper_left_corner_index = get_from_toml(toml_data, toml_path)

    hor_offset = 0
    cell_index = upper_left_corner_index
    for hdr_cell_name in LID_PARS_COL_HEADERS:
        hdr_cell_index = apply_offset_to_cell_index(cell_index,
                                                    hor_offset=hor_offset)
        add_hdr_cell(wsh, hdr_cell_index, hdr_cell_name)
        hor_offset += 1

    ver_offset = 1
    cell_index = upper_left_corner_index
    for row_def in LID_PARS_ROW_DEFINITIONS:
        name_cell_index = apply_offset_to_cell_index(cell_index,
                                                     ver_offset=ver_offset)
        add_hor_data_cell_pair(wsh, name_cell_index, row_def[:2])

        # add the value
        data_cell_index = apply_offset_to_cell_index(name_cell_index,
                                                     hor_offset=1)
        lrc_index = data_cell_index
        toml_path = row_def[2]
        if toml_path is not None:
            value = get_from_toml(toml_data, toml_path)
            wsh[data_cell_index] = value
        ver_offset += 1

    ulc_index = upper_left_corner_index

    return ulc_index, lrc_index
    #  #]

def add_domain(wsh, toml_data):
    #  #[ add the domain
    ''' add the cells needed to define the domain to simulate '''
    toml_path = 'Table_Position_Definitions.domain_table'
    upper_left_corner_index = get_from_toml(toml_data, toml_path)

    hor_offset = 0
    cell_index = upper_left_corner_index
    for hdr_cell_name in DOMAIN_POS_HEADERS:
        hdr_cell_index = apply_offset_to_cell_index(cell_index,
                                                    hor_offset=hor_offset)
        add_hdr_cell(wsh, hdr_cell_index, hdr_cell_name)
        hor_offset += 1

    ver_offset = 1
    cell_index = upper_left_corner_index
    for row_def in DOMAIN_PARS_ROW_DEFINITIONS:
        name_cell_index = apply_offset_to_cell_index(cell_index,
                                                     ver_offset=ver_offset)
        add_hor_data_cell_pair(wsh, name_cell_index, row_def[:2])

        # add the value
        data_cell_index = apply_offset_to_cell_index(name_cell_index,
                                                     hor_offset=1)
        lrc_index = data_cell_index

        toml_path = row_def[2]
        if toml_path is not None:
            value = get_from_toml(toml_data, toml_path)
            wsh[data_cell_index] = value
        ver_offset += 1

    ulc_index = upper_left_corner_index

    return ulc_index, lrc_index
    #  #]

def add_qc_params(wsh, toml_data):
     #  #[ add QC params cells
    ''' adds the QC params cells
        and adds, formats and fills the data cells next to them'''
    toml_path = 'Table_Position_Definitions.qc_table'
    upper_left_corner_index = get_from_toml(toml_data, toml_path)

    hor_offset = 0
    cell_index = upper_left_corner_index
    for hdr_cell_name in QC_PARS_HEADERS:
        hdr_cell_index = apply_offset_to_cell_index(cell_index,
                                                    hor_offset=hor_offset)
        add_hdr_cell(wsh, hdr_cell_index, hdr_cell_name)
        hor_offset += 1

    ver_offset = 1
    cell_index = upper_left_corner_index
    for row_def in QC_PARS_ROW_DEFINITIONS:
        name_cell_index = apply_offset_to_cell_index(cell_index,
                                                     ver_offset=ver_offset)
        add_hor_data_cell_pair(wsh, name_cell_index, row_def[:2])

        # add the value
        data_cell_index = apply_offset_to_cell_index(name_cell_index,
                                                     hor_offset=1)
        lrc_index = data_cell_index
        toml_path = row_def[2]
        if toml_path is not None:
            value = get_from_toml(toml_data, toml_path)
            wsh[data_cell_index] = value
        ver_offset += 1

    ulc_index = upper_left_corner_index

    return ulc_index, lrc_index
    #  #]

def add_file_list(wsh, toml_data):
    #  #[ add the data dir definition cell
    ''' adds the data dir and file list cells '''
    toml_path = 'Table_Position_Definitions.file_table'
    upper_left_corner_index = get_from_toml(toml_data, toml_path)

    # define the table heading
    hor_offset = 0
    cell_index = upper_left_corner_index
    for hdr_cell_name in FILE_LIST_COL_HEADERS:
        hdr_cell_index = apply_offset_to_cell_index(cell_index,
                                                    hor_offset=hor_offset)
        add_hdr_cell(wsh, hdr_cell_index, hdr_cell_name)
        hor_offset += 1

    # define the fixed items for this table
    ver_offset = 1
    cell_index = upper_left_corner_index
    for row_def in FILE_LIST_ROW_DEFINITIONS:
        name_cell_index = apply_offset_to_cell_index(cell_index,
                                                     ver_offset=ver_offset)
        add_hor_data_cell_pair(wsh, name_cell_index, row_def[:2])

        # add the value
        toml_path = row_def[2]
        if toml_path is not None:
            data_cell_index = apply_offset_to_cell_index(name_cell_index,
                                                         hor_offset=1)
            value = get_from_toml(toml_data, toml_path)
            wsh[data_cell_index] = value
        ver_offset += 1

    # define the variable length list of filenames
    toml_path = 'File_List.number_of_input_files'
    num_input_files = get_from_toml(toml_data, toml_path)
    for i_inp_file in range(num_input_files):
        toml_path = f'File_List.input_file.{i_inp_file+1:02d}.refname'
        refname = get_from_toml(toml_data, toml_path)
        toml_path = f'File_List.input_file.{i_inp_file+1:02d}.filename'
        filename = get_from_toml(toml_data, toml_path)
        name_cell_index = apply_offset_to_cell_index(cell_index,
                                                     ver_offset=ver_offset)
        add_hor_data_cell_pair(wsh, name_cell_index, (None, refname, filename))

        # add the value
        data_cell_index = apply_offset_to_cell_index(name_cell_index,
                                                     hor_offset=1)
        wsh[data_cell_index] = filename
        ver_offset += 1
        lower_right_corner_index = data_cell_index

    # annotation
    ann_cell_index = apply_offset_to_cell_index(cell_index, ver_offset=-1)
    wsh[ann_cell_index] = 'File List'
    wsh[ann_cell_index].font = Font(bold=True)

    return upper_left_corner_index, lower_right_corner_index
    #  #]

def add_species_count(wsh, toml_data):
    #  #[ add the species count table
    ''' adds the species_count table and cells'''
    toml_path = 'Table_Position_Definitions.species_count_table'
    upper_left_corner_index = get_from_toml(toml_data, toml_path)

    # define the table heading
    hor_offset = 0
    cell_index = upper_left_corner_index
    for hdr_cell_name in SPECIES_COUNT_HEADERS:
        hdr_cell_index = apply_offset_to_cell_index(cell_index,
                                                    hor_offset=hor_offset)
        add_hdr_cell(wsh, hdr_cell_index, hdr_cell_name)
        hor_offset += 1

    # define the fixed items for this table
    ver_offset = 1
    cell_index = upper_left_corner_index
    for row_def in SPECIES_COUNT_DEFINITIONS:
        name_cell_index = apply_offset_to_cell_index(cell_index,
                                                     ver_offset=ver_offset)
        add_hor_data_cell_pair(wsh, name_cell_index, row_def[:2])

        # add the value
        toml_path = row_def[2]
        if toml_path is not None:
            data_cell_index = apply_offset_to_cell_index(name_cell_index,
                                                         hor_offset=1)
            value = get_from_toml(toml_data, toml_path)
            wsh[data_cell_index] = value
            lower_right_corner_index = data_cell_index
        ver_offset += 1

    # annotation
    ann_cell_index = apply_offset_to_cell_index(cell_index, ver_offset=-1)
    wsh[ann_cell_index] = 'Species count'
    wsh[ann_cell_index].font = Font(bold=True)

    # Number_of_Blocks annotation
    ann_cell_index = apply_offset_to_cell_index(cell_index,
        hor_offset=2, ver_offset=1)
    wsh[ann_cell_index] = (' This defines the number of blocks of species '+
                           'definitions given below.')

    return upper_left_corner_index, lower_right_corner_index
    #  #]

def add_value_at_offset(wsh, toml_data, row_start_index, toml_section_path,
                        hor_offset, element):
    #  #[ add a value at given offset
    ''' adds a value at a given cell index, allowing to add
    a horizontal offset to the index if needed '''
    cell_index = apply_offset_to_cell_index(row_start_index, hor_offset)
    toml_path = toml_section_path+'.'+element
    value = get_from_toml(toml_data, toml_path)
    wsh[cell_index] = value
    #  #]

def add_meteo_variables(wsh, toml_data):
    #  #[ add the meteo table of parameter definitions
    ''' add the meteo table of input parameters '''

    row_def = SIMVAR_METEO_DEFINITIONS
    toml_path = 'Table_Position_Definitions.meteo_simvar_table'

    upper_left_corner_index = get_from_toml(toml_data, toml_path)

    hor_offset = 0
    cell_index = upper_left_corner_index
    hdr_cell_index = cell_index

    # fill the table header cells
    for hdr_cell_name in MAIN_COL_HEADERS:
        cell_index = apply_offset_to_cell_index(hdr_cell_index,
                                                hor_offset=hor_offset)
        add_hdr_cell(wsh, cell_index, hdr_cell_name)
        hor_offset += 1

    ver_offset = 1
    cell_index = upper_left_corner_index
    for val_cell_def in row_def:
        # define row name and 6 empty data cells
        name_cell_index = apply_offset_to_cell_index(cell_index,
                                                     ver_offset=ver_offset)
        add_hor_data_cell_pair(wsh, name_cell_index,
                                   val_cell_def, num_data_cells=6)

        # fill the data cells
        species_count = 1
        toml_path = val_cell_def[2].replace('<count>', f'{species_count}')
        row_start_index = name_cell_index

        add_value_at_offset(wsh, toml_data, row_start_index,
                            toml_path, 1, 'input_file')
        add_value_at_offset(wsh, toml_data, row_start_index,
                            toml_path, 2, 'file_variable')
        add_value_at_offset(wsh, toml_data, row_start_index,
                            toml_path, 3, 'dimension_order')
        add_value_at_offset(wsh, toml_data, row_start_index,
                            toml_path, 4, 'scale_factor')
        add_value_at_offset(wsh, toml_data, row_start_index,
                            toml_path, 5, 'shift')
        add_value_at_offset(wsh, toml_data, row_start_index,
                            toml_path, 6, 'fixed_value')
        lower_right_corner_index = \
          apply_offset_to_cell_index(row_start_index, hor_offset=6)
        ver_offset += 1

    # add some annotations
    cell_index = upper_left_corner_index

    ann_cell_index = apply_offset_to_cell_index(cell_index, ver_offset=-1)
    wsh[ann_cell_index] = 'Meteorological inputs'
    wsh[ann_cell_index].font = Font(bold=True)

    ann_cell_index = apply_offset_to_cell_index(cell_index,
        hor_offset=3, ver_offset=-3)
    wsh[ann_cell_index] = '0: vertical'

    ann_cell_index = apply_offset_to_cell_index(cell_index,
        hor_offset=3, ver_offset=-2)
    wsh[ann_cell_index] = '1: latitude or time'

    ann_cell_index = apply_offset_to_cell_index(cell_index,
        hor_offset=3, ver_offset=-1)
    wsh[ann_cell_index] = '2:longitude'

    ulc_index = \
      apply_offset_to_cell_index(upper_left_corner_index, ver_offset=1)
    return ulc_index, lower_right_corner_index
    #  #]

def add_main_variables(wsh, toml_data):
    #  #[ add the main table of parameter definitions
    ''' add the main table of input parameters '''

    toml_path = 'Table_Position_Definitions.generic_simvar_table'
    upper_left_corner_index = get_from_toml(toml_data, toml_path)

    # extract the number of species to be added
    row_def = SPECIES_COUNT_DEFINITIONS[0]
    toml_path = row_def[2]
    species_count = get_from_toml(toml_data, toml_path)

    list_of_ulc_lrc = []
    species_start_index = upper_left_corner_index
    for species_index in range(1,species_count+1):

        # define the heading for the species type and name section
        hor_offset = 0
        for hdr_cell_name in SPECIES_TYPE_HEADERS:
            hdr_cell_index = apply_offset_to_cell_index(species_start_index,
                                                    hor_offset=hor_offset)
            add_hdr_cell(wsh, hdr_cell_index, hdr_cell_name)
            hor_offset += 1

        # define the fixed items for this table
        ver_offset = 1
        #cell_index = species_start_index
        for row_def in SPECIES_TYPE_DEFINITIONS:
            name_cell_index = apply_offset_to_cell_index(species_start_index,
                                                     ver_offset=ver_offset)
            add_hor_data_cell_pair(wsh, name_cell_index, row_def[:2])

            # add the value
            toml_path = row_def[2].replace('<count>', f'{species_index}')
            if toml_path is not None:
                data_cell_index = apply_offset_to_cell_index(name_cell_index,
                                                             hor_offset=1)
                value = get_from_toml(toml_data, toml_path)
                wsh[data_cell_index] = value
            ver_offset += 1

       # annotations

        # Species_index annotation
        ann_cell_index = apply_offset_to_cell_index(species_start_index,
            hor_offset=2, ver_offset=0)
        wsh[ann_cell_index] = (' This block defines a species with '+
                               f'index {species_index}')
        wsh[ann_cell_index].font = Font(bold=True)

        # Species_Type annotation
        ann_cell_index = apply_offset_to_cell_index(species_start_index,
            hor_offset=2, ver_offset=1)
        wsh[ann_cell_index] = (' This must be one of "generic", "water", '+
                               '"ice" or "aerosol"')
        # apply bold font for Species_Type name and value
        ann_cell_index = apply_offset_to_cell_index(species_start_index,
            hor_offset=0, ver_offset=1)
        wsh[ann_cell_index].font = Font(bold=True)
        ann_cell_index = apply_offset_to_cell_index(species_start_index,
            hor_offset=1, ver_offset=1)
        wsh[ann_cell_index].font = Font(bold=True)

        # Species_Name annotation
        ann_cell_index = apply_offset_to_cell_index(species_start_index,
            hor_offset=2, ver_offset=2)
        wsh[ann_cell_index] = (' This name will be used to define the '+
                               'variable name in the output netcdf file')

        row_def = SIMVAR_GENERIC_DEFINITIONS

        # account for the species type and name cells just above
        hdr_cell_index = apply_offset_to_cell_index(species_start_index,
            ver_offset=3)

        # fill the table header cells
        hor_offset = 0
        for hdr_cell_name in MAIN_COL_HEADERS:
            cell_index = apply_offset_to_cell_index(hdr_cell_index,
                hor_offset=hor_offset)
            add_hdr_cell(wsh, cell_index, hdr_cell_name)
            hor_offset += 1

        ver_offset = 4
        for val_cell_def in row_def:
            # define row name and 6 empty data cells
            var_start_index = apply_offset_to_cell_index(species_start_index,
                ver_offset=ver_offset)
            add_hor_data_cell_pair(wsh, var_start_index,
                                   val_cell_def, num_data_cells=6)

            # fill the data cells
            toml_path = val_cell_def[2].replace('<count>', f'{species_index}')

            add_value_at_offset(wsh, toml_data, var_start_index,
                                toml_path, 1, 'input_file')
            add_value_at_offset(wsh, toml_data, var_start_index,
                                toml_path, 2, 'file_variable')
            add_value_at_offset(wsh, toml_data, var_start_index,
                                toml_path, 3, 'dimension_order')
            add_value_at_offset(wsh, toml_data, var_start_index,
                                toml_path, 4, 'scale_factor')
            add_value_at_offset(wsh, toml_data, var_start_index,
                                toml_path, 5, 'shift')
            add_value_at_offset(wsh, toml_data, var_start_index,
                                toml_path, 6, 'fixed_value')
            lower_right_corner_index = \
              apply_offset_to_cell_index(var_start_index, hor_offset=6)
            ver_offset += 1

        # add some annotations

        if species_index == 1:
            cell_index = species_start_index
            ann_cell_index = apply_offset_to_cell_index(cell_index,
                                                        ver_offset=-1)
            wsh[ann_cell_index] = ('Here follows a list of identical blocks, '+
                                'one for each species, devided by a single '+
                                'empty line.')
        #wsh[ann_cell_index].font = Font(bold=True)

        # species_ID annotation
        ann_cell_index = apply_offset_to_cell_index(species_start_index,
            hor_offset=7, ver_offset=8)
        wsh[ann_cell_index] = ' Only used for the generic species type'

        # Species_to_include annotation
        ann_cell_index = apply_offset_to_cell_index(species_start_index,
            hor_offset=7, ver_offset=9)
        wsh[ann_cell_index] = ' Only used for the generic species type'

        # WC annotation
        ann_cell_index = apply_offset_to_cell_index(species_start_index,
            hor_offset=7, ver_offset=10)
        wsh[ann_cell_index] = (' This is lwc for water, iwc for ice '+
                            '(not used for aerosol). '+
                            'It is only used if Extinction is not defined')

        # Reff annotation
        ann_cell_index = apply_offset_to_cell_index(species_start_index,
            hor_offset=7, ver_offset=11)
        wsh[ann_cell_index] = ' Only used if Extinction is not defined'

        # Extinction annotation
        ann_cell_index = apply_offset_to_cell_index(species_start_index,
            hor_offset=7, ver_offset=12)
        wsh[ann_cell_index] = (' May be left undefined, in which case '+
                            'it will be estimated using the provided '+
                            'WC and Reff values.')

        ulc_index = \
          apply_offset_to_cell_index(species_start_index, ver_offset=4)
        lrc_index = lower_right_corner_index

        list_of_ulc_lrc.append((ulc_index, lrc_index))

        # move 17 rows down for the next species
        species_start_index = apply_offset_to_cell_index(species_start_index,
                                                     ver_offset=17)

    # color background of some blocks gray
#    cells_to_color = [
#        14,15,16,17,18,19,20, # water
#        40,41,42,43,44,45,46, # IWC_1
#        54,55,56,57,58,59,60, # IWC_3
#        67,68,69,70,71,72,    # AER_1
#        79,80,81,82,83,84,    # AER_3
#        ]
#    cell_index = upper_left_corner_index
#    fill = PatternFill(start_color=GRAY, end_color=GRAY, fill_type = "solid")
#    for cell_index_offset in cells_to_color:
#        color_cell_index = apply_offset_to_cell_index(cell_index,
#                                 ver_offset=cell_index_offset)
#        wsh[color_cell_index].fill = fill

#    ulc_index = \
#      apply_offset_to_cell_index(upper_left_corner_index, ver_offset=1)

    return list_of_ulc_lrc
    #  #]

def create_config_file(config_file_in, config_file_out):
    #  #[ create the file
    ''' the main routine to create the config file
    and its formatting
    '''

    # load the input values from the TOML input config file
    with open(config_file_in, 'rb') as fd_toml:
        toml_data = tomllib.load(fd_toml)

    wbk = Workbook()
    wsh = wbk.active

    # left half of worksheet
    ulc_index, lrc_index = add_version(wsh)
    adapt_column_width_to_cell_content(wsh, ulc_index, lrc_index)

    ulc_index, lrc_index = add_table_positions(wsh, toml_data)
    adapt_column_width_to_cell_content(wsh, ulc_index, lrc_index)

    ulc_index, lrc_index = add_lidar_params(wsh, toml_data)
    adapt_column_width_to_cell_content(wsh, ulc_index, lrc_index)

    ulc_index, lrc_index = add_domain(wsh, toml_data)
    adapt_column_width_to_cell_content(wsh, ulc_index, lrc_index)

    ulc_index, lrc_index = add_qc_params(wsh, toml_data)
    adapt_column_width_to_cell_content(wsh, ulc_index, lrc_index)

    # right half of worksheet
    ulc_index, lrc_index = add_file_list(wsh, toml_data)
    #adapt_column_width_to_cell_content(wsh, ulc_index, lrc_index)
    merge_file_table_cells(wsh, ulc_index, lrc_index)

    ulc_index, lrc_index = add_meteo_variables(wsh, toml_data)
    adapt_column_width_to_cell_content(wsh, ulc_index, lrc_index)

    add_species_count(wsh, toml_data)

    list_of_ulc_lrc = add_main_variables(wsh, toml_data)
    for ulc_index, lrc_index in list_of_ulc_lrc:
        adapt_column_width_to_cell_content(wsh, ulc_index, lrc_index)

    # note that there seems a maximum lenght for the sheet title
    # maxlen=31:1234567890123456789012345678901
    wsh.title = "Sheet1"
    #wsh.title = "CARDINAL Lidar module config"

    # some formatting for the spreadsheet

#    adapt_column_width_to_cell_content(wsh, ulc_index=None, lrc_index=None)

#    adapt_column_width_to_cell_content(wsh, mv_ulc_index, mv_lrc_index)

    # set the zoom level of the worksheer
    wsh.sheet_view.zoomScale = 110

    wbk.save(config_file_out)
    #  #]

if __name__ == '__main__':
    #  #[ main script
    try:
        CONFIG_FILE_IN = sys.argv[1] # 'config_GEM.toml'
        CONFIG_FILE_OUT = sys.argv[2] # 'test_spreadsheet.xlsx'
    except IndexError:
        print('')
        print('Error: missing commandline inputs.')
        print('')
        print('Usage:')
        print(f'{sys.argv[0]} '+
              '<config_input_file.toml> <config_output_file.xlsx>')
        print('')
        sys.exit(1)

    create_config_file(CONFIG_FILE_IN, CONFIG_FILE_OUT)
    #  #]

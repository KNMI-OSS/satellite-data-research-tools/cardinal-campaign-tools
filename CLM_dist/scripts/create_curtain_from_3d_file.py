#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 15-Feb-2024
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

'''
a script to extract profiles from a 3D simulated product along a trajectory
so generating a curtain of simulated profiles.
This is needed to allow exporting the simulation results from 3D model
data to EarthCARE formatted files and to allow curtain plots to be generated.

Possible options to define such a curtain are:
* a linear path, defined by a start and stop location
* a file holding lat,lon values, for example an aircraft trajectory.
'''
#  #]
#  #[ imported modules
import os
import sys
import getopt
import datetime
import numpy as np
from numpy import dtype

# this import gives an error with pylint, even if I have installed netCDF4
# so disable checking for it.

# pylint: disable=no-name-in-module
from netCDF4 import Dataset
# pylint: enable=no-name-in-module

# this import gives an error with pylint, even if I have installed geopy
# so disable checking for it.

# pylint: disable=import-error
from geopy.distance import geodesic
# pylint: enable=import-error

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.netcdf_handler import NetCdfFileHandler
# pylint: enable=wrong-import-position, import-error

#  #]

DT_FORMAT = '%Y%m%dT%H%M%S'

def get_trajectory_indices_for_linear_path(settings):
    #  #[ compose a linear trajectory
    '''
    this function defines the ilat,ilon index values along
    a linear path defined by a start and stop location.
    '''
    traj_start_index_pair = settings['traj_start_index_pair']
    traj_stop_index_pair = settings['traj_stop_index_pair']
    ilat1 = float(traj_start_index_pair[0])
    ilat2 = float(traj_stop_index_pair[0])
    ilon1 = float(traj_start_index_pair[1])
    ilon2 = float(traj_stop_index_pair[1])

    # calculate number of profiles to be reported
    path_len = int(np.sqrt((ilat1-ilat2)**2+(ilon1-ilon2)**2))

    # calculate lat,lon values along this path
    ilat_path = np.zeros(path_len, dtype=int)
    ilon_path = np.zeros(path_len, dtype=int)
    a_par = (ilat2-ilat1)/path_len
    b_par = (ilon2-ilon1)/path_len
    for index in range(path_len):
        ilat_path[index]=int(ilat1+a_par*index)
        ilon_path[index]=int(ilon1+b_par*index)

    # select_profiles_along_path
    return ilat_path, ilon_path, path_len
    #  #]

def create_curtain(settings):
    #  #[ create 2d curtain
    ''' create a 2d curtain from a 3d field '''
    input_file_3d = settings['inputfile']
    output_file_2d = settings['outputfile']

    try:
        ncf = Dataset(input_file_3d)
    except FileNotFoundError:
        print('Can not open ', input_file_3d)
        raise

    ilat_path, ilon_path, path_len = \
      get_trajectory_indices_for_linear_path(settings)

    # extract lat,lon values from the 3D input files (these should be 2D fields)
    lat = np.array(ncf['Latitude'])
    lon = np.array(ncf['Longitude'])

    print('Properties for the 3D field:')
    print('np.shape(lat) = ', np.shape(lat))
    print('np.shape(lon) = ', np.shape(lon))
    print('min/max latitude = ', np.min(lat), 'np.max(lat) = ', np.max(lat))
    print('min/max longitude = ', np.min(lon), 'np.max(on) = ', np.max(lon))
    print('path_len = ', path_len)

    # get num_levels (ATB_Mie is not further used here)
    array_3d = np.array(ncf['ATB_Mie'])
    num_levels = len(array_3d[:,0,0])

    # instantiate the NetCdfFileHandler to create the 2D output file
    ncfh = NetCdfFileHandler([], output_file_2d)
    ncfh.define_netcdf_file(num_x=path_len, num_y=0, num_z=num_levels)

    # all 1d variables stay as they are. These are: altitude_mid, range_mid
    ncfh.sattelite_range_mid_var[:] = np.array(ncf['range_mid'])
    ncfh.altitude_mid_var[:] = np.array(ncf['altitude_mid'])

    #
    # the 2d variables Latitude and Longitude are reduced to 1d
    #
    latitude_of_sensor = np.zeros(path_len)
    longitude_of_sensor = np.zeros(path_len)
    for index in range(path_len):
        latitude_of_sensor[index] = lat[ilon_path[index], ilat_path[index]]
        longitude_of_sensor[index] = lon[ilon_path[index], ilat_path[index]]

    #print('latitude_of_sensor = ', latitude_of_sensor)
    #print('longitude_of_sensor = ', longitude_of_sensor)

    ncfh.lat_of_sensor_var[...] = latitude_of_sensor[:]
    ncfh.lon_of_sensor_var[...] = longitude_of_sensor[:]

    # to simulate satellite data assume that the trajectory points
    # are observed by a sensor moving with 7 km/s

    lat_start = lat[ilon_path[0], ilat_path[0]]
    lon_start = lon[ilon_path[0], ilat_path[0]]
    lat_stop = lat[ilon_path[-1], ilat_path[-1]]
    lon_stop = lon[ilon_path[-1], ilat_path[-1]]
    start_location = (lat_start, lon_start)
    stop_location = (lat_stop, lon_stop)
    print('trajectory start location = ', start_location)
    print('trajectory stop location = ', stop_location)
    path_distance = geodesic(start_location, stop_location).kilometers
    print('path distance = ', path_distance)

    sat_velocity = 7.0 # [km/s]
    time_diff = path_distance/sat_velocity # [s]

    print('time diff along trajectory = ', time_diff,
          '(assuming a sat. velocity of 7 km/s)')
    time_step = time_diff/(path_len-1)
    print('time_step = ', time_step)
    time_diff_array = [datetime.timedelta(seconds=index*time_step)
                       for index in range(path_len)]
    time_diff_array = np.array(time_diff_array)
    print('time_diff_array[-1] = ', time_diff_array[-1])

    # trajectory start datetime
    traj_start_dt = settings['traj_start_dt']
    datetime_array = traj_start_dt + time_diff_array
    print('datetime_array[0] = ', datetime_array[0])
    print('datetime_array[-1] = ', datetime_array[-1])

    first_dt = datetime_array[0]
    ref_dt = datetime.datetime(
        first_dt.year, first_dt.month, first_dt.day, 0, 0, 0)

    time = [(t - ref_dt).total_seconds()/3600. for t in datetime_array]
    ncfh.time_var[:] = np.array(time)
    var_unit = (f'hours since {first_dt.year}-{first_dt.month}-{first_dt.day} '+
                '00:00:00 +00:00')
    ncfh.time_var.setncattr_string('units', var_unit)

    # all 3d variables are reduced to 2d

    # loop over all variables in the input file
    for var_name in ncf.variables.keys():

        # special 1D cases
        if var_name in ['range_mid', 'altitude_mid']:
            # these 2 have already been exported above
            continue

        # special 2D cases
        if var_name in ['Latitude', 'Longitude']:
            # these are exported above as latitude/longitude_of_sensor
            continue

        array_3d = np.array(ncf[var_name])
        #num_levels = len(array_3d[:,0,0])

        # finally reduce the 3D field to a 2D curtain
        array_2d = np.zeros([num_levels, path_len])
        for index in range(path_len):
            array_2d[:,index] = array_3d[:, ilon_path[index], ilat_path[index]]
        if var_name == 'ATB_Mie':
            ncfh.atb_mie_var[...] = array_2d[...]
        elif var_name == 'ATB_Ray':
            ncfh.atb_ray_var[...] = array_2d[...]
        elif var_name == 'ATB_Mie_co':
            ncfh.atb_mie_co_var[...] = array_2d[...]
        elif var_name == 'ATB_Ray_co':
            ncfh.atb_ray_co_var[...] = array_2d[...]
        elif var_name == 'ATB_cr':
            ncfh.atb_cr_var[...] = array_2d[...]
        elif var_name == 'ATB_Mie_cr':
            ncfh.atb_mie_cr_var[...] = array_2d[...]
        elif var_name == 'MS_SS_ratio':
            ncfh.ms_ss_ratio[...] = array_2d[...]
        elif var_name == 'linear_particulate_depolarization_ratio':
            ncfh.part_depol_var[...] = array_2d[...]
        elif var_name == 'temperature':
            ncfh.temperature_var[...] = array_2d[...]
        elif var_name == 'pressure':
            ncfh.pressure_var[...] = array_2d[...]
        elif var_name == 'u_wind':
            ncfh.u_wind_var[...] = array_2d[...]
        elif var_name == 'v_wind':
            ncfh.v_wind_var[...] = array_2d[...]
        elif var_name == 'molecular_extinction':
            ncfh.ray_ext_var[...] = array_2d[...]
        elif var_name == 'molecular_rayleigh_backscatter':
            ncfh.ray_beta_var[...] = array_2d[...]
        else:
            if var_name.startswith('Extinction_'):
                nc_var_unit = 'm-1'
            elif var_name.startswith('Reff_'):
                nc_var_unit = 'microns'

            # print('DEBUG: creating variable with name: ', var_name)
            var = ncfh.ncfile.createVariable(
                var_name, dtype('float64').char, ncfh.dims)
            var.setncattr_string('units', nc_var_unit)
            var[...] = array_2d[...]

    ncfh.close()
    ncf.close()
    #  #]

def print_usage_and_stop(error_txt=None):
    #  #[ print some help
    '''
    print a help message, and if needed an error message,
    then stop the script.
    '''
    script_name = sys.argv[0]
    print('')
    print(f'USAGE: {script_name} [OPTIONS]')
    print('')
    print('Explanation of the options:')
    print('    -h/--help')
    print('           Display this help message.')
    print('    -i/--inputfile <input filename>')
    print('           Define the name of the input NetCDF file to use')
    print('           for 3D fields (required).')
    print('    -o/--outputfile <output filename>')
    print('           Define the name of the output NetCDF file to use')
    print('           for the 2D/curtain fields (required).')
    print('    --traj_start <ilat1,ilon1>')
    print('           Define the coordinate (index values) for the start')
    print('           point for the surface trajectory to use')
    print('           through the 3D field (required).')
    print('    --traj_stop <ilat2,ilon2>')
    print('           Define the coordinate (index values) for the stop')
    print('           point for the surface trajectory to use')
    print('           through the 3D field (required).')
    print(f'    --traj_start_dt <datetime, format:{DT_FORMAT}>')
    print('           Define a start datetime to be used to define a')
    print('           datetime coordinate along the trajectory')
    print('           (assuming a satellite speed of 7 km/s) (required).')
    print('')

    if error_txt is not None:
        print('')
        print('ERROR: '+error_txt)
        print('')
        sys.exit(1)
    else:
        sys.exit(0)
    #  #]

def handle_commandline_settings():
    #  #[ handle commandline inputs
    '''
    Process the command line arguments
    '''

    # init to None
    inputfile = None
    outputfile = None

    # start/stop index pairs for surface trajectory to use
    traj_start = None
    traj_stop = None

    # start datetime value for the trajectory
    traj_start_dt = None

    # note: the ':' and '=' options must have a value following it
    short_options = 'hi:o:'
    long_options = ['help',
        'inputfile=', 'outputfile=', 'traj_start=', 'traj_stop=',
        'traj_start_dt=']

    args = sys.argv[1:]
    try:
        options = getopt.getopt(args, short_options, long_options)[0]
    except getopt.GetoptError as err:
        print_usage_and_stop(f'invalid option in argument list: {err}')

    for opt, value in options:
        if opt in ['-h', '--help']:
            print_usage_and_stop()
        elif opt in ('-i', '--inputfile'):
            inputfile = value
        elif opt in ('-o', '--outputfile'):
            outputfile = value
        elif opt == '--traj_start':
            traj_start = [int(i) for i in value.split(',')]
        elif opt == '--traj_stop':
            traj_stop = [int(i) for i in value.split(',')]
        elif opt =='--traj_start_dt':
            traj_start_dt = datetime.datetime.strptime(value, DT_FORMAT)
        else:
            # this point should never be reached
            # (it would be a programming error if it still gets here)
            print('Unhandled option: ', opt, value)
            sys.exit(1)

    # some sanity checks
    if inputfile is None:
        print_usage_and_stop(error_txt='inputfile not defined')
    if outputfile is None :
        print_usage_and_stop(error_txt='outputfile not defined')
    if traj_start is None:
        print_usage_and_stop(error_txt='traj_start not defined')
    if traj_stop is None:
        print_usage_and_stop(error_txt='traj_stop not defined')
    if traj_start_dt is None:
        print_usage_and_stop(error_txt='traj_start_dt not defined')

    # combine settings in a dict for later use by the program
    settings = {}
    settings['inputfile'] = inputfile
    settings['outputfile'] = outputfile
    settings['traj_start_index_pair'] = traj_start
    settings['traj_stop_index_pair'] = traj_stop
    settings['traj_start_dt'] = traj_start_dt

    print('Selected settings are:')
    for key, value in settings.items():
        print(f'({key} = {value}')

    return settings
    #  #]

def main():
    #  #[ main script
    ''' main script '''
    settings = handle_commandline_settings()
    create_curtain(settings)
    #  #]

if __name__ == '__main__':
    main()

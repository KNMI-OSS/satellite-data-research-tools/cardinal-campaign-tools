#!/usr/bin/env python3

#  #[ documentation

#   Created on 24-May-2023 as bash script
#   Converted on 10-Nov-2023 to python
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# a small python script to be able to run pytest
# on different installations where the tool might have
# a different name, i.e.:
# * on Fedora it is called pytest-3 (my current devel. machine)
# * on debian it is called pytest   (as used by gitlab ci/cd)

'''
a small tool to check for different possible pytest tool names.
It checks in the paths found by the /usr/bin/env command
so this should also include virtualenv, anaconda, miniconda and other
similar user defined installations of tools and python modules.
'''

#  #]
#  #[ imported modules
import os
import sys

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.helpers import run_shell_command
# pylint: enable=wrong-import-position, import-error

#  #]
#  #[ settings

# note that the below get_pytest_tool function which check
# if it can execute all these options, and will select
# the last valid one for execution of the unit tests.
PYTEST_TOOL_NAMES = ['pytest', 'pytest3', 'pytest-3']

#  #]

def get_pytest_tool():
    #  #[ find pytest
    '''
    use the env command to find the name of the pytest tool to be used
    '''

    pytest_tool_name = None
    for tool_name in PYTEST_TOOL_NAMES:
        env_cmd = f'/usr/bin/env {tool_name} -h'
        exit_code = run_shell_command(env_cmd)[2]
        if exit_code != 0:
            print(f'name check: {tool_name} not found')
        else:
            pytest_tool_name = tool_name
            print(f'name check: {pytest_tool_name} found')

    if pytest_tool_name is None:
        print('Sorry, pytest seems not installed')
        sys.exit(1)

    return pytest_tool_name
#  #]

if __name__ == '__main__':
    pytest_tool = get_pytest_tool()
    cmd = f'/usr/bin/env {pytest_tool} | tee unittest_logfile.txt'
    print(f'Executing: {cmd}')
    os.system(cmd)

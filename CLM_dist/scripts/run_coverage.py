#!/usr/bin/env python3

#  #[ documentation

#   Created on 20-Jun-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

'''
A small script to be able to run the coverage tool
on pytest test runs (but only on the developer machine
and in the gitlab CI pipeline).

It should allow for different installations where the pytest and coverage
tools might have a different name, i.e.:
* on Fedora it is called pytest-3 (my current devel. machine)
* on debian it is called pytest   (as used by gitlab ci/cd)
'''
#  #]
#  #[ imported modules
import os
import sys
import shutil
#  #]

def get_pytest_name():
    #  #[ get pytest name
    ''' get the name of the pytest tool '''
    pytest_name_1 = '/usr/bin/pytest-3'
    pytest_name_2 = '/usr/bin/pytest'

    pytest_name = None
    if os.path.exists(pytest_name_1):
        pytest_name = pytest_name_1
    elif os.path.exists(pytest_name_2):
        pytest_name = pytest_name_2
    return pytest_name
    #  #]

def get_coverage_name():
    #  #[ get coverage name
    ''' get the name of the coverage tool '''
    coverage_name_1 = '/usr/bin/coverage3'
    coverage_name_2 = '/usr/bin/coverage'
    coverage_name_3 = '/usr/bin/python3-coverage'
    coverage_name_4 = '/usr/bin/python-coverage'

    coverage_name = None
    if os.path.exists(coverage_name_1):
        coverage_name = coverage_name_1
    elif os.path.exists(coverage_name_2):
        coverage_name = coverage_name_2
    elif os.path.exists(coverage_name_3):
        coverage_name = coverage_name_3
    elif os.path.exists(coverage_name_4):
        coverage_name = coverage_name_4

    return coverage_name
    #  #]

def execute_coverage_check(coverage_tool, pytest_tool):
    #  #[ run coverage
    ''' execute the coverage tool on the pylint test run '''

    # see:
    # https://coverage.readthedocs.io/en/coverage-5.0.4/cmd.html#xml-reporting
    # and:
    # https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html
    # and:
    # https://app.codecov.io/gh/pyproj4/pyproj

    # for Fedora coverage is part of the package python3-coverage
    # for debian it will be part of the package python3-coverage
    #                                   or maybe python-coverage

    success = True
    file_to_remove = '.coverage'
    if os.path.exists(file_to_remove):
        os.remove(file_to_remove)

    dir_to_remove = 'coverage_html'
    if os.path.exists(dir_to_remove):
        shutil.rmtree(dir_to_remove)

    cmd0 = 'echo pwd=`pwd`'
    cmd1 = f'{coverage_tool} run --source=lib {pytest_tool}'
    cmd2 = f'{coverage_tool} report | tee coverage_logfile.txt'
    #cmd2 = f'{coverage_tool} report --fail-under=80| tee coverage_logfile.txt'
    cmd3 = f'{coverage_tool} html -d coverage_html'

    for cmd in [cmd0, cmd1, cmd2, cmd3]:
        print('executing: ', cmd)
        os.system(cmd)

    return success
    #  #]

def exit_with_debug_prints(coverage_tool, pytest_tool):
    #  #[ exit
    ''' exit and print some hopefully useful output '''
    # debug
    print('coverage_tool = ', coverage_tool)
    print('pytest_tool   = ', pytest_tool)
    sys.exit(1)
    #  #]

def main():
    #  #[ main
    ''' run the main code '''
    coverage_tool = get_coverage_name()
    pytest_tool = get_pytest_name()

    result = os.system(f'{coverage_tool} -h > /dev/null')
    if result != 0:
        # coverage seems not to work
        exit_with_debug_prints(coverage_tool, pytest_tool)

    success = execute_coverage_check(coverage_tool, pytest_tool)
    if not success:
        exit_with_debug_prints(coverage_tool, pytest_tool)
    #  #]

if __name__ == '__main__':
    # skip this script in case it is not run on the developers machine
    machine_name = os.uname()[1]
    if not 'pc230014' in machine_name:
        print('skipping coverage check')
        sys.exit(0)

    # run the coverage check
    main()

#!/usr/bin/env python3

#  #[ documentation

#   Created on 24-May-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
a small python script to be able to run pylint of each python file
on different installations where the tool might have
a different name, i.e.:
* on Fedora it is called pylint-3 (my current devel. machine)
* on debian it is called pylint   (as used by gitlab ci/cd)
'''
#  #]
#  #[ imported modules
import os
import sys

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('..')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# pylint: disable=wrong-import-position, import-error
from lib.helpers import run_shell_command
# pylint: enable=wrong-import-position, import-error

#  #]
#  #[ settings
PYLINT_TOOL = 'undefined'
PYLINT_TOOL_OPTION_1 = '/usr/bin/pylint-3'
PYLINT_TOOL_OPTION_2 = '/usr/bin/pylint'

if os.path.exists(PYLINT_TOOL_OPTION_1):
    PYLINT_TOOL = PYLINT_TOOL_OPTION_1
elif os.path.exists(PYLINT_TOOL_OPTION_2):
    PYLINT_TOOL = PYLINT_TOOL_OPTION_2
else:
    print('Sorry, pylint seems not installed')
    sys.exit(1)

STARTDIR = './'
OPT = '--fail-under=9.0'
#  #]
#  #[ commandline handling
score_limit = None
try:
    score_limit = float(sys.argv[1])
except IndexError:
    pass

if score_limit:
    OPT = f'--fail-under={score_limit}'
    print('Using updated score limit: ', OPT)
else:
    print('Using default score limit: ', OPT)
#  #]

failed_files = []
passed_files = []
for (dirpath, dirnames, filenames) in os.walk(STARTDIR):
    for fname in filenames:
        path_and_filename = os.path.join(dirpath, fname)
        basename, ext = os.path.splitext(fname)
        if ext == '.py':
            print('pylinting file: ', fname)
            cmd = f'{PYLINT_TOOL} {path_and_filename} {OPT}'
            result = run_shell_command(cmd)
            (lines_stdout, lines_stderr, exit_status) = result

            if exit_status == 0:
                passed_files.append(path_and_filename)
            else:
                print('='*50)
                print(f'PYLINT FAILED ON FILE: {path_and_filename}')
                print('='*50)
                failed_files.append(path_and_filename)

                # extract some text to be printed
                for line in lines_stdout:
                    if 'Your code has been rated' in line:
                        print(line.strip())
                print('-'*50)
                #print('lines_stdout = ', lines_stdout)

print('-'*50)
print('pylint overview:')
print(f'{len(passed_files)} passed the pylint test')
print(f'{len(failed_files)} failed the pylint test')
print(f'pylint threshold was: {OPT}')
print('-'*50)
for fname in failed_files:
    print(f'failed file: {fname}')

if len(failed_files) > 0:
    sys.exit(1)

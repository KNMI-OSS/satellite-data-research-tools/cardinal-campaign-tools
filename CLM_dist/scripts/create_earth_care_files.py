#!/usr/bin/env python3

#  #[ documentation
#
#   Copyright (C) 2023-2024  KNMI
#
#   Author: Gerd-Jan van Zadelhoff <gerd-jan.van.zadelhoff@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
'''
a script to reformat the NetCDF output generated by the lidar_module.py
script to Earth Care formatted files.
'''
#
#  #]

#  #[ imported modules
import os
import sys
import shutil
import re
import glob
import getopt
from datetime import datetime, timedelta, time
import datatree as dt
import h5py
import numpy as np
from scipy.interpolate import interp1d

# allow importing the lib modules
sys.path.insert(0, os.path.abspath('..'))
sys.path.insert(0, os.path.abspath('.'))
#print('sys.path = ', sys.path)

#from common_anom_xmet_xjsg import *
# pylint: disable=wrong-import-position, import-error
from lib.common_anom_xmet_xjsg import (
    copy_create_output_files, create_lat_lon_slice, edit_anom_file,
    edit_x_met, edit_x_jsg)
from lib.instrument_module import add_error_atlid, add_error_estimates
from lib.custom_exceptions import FileGenerationError
# pylint: enable=wrong-import-position, import-error

#  #]
#  #[ default settings
DEFAULT_LIDAR_COUNT = 2 # num. of profiles to skip while creating x_jsg
DEFAULT_INDEX = '000'
DEFAULT_OVERPASS_TIME = '03:00:00' # 'hh:mm:ss'
DEFAULT_INSTRUMENT_MODULE = 'atlid' # "random" or 'atlid'
DEFAULT_OUTPUT_DIR = 'output_atlid'
DEFAULT_FIXED_DAY = None # "%Y%m%d" or None
DEFAULT_TEMPLATES_FOLDER = './templates'
DEFAULT_INSTR_DATA_FOLDER = './'
DEFAULT_ADD_NOISE = True
DEFAULT_ADVECT_VELOCITY = 7.0
#  #]
#  #[ constant settings

# Definition of the used auxiliry files needed to have
# the PDGS file structure for
# the ATL_NOM (nominal ATLID L1B output)
# the AUX_MET/XMET (meteo data inputs)
# and AUX_JSG (joint standard grid inputs)

TEMPLATE_ANOM_PATTERN = 'ECA_EXAA_ATL_NOM_1B_20241231T183449Z_*_39316D'
TEMPLATE_XMET_PATTERN = 'ECA_EXAA_AUX_MET_1D_20241231T183312Z_*_39316D'
TEMPLATE_XJSG_PATTERN = 'ECA_EXAA_AUX_JSG_1D_20241231T183435Z_*_39316D'

#  #]

def is_valid_time_string(time_str):
    ''' check for a valid time string '''
    pattern = r'^\d{2}:\d{2}:\d{2}$'
    return bool(re.match(pattern, time_str))

# to do:
# split this one in 2 functions: get_day_night_flag + construct_dt_array
def calc_day_night_flag(inp_time, settings):
    #  #[ datetime juggling
    '''
    convert the inp_time offset array to an array holding actual
    datetime objects. Als decide if we are during night or day.
    Finally returns the array index closest to the provided overpass_time.

    returns: day_night_flag, dt_objects, closest_index
    '''

    # get the reference datetime from the units attribute
    attribute_value = inp_time.attrs['units']
    attribute_value = str(attribute_value[0])
    reference_datetime_str = (attribute_value.
                              split('since ')[1].
                              split('+')[0].strip())
    reference_datetime = datetime.strptime(reference_datetime_str,
                                           '%Y-%m-%d %H:%M:%S')

    # convert the reference date to a string
    ref_date_str = reference_datetime.strftime('%Y-%m-%d')

    # convert inp_time from hours to seconds
    inp_time_seconds = np.array(inp_time)*3600.

    # construct a numpy array holding actual datetimes
    dt_objects = np.array(
        [reference_datetime + timedelta(seconds=delta_sec)
         for delta_sec in inp_time_seconds])

    if settings['overpass_time'] is None:
        # If no t0 is provided take the center point to evaluate the data
        n_along=np.size(inp_time_seconds)
        closest_index = int(n_along/2)
    else:
        # construct a reference datetime by glueing together
        # the date from the units attribute with the provided overpass_time
        overpass_dt_str = ref_date_str+' '+settings['overpass_time']
        overpass_dt = datetime.strptime(overpass_dt_str, '%Y-%m-%d %H:%M:%S')

        # Find the index of the minimum difference to the provided overpass_time
        time_diff = np.abs(dt_objects - overpass_dt)
        closest_index = np.argmin(time_diff)

    day_threshold = time(6,0,0)   # 6:00:00 AM
    night_threshold = time(18,0,0) # 6:00:00 PM

    time_only = dt_objects[closest_index].time()

    if day_threshold < time_only < night_threshold:
        day_night_flag = "day"
    else:
        day_night_flag = "night"

    return day_night_flag, dt_objects, closest_index
    #  #]

def get_latitude_band_id(day_night_flag, lat_sensor):
    #  #[ defines the frame (orbit lat. section) ID code
    '''
    Definition of the EarthCARE frames in latitude bins.
    Note that EarthCARE is descending and therefore moves down during daytime
    We only check the starting point.
    '''
    orbit_val={
        'A':[-22.5,22.5],  # asc
        'B':[22.5,65.5],   # asc
        'C':[65.5,90],     # asc
        'D':[65.5,22.5],   # desc
        'E':[22.5,-22.5],  # desc
        'F':[-22.5,-65.5], # desc
        'G':[-65.5,-90],   # desc
        'H':[-65,5,-22.5]} # asc

    # the next code finds the latitude band (A up to H)
    # for the given lat band boundaries and sets frame_id accordingly

    frame_id = None
    if day_night_flag == 'night':
        # find the orbit key for ascending (nighttime) flights
        for key, value in orbit_val.items():
            if value[0] <= lat_sensor <= value[1]:
                frame_id = key
                break
    elif day_night_flag  == 'day':
        # find the orbit key for descending (daytime) flights
        for key, value in orbit_val.items():
            if value[1] <= lat_sensor <= value[0]:
                frame_id = key
                break
    else:
        print('Night or day has not been defined correctly')
        sys.exit(1)

    if frame_id is None:
        error_txt = ('error in get_latitude_band_id: could not determine '+
            f'frame_id for lat_sensor = {lat_sensor}')
        raise FileGenerationError(error_txt)

    return frame_id
    #  #]

def create_ec_files(simulated_data, settings):
    #  #[ convert a multiscatter nc output file to ANOM
    '''
    convert a multiscatter nc output file to ANOM
    '''
    
    h5_f = h5py.File(simulated_data)

    range_mid = np.array(h5_f['range_mid'])
    altitude_mid=np.array(h5_f['altitude_mid'])

    sat_alt=  range_mid[0]+  altitude_mid[0]
    time_arr_hours = h5_f['Time']
    time_arr_seconds = np.array(time_arr_hours)*3600.

    atb_mie = np.transpose(np.array(h5_f['ATB_Mie']))
    atb_ray = np.transpose(np.array(h5_f['ATB_Ray']))
    atb_mie_co = np.transpose(np.array(h5_f['ATB_Mie_co']))
    atb_mie_cr = np.transpose(np.array(h5_f['ATB_Mie_cr']))
    temp = np.transpose(np.array(h5_f['temperature']))
    pres = np.transpose(np.array(h5_f['pressure']))
    try:
        ext_water=np.transpose(np.array(h5_f['Extinction_water']))
    except KeyError:
        # no water in the incoming nc product!
        ext_water = atb_mie.copy()*0.0

    try:
        ext_ice1=np.transpose(np.array(h5_f['Extinction_ice_1']))
    except KeyError:
        # no ice in the incoming nc product!
        ext_ice1 = atb_mie.copy()*0.0

    lon_sen=np.array(h5_f['longitude_of_sensor'])
    lat_sen=np.array(h5_f['latitude_of_sensor'])

    wind_info = None
    vwind = None
    uwind = None
    try:
        vwind=np.array(h5_f['vwind'])
        uwind=np.array(h5_f['uwind'])
        wind_info=1
    except KeyError:
        pass

    ext=np.copy(ext_water)*0.0
    ext=ext_water+ext_ice1

    n_along=np.size(time_arr_hours)

    day_night_flag, time_final, closest_index = \
      calc_day_night_flag(time_arr_hours, settings)

    frame_id = get_latitude_band_id(day_night_flag, lat_sen[0])

    orbit = '21'+settings['index']
    if settings['fixed_day']:
        # set the creation datetime to a fixed value to ensure
        # we get reproducible output filenames
        # (this makes running automated tests easier)
        print('Using fixed day: ', settings['fixed_day'])
        creation_datetime = settings['fixed_day']
    else:
        creation_datetime = datetime.utcnow()

    orbit_id=orbit+frame_id

    # determine datetime of first and last datapoint
    start_datetime = time_final[0]
    stop_datetime = time_final[-1]

    # date_format = "%Y%m%dT%H%M%S" # not used
    reference_date = datetime(2000, 1, 1, 0, 0, 0)
    #
    # construct timediff around reference_date
    delta1 = time_final - reference_date
    #
    seconds1 = np.array([delta.total_seconds() for delta in delta1])
    #

    dir1 = settings['output_dir']
    # check existence of output directory and create one if needed
    #
    try:
        os.makedirs(dir1)
    except FileExistsError:
        # directory already exists
        pass

    templates_folder = settings['templates_folder']

    template_anom_pattern = os.path.join(templates_folder,
                                         TEMPLATE_ANOM_PATTERN+'.h5')
    template_xmet_pattern = os.path.join(templates_folder,
                                         TEMPLATE_XMET_PATTERN+'.h5')
    template_xjsg_pattern = os.path.join(templates_folder,
                                         TEMPLATE_XJSG_PATTERN+'.h5')

    template_file_anom = glob.glob(template_anom_pattern)[0]
    template_file_xmet = glob.glob(template_xmet_pattern)[0]
    template_file_xjsg = glob.glob(template_xjsg_pattern)[0]

    info_local = {}
    info_local['start_datetime']     = start_datetime
    info_local['stop_datetime']      = stop_datetime
    info_local['creation_datetime']  = creation_datetime
    info_local['orbitID']            = orbit_id
    info_local['output_dir']         = dir1
    info_local['Frame_ID']           = frame_id
    info_local['orbit']              = orbit
    info_local['template_file_anom'] = template_file_anom
    info_local['template_file_xmet'] = template_file_xmet
    info_local['template_file_xjsg'] = template_file_xjsg

    #
    dirs_and_files = copy_create_output_files(info_local)
    dirout_anom = dirs_and_files[0]
    files_out = dirs_and_files[3]

    #
    # Read in the ANOM sample file
    #
    #

    dfr = dt.open_datatree(template_file_anom)
    #
    # Read in the height and random errors of the three signals
    #
    height_ori = np.array(dfr["ScienceData/sample_altitude"])

    # not used:
    # dmie_ori = np.array(dfr["ScienceData/mie_attenuated_backscatter_total_error"])
    #
    d_rand_mie_ori = np.array(dfr["ScienceData/mie_attenuated_backscatter_random_error"])
    d_rand_cro_ori = np.array(dfr["ScienceData/crosspolar_attenuated_backscatter_random_error"])
    d_rand_ray_ori = np.array(dfr["ScienceData/rayleigh_attenuated_backscatter_random_error"])
    #
    #dmie_ori2=(np.copy(d_rand_mie_ori)) # not used
    #dmie_ori1=(np.copy(dmie_ori))       # not used
    #

    #flat_data1 = dmie_ori1.flatten()   # total error  # never used
    #flat_data2 = dmie_ori2.flatten()   # random error # never used
    #
    #
    # Copy the 1D files to 2D arrays
    #
    height = altitude_mid.copy()
    n_height = np.size(height)

    height_out = np.tile(height, (n_height,1))
    # Copying a height array for all columns

    height2 = np.copy(height_ori[1000,:])

    # define 1D arrays
    surface_height=np.zeros(n_along,dtype=np.float64)

    # this is now default 7 km/s and can be modified by the user.
    # We could also get this info from the model wind fields
    # at 3 km in the future.
    u_wind = float(settings['advect_velocity'])*1.0e+3 # m/s
    v_wind = 0.0 # m/s

    height_index=5
    if wind_info == 1:
        #print(np.shape(height),np.shape(uwind))
        height_index = np.argmin(np.abs(height-3e3))
        u_wind = uwind[height_index,closest_index]
        v_wind = vwind[height_index,closest_index]

    #print('DEBUG: time = ', time)

    # note:
    # along_track is an array filled with distances in km
    # w.r.t. the "closest_index" location
    longitude_out,latitude_out,along_track = create_lat_lon_slice(
        settings['output_dir'], time_arr_seconds,
        closest_index,lat_sen,lon_sen,u_wind,v_wind,dirout_anom,
        settings['overpass_time'],
        plot_overpass=settings['plot_overpass'])

    # Interpolate to Level 1 height fields
    #
    interpolation_dim=1
    #
    interp_func = interp1d(height, atb_mie_co, axis=interpolation_dim,
                           kind='nearest', fill_value="extrapolate")
    atb_mie_co_int = interp_func(height2)
    #
    interp_func = interp1d(height, atb_ray, axis=interpolation_dim,
                           kind='nearest', fill_value="extrapolate")
    atb_ray_int = interp_func(height2)
    #
    interp_func = interp1d(height, atb_mie_cr, axis=interpolation_dim,
                           kind='nearest', fill_value="extrapolate")
    atb_mie_cr_int = interp_func(height2)
    #
    interp_func = interp1d(height, temp, axis=interpolation_dim,
                           kind='nearest', fill_value="extrapolate")
    temp_int = interp_func(height2)
    #
    interp_func = interp1d(height, pres, axis=interpolation_dim,
                           kind='nearest', fill_value="extrapolate")
    pres_int = interp_func(height2)
    #
    interp_func = interp1d(height, ext, axis=interpolation_dim,
                           kind='nearest', fill_value="extrapolate")
    ext_int = interp_func(height2)
    # Define sizes for the output file
    #
    height_out=np.tile(height2, (n_along,1))
    n_height_out=np.size(height_out[0,:])
    #
    # Copy the 1d arrays to 2D
    #
    lat_out = (np.reshape(latitude_out, (-1, 1)) *
               np.ones((1, n_height_out), dtype=latitude_out.dtype))
    lon_out = (np.reshape(longitude_out, (-1, 1)) *
               np.ones((1, n_height_out), dtype=longitude_out.dtype))

    #
    # the sensor altitude is the range+height for each individual pixel
    #
    sensor_alt=sat_alt
    #
    # First go through all 2D arrays and fill in with dummy array
    #
    if settings['instrument_module'] == 'atlid':
        #  call to instrument module for atlid to be designed by Dave
        (d_atb_mie_co_int, d_atb_mie_cr_int, d_atb_ray_int,
         atb_mie_co_int, atb_mie_cr_int, atb_ray_int) = add_error_atlid(
             atb_mie_co_int, atb_mie_cr_int, atb_ray_int)
    elif settings['instrument_module'] == 'random':
        #
        #  create the error arrays using the median of the original L1 file
        # if add_error is not equal to 0 a random normal error is added
        # to the signals as well
        #
        (d_atb_mie_co_int, d_atb_mie_cr_int, d_atb_ray_int,
         atb_mie_co_int, atb_mie_cr_int, atb_ray_int) = add_error_estimates(
             atb_mie_co_int,atb_mie_cr_int, atb_ray_int,
             np.median(d_rand_mie_ori),np.median(d_rand_cro_ori),
             np.median(d_rand_ray_ori),
             settings['instr_data_folder'],
             settings['add_noise'])
        #
    else:
        print('Incorrect defintion of instrumemt module')
        sys.exit(1)

    #
    # the following goes through all keys and changes the size and
    # nullifies these.
    # Not sure why but the writing of vertica (some text seems missing here)
    #
    print('Create the data output structure and call the routine to fill ANOM')
    #
    #
    atb_mie_co_int[np.isinf(atb_mie_co_int) | np.isnan(atb_mie_co_int)] = 1.e-10
    atb_mie_cr_int[np.isinf(atb_mie_cr_int) | np.isnan(atb_mie_cr_int)] = 1.e-10
    atb_ray_int[np.isinf(atb_ray_int) | np.isnan(atb_ray_int)] = 1.e-10
    d_atb_ray_int[ atb_ray_int< -1.e1] = 1.e-7
    atb_ray_int[ atb_ray_int< -1.e1] = 1.e-7

    d_atb_mie_co_int[np.isinf(d_atb_mie_co_int) | np.isnan(d_atb_mie_co_int)] = 1.e-7
    d_atb_mie_cr_int[np.isinf(d_atb_mie_cr_int) | np.isnan(d_atb_mie_cr_int)] = 1.e-7
    d_atb_ray_int[np.isinf(d_atb_ray_int) | np.isnan(d_atb_ray_int)] = 1.e-7

    d_atb_mie_co_int = np.where(atb_mie_co_int <-1.e-2, 1.e-7, d_atb_mie_co_int)
    atb_mie_co_int   = np.where(atb_mie_co_int <-1.e-2, 1.e-10, atb_mie_co_int)
    d_atb_mie_cr_int     = np.where(atb_mie_cr_int <-1.e-2, 1.e-7, d_atb_mie_cr_int)
    atb_mie_cr_int       = np.where(atb_mie_cr_int <-1.e-2, 1.e-10, atb_mie_cr_int)
    d_atb_ray_int = np.where(atb_ray_int <-1.e-2, 1.e-7, d_atb_ray_int)
    atb_ray_int   = np.where(atb_ray_int <-1.e-2, 1.e-10, atb_ray_int)

    #print(' Channel       Co-Polar Mie   Co-Polar RAY, Cross polar             '      )
    #print('Max channel : ',np.max(atb_mie_co_int) ,np.max(atb_ray_int) ,np.max(atb_mie_cr_int))
    #print('Min channel : ',np.min(atb_mie_co_int) ,np.min(atb_ray_int) ,np.min(atb_mie_cr_int))
    #print('Mean_channel: ',np.mean(atb_mie_co_int),np.mean(atb_ray_int),np.mean(atb_mie_cr_int))
    #
    #
    out_h5_file=np.copy(dirout_anom)
    #
    #
    # Note: ATB_cr is now ATB_Mie_cr !!!
    #
    # This needs to be checked 
    #
    reverse_height = True
    #
    if reverse_height:
        data_to_hdf = {
            'n_along':np.size(height_out[:,0]),
            'n_height':np.size(height_out[0,:]),
            'along_track':along_track,
            'temp_int':temp_int[:,::-1],
            'pres_int':pres_int[:,::-1],
            'surface_height':surface_height,
            'extinction':ext_int,
            'latitude':lat_out,
            'longitude':lon_out,
            'height_out':height_out,
            'sensor_alt':sensor_alt,
            'ATB_Mie_co_int':atb_mie_co_int[:,::-1],
            'ATB_cr_int':atb_mie_cr_int[:,::-1],
            'ATB_Ray_int':atb_ray_int[:,::-1],
            'd_ATB_Mie_co_int':d_atb_mie_co_int[:,::-1],
            'd_ATB_Ray_int':d_atb_ray_int[:,::-1],
            'd_ATB_cr_int':d_atb_mie_cr_int[:,::-1],
            'time':seconds1,
            'out_h5_file':out_h5_file,
            'n_along_hdf_ori':np.size(height_ori[:,0]),
            'n_height_hdf_ori':np.size(height_ori[0,:])
        }
    else:
        data_to_hdf = {
            'n_along':np.size(height_out[:,0]),
            'n_height':np.size(height_out[0,:]),
            'along_track':along_track,
            'temp_int':temp_int,
            'pres_int':pres_int,
            'surface_height':surface_height,
            'extinction':ext_int,
            'latitude':lat_out,
            'longitude':lon_out,
            'height_out':height_out,
            'sensor_alt':sensor_alt,
            'ATB_Mie_co_int':atb_mie_co_int,
            'ATB_cr_int':atb_mie_cr_int,
            'ATB_Ray_int':atb_ray_int,
            'd_ATB_Mie_co_int':d_atb_mie_co_int,
            'd_ATB_Ray_int':d_atb_ray_int,
            'd_ATB_cr_int':d_atb_mie_cr_int,
            'time':seconds1,
            'out_h5_file':out_h5_file,
            'n_along_hdf_ori':np.size(height_ori[:,0]),
            'n_height_hdf_ori':np.size(height_ori[0,:])
        }
                
    outfile3='./OUTPUT_LOC.h5'
    edit_anom_file(data_to_hdf, template_file_anom, outfile3)

    x_met_out='./xmet_dummy.h5'
    edit_x_met(data_to_hdf, template_file_xmet, x_met_out)

    x_jsg_out='./xjsg_dummy.h5'
    edit_x_jsg(data_to_hdf, template_file_xjsg, x_jsg_out,
               step=settings['lidar_count'])

    # copy the local output file to the final place
    #   warning: os.rename only works if source and destination
    #   are at the same filesystem, which may not be the case
    #   when the script is running inside a container
    #   with attached external folders to write the output.
    #os.rename('OUTPUT_LOC.h5', files_out['h5_out_0'])
    #   so use a copy and remove in stead.
    shutil.copy('OUTPUT_LOC.h5', files_out['h5_out_0'])
    os.remove('OUTPUT_LOC.h5')
    
    # copy the dummy h5 file to the correct position
    #os.rename('xmet_dummy.h5', files_out['h5_out_1'])
    # ditto
    shutil.copy('xmet_dummy.h5', files_out['h5_out_1'])
    os.remove('xmet_dummy.h5')

    # copy the dummy h5 file to the correct position
    #os.rename('xjsg_dummy.h5', files_out['h5_out_2'])
    # ditto
    shutil.copy('xjsg_dummy.h5', files_out['h5_out_2'])
    os.remove('xjsg_dummy.h5')
    #  #]

def main():
    #  #[ main program
    '''
    main program
    '''
    # the day_night_flag only affects which Orbit ID is made
    # as daylight is descending for EarthCARE
    #
    #num_profs=500

    script_name = sys.argv[0]
    usage = f"""
USAGE: {script_name} [OPTIONS]

Explanation of the options:
    -h/--help
           Displays some help on usage of this script.
    --lidar_count <int>
           The lidar_count is the link from A-NOM to JSG.
    -i/--ifile <inputfile>
           Defines the input NetCDF file to be used.
    --plot_overpass
           Enables creation of overpass plots (default is disabled)
    --overpass_time <03:00:00> or None
           Overpass_time needs to be in shape 'hh:mm:ss',
           or provide None to calculate it from the data center point
    --instrument_module <atlid or random>
           Defines the instrument module to be used.
    --no_noise <0 or 1>
           0: do not include noise, 1:Include noise
    --index=<001>
           ???
    --output_dir <output_dir>
           Defines output dir
    --reproducible_files_day <YYYYMMDD>
           Disables noise and forces the file generation field
           in the file name to a fixed value in stead of "now".
    --templates_folder <dir>
           Define the directory in which the template can be found.
    --advect_velocity <km/sec>
           Used to set the "scene velocity" Default 7km/sec
"""

    short_options = "hi:"
    # Define long options

    long_options = [
        'help',
        'lidar_count=',
        'ifile=',
        'plot_overpass',
        'overpass_time=',
        'instrument_module=',
        'no_noise=',
        'index=',
        'output_dir=',
        'reproducible_files_day=',
        'templates_folder=',
        'instr_data_folder=',
        'advect_velocity=',
        ]

    # set defaults
    dir_data = os.getcwd()
    simulated_data = os.path.join(dir_data,
        'test/output/Test_CLOUDNET_data_RT_22.nc')
    instrument_module = DEFAULT_INSTRUMENT_MODULE
    plot_overpass = False
    overpass_time = DEFAULT_OVERPASS_TIME
    index = DEFAULT_INDEX
    fixed_day = DEFAULT_FIXED_DAY
    templates_folder = DEFAULT_TEMPLATES_FOLDER
    instr_data_folder = DEFAULT_INSTR_DATA_FOLDER
    output_dir = DEFAULT_OUTPUT_DIR
    add_noise = DEFAULT_ADD_NOISE
    lidar_count = DEFAULT_LIDAR_COUNT
    advect_velocity = DEFAULT_ADVECT_VELOCITY

    try:
        opts = getopt.getopt(sys.argv[1:], short_options, long_options)[0]
        #opts, args = getopt.getopt(sys.argv[1:], short_options, long_options)
    except getopt.GetoptError:
        print (usage)
        raise

    for opt, arg in opts:
        print(opt, arg)
        if opt in ('-h', '--help'):
            print (usage)
            sys.exit()
        elif opt in ("-i", "--ifile"):
            simulated_data = arg
        elif opt == '--no_noise':
            add_noise = False
        elif opt == '--index':
            index = f'{int(arg):03d}'
        elif opt == '--lidar_count':
            num_lidar = int(arg)
            lidar_count = num_lidar
        elif opt == '--instrument_module':
            instrument_module = arg
        elif opt == '--advect_velocity':
            advect_velocity = arg
        elif opt == '--output_dir':
            output_dir= arg
        elif opt == '--plot_overpass':
            plot_overpass = True
        elif opt == '--overpass_time':
            if is_valid_time_string(arg):
                overpass_time = arg
            elif arg == 'None':
                overpass_time = None
            else:
                sys.exit(1)
        elif opt == "--reproducible_files_day":
            # switch off noise.
            add_noise = False
            # fix the day to be used in the output file names
            dtformat="%Y%m%d"
            fixed_day = datetime.strptime(arg, dtformat)
            # these 2 settings make running automated tests easier
        elif opt == '--templates_folder':
            templates_folder = arg
        elif opt == '--instr_data_folder':
            instr_data_folder = arg

        #If num_profs is an integer there will be that many profiles.
        #If it is a float it is seen as the spacing in km between 2 profiles.

    settings = {}
    settings['instrument_module'] = instrument_module
    settings['plot_overpass'] = plot_overpass
    settings['overpass_time'] = overpass_time
    settings['index'] = index
    settings['fixed_day'] = fixed_day
    settings['output_dir'] = output_dir
    settings['templates_folder'] = templates_folder
    settings['instr_data_folder'] = instr_data_folder
    settings['add_noise'] = add_noise
    settings['lidar_count'] = lidar_count
    settings['advect_velocity'] = advect_velocity

    print('settings = ', settings)

    create_ec_files(simulated_data, settings)

    print('Done!')
    #  #]

if __name__ == "__main__":
    main()

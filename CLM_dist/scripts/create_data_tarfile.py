#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 23-Feb-2024
#
#   Copyright (C) 2024  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

'''
A simple convenience script to generate a new datapack
from the datapack directory at the developers machine.
'''
#  #]
#  #[ imported modules
import os
import sys
import glob
#  #]

DATAPACKDATE=sys.argv[1]

# sanity check
# ensure only "no_text" versions are provided as reference
PATTERN = 'CLM_dist/test/ref/figures_*/*.png'

def main():
    ''' main code '''
    num_wrong_files = 0
    for ref_plot_file in glob.glob(PATTERN):
        if not 'no_text' in ref_plot_file:
            print('problematic file: ', ref_plot_file)
            num_wrong_files += 1

    if num_wrong_files > 1:
        print('please fix this first, before creating a new datapack')
        sys.exit(1)

    print(40*'-')
    print(f'Creating: ../Cardinal_LIDAR_Module_Data_{DATAPACKDATE}.tar.gz')
    print(40*'-')
    cmd = (f'tar cvfz  ../Cardinal_LIDAR_Module_Data_{DATAPACKDATE}.tar.gz '+
        'CLM_dist MSI')
    os.system(cmd)
    print(f'Created: ../Cardinal_LIDAR_Module_Data_{DATAPACKDATE}.tar.gz')
    print(40*'-')
    print('Done')
    print(40*'-')

if __name__ == '__main__':
    main()

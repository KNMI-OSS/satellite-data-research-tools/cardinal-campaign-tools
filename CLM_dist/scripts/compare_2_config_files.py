#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 02-Aug-2023
#
#   Copyright (C) 2023 KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

'''
a small script to easily compare to xmlx files
'''
#  #]
#  #[ imported modules
import sys
from openpyxl import load_workbook
#  #]

#FILE1 = 'test_spreadsheet.xlsx'
#FILE2 = '../test/config/Test_input_spread_sheet_GEM_data.xlsx'

FILE1 = sys.argv[1]
FILE2 = sys.argv[2]

wb1 = load_workbook(filename = FILE1)
wb2 = load_workbook(filename = FILE2)
ws1 = wb1['Sheet1']
ws2 = wb2['Sheet1']

for row in ws1.iter_rows():
    for cell in row:
        idx = cell.coordinate
        #print('checking index: ', idx)
        value1 = ws1[idx].value
        value2 = ws2[idx].value
        if value1 != value2:
            print('difference found at index: ', idx)
            print('value1 = ', value1, type(value1))
            print('value2 = ', value2, type(value2))

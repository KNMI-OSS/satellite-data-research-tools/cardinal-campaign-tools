#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 17-Jan-2024
#
#   Copyright (C) 2024  KNMI
#
#   Author: Gerd-Jan van Zadelhoff <gerd-jan.van.zadelhoff@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

'''
a little example script to demonstrate how a simple quality control
on an input EVE data file can be used.
In this case a threshold is used on EXT_LIN/EXT_LIN_ERROR
and on LR_LIN.
If the QC failed the EXT_LIN is set to zero to ensure noisy ext
values in the upper atmosphere dont affect the simulation.
'''
#
#  #]
#  #[ imported modules
import numpy as np

# this import gives an error with pylint, even if I have installed datatree
# so disable checking for it.

# pylint: disable=import-error
import datatree as dt
# pylint: enable=import-error

#  #]

def edit_eve_file(eve_infile, outfile3):
    #  #[ perform QC and creates a new file
    '''
    creates an EVE file by modifying the data based on QC
    using SNR of extinction and LR

    arguments:
       data:          data to be exported
       eve_infile:    input filename for the template file 
       outfile3:      output filename for the resulting file
    '''

    eve_df = dt.open_datatree(eve_infile)
    #
    # reading in the appropriate arrays
    #
    ext = np.array(eve_df['EXT_LIN'])
    d_ext = np.array(eve_df['EXT_LIN_ERROR'])
    lr_lin = np.array(eve_df['LR_LIN'])
    #
    # Creating output file and remove data
    #
    ext_out = np.copy(ext)
    ext_out = ext_out.astype(ext.dtype)
    ext_out[ext/d_ext < 3] = 0.0
    ext_out[(lr_lin > 200) & (lr_lin < 10)] = 0.0

    eve_df['EXT_LIN']=ext_out

    print('Writing out to new file')

    # create a local output file
    eve_df.to_netcdf(outfile3)
    #  #]

def main():
    #  #[ main script
    ''' main script '''
    infile='eVe_profiles_mindelo_20220911T2114_20220911T2234UTC.nc'
    outfile='clip_eVe_profiles_mindelo_20220911T2114_20220911T2234UTC.nc'

    edit_eve_file(infile,outfile)
    #  #]

if __name__ == "__main__":
    main()

#!/usr/bin/env python3

#  #[ documentation

#   Copyright (C) 2022-2024  KNMI
#
#   Author: Dave Donovan <dave.donovan@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   This is the CARDINAL lidar_module (CLM).
#   This module implements the main interface to the multiscatter library
#   and handles the reading of input data and user settings.
"""
This is the main ATLID simulator script.
"""
#  #]
#  #[ imported modules
####################################
#
import os
import sys
import getopt
import numpy as np
import openpyxl
from lib.handle_xlsx import AllInputs
from lib.phys_constants import K_BOLTZ
from lib.rayleigh_scat_util import get_ray_sigma_beta
from lib.index_handler import IndexIterClass
from lib.species_handler import (SpeciesHandler, add_generic_species,
    get_accumulated_properties, get_accumulated_properties_after)
from lib.multiscatter_interface import Multiscatter
from lib.input_file_loader import load_input_data
from lib.netcdf_handler import NetCdfFileHandler
from lib.vertical_axis import VerticalAxis
# from lib.custom_exceptions import NotYetImplementedFeature

####################################
#  #]

def get_sensor_lat_lon(input_data, num_species, idx):
    #  #[ get sensor lat,lon
    ''' inspect the lat,lon values from the various
    geolocations and try to pick a sensible sensor lat,lon value
    '''
    possible_lat_keys = []
    possible_lon_keys = []
    for species_index in range(1, num_species+1):
        possible_lat_keys.append(f'species_{species_index}_latitude')
        possible_lon_keys.append(f'species_{species_index}_longitude')
    # ?maybe add 'met_latitude'/'met_longitude'?

    lat_of_sensor = 0.0
    for key in possible_lat_keys:
        if input_data[key] is not None:
            # print('DEBUG: getting lat_of_sensor from key: ', key)
            try:
                lat_of_sensor = input_data[key][idx.ci]
            except (IndexError, TypeError):
                # lat is just a constant float in this case
                lat_of_sensor = input_data[key]
            break

    lon_of_sensor = 0.0
    for key in possible_lon_keys:
        if input_data[key] is not None:
            # print('DEBUG: getting lon_of_sensor from key: ', key)
            try:
                lon_of_sensor = input_data[key][idx.ci]
            except (IndexError, TypeError):
                # lon is just a constant float in this case
                lon_of_sensor = input_data[key]
            break

    if isinstance(lat_of_sensor, np.ndarray):
        if lat_of_sensor.size == 1:
            # ensure we have a float in stead of a 0D numpy array.
            lat_of_sensor = float(lat_of_sensor)
            lon_of_sensor = float(lon_of_sensor)

    return lat_of_sensor, lon_of_sensor
    #  #]

def is_time_matchup_needed(input_data, num_species):
    #  #[ see if time matchup is needed or not
    '''
    Detect if time matchup is needed or not.
    note: time matchup is needed if the meteo data coordinate
    differs from the data coordinate(s)>
    '''

    # if met_time is undefined (None) then assume we do no time matchup
    do_time_matchup = True
    if input_data['met_time'] is None:
        do_time_matchup = False

    # if none of the species specify a time coordinate
    # assume we do no time matchup
    possible_time_keys = []
    time_key_to_use = None
    for species_index in range(1, num_species+1):
        possible_time_keys.append(f'species_{species_index}_time')

        data_time_key_found = False
        for key in possible_time_keys:
            if input_data[key] is not None:
                data_time_key_found = True
                time_key_to_use = key
                break

        if not data_time_key_found:
            do_time_matchup = False

    return do_time_matchup, time_key_to_use
    #  #]

###############################################
#  MAIN LIDAR INPUT PARSING AND RT ROUTINE
###############################################

def lidar_module(fn_excel, outfile):
    ''' this is the main lidar simulation module '''
    print('Starting lidar module with inputs:')
    print('fn_excel = ', fn_excel)
    print('outfile  = ', outfile)

    #------------------------------------------
    #   Read inputs from spread-sheet
    #------------------------------------------
    wbk = openpyxl.load_workbook(filename=fn_excel)
    worksheet =  wbk['Sheet1']
    all_inp = AllInputs()
    all_inp.load_all_from_worksheet(worksheet)
    wbk.close()

    #------------------------------------------
    # Read the Files and variables to read
    #------------------------------------------

    infc, input_data = load_input_data(all_inp)

    #-------------------------------------------------
    #        Now build up the atmosphere
    #  The aerosol parts still need to be added
    #-------------------------------------------------

    # inspect dimensions of the height variable
    # to see what looping we need to do

    # try to get the height variable, from the first species
    # since there should always be at least one species defined
    tmp_source = all_inp.dict_of_sp_params['species_1_type']
    tmp_height_var = input_data['species_1_height']
    tmp_test_var = input_data['species_1_extinction']
    if tmp_test_var is None:
        # fall back to wc if extinction is not defined
        tmp_test_var = input_data['species_1_wc']

    num_dims_for_height = len(np.shape(tmp_height_var))

    num_x = None
    num_y = None
    num_z_orig = None

    # z-versus-t case (CLOUDNET, ACTIVATE and EVE test cases)
    if num_dims_for_height == 1:
        if tmp_source == 'generic': # ACTIVATE case
            # in this case height is taken from the /DataProducts/Altitude
            # variable in ACTIVATE-HSRL2_UC12_20200217_R2.h5
            # This variable has 2 dimensions in the file (1, 641)
            # but the read_data() routine automatically
            # removes the dummy first dimension.
            #
            # In this case we need to read another variable to get the
            # size of the time dimension.
            # This is taken from the /DataProducts/355_ext variable in
            # ACTIVATE-HSRL2_UC12_20200217_R2.h5
            #
            # This variable has 2 dimensions: (time=1111, height=641)
            # The read_data() routine trims the time dimension
            # using the defined domain (=ix1,ix2,iy1,iy2 = 1,500,-1,-1)
            # and applies the selection [ix1:ix2,:] for this 2D case
            # Note that vertical interpolation is used based on the lidar_paras
            # config section, which defines 406 levels for this case.
            # So combined this gives:
            # num_x: time: input has size 1111; trimmed to size 499
            # num_y:       not used
            # num_z: z:    input has size 641; interpolated to size 406

            num_x = np.shape(tmp_test_var)[1]
            num_y = 0
            num_z_orig = np.shape(tmp_test_var)[0]

        if tmp_source == 'aerosol': # EVE case
            # in this case height is taken from the /ALTITUDE
            # variable in eVe_profiles_mindelo_20220911T2114_20220911T2234UTC.nc
            # This variable has 1 dimension in the file (4817)
            #
            # In this case we need to read another variable to get the
            # size of the time dimension.
            # This is taken from the /EXT_LIN variable in the same file
            #
            # This variable has 2 dimensions: (time=4, height=4817)
            # The read_data() routine trims the time dimension
            # using the defined domain (=ix1,ix2,iy1,iy2 = 0,5,-1,-1)
            # and applies the selection [ix1:ix2,:] for this 2D case
            # Note that vertical interpolation is used based on the lidar_paras
            # config section, which defines 406 levels for this case.
            # So combined this gives:
            # num_x: time: input has size 4; trimmed to size 4
            # num_y:       not used
            # num_z: z:    input has size 4817; interpolated to size 406

            try:
                # case with multiple time steps
                num_x = np.shape(tmp_test_var)[1]
            except IndexError:
                # case with just a single timestep
                num_x = 1

            num_y = 0
            num_z_orig = np.shape(tmp_test_var)[0]

        if tmp_source == 'water': # CLOUDNET case
            # in this case height is taken from the height
            # variable in 20221024_lindenberg_categorize.nc
            # This variable has 1 dimension (height=363)
            # In this case we need to read another variable to get the
            # size of the time dimension.
            # This is taken from the lwc variable in
            # 20221024_lindenberg_lwc-scaled-adiabatic.nc
            # This variable has 2 dimensions: (time=2880, height=363)
            # The read_data() routine trims the time dimension
            # using the defined domain (=ix1,ix2,iy1,iy2 = 1,100,-1,-1)
            # and applies the selection [ix1:ix2,:] for this 2D case
            # Note that vertical interpolation is used based on the lidar_paras
            # config section, which defines 406 levels for this case.
            # So combined this gives:
            # num_x: time: input has size 2880; trimmed to size 99
            # num_y:       not used
            # num_z: z:    input has size 383; interpolated to size 406

            num_x = np.shape(tmp_test_var)[1]
            num_y = 0
            num_z_orig = np.shape(tmp_test_var)[0]

    # z-versus lat/lon case (GEM)
    if num_dims_for_height == 3:
        # in this case height is taken from the height_thermodynamic
        # variable in Cape_Verde_F12_GEM_2021091812_300min_SIPon_trimmed.nc
        # This variable has 3 dimensions (level=62, lat=61, lon=111)
        # The read_data() routine trims the lat,lon dimensions
        # using the defined domain (=ix1,ix2,iy1,iy2 = 1,100,1,50)
        # and applies the selection [:,iy1:iy2,ix1:ix2] for this 3D case.
        # Note that vertical interpolation is used based on the lidar_paras
        # config section, which defines 406 levels for this case.
        # So combined this gives:
        # num_x: lon: input has size 111; trimmed to size 99
        # num_y: lat: input has size  61; trimmed to size 49
        # num_z: z:   input has size  62' interpolated to size 406
        num_x = np.shape(tmp_height_var)[2]
        num_y = np.shape(tmp_height_var)[1]
        num_z_orig = np.shape(tmp_height_var)[0]

    if num_z_orig is None:
        # unhandled case
        print('='*50)
        print('num_z_orig is not yet defined')
        print('This is caused by an unhandled case for guessing num_z_orig')
        print('in lidar_module.py ...')
        print('Please report this case to the software developer.')
        print('num_dims_for_height : ', num_dims_for_height)
        print('='*50)
        sys.exit(1)

    # handle the vertical dimension of the simulation
    v_ax = VerticalAxis(all_inp, num_z_orig)

    # always the same, since we interpolate to this vertical grid
    num_z_interp = v_ax.nz_interp

    #print('DEBUG: tmp_height_var= ', tmp_height_var)
    #print('DEBUG: tmp_source = ', tmp_source)
    #print('DEBUG: tmp_test_var = ', tmp_test_var)
    #print('DEBUG: np.shape(tmp_test_var) = ', np.shape(tmp_test_var))
    #print('DEBUG: num_x = ', num_x)
    #print('DEBUG: num_y = ', num_y)
    #print('DEBUG: num_z_orig = ', num_z_orig)
    #sys.exit(1)

    if num_y == 0:
        index_iterator = IndexIterClass(num_x)
    else:
        index_iterator = IndexIterClass(num_x, num_y)

    size_def = index_iterator.get_size_def(num_z_interp)

    # Rayleigh extinction and backscatter cross-sections
    # pylint: disable=no-member
    wavelen = all_inp.lidar_params.laser_wavelength
    # pylint: enable=no-member
    result = get_ray_sigma_beta(wavelen)
    d_sigma = result["sigma"]*1.0e-3*1.0e-6
    # 1/km ==> 1/m and 1/m^3 ==> 1/cm^3

    d_beta = result["beta"]*1.0e-3*1.0e-6

    num_species =  int(all_inp.dict_of_sp_params['num_species'])
    print('num_species = ', num_species)

    list_of_species = []
    for species_index in range(1, num_species+1):
        species_type = all_inp.dict_of_sp_params[
                               f'species_{species_index}_type']
        if species_type == 'generic':
            list_of_gen_species = \
              add_generic_species(infc, all_inp, input_data,
                                  species_index, v_ax, size_def)
            list_of_species.extend(list_of_gen_species)
        else:
            this_species = SpeciesHandler(input_data, all_inp,
                                          v_ax, size_def, species_index)
            list_of_species.append(this_species)

    # allocate arrays to allow later export to NetCDF output file
    ext_ray = np.zeros(size_def)
    beta_ray = np.zeros(size_def)

    temperature = np.zeros(size_def)
    pressure = np.zeros(size_def)
    u_wind = np.zeros(size_def)
    v_wind = np.zeros(size_def)

    # define arrays for accumulated properties
    spec = list_of_species[0]
    shape = np.shape(spec.extinction)
    part_extinction = np.zeros(shape)
    part_reff = np.ones(shape) * 0.01
    part_g = np.zeros(shape)

    # TBD...need to add this info to the inputs and properly process
    droplet_fraction_part = np.zeros(shape)
    # TBD...need to add this info to the inputs and properly process
    pristine_ice_fraction_part = np.zeros(shape)

    lidar_ratio = np.ones(shape)
    tot_beta = np.zeros(shape)

    latitude_of_sensor = np.zeros(shape)
    longitude_of_sensor = np.zeros(shape)

    # For the cloudnet case the meteo data has a different time
    # dimension (once per our, 25 values for 1 day)
    # than the data itself (2880 steps, so twice per minute for 1 day).
    # So need to matchup the 2 grids by getting the nearest hour
    # for each data time:

    # note: time matchup is needed if the meteo data coordinate
    # differs from the data coordinate(s)>
    do_time_matchup, time_key_to_use = \
      is_time_matchup_needed(input_data, num_species)
    #print('DEBUG: do_time_matchup = ', do_time_matchup)
    #print('DEBUG: time_key_to_use = ', time_key_to_use)

    #-------------------------------------------------------
    #            Now loop over all the columns
    #-------------------------------------------------------
    print('-'*40)
    print('Preparing input profiles:')
    print('-'*40)
    for idx in index_iterator:
        index_iterator.display_progress(in_steps_of=10)

        lat_of_sensor, lon_of_sensor = \
          get_sensor_lat_lon(input_data, num_species, idx)
        #print('DEBUG: lat_of_sensor, lon_of_sensor = ',
        #      lat_of_sensor, lon_of_sensor)

        if len(np.shape(latitude_of_sensor))==2:
            # curtain case
            latitude_of_sensor[idx.ci] = lat_of_sensor
            longitude_of_sensor[idx.ci] = lon_of_sensor
        else:
            # 3D case (GEM)
            latitude_of_sensor[idx.ci] = lat_of_sensor[idx.ci[1], idx.ci[2]]
            longitude_of_sensor[idx.ci] = lon_of_sensor[idx.ci[1], idx.ci[2]]

        # TBD....find iy and ix of met data
        # right now assume that the input x-y grid of the met data
        # matches that of the rest of the input.
        # Suggest implementing a simple nearest lat-lon neighbour
        # column approach

        z_inp = v_ax.define_z_array(tmp_height_var, idx)
        z_met_inp = v_ax.define_z_array(input_data['met_height'], idx)

        if do_time_matchup:
            data_time = input_data[time_key_to_use][idx.ci] # scalar value
            model_time = input_data['met_time'] # array of 25 values
            # use nearest neighbour for now
            model_index = np.abs(model_time - data_time).argmin()
            press_profile = input_data['met_pressure'][Ellipsis, model_index]
            temp_profile = input_data['met_temperature'][Ellipsis, model_index]
            u_wind_profile = input_data['met_wind_u'][Ellipsis, model_index]
            v_wind_profile = input_data['met_wind_v'][Ellipsis, model_index]
        else:
            pres_shape = np.shape(input_data['met_pressure'])
            if len(pres_shape) == 1:
                # seems pressure is a 1D array
                # this happens for the eVe data,
                # even if we have a time dimension
                press_profile = input_data['met_pressure']
                temp_profile = input_data['met_temperature']
            elif len(pres_shape) > 1:
                # curtain case and 3D case (idx.ci should handle both cases)
                press_profile = input_data['met_pressure'][idx.ci]
                temp_profile = input_data['met_temperature'][idx.ci]

            if isinstance(input_data['met_wind_u'], np.ndarray):
                u_wind_profile = input_data['met_wind_u'][idx.ci]
                v_wind_profile = input_data['met_wind_v'][idx.ci]
            else:
                shape = np.shape(z_met_inp)
                # note: u/v is a required input now,
                # so remove this workaround
                if input_data['met_wind_u'] is None:
                    # no wind_u profile defined, so assume zeros
                    u_wind_profile = np.zeros(shape, dtype=float)
                else:
                    # u is just a constant float in this case
                    u_wind_profile = (
                        np.ones(shape, dtype=float) * input_data['met_wind_u'])
                # note: u/v is a required input now,
                # so remove this workaround
                if input_data['met_wind_v'] is None:
                    # no wind_u profile defined, so assume zeros
                    v_wind_profile = np.zeros(shape, dtype=float)
                else:
                    # v is just a constant float in this case
                    v_wind_profile = (
                        np.ones(shape, dtype=float) * input_data['met_wind_v'])

        logp = np.log10(press_profile*100.0) # now in Pa

        # Interpolate the met T, P, u and v to the data vertical grid
        log10_press_interp = v_ax.interpolate_to_lidar_range(z_met_inp, logp,
                                                           key='met_pressure')
        press_interp = 10.**(log10_press_interp)
        t_interp = v_ax.interpolate_to_lidar_range(z_met_inp, temp_profile,
                                                   key='met_temperature')

        u_wind_interp = v_ax.interpolate_to_lidar_range(
                             z_met_inp, u_wind_profile, key='met_wind_u')
        v_wind_interp = v_ax.interpolate_to_lidar_range(
                             z_met_inp, v_wind_profile, key='met_wind_v')

        # then calculate interpolated particle density
        den_interp = press_interp/t_interp/K_BOLTZ # molecules/m^3

        # Collect the pressure, temperature and wind compenents
        # since we want to write it out to the NetCDF file.
        temperature[idx.ci] = t_interp
        pressure[idx.ci] = press_interp
        u_wind[idx.ci] = u_wind_interp
        v_wind[idx.ci] = v_wind_interp

        # Then calculate molecular extinction and backscatter
        ext_ray[idx.ci] = den_interp*d_sigma   # in m-1
        beta_ray[idx.ci] = den_interp*d_beta   # in m-1 sr-1

        #=========================================================
        #
        # Now build up the particulate atmospheric ext and Reff etc.
        # species by species
        #
        #=========================================================

        # Each species has its own extinction,reff etc...
        for species in list_of_species:
            # Find extinction, Reff, S and lin_depol
            species.calc_properties(z_inp, idx)

        #####################################################
        # TBD add the aerosol info
        # Need to take into account that the
        # height aerosol field lat,lon,time can be different
        # than the cloud info....so need to co-locate in x-y
        # and interpolate in height
        #####################################################

        #--------------------------------------------------------
        #
        #   All the species segregated particulate info has been parsed
        #   So now we can combine them to form the effective total particulate
        #   optical property arrays
        #
        #--------------------------------------------------------

        (part_extinction[idx.ci],
         part_reff[idx.ci],
         part_g[idx.ci],
         droplet_fraction_part[idx.ci],
         pristine_ice_fraction_part[idx.ci],
         lidar_ratio[idx.ci],
         tot_beta[idx.ci]) = get_accumulated_properties(list_of_species, idx)

    part_omega = np.ones(np.shape(part_reff))
    # prevent absorption from being exactly zero
    # since the multiscatter code not like that.
    ssa_ray = np.ones(np.shape(part_reff))*0.9999 # ssa = single scatter albedo

    # =============================================================
    #  This may be a good place to put the call to the MSI Jason
    #  input file generator !!!
    # =============================================================

    #--------------------------------------------------------------
    #  The total effective arrays have now all been built so we can
    #  setup the specific things needed to call the multiscatter RT code
    #--------------------------------------------------------------

    # setup 3D arrays to store the simulated profiles
    atb_mie = np.zeros(size_def)
    atb_co_mie = np.zeros(size_def)
    atb_cr = np.zeros(size_def)
    atb_ray = np.zeros(size_def)
    #atb_Mie_ss = np.zeros(size_def)
    atb_ray_ss = np.zeros(size_def)

    msc = Multiscatter(v_ax.lid_alt)
    # pylint: disable=no-member
    wavelen_nm = all_inp.lidar_params.laser_wavelength
    laser_div = all_inp.lidar_params.div_laser
    fov_t = all_inp.lidar_params.fov_telescope
    # pylint: enable=no-member
    sat_alt = v_ax.sat_alt

    #-------------------------------------------------------
    #            Now loop over all the columns
    #            call the MS code and
    #            doing the other necessary calculations
    #-------------------------------------------------------
    print('-'*40)
    print('calling multiscatter on each profile:')
    print('-'*40)
    index_iterator.reset()
    for idx in index_iterator:
#        ix = idx.index_x
#        iy = idx.index_y
        index_iterator.display_progress(in_steps_of=10)
        #index_iterator.display_progress_hashes(width=60)

        # setup_instrument_settings Needs to be called each time.
        # Otherwise the pointer to fov_t elements gets lost somehow...
        msc.setup_instrument_settings(sat_alt, wavelen_nm, laser_div, fov_t)

        # Particle size must be in meters (hence the 1.0e-6) below !
        msc.setup_profile_inputs(
            part_reff[idx.ci]*1.0e-6,
            part_extinction[idx.ci],
            part_omega[idx.ci],
            part_g[idx.ci],
            lidar_ratio[idx.ci],
            ext_ray[idx.ci],
            ssa_ray[idx.ci],
            droplet_fraction_part[idx.ci],
            pristine_ice_fraction_part[idx.ci])

        msc.setup_config_settings(single_scatter=False)
        msc.call()

        # store the results
        atb_mie[idx.ci] = np.copy(msc.atb_mie_1d)
        atb_ray[idx.ci] = np.copy(msc.atb_ray_1d)

        # call ms again, now for the single scatter case
        # because the Rayleigh result is needed to estimate the f factor.
        msc.setup_config_settings(single_scatter=True)
        msc.call()

        atb_ray_ss[idx.ci] = np.copy(msc.atb_ray_1d)

    print("")
    print('Done main loop')

    not_nan_atb_ray = ~np.isnan(atb_ray)
    not_nan_atb_ray_ss = ~np.isnan(atb_ray_ss)
    above_zero_atb_ray = atb_ray > 0
    index_not_nan = np.where(not_nan_atb_ray * not_nan_atb_ray_ss)
    index_above_zero_and_not_nan = np.where(
        not_nan_atb_ray * not_nan_atb_ray_ss * above_zero_atb_ray)

    ms_ss_ratio = np.zeros(np.shape(atb_ray))
    ms_ss_ratio[index_above_zero_and_not_nan] = (
        atb_ray[index_above_zero_and_not_nan] /
        atb_ray_ss[index_above_zero_and_not_nan] )

    index_negative_f = np.where(ms_ss_ratio < 0.0)
    ms_ss_ratio[index_negative_f] = 0.0
    ms_induced_depol = (np.sqrt(ms_ss_ratio)-1)/(np.sqrt(ms_ss_ratio)+1)

    depol_tot = get_accumulated_properties_after(
        tot_beta, ms_induced_depol, list_of_species)

    debug = False
    if debug:
        print('====================')
        print('np.min(atb_mie)    = ', np.min(atb_mie))
        print('np.max(atb_mie)    = ', np.max(atb_mie))
        print('np.min(atb_ray)    = ', np.min(atb_ray))
        print('np.max(atb_ray)    = ', np.max(atb_ray))
        print('np.min(atb_ray[index_above_zero_and_not_nan])    = ',
            np.min(atb_ray[index_above_zero_and_not_nan]))
        print('np.max(atb_ray[index_above_zero_and_not_nan])    = ',
            np.max(atb_ray[index_above_zero_and_not_nan]))

    if debug:
        num_datapoints = atb_ray.size
        num_above_zero = np.count_nonzero(above_zero_atb_ray)
        num_zero_or_below = num_datapoints - num_above_zero
        num_nan = np.count_nonzero(np.isnan(atb_ray))
        percentage_nan_values = 100.*num_nan/num_datapoints
        percentage_zero_or_below = 100.*num_zero_or_below/num_datapoints
        print(f'number of nan values in atb_ray: {num_nan} '+
            f'out of {num_datapoints} ({percentage_nan_values:4.1f}%)')

        print('number of zero (or below) values in atb_ray: '+
              f'{num_zero_or_below} '+
              f'out of {num_datapoints} ({percentage_zero_or_below:4.1f}%)')
        print(f'np.min(atb_ray_ss) = {np.min(atb_ray_ss)}')
        print(f'np.max(atb_ray_ss) = {np.max(atb_ray_ss)}')

    if debug:
        num_datapoints = atb_ray_ss.size
        num_nan = np.count_nonzero(np.isnan(atb_ray_ss))
        print(f'number of nan values in atb_ray_ss: {num_nan} '+
            f'out of {num_datapoints} '+
            f'({100.*num_nan/num_datapoints:4.1f}%)')

    print('====================')
    #
    #
    # ATID (physically) Measures
    #
    # 1) A co-polar Mie (with X-talk from Ray channel)
    # 2) A co-polar Ray (with X-talk from Mie channel)
    # 3) A cross-polar Mie + Ray channel
    #
    # However, after spectral+polar cross-talk correction,
    # the following output are produced
    #
    # 1) A co-polar Mie
    # 2) A total Ray
    # 3) A cross-polar Mie
    #
    # Thus to make this explicit, a new output variable
    # named atb_cr_mie was added.
    #
    # prevent division by zero in the next division.
    nonzero_denominator = np.where(np.abs(1.0+depol_tot) > 0.)

    atb_cr[nonzero_denominator] = (
        depol_tot[nonzero_denominator]/
        (1.0+depol_tot[nonzero_denominator])*
        atb_mie[nonzero_denominator])
    #
    #depol_ray = 0.014 (THIS LOOKS LIKE IT WAS A TYPO!)
    depol_ray = 0.0041  # D.D. Jun 11, 2024
    #
    atb_co_mie = atb_mie - atb_cr

    atb_cr_mie = np.zeros(np.shape(atb_ray))
    atb_cr_mie[index_not_nan] = atb_cr[index_not_nan]

    atb_cr[index_not_nan] = (atb_cr[index_not_nan] +
                             atb_ray[index_not_nan]*depol_ray)
    #
    # atb_co_ray = atb_ray - depol_ray*atb_ray/(1.0 + depol_ray)
    # simplified to:
    #atb_co_ray = atb_ray/(1.0 + depol_ray)

    atb_co_ray = np.zeros(np.shape(atb_ray))
    atb_co_ray[index_not_nan] = atb_ray[index_not_nan]/(1.0 + depol_ray)

    if outfile is not None:
        # Write the data to the output file
        # For 2D data make 3d with y1=y2...
        # will have done this earlier at read time !
        print('starting to write to netcdf output file: ', outfile)
        print('====================')

        # ensure output folder exists
        output_path = os.path.split(outfile)[0]
        if output_path != '':
            if not os.path.exists(output_path):
                os.makedirs(output_path)

        ncfh = NetCdfFileHandler(list_of_species, outfile)

        ncfh.define_netcdf_file(num_x, num_y, num_z_interp)

        # TODO: add outputs for the multiscatter inputs to a debug.nc file:
        # (useful for debugging)
        #  part_reff
        #  part_extinction
        #  part_omega
        #  part_g
        #  lidar_ratio
        #  ext_ray
        #  ssa_ray
        #  droplet_fraction_part
        #  pristine_ice_fraction_part

        ncfh.fill_with_data(v_ax, input_data, time_key_to_use,
                            atb_mie, atb_ray, atb_co_mie, atb_co_ray,
                            atb_cr, atb_cr_mie, ms_ss_ratio, depol_tot,
                            temperature, pressure, u_wind, v_wind,
                            ext_ray, beta_ray,
                            latitude_of_sensor, longitude_of_sensor)
        ncfh.close()

def main():
    #  #[ main program
    ''' Processes the command line arguments and launches the simulation '''

    inputfile = ''
    outputfile = ''
    #
    usage = 'lidar_module.py -i <inputfile> -o <outputfile>'
    #
    try:
        opts = getopt.getopt(sys.argv[1:],"hi:o:",["ifile=","ofile="])[0]
    except getopt.GetoptError: # as err:
        print(usage)
        sys.exit(2)
    for opt, arg in opts:

        if opt == '-h':
            print(usage)
            sys.exit(0)
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg

    if inputfile == '' :
        print ("inputfile not defined")
        print(usage)
        sys.exit(1)
    if outputfile == '' :
        print ("outputfile not defined")
        print (usage)
        sys.exit(1)

    lidar_module(inputfile, outputfile)
    #  #]

if __name__ == "__main__":
    main()

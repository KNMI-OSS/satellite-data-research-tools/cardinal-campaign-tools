#!/usr/bin/env python3
#
#  #[ documentation
#
#   Created on 23-Mar-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module implements some
Rayleigh scattering utility routines
'''
#  #]
#  #[ imported modules
import numpy as np
#  #]

def get_ray_sigma(wavelen):
    #  #[ calc. extinction coeff.
    '''
    returns the Rayleigh extinction coefficient
    in cm^2/molecule. Input wavelength is in nm
    '''
    work=wavelen/1000.0
    #
    if wavelen < 550:
        xval = 0.389*work + 0.09426/(work) - 0.3228
    else:
        xval=0.04

    sigma = 4.02e-28/(work**(4+xval))

    return sigma
    #  #]

def get_ray_sigma_beta(wavelen):
    #  #[ calc. extinct. and backsc.
    '''
    returns Rayleigh extinction and backscatter coefficients.

    Input:
        wavelength in nm

    Returns:

        beta in cm^3/sr/km/molecule
        sigma in cm^3/km/molecule

    To get the atmospheric extinction coefficient [1/km]
    multiply sigma by rho[molecules/cm3]
    To get the backscatter extinction coefficient [1/km]
    multiply beta by rho[molecules/cm3]

    Using data of D.R. Bates
    find the rayleigh extinction and backscattering
    cross sections (valid from about 300 to 1000 nm)
    '''

    sigma = get_ray_sigma(wavelen)

    fk_val = (
        1.225e-13*wavelen**4 -
        3.911e-10*wavelen**3 +
        4.6100e-7*wavelen**2 -
        2.410e-4*wavelen +
        1.095 )
    #
    e_val = (fk_val-1.0)*9.0/2.0
    #
    beta = 3.0/(4.0*np.pi)*(180.0+28.0*e_val)/(360.0+82*e_val)*sigma
    #
    sigma = sigma*1.e+5
    beta = beta*1.e+5
    #
    return {'sigma':sigma,'beta':beta}
    #  #]

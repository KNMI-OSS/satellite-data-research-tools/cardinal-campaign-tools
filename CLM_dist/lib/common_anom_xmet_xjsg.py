#!/usr/bin/env python3

#  #[ documentation
#   Copyright (C) 2023  KNMI
#
#   Author: Gerd-Jan van Zadelhoff <gerd-jan.van.zadelhoff@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
'''
This module defines functions to create the EathCare formatted
L1 products XMET, XJSG and XNOM
'''
#  #]
#  #[ imported modules
import os
import shutil
import datetime
import xarray as xr
import datatree as dt
import h5py
import numpy as np
from scipy.interpolate import interp1d
from scipy.spatial.distance import cdist
from geopy.distance import geodesic
from geopy.point import Point

from .helpers import fix_permissions

#  #]

def create_overpass_plot(longitude_out,latitude_out, lon_sen, lat_sen,
                         overpass_time, outp_dir, anom_file):
    #  #[ create a simple overpass plot
    '''
    creates a plot using cartopy to show where the
    ground data was located
    '''
    # pylint: disable=import-outside-toplevel
    import matplotlib.pyplot as plt
    import cartopy.crs as ccrs
    from cartopy.feature import NaturalEarthFeature
    from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
    #from cartopy.io import shapereader
    # pylint: enable=import-outside-toplevel

    print('Creating overpass plot')

    # Create a map projection
    projection = ccrs.PlateCarree()

    # Create a figure and axes
    fig, ax_op = plt.subplots(subplot_kw={'projection': projection})

    # Add continents to the plot
    ax_op.add_feature(NaturalEarthFeature(category='physical',
                                          name='land', scale='50m',
                                          facecolor='lightgray'))
    # Add cities to the plot

    # this command loads the file 'ne_110m_populated_places.shp'
    # if available in the default location such as
    # /usr/share/cartopy/shapefiles/natural_earth/cultural/
    # and tries to download it if it is not there.
    #
    # a copy can also be downloaded manually from:
    # https://github.com/nvkelso/natural-earth-vector/blob/master/110m_cultural/ne_110m_populated_places.shp
    #shpfilename = shapereader.natural_earth(resolution='110m',
    #                                    category='cultural',
    #                                    name='populated_places')

    # hardcoded alternative that only works on KNMI:
    #cities = shapereader.Reader('/net/pc200278/nobackup/users/zadelhof/'+
    #    'Natural/ne_110m_populated_places.shp')

    #for city in cities.records():
    #    city_lon = city.geometry.x
    #    city_lat = city.geometry.y
    #    # city_name = city.attributes['NAME']
    #    ax_op.plot(city_lon, city_lat, 'bo', markersize=2, transform=projection)
    #    #ax_op.text(city_lon, city_lat, city_name, transform=projection)

    # add line
    ax_op.plot(longitude_out,latitude_out)
    ax_op.plot(lon_sen, lat_sen, marker='x', color='red',
            markersize=9, markeredgewidth=2)
    # Set the extent of the plot
    ax_op.set_extent([np.min(longitude_out)-5, np.max(longitude_out)+5,
                      np.min(latitude_out)-5, np.max(latitude_out)+5],
                      crs=projection)

    # Set the map gridlines and labels
    ax_op.gridlines(draw_labels=True, linewidth=0.5, color='gray', alpha=0.5,
                    linestyle='--')
    ax_op.xaxis.set_major_formatter(LongitudeFormatter())
    ax_op.yaxis.set_major_formatter(LatitudeFormatter())
    ax_op.set_xlabel('Longitude')
    ax_op.set_ylabel('Latitude')
    ax_op.set_title('Created overpass track using winds at '+overpass_time)

    fileout = os.path.join(outp_dir, f"{anom_file}_track.png")
    fig.savefig(fileout)
    #  #]

def create_lat_lon_slice(outp_dir, time, closest_index,lat_sen,lon_sen,
                         u_wind,v_wind,anom_file,overpass_time, plot_overpass):
    #  #[ creates a slice of the provided data
    '''
    creates a slice of the created data along track
    '''
    n_along=np.size(time)
    #
    latitude_out=np.zeros(n_along,dtype=np.float64)
    longitude_out=np.zeros(n_along,dtype=np.float64)
    #x_out=np.zeros(n_along,dtype=np.float64)
    #y_out=np.zeros(n_along,dtype=np.float64)
    along_track=np.zeros(n_along,dtype=np.float64)
    #surface_height=np.zeros(n_along,dtype=np.float64)
    #
    # estimates wind at start, later use model winds at 3 km
    #u_wind=16.4  #m/s
    #v_wind= 5.98 #m/s
    wind=np.sqrt(u_wind**2+v_wind**2)

    # Calculate wind direction in radians
    wind_direction_rad = np.arctan2(v_wind, u_wind)
    #
    # Convert wind direction to degrees
    wind_direction_deg = np.degrees(wind_direction_rad)

    # Ensure wind direction is within [0, 360) range
    if wind_direction_deg < 0:
        wind_direction_deg += 360

    along_track[:]=(time[:]-time[closest_index])*wind*1e-3 # m --> km
    # Create the starting point
    try:
        starting_point = Point(lat_sen[0], lon_sen[0])
    except IndexError:
        starting_point = Point(lat_sen, lon_sen)
    print(starting_point)

    # Iterate over distances
    for index, distance in enumerate(along_track):
        # Calculate latitude and longitude offsets
        # Calculate the destination point in the wind direction and distance
        destination_point = geodesic(kilometers=distance).\
                                destination(starting_point, wind_direction_deg)
        latitude_out[index] = destination_point.latitude
        longitude_out[index] = destination_point.longitude

    if plot_overpass:
        create_overpass_plot(longitude_out,latitude_out, lon_sen, lat_sen,
                             overpass_time, outp_dir, anom_file)

    return longitude_out,latitude_out,along_track
    #  #]

def interpol_height(data_in,data_out,height_in, height_out):
    #  #[ height interpolation
    '''
    does altitude interpolations and if needed extrapolation
    '''
    #data_out=np.copy(height_out)
    #nx=np.size(height_in[:,0]) # not used
    #nz=np.size(height_out[0,:]) # nod used
    #print(data_in[1,:])
    #print(np.shape(height_in))
    data_out2=data_out.copy()
    #data_out2=data_out2.values

    for i in range(height_in.shape[0]):
        interp_func = interp1d(
            height_in[i,:].flatten(),
            data_in[i,:].flatten(),
            kind='nearest', fill_value="extrapolate")
        values=height_out[i,:].values
        outside_range = np.where(values  < np.max(height_in[i,:]))
        interpolated_array2=interp_func(values.flatten())
        data_out2[i,outside_range]=interpolated_array2[outside_range]

    return data_out2
    #  #]

def edit_x_jsg(data,x_jsg_in,x_jsg_out,step=2):
    #  #[ creates xjsg
    '''
    Creates an XJSG file by modifying a provided tenplate
    '''
    #from geopy import distance

    print('lidar_count=',step)

    df_jsg = dt.open_datatree(x_jsg_in)
    #print(step)
    #stop
    new_lat = data["latitude"][::step,-1]
    new_lon = data["longitude"][::step,-1]
    new_time = data["time"][::step]

    #new_ext=data[""][::step,:]

    new_lat=new_lat.reshape(-1)
    new_lon=new_lon.reshape(-1)
    new_time=new_time.reshape(-1)

    nx_in=np.size(data["latitude"][:,0])

    nx_out=np.size(new_lat)

    # Create the lidar count array

    lidar_count = np.ones(nx_out) * step
    land_flag = np.ones(nx_out)
    if sum(lidar_count) > nx_in:
        lidar_count[-1]=sum(lidar_count)-nx_in

    nx_in=data["n_along"]
    # nz_in=data["n_height"] # not used

    height_out=df_jsg['ScienceData/altitude']
    n_along_ori=np.size(height_out[:,0])
    n_height_ori=np.size(height_out[0,:])

    #  Check which cross track pixel is the nadir one and create lat and lon grid

    cross_track=np.array(df_jsg['ScienceData/across_track'])
    ncross=np.size(cross_track)
    nadir= np.where(cross_track == 0)[0]

    lat_out = np.zeros((nx_out, ncross), dtype=np.float64)
    lon_out = np.zeros((nx_out, ncross), dtype=np.float64)

    sen_az_out = np.zeros((nx_out, ncross), dtype=np.float32)
    sen_ev_out = np.zeros((nx_out, ncross), dtype=np.float32)
    sol_az_out = np.zeros((nx_out, ncross), dtype=np.float32)
    sol_ev_out = np.zeros((nx_out, ncross), dtype=np.float32)

    lat_out[: ,nadir[0]]=new_lat
    lon_out[: ,nadir[0]]=new_lon

    for index in range(nx_out-1):
        point1 = np.array([new_lon[index], new_lat[index]])  # First point [longitude, latitude]
        point2 = np.array([new_lon[index+1],new_lat[index+1]])  # Second point [longitude, latitude]

        # Calculate the distance between point1 and point2
        spacing = cdist([point1], [point2])[0, 0]

        # Calculate the perpendicular direction
        direction = point2 - point1
        perpendicular_dir = np.array([-direction[1], direction[0]])  # Perpendicular direction

        # Normalize the direction vector
        perpendicular_dir /= np.linalg.norm(perpendicular_dir)

        offsets = np.arange(ncross) - nadir
        offsets =offsets.astype(np.float64)

        offsets *= spacing
        # Create the crosstrack grid
        grid = np.outer(offsets, perpendicular_dir) + point1

        # Fill in the grid to the 2D-grid
        lat_out[index,:]=grid[:,1]
        lon_out[index,:]=grid[:,0]

        if index == nx_out-2:
            grid = np.outer(offsets, perpendicular_dir) + point2
            lat_out[nx_out-1,:]=grid[:,1]
            lon_out[nx_out-1,:]=grid[:,0]

    # First check where the closest pixel in the aux file is to get
    # a reasonable value when we reformat all arrays
    #coordinates = np.column_stack((df_jsg['ScienceData/latitude'][:,nadir],
    #                               df_jsg['ScienceData/longitude'][:,nadir]))

    # never used
    #lat_ori=np.array(df_jsg['ScienceData/latitude'])
    # never used
    #lon_ori=np.array(df_jsg['ScienceData/longitude'])

    # dit moet netter
    i11=nx_out#along_ori-1
    i00=0#i11-nx_out

    coords = {'dim_0': np.arange(0,nx_out,1)}
    df_jsg["ScienceData/along_track"] = xr.DataArray(np.arange(0, nx_out, 1) ,
                                                 coords=coords, dims=['dim_0'])
    coords = {'dim_1': cross_track}
    df_jsg['ScienceData/across_track'] = xr.DataArray(cross_track ,
                                                  coords=coords, dims=['dim_1'])
#    df_jsg["ScienceData/"+key]=df_jsg["ScienceData/"+key].assign_coords(dim_0=dim_0, dim_2=dim_2)
    for key in df_jsg['ScienceData']:
        dum_in=np.array(df_jsg["ScienceData/"+key])
        dum_type=dum_in.dtype
        shape=np.shape(dum_in)
        #print(key,np.shape(key))
        if np.size(shape)==1:
            if shape[0]==n_along_ori:

                dum_1d_along=dum_in[i00:i11]
                dum_1d_along=dum_1d_along.astype(dum_in.dtype)
                df_jsg["ScienceData/"+key]=dum_1d_along

            elif shape[0] ==n_height_ori:

                dum_1d_height = df_jsg["ScienceData/"+key]
                dum_1d_height=dum_1d_height.astype(dum_in.dtype)
                #
                df_jsg["ScienceData/"+key]=dum_1d_height
            else:
                print('No change in 1D array ',key)
        elif np.size(shape)==2:
            if (shape[0] == n_along_ori) & (shape[1] == n_height_ori):
                dum_22d=np.copy(dum_in[i00:i11,:])
                #print('TTTT',np.shape(dum_22d),np.shape(dum_in),dim_0,dim_1,n_along_ori,i00,i11)
                dim_0 = nx_out
                dim_2 = n_height_ori
                dum_22d = dum_22d.astype(dum_in.dtype)
                coords = {'dim_0': np.arange(dim_0), 'dim_3': np.arange(dim_2)}
                df_jsg["ScienceData/"+key] = xr.DataArray(dum_22d,
                                                      coords=coords, dims=['dim_0', 'dim_3'])
            elif (shape[0] == n_along_ori) & (shape[1] == ncross):
                dim_0=nx_out
                dim_1=ncross

                dum_2d=dum_in[i00:i11,:]
                #print('TTTT',np.shape(dum_2d),np.shape(dum_in),dim_0,dim_1,n_along_ori,i00,i11)
                dum_2d = dum_2d.astype(dum_in.dtype)
                coords = {'dim_0': np.arange(dim_0), 'dim_1': np.arange(dim_1)}
                df_jsg["ScienceData/"+key] = xr.DataArray(dum_2d,
                                                      coords=coords, dims=['dim_0', 'dim_1'])
            else:
                print('No change in 2D array:' ,key)

        for key in df_jsg['ScienceData']:
            dum_in=np.array(df_jsg["ScienceData/"+key])
            dum_type=dum_in.dtype
            shape=np.shape(dum_in)
            #
            if key =='cross_track':
                df_jsg["ScienceData/"+key]=cross_track.astype(dum_type)
            if key =='time':
                df_jsg["ScienceData/"+key]=new_time.astype(dum_type)
            if key =='latitude':
                df_jsg["ScienceData/"+key]=lat_out.astype(dum_type)
            if key =='longitude':
                df_jsg["ScienceData/"+key]=lon_out.astype(dum_type)
            if key =='lidar_profile_count':
                df_jsg["ScienceData/"+key]=lidar_count.astype(dum_type)
            if key =='land_flag':
                df_jsg["ScienceData/"+key]=land_flag.astype(dum_type)
            if key =='radar_profile_count':
                df_jsg["ScienceData/"+key]=lidar_count.astype(dum_type)
            if key =='solar_azimuth_angle':
                dum=df_jsg["ScienceData/"+key]
                if np.shape(dum)[1] == np.shape(sol_az_out)[1]:
                    nx_dum=np.int32(np.shape(dum)[0]/2.)
                    sol_az_out[:,:]=dum[nx_dum,:]
                df_jsg["ScienceData/"+key]=sol_az_out.astype(dum_type)
            if key =='solar_elevation_angle':
                if np.shape(dum)[1] == np.shape(sol_az_out)[1]:
                    nx_dum=np.int32(np.shape(dum)[0]/2.)
                    sol_ev_out[:,:]=dum[nx_dum,:]
                df_jsg["ScienceData/"+key]=sol_ev_out.astype(dum_type)
            if key =='sensor_azimuth_angle':
                if np.shape(dum)[1] == np.shape(sol_az_out)[1]:
                    nx_dum=np.int32(np.shape(dum)[0]/2.)
                    sen_az_out[:,:]=dum[nx_dum,:]
                df_jsg["ScienceData/"+key]=sen_az_out.astype(dum_type)
            if key =='sensor_elevation_angle':
                if np.shape(dum)[1] == np.shape(sol_az_out)[1]:
                    nx_dum=np.int32(np.shape(dum)[0]/2.)
                    sen_ev_out[:,:]=dum[nx_dum,:]
                df_jsg["ScienceData/"+key]=sen_ev_out.astype(dum_type)
            if key =='along_track':
                dum_1d=np.arange(0, nx_out, 1)
                #print(np.shape(dum_1d))
                df_jsg["ScienceData/"+key]=dum_1d.astype(dum_type)

#         df_jsg["ScienceData/extinction"]=
    df_jsg.to_netcdf(x_jsg_out)

    with h5py.File(x_jsg_out, 'r+') as h5_file:
        group = h5_file['ScienceData']
        check=0
        if check == 1:
            group = group.rename({'across_track_old': 'across_track',
                                  'along_track_old': 'along_track'})
            group = group.rename({'across_track': 'dim_1',
                                  #'along_track':'dim_0', # defined twice!
                                  'along_track':'dim_2', # defined twice!
                                  'height':'dim_3'})
        else:
            group['along_track_old']=group.pop('along_track')
            group['across_track_old']=group.pop('across_track')
            #group['height_old']=group.pop('height')
            group['along_track']=group.pop('dim_0')
            #group['along_track']=group.pop('dim_2')
            group['across_track']=group.pop('dim_1')
            group['height']=group.pop('dim_3')

        h5_file.flush() # this is not needed when followed by a close()
        h5_file.close()
    #  #]

def edit_x_met(data, x_met_in, x_met_out):
    #  #[ creates xmet
    '''
    creates an XMET file by modifying a provided template
    arguments:
        data:          data to be exported
        x_met_in:      input filename for the template file 
        x_met_out:     output filename for the resulting file
    '''
    # not used
    #from geopy import distance

    #k_boltz=1.380658e-23 # not used
    df_met=dt.open_datatree(x_met_in)

    nx_in=data["n_along"]
    nz_in=data["n_height"]

    height_out=df_met['ScienceData/geometrical_height']
    height_in=data['height_out']
    n_height_ori=np.size(height_out[0,:])
    n_along_ori=np.size(height_out[:,0])
    #
    #
    # get the nearest position in lat, lon
    #
    coordinates = np.column_stack(
        (df_met['ScienceData/latitude'], df_met['ScienceData/longitude']))

    # Calculate the distances to the tar  get coordinates
    distances = cdist(
        [[data['latitude'][0,nz_in-1],
          data['longitude'][0,nz_in-1]-360.]],
        coordinates, metric='euclidean')[0]
     # Find the index of the nearest point
    nearest_index = np.argmin(distances)

    i11=min(nearest_index+nx_in-1,n_along_ori-1)
    i00=i11-nx_in

    for key in df_met['ScienceData']:
        dum_in=np.array(df_met["ScienceData/"+key])
        dum_type=dum_in.dtype
        shape=np.shape(dum_in)

        if np.size(shape)==1:
            if shape[0]==n_along_ori:

                dum_1d_along=dum_in[i00:i11]
                #print('aa')
                dum_1d_along=dum_1d_along.astype(dum_in.dtype)
                df_met["ScienceData/"+key]=dum_1d_along
                #print('bb')
            elif shape[0] ==n_height_ori:

                dum_1d_height = df_met["ScienceData/"+key]
                dum_1d_height=dum_1d_height.astype(dum_in.dtype)
                #print(key,np.shape(dum_1d_height))
                df_met["ScienceData/"+key]=dum_1d_height
            else:
                print('No change in 1D array ',key)
        elif np.size(shape)==2:
            if (shape[0] == n_along_ori) & (shape[1] == n_height_ori):
                dum_2d=np.copy(dum_in[i00:i11,:])
                dum_2d=dum_2d.astype(dum_in.dtype)
                df_met["ScienceData/"+key]=dum_2d
            elif (shape[1] == n_along_ori) & (shape[0] == n_height_ori):
                dum_2d=dum_in[:,i00:i11]
                dum_2d=dum_2d.astype(dum_in.dtype)
                df_met["ScienceData/"+key]=dum_2d
            else:
                print('No change in 2D array:' ,key)

    for key in df_met['ScienceData']:
        dum_in=np.array(df_met["ScienceData/"+key])
        dum_type=dum_in.dtype
        shape=np.shape(dum_in)
       #
        if key =='latitude':
            dum_1d=data["latitude"][:,nz_in-3]
            df_met["ScienceData/"+key]=dum_1d.astype(dum_type)
            #tt=0 # dit is een dummy opvul commando van GJvZ
        if key =='longitude':
            dum_1d=data["longitude"][:,nz_in-3]
            df_met["ScienceData/"+key]=dum_1d.astype(dum_type)
        #if key =='cloud_cover':
        #if key =='specific_cloud_ice_water_content':
        #if key =='specific_cloud_liquid_water_content':
        #if key =='specific_rain_water_content':
        #if key =='specific_snow_water_content':
        #if key =='divergence':
        #if key =='ozone_mass_mixing_ratio':
        #if key =='specific_humidity':
        #if key =='temperature':
        #if key =='eastward_wind':
        #if key =='northward_wind':
        #if key =='upward_air_velocity':
        #if key =='eastward_wind_at_10_metres':
        #if key =='northward_wind_at_10_metres':
        if key =='temperature_at_2_metres':
            dum_1d=data["temp_int"][:,nz_in-1].flatten()
            df_met["ScienceData/"+key]=dum_1d.astype(dum_type)
        #if key =='boundary_layer_height':
        #if key =='sea_ice_cover':
        #if key =='high_cloud_cover':
        #if key =='low_cloud_cover':
        #if key =='medium_cloud_cover':
        #if key =='snow_depth':
        if key =='skin_temperature':
            dum_1d=data["temp_int"][:,nz_in-1].flatten()
            df_met["ScienceData/"+key]=dum_1d.astype(dum_type)
        if key =='surface_pressure':
            dum_1d=data["pres_int"][:,nz_in-1].flatten()
            df_met["ScienceData/"+key]=dum_1d.astype(dum_type)
            #df_met["ScienceData/"+key]=data["pres_int"][:,nz_in-1].astype(dum_type)
        if key =='sea_surface_temperature':
            dum_1d=data["temp_int"][:,nz_in-1].flatten()
            df_met["ScienceData/"+key]=dum_1d.astype(dum_type)
            # df_met["ScienceData/"+key]=data["temp_int"][:,nz_in-1].astype(dum_type)
        if key =='temperature':
            dum_2d=np.array(df_met["ScienceData/"+key])
            #print(np.shape(dum_2d),dum_2d)
            #stop
            dum_2d=interpol_height(data["temp_int"],dum_2d,height_in, height_out)
            #print(np.shape(dum_2d))
            dum_2d=dum_2d.astype(dum_in.dtype)
            df_met["ScienceData/"+key]=dum_2d
        #if key =='soil_temperature_level1':
        #if key =='total_cloud_cover':
        #if key =='total_column_ozone':
        #if key =='total_column_water_vapour':
        #if key =='leaf_area_index_low_vegetation':
        #if key =='leaf_area_index_high_vegetation':
        #if key =='forecast_surface_roughness':
        #if key =='snow_albedo_surface':
        #if key =='near_ir_albedo_for_diffuse_radiation_surface':
        #if key =='near_ir_albedo_for_direct_radiation_surface':
        #if key =='uv_visible_albedo_for_diffuse_radiation_surface':
        #if key =='uv_visible_albedo_for_direct_radiation_surface':
        if key =='pressure':
            dum_2d=interpol_height(data["pres_int"],dum_in,height_in, height_out)
            dum_2d=dum_2d.astype(dum_in.dtype)
            df_met["ScienceData/"+key]=dum_2d
        #if key =='geometrical_height':
        #if key =='relative_humidity':
        #if key =='tropopause_height_wmo':
        #if key =='tropopause_height_calipso':
        #if key =='wet_bulb_temperature':
            #df_met["ScienceData/"+key]=data["temp_int"].astype(dum_type)

    df_met.to_netcdf(x_met_out)
    with h5py.File(x_met_out, 'r+') as h5_file:
        group = h5_file['ScienceData']
        group['horizontal_grid']=group.pop('dim_0')
        group['height']=group.pop('dim_1')
        h5_file.flush() # this is not needed when followed by a close()
        h5_file.close()
    #  #]

def edit_anom_file(data, anom_infile, outfile3):
    #  #[ creates anom
    '''
    creates an ANOM file by modifying a provided template
    arguments:
       data:          data to be exported
       anom_infile:   input filename for the template file 
       outfile3:      output filename for the resulting file
    '''

    df_anom = dt.open_datatree(anom_infile)

    #  Check the horizontal and vertical sizes
    #  Get the names of all variables of these sizes
    #  and nullify these with the new size
    #
    n_along_ori = data['n_along_hdf_ori']
    n_height_ori = data['n_height_hdf_ori']
    print('n_along_ori,n_height_ori = ', n_along_ori,n_height_ori)
    print()

    # save a copy of sensor_altitude
    # the full array is needed in the calculation below
    # (np.float64(sat_alt[0]-data['height_out'][:,::-1]))
    # and since df_anom["ScienceData/sensor_altitude"] is modified
    # in the below for loop, the result differs if the sat_alt
    # is defined below this for loop ....
    sat_alt = np.array(df_anom["ScienceData/sensor_altitude"])

    #n_along_new = np.shape(data['latitude'])[0]
    #n_height_new = np.shape(data['latitude'])[1]

    # this loop sets the dataframe arrays to the correct dimension
    # and data type, but does not yet fill it with data
    for key in df_anom['ScienceData']:
        dum_in = np.array(df_anom["ScienceData/"+key])
        shape = np.shape(dum_in)
        #print('DEBUG: key = ', key, 'shape = ', shape)

        # shape = () items are not changed

        if np.size(shape)==1:
            # conversion for 1D arrays
            if shape[0]==n_along_ori:
                # for horizontal arrays
                dum_1d_along=np.copy(data['along_track'])*0.0
                dum_1d_along=dum_1d_along.astype(dum_in.dtype)
                df_anom["ScienceData/"+key]=dum_1d_along[:]
                # this changes some arrays, dont know why
                #dum_1d_along = np.zeros(n_along_new, dtype=dum_in.dtype)
                #df_anom["ScienceData/"+key] = dum_1d_along
            elif shape[0] == n_height_ori:
                # for vertical arrays
                #print('key: ', key,' has shape ',
                #       df_anom["ScienceData/"+key].shape)
                #dum_1d_height = np.zeros(n_height_new, dtype=dum_in.dtype)
                dum_1d_height = df_anom["ScienceData/"+key]*0
                df_anom["ScienceData/"+key] = dum_1d_height
            else:
                print('No change in 1D array ',key)
        elif np.size(shape)==2:
            # conversion for 2D arrays
            if (shape[0] == n_along_ori) and (shape[1] == n_height_ori):
                #this_n_along = shape[0]
                #this_n_height = shape[1]
                #dum_2d = np.zeros((n_along_new, this_n_height),
                #                   dtype=dum_in.dtype)
                dum_2d = np.copy(data['latitude'])*0.0
                dum_2d = dum_2d.astype(dum_in.dtype)
                df_anom["ScienceData/"+key] = dum_2d[:,:]
                #print('DEBUG: key = ', key, 'shape = ', shape)
            else:
                print('No change in 2D array:' ,key)

    print('FN=', df_anom["HeaderData/FixedProductHeader/File_Name"])

    # Fill in the new data needed by the ATLID L2 processors
    #
    df_anom["HeaderData/FixedProductHeader/File_Name"]      = data['out_h5_file']
    df_anom["ScienceData/along_track"]                      = np.float32(data['along_track'][:])
    df_anom["ScienceData/time"]                             = np.float64(data['time'][:])
    df_anom["ScienceData/layer_temperature"]                = \
      np.float32(data['temp_int'][:,::-1])
    df_anom["ScienceData/surface_elevation"]                = \
      np.float32(data['surface_height'][:])
    df_anom["ScienceData/ellipsoid_latitude"]               = \
      np.float64(data['latitude'][:,-1])
    df_anom["ScienceData/ellipsoid_longitude"]              = \
      np.float64(data['longitude'][:,-1])
    df_anom["ScienceData/layer_pressure"]                   = \
      np.float32(data['pres_int'][:,::-1])
    df_anom["ScienceData/sample_altitude"]                  = \
      np.float32(data['height_out'][:,::-1])
    df_anom["ScienceData/sample_longitude"]                 = \
      np.float64(data['longitude'][:,::-1])
    df_anom["ScienceData/sample_latitude"]                  = \
      np.float64(data['latitude'][:,::-1])
    df_anom["ScienceData/sample_range"]                     = \
      np.float64(sat_alt[0]-data['height_out'][:,::-1])
    df_anom["ScienceData/mie_normalised_signal"]            = \
      np.float64(data['extinction'][:,::-1])
    df_anom["ScienceData/sensor_altitude"]                  = \
      np.float32(data['height_out'][:,0]*0.+data['sensor_alt'])
    df_anom["ScienceData/sensor_longitude"]                 = \
      np.float64(data['longitude'][:,0])
    df_anom["ScienceData/sensor_latitude"]                  = \
      np.float64(data['latitude'][:,0])
    df_anom["ScienceData/mie_attenuated_backscatter"]       = \
      np.float32(data['ATB_Mie_co_int'][:,::-1])
    df_anom["ScienceData/crosspolar_attenuated_backscatter"]= \
      np.float32(data['ATB_cr_int'][:,::-1])
    df_anom["ScienceData/rayleigh_attenuated_backscatter"]  = \
      np.float32(data['ATB_Ray_int'][:,::-1])
#    df_anom["ScienceData/mie_attenuated_backscatter"]       =np.float32(data['ATB_Mie_co)
    df_anom["ScienceData/mie_attenuated_backscatter_random_error"]       = \
      np.float32(data['d_ATB_Mie_co_int'][:,::-1])
    df_anom["ScienceData/rayleigh_attenuated_backscatter_random_error"]  = \
      np.float32(data['d_ATB_Ray_int'][:,::-1])
    df_anom["ScienceData/crosspolar_attenuated_backscatter_random_error"]= \
      np.float32(data['d_ATB_cr_int'][:,::-1])
    df_anom["ScienceData/mie_attenuated_backscatter_total_error"]        = \
      np.float32(data['d_ATB_Mie_co_int'][:,::-1])
    df_anom["ScienceData/rayleigh_attenuated_backscatter_total_error"]   = \
      np.float32(data['d_ATB_Ray_int'][:,::-1])
    df_anom["ScienceData/crosspolar_attenuated_backscatter_total_error"] = \
      np.float32(data['d_ATB_cr_int'][:,::-1])

    # create a local output file for which we will change the dimension names in  a moment
    # the file is moved to the right place after this

    outfile3='./OUTPUT_LOC.h5'
    df_anom.to_netcdf(outfile3)

    # now lets rename the dimension names so that the original codes
    # can be used
    #
    # first rename the old names and than move dim_0 and dim_1 to
    # the original names
    #
    with h5py.File(outfile3, 'r+') as h5_file:
        group = h5_file['ScienceData']
        group['along_track_original'] = group.pop('along_track')
        group['along_track'] = group.pop('dim_0')
        group['height_original'] = group.pop('height')
        group['height'] = group.pop('dim_1')
        h5_file.flush() # this is not needed when followed by a close()
        h5_file.close()
    #  #]

def update_xml_item(xml_text, tag_name, new_value):
    #  #[ update a single xml tag
    ''' a simple function that updates only the first occurrence
    of the given tag_name in the xml providing in xml_text
    '''
    start_tag = '<'+tag_name+'>'
    end_tag = '</'+tag_name+'>'
    start_index_value = (xml_text.find(start_tag) + len(start_tag))
    end_index_value = xml_text.find(end_tag, start_index_value)
    xml_text_updated = (xml_text[:start_index_value] + new_value +
                        xml_text[end_index_value:])
    return xml_text_updated
    #  #]

def create_header_file(input_template_file, file_to_create,
                       dirout, frame_id, creation_datetime,
                       orbit_number, start_datetime, stop_datetime):
    #  #[ create a HDR file
    '''
    common code for updating the HDR xml files
    '''

    dt_format_xml_hdr = "%Y-%m-%dT%H:%M:%S"
    start_dt_str_xml_hdr   = start_datetime.strftime(dt_format_xml_hdr)
    stop_dt_str_xml_hdr    = stop_datetime.strftime(dt_format_xml_hdr)
    creation_dt_str_xml_hdr = creation_datetime.strftime(dt_format_xml_hdr)
    # create a fake processing start datetime
    # by subtracting one minute from creation_datetime
    processing_start_dt = creation_datetime - datetime.timedelta(seconds=60)
    processing_start_dt_str_xml_hdr = \
      processing_start_dt.strftime(dt_format_xml_hdr)

    # Create the new output directory if needed
    output_dir = os.path.split(file_to_create)[0]
    os.makedirs(output_dir, exist_ok=True)
    print('New directory:', output_dir)

    with open(input_template_file, 'rt', encoding='ascii') as fd_hdr_in:
        text = fd_hdr_in.read()

    # Update to use the correct product name
    new_value = dirout
    for tag_name in ['File_Name',
                     'productName']:
        text = update_xml_item(text, tag_name, new_value)

    # update the Frame ID
    new_value = f'{frame_id}'
    tag_name = 'frameID'
    text = update_xml_item(text, tag_name, new_value)

    # update creation time
    new_value = 'UTC='+creation_dt_str_xml_hdr
    tag_name = 'Creation_Date'
    text = update_xml_item(text, tag_name, new_value)

    # update processingStartTime
    new_value = 'UTC='+processing_start_dt_str_xml_hdr
    tag_name = 'processingStartTime'
    text = update_xml_item(text, tag_name, new_value)

    # update processingStopTime
    new_value = 'UTC='+creation_dt_str_xml_hdr
    tag_name = 'processingStopTime'
    text = update_xml_item(text, tag_name, new_value)

    # update the orbit number
    new_value = f'{orbit_number}'
    tag_name = "orbitNumber"
    text = update_xml_item(text, tag_name, new_value)

    # update the start time
    new_value = 'UTC='+start_dt_str_xml_hdr
    for tag_name in ['Validity_Start',
                     'sensingStartTime',
                     'frameStartTime']:
        text = update_xml_item(text, tag_name, new_value)

    # update the end time
    new_value = 'UTC='+stop_dt_str_xml_hdr
    for tag_name in ['Validity_Stop',
                     'sensingStopTime',
                     'frameStopTime']:
        text = update_xml_item(text, tag_name, new_value)

    with open(file_to_create, 'wt', encoding='ascii') as fd_hdr_out:
        fd_hdr_out.write(text)

    print('created HDR file: ', file_to_create)
    #  #]

def copy_create_output_files(info_local):
    #  #[ copies files
    '''
    copies h5 and HDR files and updated the HDR fields
    where needed
    '''

    start_datetime      = info_local['start_datetime']
    stop_datetime       = info_local['stop_datetime']
    creation_datetime   = info_local['creation_datetime']
    orbitid             = info_local['orbitID']

    dir1                = info_local['output_dir']
    frame_id            = info_local['Frame_ID']
    orbit_number        = info_local['orbit']
    template_file_anom  = info_local['template_file_anom']
    template_file_xmet  = info_local['template_file_xmet']
    template_file_xjsg  = info_local['template_file_xjsg']

    dt_format_filename = "%Y%m%dT%H%M%SZ"
    start_dt_str_for_filename   = \
      start_datetime.strftime(dt_format_filename)
    creation_dt_str_for_filename = \
      creation_datetime.strftime(dt_format_filename)

    # ANOM1B file output name
    dirout1 = (
        f'ECA_EXAA_ATL_NOM_1B_{start_dt_str_for_filename}_'+
        f'{creation_dt_str_for_filename}_{orbitid}')
    # XMET output file
    dirout_xmet = (
        f'ECA_EXAA_AUX_MET_1D_{start_dt_str_for_filename}_'+
        f'{creation_dt_str_for_filename}_{orbitid}')
    # XJSG output file
    dirout_xjsg = (
        f'ECA_EXAA_AUX_JSG_1D_{start_dt_str_for_filename}_'+
        f'{creation_dt_str_for_filename}_{orbitid}')

    h5_out_0=dir1+'/'+dirout1+'/'+dirout1+'.h5'

    #
    # Create Header file and add to the correct directory
    #
    template_file_anom_hdr = os.path.splitext(template_file_anom)[0]+'.HDR'
    file_to_create = f'{dir1}/{dirout1}/{dirout1}.HDR'
    create_header_file(template_file_anom_hdr, file_to_create,
                       dirout1, frame_id, creation_datetime,
                       orbit_number, start_datetime, stop_datetime)

    #
    #  Create the XMET Output File
    #
    dirout_anom = np.copy(dirout1)
    dirout_anom = dirout_anom.astype('<U60')

    dirout1=dirout_xmet
    h5_out_1  =dir1+'/'+dirout_xmet+'/'+dirout_xmet+'.h5'

    #
    # Create Header file and add to the correct directory
    #
    template_file_xmet_hdr = os.path.splitext(template_file_xmet)[0]+'.HDR'
    file_to_create = f'{dir1}/{dirout1}/{dirout1}.HDR'
    create_header_file(template_file_xmet_hdr, file_to_create,
                       dirout1, frame_id, creation_datetime,
                       orbit_number, start_datetime, stop_datetime)

    # for now just copy the h5 file, no modifications are applied at all
    shutil.copy(template_file_xmet, f'{dir1}/{dirout1}/{dirout1}.h5')

    #
    # Create the XJSG output file
    #
    dirout_xmet = np.copy(dirout1)
    dirout1=dirout_xjsg
    h5_out_2  =dir1+'/'+dirout_xjsg+'/'+dirout_xjsg+'.h5'

    #
    # Create Header file and add to the correct directory
    #
    template_file_xjsg_hdr = os.path.splitext(template_file_xjsg)[0]+'.HDR'
    file_to_create = f'{dir1}/{dirout1}/{dirout1}.HDR'
    create_header_file(template_file_xjsg_hdr, file_to_create,
                       dirout1, frame_id, creation_datetime,
                       orbit_number, start_datetime, stop_datetime)

    # for now just copy the h5 file, no modifications are applied at all
    shutil.copy(template_file_xjsg, f'{dir1}/{dirout1}/{dirout1}.h5')

    # this should fix the permissions for all created files
    fix_permissions(dir1)

    dirout_xjsg = np.copy(dirout1)
    #
    files_out={
        'h5_out_0':h5_out_0,
        'h5_out_1':h5_out_1,
        'h5_out_2':h5_out_2,
        'dirout':dir1,
        'dirout_anom':dirout_anom,
        'dirout_xmet':dirout_xmet,
        'dirout_xjsg':dirout_xjsg}
    #
    return dirout_anom, dirout_xmet, dirout_xjsg,files_out
    #  #]

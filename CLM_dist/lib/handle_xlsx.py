#!/usr/bin/env python3
#
#  #[ documentation
#
#   Created on 23-Mar-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module implements loading of the user settings
from the xlsx config file.
'''
#  #]
#  #[ imported modules

# no need to open pandas, since below the surface it just uses openpyxl
import os
import openpyxl
from .custom_exceptions import FormatErrorXLSX
#from custom_exceptions import FormatErrorXLSX
from .config_file_definition import(
#from config_file_definition import(
    EXPECTED_CONFIG_FORMAT_VERSION,
    TABLE_POS_HEADERS, TABLE_POS_DEFINITIONS,
    LID_PARS_COL_HEADERS, LID_PARS_ROW_DEFINITIONS,
    DOMAIN_POS_HEADERS, DOMAIN_PARS_ROW_DEFINITIONS,
    QC_PARS_HEADERS, QC_PARS_ROW_DEFINITIONS,
    FILE_LIST_COL_HEADERS, FILE_LIST_ROW_DEFINITIONS,
    SPECIES_COUNT_HEADERS, SPECIES_COUNT_DEFINITIONS,
    SPECIES_TYPE_HEADERS, SPECIES_TYPE_DEFINITIONS,
    MAIN_COL_HEADERS,
    SIMVAR_METEO_DEFINITIONS, SIMVAR_GENERIC_DEFINITIONS,)
#  #]

def adapt_column_width_to_cell_content(wsh, ulc_index=None, lrc_index=None):
    #  #[ ensure we have a suitable width for a given column
    '''
    inspect cell contents and adapt cell widths accordingly
    optional inputs:
    ulc_index: upper_left_corner_index
    lrc_index: lower_right_corner_index
    '''
    debug = False
    if debug:
        print('inside adapt_column_width_to_cell_content: '+
            f'ulc_index={ulc_index}, lrc_index={lrc_index}')
    if (ulc_index is None) and (lrc_index is None):
        # get max cell width for each column
        # use a minimum width of 10
        dims = {}
        for row in wsh.rows:
            for cell in row:
                if cell.value:
                    dims[cell.column_letter] = max(
                        (dims.get(cell.column_letter, 10),
                        len(str(cell.value))))
    else:
        # only inspect part of the spreadsheed
        dims = {}
        # assume we have just 1 character
        column_index_start = ord(ulc_index[0])
        column_index_stop = ord(lrc_index[0])
        # the remainder is the row index
        row_index_start =  int(ulc_index[1:])
        row_index_stop =  int(lrc_index[1:])
        for row_index in range(row_index_start, row_index_stop+1):
            for col_index in range(column_index_start, column_index_stop+1):
                column_letter = chr(col_index)
                row_counter = int(row_index)
                cell_index = f'{column_letter:s}{row_counter:d}'
                #print('debug: ', cell_index)
                #print(cell_index, 'old col. width was: ',
                #      dims.get(column_letter, None))
                dims[column_letter] = max(
                    dims.get(column_letter, 10),
                    len(str(wsh[cell_index].value)))

    # set the cell width to ensure all text is visible
    for col, value in dims.items():
        if value > wsh.column_dimensions[col].width:
            wsh.column_dimensions[col].width = value
    #  #]

def merge_file_table_cells(wsh, ulc_index, lrc_index):
    #  #[ merge 6 columns for the file table
    '''
    merge the cells for this table to allow long filenames
    while not getting huge cells for the following simulation table
    '''
    #NOTE: this can only be done at the end since the merged cells
    #       become read-only, also for inseting text

    # wsh.merge_cells('E6:j6')
    # wsh.merge_cells('E7:j7')
    # wsh.merge_cells('E8:j8')

     # assume we have just 1 character
    column_index_start = ord(ulc_index[0]) - ord('A') + 1
    column_index_stop = ord(lrc_index[0]) - ord('A') + 1
    # the remainder is the row index
    row_index_start =  int(ulc_index[1:])
    row_index_stop =  int(lrc_index[1:])

    for row_index in range(row_index_start, row_index_stop+1):
        wsh.merge_cells(start_row=row_index, end_row=row_index,
                        start_column=column_index_start+1,
                        end_column=column_index_stop+5)
    #  #]

def apply_offset_to_cell_index(cell_index, hor_offset=0, ver_offset=0):
    #  #[ get index of neighbouring cell
    ''' a little function to calc. the cell index of a neighbouring cell '''
    cell_index_column_letter = cell_index[0] # assume we have just 1 character
    cell_index_row_counter = cell_index[1:] # the remainder is the row index

    updated_index_column_letter = chr(ord(cell_index_column_letter)+hor_offset)
    updated_index_row_counter = int(cell_index_row_counter)+ver_offset

    next_cell_index = (
        f'{updated_index_column_letter:s}'+
        f'{updated_index_row_counter:d}')

    # print('debug: next_cell_index = ', next_cell_index)

    return next_cell_index
    #  #]

def get_next_value(wsh, cell_index, hor_offset=0, ver_offset=0):
    #  #[ get content of a neighbouring cell
    '''
    given a cell index this function reads the content of a neighbouring cell
    '''
    next_cell_index = apply_offset_to_cell_index(cell_index,
                                                 hor_offset, ver_offset)

    value = wsh[next_cell_index].value
    if hasattr(value, 'strip'):
        value = value.strip()
    else:
        if value is not None:
            # convert to float, just to mimic what pandas.read_excel() is
            # doing in the original script from Dave
            # it does not really add anything but ensures numerical
            # reproducibility for the moment.
            value = float(value)
    return value
    #  #]

def verify_cell_value(cell_index, expected_value, actual_value):
    #  #[ check if the value is what we expect
    '''
    given a cell index and an actual and expected value, check the
    values against each other and raise an error if they dont match
    '''
    if actual_value != expected_value:
        errtxt = (
            'Unrecognised config format, '+
            f'cell [{cell_index}] should contain '+
            f'the value: [{expected_value}] but currently '+
            f'holds the value: [{actual_value}]')
        raise FormatErrorXLSX(errtxt)
    #  #]

def strip_if_possible(value):
    #  #[ strip if value is a string, do nothing otherwise
    ''' a little function to strip spaces in case the value is a string '''
    if hasattr(value, 'strip'):
        return value.strip()

    return value
    #  #]

def check_config_version(wsh):
    #  #[ extract and check
    '''
    extract and check the version of the config file
    '''
    row_name = strip_if_possible(wsh['A1'].value)
    config_format_version = strip_if_possible(str(wsh['B1'].value))

    # sanity check
    expected_row_name = EXPECTED_CONFIG_FORMAT_VERSION[0]
    verify_cell_value('A1', expected_row_name, row_name)

    # version check
    expected_config_format_vers = EXPECTED_CONFIG_FORMAT_VERSION[1]
    if expected_config_format_vers != config_format_version:
        errtxt = (('ERROR: you are trying to use a config file '+
            'with format version {}, but the current software '+
            'version expects a config file with version {}. '+
            'Please update the config file to the expected version.').
            format(config_format_version,
                   EXPECTED_CONFIG_FORMAT_VERSION))

        raise FormatErrorXLSX(errtxt)
    #  #]

# pylint: disable=too-many-instance-attributes
class SimulationVariable:
    '''
    a class to handle the 6 items defined for each simulation variable
    '''
    def __init__(self, cell_index, variable_name, toml_path, lkey):
        #  #[ init
        self.cell_index = cell_index
        self.variable_name = variable_name
        self.toml_path = toml_path
        self.key = lkey

        self.input_file = None
        self.file_variable = None
        self.dimension_order = None
        self.scale_factor = None
        self.shift = None
        self.fixed_value = None
        #  #]
    def load_from_worksheet(self, wsh):
        #  #[ load a variable and its properties
        '''
        load a single variable and its properties from the spreadsheet
        '''
        value = strip_if_possible(wsh[self.cell_index].value)
        verify_cell_value(self.cell_index,
                          expected_value=self.variable_name,
                          actual_value=value)

        # load the variable properties from the xlsx file
        self.input_file = get_next_value(wsh, self.cell_index, hor_offset=1)
        self.file_variable = get_next_value(wsh, self.cell_index, hor_offset=2)
        dimension_order = get_next_value(wsh, self.cell_index, hor_offset=3)
        self.scale_factor = get_next_value(wsh, self.cell_index, hor_offset=4)
        self.shift = get_next_value(wsh, self.cell_index, hor_offset=5)
        self.fixed_value = get_next_value(wsh, self.cell_index, hor_offset=6)

        # convert dimension_order to a list of integers
        self.dimension_order = []
        if dimension_order == 'constant':
            pass
        elif isinstance(dimension_order, float):
            self.dimension_order.append(int(dimension_order))
        elif isinstance(dimension_order, str):
            # assume it is a string formatted like this "0,1,2"
            parts = dimension_order.split(',')
            for part in parts:
                self.dimension_order.append(int(part))
        #  #]
    def enforce_presence_if_required(self, lkey):
        #  #[ check for required inputs
        ''' check for required inputs and raise an error if one is missing '''
        required_variables = [
            'met_height', 'met_temperature', 'met_pressure',
            'met_latitude', 'met_longitude',
            'height', 'latitude', 'longitude',
            ]

        if lkey in required_variables:
            if (self.input_file is None) and (self.fixed_value is None):
                errtxt = ('ERROR: it is required that the variable [{}] '+
                    'at cell index [{}] is defined but it seems missing '+
                    'at the moment.').format(self.variable_name,
                                             self.cell_index)
                raise FormatErrorXLSX(errtxt)
        #  #]
    def print(self):
        #  #[ print a variable and its properties
        '''
        print a single variable and its properties
        '''
        print('cell_index: ', self.cell_index,
              'variable_name: ', self.variable_name,
              'toml_path: ', self.toml_path,
              'input_file: ', self.input_file,
              'file_variable: ', self.file_variable,
              'dimension_order: ', self.dimension_order,
              'scale_factor: ', self.scale_factor,
              'shift: ', self.shift,
              'fixed_value: ', self.fixed_value)
        #  #]
# pylint: enable=too-many-instance-attributes

def generic_extract_from_table(wsh, ulc_index, col_headers, row_defs,
                               simvar=False):
    #  #[ extract the lidar variables
    '''
    a generic extract function to load a table
    from the spreadsheet
    '''
    # ulc_index = upper left corner of the table

    # verify config file format
    hor_offset = 0
    for expected_col_name in col_headers:
        col_cell_index = apply_offset_to_cell_index(ulc_index,
                               hor_offset=hor_offset)
        col_name = strip_if_possible(wsh[col_cell_index].value)
        verify_cell_value(col_cell_index, expected_col_name, col_name)
        hor_offset += 1

    # extract the values
    ldict_of_params = {}
    llist_of_keys = []
    ver_offset = 1
    for row_def in row_defs:
        row_index = apply_offset_to_cell_index(ulc_index,
                          ver_offset=ver_offset)
        lkey, expected_row_name = row_def[:2]
        if simvar:
            # we have a simulation variable instance
            # consisting of 6 elements:
            # input_file, file_variable, dimension_order,
            # scale_factor, shift and fixed_value
            toml_path = row_def[2]
            value = SimulationVariable(row_index, expected_row_name,
                                       toml_path, lkey)
            value.load_from_worksheet(wsh)
        else:
            # we have a normal variable
            row_name = strip_if_possible(wsh[row_index].value)
            verify_cell_value(row_index, expected_row_name, row_name)
            value = get_next_value(wsh, row_index, hor_offset=1)

        ldict_of_params[lkey] = value
        llist_of_keys.append(lkey)
        ver_offset += 1

    if simvar:
        # check if any input if defined for this species
        # if this is the case, filling the geolocation fields
        # for this species is required, so check this.
        any_input_defined = False
        for lkey in llist_of_keys:
            value = ldict_of_params[lkey]
            if value.input_file is not None:
                any_input_defined = True

        if any_input_defined:
            for lkey in llist_of_keys:
                value = ldict_of_params[lkey]
                value.enforce_presence_if_required(lkey)

    return ldict_of_params, llist_of_keys
    #  #]

def extract_table_positions(wsh):
    #  #[ load the table positions
    '''
    inspect and read the table holding the positions of the other tables
    '''
    tp_dict = {}
    # warning: the order of the next keys MUST be identical
    # to what is defined in config_file_definition.py!
    tp_list_of_keys =[
        'lidar_table', 'domain_table', 'qc_table',
        'file_table', 'meteo_simvar_table',
        'species_count_table', 'generic_simvar_table']

    # verify config file format
    for cell_index, expected_col_name in TABLE_POS_HEADERS:
        col_name = strip_if_possible(wsh[cell_index].value)
        verify_cell_value(cell_index, expected_col_name, col_name)

    for kwi, kword in enumerate(tp_list_of_keys):
        cell_index = TABLE_POS_DEFINITIONS[kwi][0]
        result = get_next_value(wsh, cell_index, hor_offset=1)
        tp_dict[kword] = result

    return tp_dict #, tp_list_of_keys
    #  #]

def extract_domain(wsh, tpos):
    #  #[ extract the domain definition
    '''
    extract the domain definition from the spreadsheet
    '''
    # Remark:
    # if iy1 and iy2 are empty then the intention is that the data
    # is a time-series of profiles and NOT 3D model data
    # NOTE ==> internally this is (for now) signalled by
    #          setting iy1 and iy2 to the value of -1

    # get location of the domain table
    ulc_index = tpos['domain_table']

    ldict_of_domain_params, llist_of_keys = \
      generic_extract_from_table(wsh, ulc_index, DOMAIN_POS_HEADERS,
                                 DOMAIN_PARS_ROW_DEFINITIONS)

    # some specific handling for this table
    for lkey in llist_of_keys:
        value = ldict_of_domain_params[lkey]

        # handle None values
        if value is None:
            value = -1

        # enforce integer values
        value = int(value)

        ldict_of_domain_params[lkey] = value

    return ldict_of_domain_params, llist_of_keys
    #  #]

def extract_lidar_params(wsh, tpos):
    #  #[ extract the lidar variables
    '''
    extract all lidar variables and their properties from the spreadsheet
    '''

    # get location of the lidar table
    ulc_index = tpos['lidar_table']

    ldict_of_lidar_params, llist_of_keys = \
      generic_extract_from_table(wsh, ulc_index, LID_PARS_COL_HEADERS,
                                 LID_PARS_ROW_DEFINITIONS)

    return ldict_of_lidar_params, llist_of_keys
    #  #]

def extract_qc_params(wsh, tpos):
    #  #[ extract the lidar variables
    '''
    extract all QC variables and their properties from the spreadsheet
    '''

    # get location of the lidar table
    ulc_index = tpos['qc_table']

    ldict_of_qc_params, llist_of_keys = \
      generic_extract_from_table(wsh, ulc_index, QC_PARS_HEADERS,
                                 QC_PARS_ROW_DEFINITIONS)

    return ldict_of_qc_params, llist_of_keys
    #  #]

def extract_file_list(wsh, tpos):
    #  #[ extract the data dir from cells G2:H2
    '''
    extract the data dir setting from the spreadsheet
    '''
    # get location of the file table
    cell_index = tpos['file_table']

    # verify config file format
    hor_offset = 0
    for expected_col_name in FILE_LIST_COL_HEADERS:
        col_cell_index = apply_offset_to_cell_index(cell_index,
                               hor_offset=hor_offset)
        col_name = strip_if_possible(wsh[col_cell_index].value)
        verify_cell_value(col_cell_index, expected_col_name, col_name)
        hor_offset += 1

    # extract the fixed values
    ldict_of_file_names = {}
    llist_of_keys = []
    ver_offset = 1
    for row_def in FILE_LIST_ROW_DEFINITIONS:
        row_index = apply_offset_to_cell_index(cell_index,
                          ver_offset=ver_offset)
        row_name = strip_if_possible(wsh[row_index].value)
        value = get_next_value(wsh, row_index, hor_offset=1)
        value = strip_if_possible(value)
        lkey = row_def[0]
        ldict_of_file_names[lkey] = value
        llist_of_keys.append(lkey)

        # sanity check
        expected_col_name = row_def[1]
        row_name = strip_if_possible(wsh[row_index].value)
        verify_cell_value(row_index, expected_col_name, row_name)

        ver_offset += 1

    # extract the variable values
    num_inp_files = int(ldict_of_file_names['num_inp_files'])
    # pylint: disable=unused-variable
    for ifile in range(num_inp_files):
    # pylint: enable=unused-variable
        row_index = apply_offset_to_cell_index(cell_index,
                          ver_offset=ver_offset)
        ref_name = strip_if_possible(wsh[row_index].value)
        file_name = get_next_value(wsh, row_index, hor_offset=1)

        ref_name = strip_if_possible(ref_name)
        file_name = strip_if_possible(file_name)

        ldict_of_file_names[ref_name] = file_name
        llist_of_keys.append(ref_name)
        ver_offset += 1

    return ldict_of_file_names, llist_of_keys
    #  #]

def extract_simulation_variables(wsh, ulc_index, row_defs):
    #  #[ load all simulation variables from a given table
    '''
    extract all simulation variables from the spreadsheet
    for one table (meteo or generic)
    '''
    col_headers = MAIN_COL_HEADERS
    ldict_of_sv, llist_of_keys = \
      generic_extract_from_table(wsh, ulc_index, col_headers,
                                 row_defs, simvar=True)

    return ldict_of_sv, llist_of_keys
    #  #]

def extract_species_data(wsh, ulc_index, species_index):
    #  #[ load data for a given species
    '''
    loads all definitions from the spreadsheet for a given species,
    first the type and name settings, and then
    all simulation variables.
    '''

    row_defs = SIMVAR_GENERIC_DEFINITIONS

    # first read the species type and name
    ldict_of_params, llist_of_keys = \
      generic_extract_from_table(wsh, ulc_index, SPECIES_TYPE_HEADERS,
                                 SPECIES_TYPE_DEFINITIONS)

    # move 3 rows down
    ulc_index2 = apply_offset_to_cell_index(ulc_index,
        hor_offset=0, ver_offset=3)

    # then read the remaining simulation variables
    ldict_of_sv, llist_of_sv_keys = \
      extract_simulation_variables(wsh, ulc_index2, row_defs)

    # replace <count> with the actual value in the keys
    llist_of_keys2 = []
    ldict_of_params2 = {}
    for lkey in llist_of_keys:
        new_key = lkey.replace('<count>', str(species_index))
        llist_of_keys2.append(new_key)
        ldict_of_params2[new_key] = ldict_of_params[lkey]

    llist_of_sv_keys2 = []
    ldict_of_sv2 = {}
    for lkey in llist_of_sv_keys:
        new_key = lkey.replace('<count>', str(species_index))
        llist_of_sv_keys2.append(new_key)
        ldict_of_sv2[new_key] = ldict_of_sv[lkey]

    #print('DEBUG: llist_of_keys2 = ', llist_of_keys2)
    #print('DEBUG: llist_of_sv_keys2 = ', llist_of_sv_keys2)

    return (llist_of_keys2, ldict_of_params2, llist_of_sv_keys2, ldict_of_sv2)
    #  #]

def extract_all_simulation_variable_tables(wsh, ltpos):
    #  #[ load all simulation variables from all tables
    '''
    extract all simulation variables from the spreadsheet
    for all table (meteo, water and ice, and aerosol)
    '''
    llist_of_keys = []
    ldict_of_params = {}
    ldict_of_sv = {}
    llist_of_sv_keys = []
    debug = False

    # load Meteo data
    if debug:
        print('debug: start load Meteo data')
    row_defs = SIMVAR_METEO_DEFINITIONS
    ulc_index = ltpos['meteo_simvar_table']
    dict_of_meteo_sv, list_of_meteo_sv_keys = \
      extract_simulation_variables(wsh, ulc_index, row_defs)
    ldict_of_sv.update(dict_of_meteo_sv)
    llist_of_sv_keys.extend(list_of_meteo_sv_keys)
    if debug:
        print('debug: finished load Meteo data')

    # load species count
    # get location of the species count table
    if debug:
        print('debug: start load species count')
    ulc_index = ltpos['species_count_table']

    ldict_of_species_count, llist_of_sp_count_keys = \
      generic_extract_from_table(wsh, ulc_index, SPECIES_COUNT_HEADERS,
                                 SPECIES_COUNT_DEFINITIONS)
    llist_of_keys.extend(llist_of_sp_count_keys)
    ldict_of_params.update(ldict_of_species_count)

    species_count = int(ldict_of_species_count['num_species'])
    if debug:
        print('species_count = ', species_count)
        print('debug: finished load species count')

    # load the generic data for each species
    ulc_index = ltpos['generic_simvar_table']
    for species_index in range(1, species_count+1):
        if debug:
            print('debug: start load species for index: ', species_index)
            print('DEBUG: ulc_index = ', ulc_index)
        (llist_of_sp_keys, ldict_of_sp_pars, llist_of_sp_sv_keys,
         ldict_of_sp_sv) = extract_species_data(wsh, ulc_index, species_index)

        ldict_of_params.update(ldict_of_sp_pars)
        llist_of_keys.extend(llist_of_sp_keys)

        ldict_of_sv.update(ldict_of_sp_sv)
        llist_of_sv_keys.extend(llist_of_sp_sv_keys)
        if debug:
            print('debug: finished load species for index: ', species_index)

        # move to next species block
        ulc_index = apply_offset_to_cell_index(ulc_index, ver_offset=17)

    return ldict_of_params, llist_of_keys, ldict_of_sv, llist_of_sv_keys
    #  #]

# pylint: disable=too-few-public-methods
class Input:
    ''' a little empty utility class al allow dot notation for
    parameters in an AllInputs instance '''
# pylint: enable=too-few-public-methods

# pylint: disable=too-few-public-methods,too-many-instance-attributes
class AllInputs:
    ''' define the a class to hold all spreadsheet inputs '''
    def __init__(self):
        #  #[ init
        self.data_dir = None
        self.domain = None
        self.file_list = Input()
        self.simulation_variables = Input()
        self.lidar_params = Input()
        self.species_params = Input()

        self.dict_of_qcp = {}
        self.list_of_qcp_keys = []

        self.dict_of_file_names = {}
        self.list_of_fn_keys = []

        self.dict_of_sv = {}
        self.list_of_sv_keys = []

        self.dict_of_lp = {}
        self.list_of_lp_keys = []

        self.dict_of_sp_params = {}
        self.list_of_sp_keys = []
        #  #]
    def load_all_from_worksheet(self, wsh):
        #  #[ load and store
        ''' a function to load it all in memory '''
        check_config_version(wsh)
        ltpos = extract_table_positions(wsh)

        # make results available with dot notation
        #for lkey in llist_of_fn_keys:
        #    setattr(self.file_list, lkey, ldict_of_file_names[lkey])

        ldict_of_lp, list_of_lp_keys = extract_lidar_params(wsh, ltpos)
        self.dict_of_lp = ldict_of_lp
        self.list_of_lp_keys = list_of_lp_keys

        # make results available with dot notation
        for lkey in list_of_lp_keys:
            setattr(self.lidar_params, lkey, ldict_of_lp[lkey])

        dict_of_domain_params = extract_domain(wsh, ltpos)[0]

        self.domain = (dict_of_domain_params['ix1'],
                       dict_of_domain_params['ix2'],
                       dict_of_domain_params['iy1'],
                       dict_of_domain_params['iy2'],)

        ldict_of_qcp, list_of_qcp_keys = extract_qc_params(wsh, ltpos)
        self.dict_of_qcp = ldict_of_qcp
        self.list_of_qcp_keys = list_of_qcp_keys

        ldict_of_file_names, llist_of_fn_keys = extract_file_list(wsh, ltpos)
        self.dict_of_file_names = ldict_of_file_names
        self.list_of_fn_keys = llist_of_fn_keys

        self.data_dir = ldict_of_file_names['data_dir']

        ldict_of_sp_params, llist_of_sp_keys, ldict_of_sv, llist_of_sv_keys = \
          extract_all_simulation_variable_tables(wsh, ltpos)
        self.dict_of_sv = ldict_of_sv
        self.list_of_sv_keys = llist_of_sv_keys
        self.dict_of_sp_params = ldict_of_sp_params
        self.list_of_sp_keys = llist_of_sp_keys

        # make results available with dot notation
        for lkey in llist_of_sp_keys:
            setattr(self.species_params, lkey, ldict_of_sp_params[lkey])

        # make results available with dot notation
        for lkey in llist_of_sv_keys:
            setattr(self.simulation_variables, lkey, ldict_of_sv[lkey])

        #  #]
# pylint: enable=too-few-public-methods,too-many-instance-attributes

if __name__ == '__main__': # pragma: no cover
    #  #[ main testing code

    # open an xlsx file as a workbook
    FN_XLSX = '../test/config_xlsx/Test_input_spread_sheet_GEM_data.xlsx'
    if not os.path.exists(FN_XLSX):
        FN_XLSX = 'test/config_xlsx/Test_input_spread_sheet_GEM_data.xlsx'

#    FN_XLSX = '../test/test.xlsx'

    workbook = openpyxl.load_workbook(filename=FN_XLSX)

    # point to the relevant sheet
    worksheet = workbook['Sheet1']

    check_config_version(worksheet)

    print('='*50)
    tabpos = extract_table_positions(worksheet)
    print(f'tabpos = [{tabpos}]')
    print('='*50)

    print('='*50)
    domain = extract_domain(worksheet, tabpos)
    print('domain = ', domain)

    print('='*50)
    dict_of_lidar_params, list_of_keys = extract_lidar_params(worksheet, tabpos)
    for key in list_of_keys:
        print(f'{key} : {dict_of_lidar_params[key]}')

    print('='*50)
    dict_of_qc_params, list_of_keys = extract_qc_params(worksheet, tabpos)
    for key in list_of_keys:
        print(f'{key} : {dict_of_qc_params[key]}')

    print('='*50)
    dd, dd_keys = extract_file_list(worksheet, tabpos)
    print(f'dd = [{dd}] dd_keys = [{dd_keys}]')

    print('='*50)
    dict_of_sp_params, list_of_sp_keys, dict_of_sv, list_of_sv_keys = \
      extract_all_simulation_variable_tables(worksheet, tabpos)
    for key in list_of_sp_keys:
        print(f'{key} : {dict_of_sp_params[key]}')
    for key in list_of_sv_keys:
        dict_of_sv[key].print()

    print('='*50)
    print('test load it all ...')
    all_inp = AllInputs()
    all_inp.load_all_from_worksheet(worksheet)

    print('all_inp.data_dir = ', all_inp.data_dir)

    print('all_inp.list_of_sp_keys = ', all_inp.list_of_sp_keys)
    for key in all_inp.list_of_sp_keys:
        print(f'{key} : {dict_of_sp_params[key]}')
    #print('all_inp.list_of_sv_keys = ', all_inp.list_of_sv_keys)

# pylint: disable=no-member
    print('all_inp.simulation_variables.species_3_wc.input_file = ',
          all_inp.simulation_variables.species_3_wc.input_file)
    print('all_inp.lidar_params.z1 = ',
          all_inp.lidar_params.z1)
# pylint: enable=no-member
    print('='*50)
    #  #]

#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 30-Aug-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
a generic module for loading input files.
It dispatches to the netcdf_handler or the hdf5_handler
to do the actual work.
'''
#  #]
#  #[ imported modules
import os
import datetime
import numpy as np
from .netcdf_handler import NetCDFfileCache
from .hdf5_handler import HDF5fileCache
from .data_loader import read_data
from .custom_exceptions import NotYetImplementedFeature
#  #]

def convert_time_offset_to_datetime(data, units):
    #  #[ convert to a proper datetime
    '''
    convert time variables that reports an offset against
    a reference date time to a proper python list of datetime objects
    '''

    time_offset = data
    ref_time_txt = units

    ref_dt = None
    data_dt = None
    if ref_time_txt.startswith('hours since'):
        # parse the reference.
        # typical value is:
        #   "hours since 2022-10-24 00:00:00 +00:00"
        dt_format = 'hours since %Y-%m-%d %H:%M:%S +00:00'
        ref_dt = datetime.datetime. strptime(ref_time_txt, dt_format)

        data_dt = [ref_dt + datetime.timedelta(hours=float(time_offs))
                   for time_offs in time_offset]

    if ref_time_txt.startswith('seconds since'):
        # parse the reference.
        # typical value is:
        #   "units = "seconds since 2020-02-17T00:00:00Z""
        dt_format_1 = 'seconds since %Y-%m-%dT%H:%M:%SZ'
        dt_format_2 = 'seconds since %Y-%m-%d %H:%M:%S UTC'
        try:
            # first try format 1
            ref_dt = datetime.datetime. strptime(ref_time_txt, dt_format_1)
        except ValueError:
            pass

        if ref_dt is None:
            try:
                # then try format 2
                ref_dt = datetime.datetime. strptime(ref_time_txt, dt_format_2)
            except ValueError:
                pass

        if ref_dt is not None:
            data_dt = [ref_dt + datetime.timedelta(seconds=float(time_offs))
                for time_offs in time_offset]

    if ref_dt is None:
        error_txt = ('Unrecognised time format in input file! '+
            f'Detected time format is: {ref_time_txt}. '+
            'Please contact the software developers to solve this.')
        raise NotYetImplementedFeature(error_txt)

    return np.array(data_dt)
    #  #]

def convert_time_string_to_datetime(data, units, key):
    #  #[ convert to a proper datetime
    '''
    convert time variables that reports date time as a text string
    to a proper python list of datetime objects
    '''

    time_arr = data
    time_format = units # input_units[key]

    #print('DEBUG: time_format = ', time_format)
    if time_format == 'YYYYMMDDThhmmssZ':
        dt_format = '%Y%m%dT%H%M%SZ'
    else:
        error_txt = ('Unrecognised time format in input file! '+
            f'Detected time format for key {key} is: {time_format}. '+
            'Please contact the software developers to solve this.')
        raise NotYetImplementedFeature(error_txt)

    # typical value is: '20220911T211401Z'
    data_dt = [datetime.datetime. strptime(time_txt, dt_format)
               for time_txt in time_arr]

    return np.array(data_dt)
    #  #]

def translate_ref_to_real_fn(all_inp, ref_name):
    #  #[ translate file reference
    '''
    translate a file reference code from the config file
    to a real file name.
    '''
    inp_file = all_inp.dict_of_file_names[ref_name]

    return inp_file
    #  #]

def load_input_data(all_inp):
    #  #[ load all input arrays
    '''
    load all needed input arrays from the input files
    '''
    input_data = {}
    # input_units = {} # not used

    data_dir = all_inp.data_dir

    # init infc (in file cache) using a variable that must always be present
    sim_var = all_inp.dict_of_sv['met_height']
    inp_file = translate_ref_to_real_fn(all_inp, sim_var.input_file)
    inp_file = os.path.join(data_dir, inp_file)

    # see what type of file we have
    ext = os.path.splitext(inp_file)[1]

    if ext.lower() == '.nc':
        # we have a NetCDF file
        infc = NetCDFfileCache(inp_file)
    elif ext.lower() == '.h5':
        # we have a HDF5 file
        infc = HDF5fileCache(inp_file)
    else:
        errtxt = (f'unsupported input file type: {ext} '+
            'If you really need this type of input file please contact '+
            'the developers of this software.')
        raise NotImplementedError(errtxt)

    # Read all the required data in memory
    for key in all_inp.list_of_sv_keys:
        sim_var = all_inp.dict_of_sv[key]
        if sim_var.input_file:
            inp_file = translate_ref_to_real_fn(all_inp, sim_var.input_file)
            inp_file = os.path.join(data_dir, inp_file)
            infc.update_to_new_fn(inp_file)

        # note: even if no file was specified there may be
        # a fixed value to be used, which is also handled
        # by the read_data subroutine, so we always call it.
        data, units, comment = read_data(sim_var, infc.in_ds, all_inp.domain)
        input_data[key] = data
        #input_units[key] = units

        if 'time' in key and input_data[key] is not None:
            # see if I can convert this time to a proper python datetime object
            if input_data[key].dtype in (np.float32, np.float64):
                input_data[key] = \
                  convert_time_offset_to_datetime(data, units)
            else:
                # this exception is needed for eVe data
                input_data[key] = \
                  convert_time_string_to_datetime(data, units, key)

        if data is not None:
            print(f'loaded data for key: {key} ({comment})')

    return infc, input_data # , input_units
    #  #]

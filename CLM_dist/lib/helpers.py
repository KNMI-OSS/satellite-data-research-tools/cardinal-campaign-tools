#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 26-May-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module implements some helper routines
that can be useful for performing tests.
'''
#  #]
#  #[ imported modules
import os
import sys
import subprocess
import matplotlib as mpl
from .custom_exceptions import ColorMapException, ToolUsageError
#  #]
#  #[ constants
colorvalues = {
    'grey':30,
    'red':31,
    'green':32,
    'yellow':33,
    'blue':34,
    'magenta':35,
    'cyan':36,
    'white':37}

DEV_MACHINE = 'pc230014'
#  #]

def textcolor(color, text):
    #  #[ add color to text
    ''' a little helper function to print coloured text '''
    color_val = colorvalues[color]
    colored_text = f'\033[{color_val:d}m{text:s}\033[0m'
    return colored_text
    #  #]

def run_shell_command(cmd, verbose=False):
    #  #[ execute a given cmd
    '''
    a wrapper routine around subprocess.Popen intended
    to make it a bit easier to call this functionality.
    Options:
    -verbose: give some feedback to the user while executing the
     code (usefull for debugging)
    '''

    if verbose:
        print("Executing command: ",cmd)

    subpr = subprocess.run(cmd, shell=True, capture_output=True,
                           check=False)

    # convert bytes to str
    text_stdout = subpr.stdout.decode('utf8', errors='replace')
    text_stderr = subpr.stderr.decode('utf8', errors='replace')
    exit_status = subpr.returncode

    # split lines
    lines_stdout = text_stdout.split('\n')
    lines_stderr = text_stderr.split('\n')

    # remove empty lines
    lines_stdout = [line for line in lines_stdout if line.strip() != '']
    lines_stderr = [line for line in lines_stderr if line.strip() != '']

    return (lines_stdout, lines_stderr, exit_status)
    #  #]

def get_cmap_wrapper(cm_name):
    #  #[
    ''' a little warpper around mpl.cm.get_cmap to prevent matplotlib to
    # issue lots of MatplotlibDeprecationWarnings '''
    if hasattr(mpl, 'colormaps'):
        # pylint: disable=no-member
        cmap = mpl.colormaps[cm_name]
        # pylint: enable=no-member
    elif hasattr(mpl, 'cm'):
        cmap = mpl.cm.get_cmap(cm_name)
    else:
        errtxt = ('Unknown colormaps interface for matplotlib. '+
            'It seems you are using a matplotlib version that is '+
            'not compatible with this software. '+
            'Please report this to the CCT maintainers.')
        raise ColorMapException(errtxt)
    return cmap
    #  #]

def ensure_rx(file):
    #  #[ set rx permission
    '''
    a little function to set read and execute permission on a given file
    '''
    if os.path.islink(file):
        # dont try to change symlinks
        return

    new_mode = int('755',8)
    os.chmod(file,new_mode)
    #  #]

def ensure_r(file):
    #  #[ set r permission
    '''
    a little function to set read permission on a given file
    '''
    if os.path.islink(file):
        # dont try to change symlinks
        return

    new_mode = int('644',8)
    os.chmod(file,new_mode)
    #  #]

def fix_permissions(dir_to_fix):
    #  #[ loop over dirs and files and fix permissions
    '''
    a little function that recursively walks along all files and
    sub-directories in the provided directory, and then
    sets read permission on all files and read+execute permission
    on all directories
    '''
    if os.path.isfile(dir_to_fix):
        ensure_r(dir_to_fix)
        return

    for (dirpath,dirnames,filenames) in os.walk(dir_to_fix,followlinks=False):
        ensure_rx(dirpath)
        for dirname in dirnames:
            ensure_rx(os.path.join(dirpath, dirname))
        for filename in filenames:
            ensure_r(os.path.join(dirpath, filename))
    #  #]

def check_in_sw_root():
    #  #[ ensure in sw root
    ''' use the presence of the README.md file to check
    if the curred working dir is the software root or not '''

    toolname = sys.argv[0]
    filelist = os.listdir('./')
    if 'README.md' not in filelist:
        current_path = os.getcwd()
        errortxt = (
            f'ERROR: the {toolname} tool should be executed in '+
            'the CCT software root, i.e. the location holding the README.md '+
            'file, but this file seems not present in the current directory: '+
            f'{current_path}')
        raise ToolUsageError(errortxt)
    #  #]

def on_devel_machine():
    #  #[ on devel machine?
    ''' see if we are on the developers machine or not '''
    machine_name = os.uname()[1]
    on_dev_machine = False
    if DEV_MACHINE in machine_name:
        on_dev_machine = True
    return on_dev_machine
    #  #]

def on_knmi_machine():
    #  #[ on devel machine?
    ''' see if we are on a KNMI workstation or not '''
    test_path = '/net/'+DEV_MACHINE
    running_on_knmi_machine = False
    if os.path.exists(test_path):
        if 'nobackup' in os.listdir(test_path):
            running_on_knmi_machine = True
    return running_on_knmi_machine
    #  #]

#!/usr/bin/env python3
#
#  #[ documentation
#
#   Created on 04-May-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module implements the python to c interfacing to the
multiscatter c-library using ctypes.
'''
#  #]
#  #[ imported modules
import os
import sys
import ctypes as c
import numpy as np
from .custom_exceptions import MultiScatterInterfaceError
#  #]

#  #[ ctypes interface

#############################################################################
#
# Enumeration and Structures definitions to enable ctype-multiscatter
# interoperability
#
#############################################################################

MsWideAngleAlgorithm = c.c_int
MS_WIDE_ANGLE_NONE = c.c_int(0)
MS_WIDE_ANGLE_TDTS_FORWARD_LOBE = c.c_int(1)
MS_WIDE_ANGLE_TDTS_NO_FORWARD_LOBE = c.c_int(2)
MS_NUM_WIDE_ANGLE_ALGORITHMS = c.c_int(3)

MsSmallAngleAlgorithm = c.c_int
MS_SINGLE_AND_SMALL_ANGLE_NONE = c.c_int(0)
MS_SINGLE_SCATTERING = c.c_int(1)
MS_SMALL_ANGLE_PVC_ORIGINAL = c.c_int(2)
MS_SMALL_ANGLE_PVC_FAST = c.c_int(3)
MS_SMALL_ANGLE_PVC_EXPLICIT = c.c_int(4)
MS_SMALL_ANGLE_PVC_FAST_LAG = c.c_int(5)
MS_NUM_SMALL_ANGLE_ALGORITHMS = c.c_int(6)

MsReceiverType = c.c_int
MS_TOP_HAT = c.c_int(0)
MS_GAUSSIAN = c.c_int(1)

class MsConfig(c.Structure):
    ''' defines the config structure '''
    _fields_ = [("small_angle_algorithm", MsSmallAngleAlgorithm),
                ("wide_angle_algorithm", MsWideAngleAlgorithm),
                ("options", c.c_int),
                ("max_scattering_order", c.c_int),
                ("max_theta", c.c_double),
                ("first_wide_angle_gate", c.c_int),
                ("coherent_backscatter_enhancement", c.c_double),
                ("small_angle_lag", c.POINTER(c.c_double)),
                ("total_src", c.c_double),
                ("total_reflected", c.c_double),
                ("ss_multiplier", c.c_double)]

###############
# Set defaults
###############

MS_CONFIG_DEFAULT = MsConfig(MS_SMALL_ANGLE_PVC_FAST,
                             MS_WIDE_ANGLE_TDTS_FORWARD_LOBE,
                             c.c_int(1),
                             c.c_int(4),
                             c.c_double(0.1),
                             c.c_int(0),
                             c.c_double(1.0),
                             None,
                             c.c_double(-1.0),
                             c.c_double(-1.0),
                             c.c_double(1.0))

MS_CONFIG_DEFAULT_SS = MsConfig(MS_SINGLE_SCATTERING,
                               MS_WIDE_ANGLE_NONE,
                               c.c_int(1),
                               c.c_int(4),
                               c.c_double(0.1),
                               c.c_int(0),
                               c.c_double(1.0),
                               None,
                               c.c_double(-1.0),
                               c.c_double(-1.0),
                               c.c_double(1.0))

class MsSurface(c.Structure):
    ''' defines the surface structure '''
    _fields_ = [("sigma0", c.c_double),
                ("diffuse_albedo", c.c_double),
                ("direct_to_diffuse_albedo", c.c_double),
                ("diffuse_to_direct_backscatter", c.c_double),
                ("range", c.c_double)]

MS_SURFACE_DEFAULT = MsSurface(c.c_double(0.0),c.c_double(0.0),
                               c.c_double(0.0),
                               c.c_double(0.0),c.c_double(0.0))

class MsInstrument(c.Structure):
    ''' defines the instrument structure '''
    _fields_ = [("receiver_type", MsReceiverType),
                ("altitude", c.c_double),
                ("wavelength", c.c_double),
                ("rho_transmitter", c.c_double),
                ("rho_receiver", c.POINTER(c.c_double)),
                ("nfov", c.c_int)]

ms_instrument_default = MsInstrument(MS_GAUSSIAN,\
                                     c.c_double(0.0),c.c_double(1.0),
                                     c.c_double(1.0),None,c.c_int(1))

# an alias for shorter notation
c_double_p = c.POINTER(c.c_double)

#  #]

class Multiscatter:
    ''' a class to make it more easy to call the
    multiscatter ctypes interface.
    '''
    def __init__(self, lidar_altitude):
        #  #[ init

        # some variables to store simulation settings
        self.config = None
        self.config_ss = None
        self.instrument = None

        # define the length of the arrays
        arr_size = np.shape(lidar_altitude)[0]

        # setup input constants
        # pylint: disable=invalid-name
        self.n = c.c_int(arr_size)
        self.m = self.n
        # pylint: enable=invalid-name

        # setup input array (also constant for now)
        #
        # We will need to pass the altitude NOT the range to MS
        #
        self.lidar_altitude = np.array(lidar_altitude, dtype=c.c_double)

        # these next 9 arrays define inputs
        # that vary for each profile, and are defined in
        # the setup_profile_inputs method.
        self.radius = None
        self.extinction = None
        self.ssa = None
        # pylint: disable=invalid-name
        self.g = None
        # pylint: enable=invalid-name
        self.ext_bscat_ratio = None
        self.ext_air = None
        self.ssa_air = None
        self.droplet_fraction = None
        self.pristine_ice_fraction = None

        # setup arrays to store the results
        self.atb_mie_1d = np.zeros(arr_size, dtype=c.c_double)
        self.atb_ray_1d = np.zeros(arr_size, dtype=c.c_double)

        # store pointers to the arrays as required by ctypes
        # We need to pass the lidar_altitude...NOT the range to MS
        self.lidar_altitude_p = self.lidar_altitude.ctypes.data_as(c_double_p)

        # these next 9 pointers are defined in
        # the setup_profile_inputs method.
        self.radius_p = None
        self.extinction_p = None
        self.ssa_p = None
        self.g_p = None
        self.ext_bscat_ratio_p = None
        self.ext_air_p = None
        self.ssa_air_p = None
        self.droplet_fraction_p = None
        self.pristine_ice_fraction_p = None

        self.atb_mie_1d_p = self.atb_mie_1d.ctypes.data_as(c_double_p)
        self.atb_ray_1d_p = self.atb_ray_1d.ctypes.data_as(c_double_p)

        # load the shared object multiscatter library
        exec_path = os.path.dirname(sys.argv[0])
        if exec_path=="":
            exec_path="."
        #exec_path = '..'

        # this allows running the code from any location,
        # also from outside the install/source directory,
        # as long as the src and lib folders are next to each other.
        lib_location = os.path.split(__file__)[0]
        so_path = os.path.join(lib_location,
            '../src/multiscatter-1.2.11/lib/libmultiscatter.so')

        # safety check
        if not os.path.exists(so_path):
            errtxt = ('ERROR: cannot load libmultiscatter.so!!! '+
                'Something is wrong with your configuration. '+
                'Maybe you forgot to run the make step?')
            raise MultiScatterInterfaceError(errtxt)

        self.libms=c.CDLL(so_path)
        #  #]
    def setup_config_settings(self, single_scatter):
        #  #[ setup config
        ''' choose which config to use '''
        if single_scatter:
            self.config = MS_CONFIG_DEFAULT_SS
        else:
            self.config = MS_CONFIG_DEFAULT
        #  #]
    def setup_instrument_settings(self, sat_alt, wavelen, laser_div, fov_t):
        #  #[ setup instrument
        ''' define the instrument struct needed as input
        for the simulation '''

        # Needs to be at least an array of 2 elements
        # a single element array will not pass pass properly !
        # (this is needed due to a change in newer numpy versions)
        rho_reciever = np.zeros(2,dtype=c.c_double)

        rho_reciever[:] = fov_t*1.0e-3/2 # mrad full angle to rads 1/2 angle

        # store pointers to the arrays as required by ctypes
        rho_reciever_p = rho_reciever.ctypes.data_as(c_double_p)

        self.instrument = MsInstrument(MS_GAUSSIAN,
            c.c_double(sat_alt*1.0e+3),                    # km == > m
            c.c_double(wavelen*1.0e-9),                    # nm ==> m
            c.c_double(laser_div*1.0e-3/2.0),              # mrad full angle to rads 1/2 angle
            rho_reciever_p,
            c.c_int(1))
        #  #]
    def setup_profile_inputs(self,
                             part_reff, part_extinction,
                             part_omega, part_g, s_ratio,
                             ext_ray, ssa_ray,
                             droplet_fraction_part,
                             pristine_ice_fraction_part):
        #  #[ setup profile
        '''
        store all needed profile inputs as instance variables.
        '''

        # per profile input arrays
        # Seems to be required in meters...comments are wrong in multiscatter
        self.radius = np.array(part_reff, dtype=c.c_double)

        self.extinction = np.array(part_extinction, dtype=c.c_double)
        self.ssa = np.array(part_omega, dtype=c.c_double)
        self.g = np.array(part_g, dtype=c.c_double)
        #
        # adjust the surface TODO!!
        #
        self.ext_bscat_ratio = np.array(s_ratio, dtype=c.c_double)
        self.ext_air = np.array(ext_ray, dtype=c.c_double)
        self.ssa_air = np.array(ssa_ray, dtype=c.c_double)
        self.droplet_fraction = np.array(droplet_fraction_part,
            dtype=c.c_double)
        self.pristine_ice_fraction = np.array(pristine_ice_fraction_part,
            dtype=c.c_double)

        # store pointers to the arrays as required by ctypes
        self.radius_p = self.radius.ctypes.data_as(c_double_p)
        self.extinction_p = self.extinction.ctypes.data_as(c_double_p)
        self.ssa_p = self.ssa.ctypes.data_as(c_double_p)
        self.g_p = self.g.ctypes.data_as(c_double_p)
        self.ext_bscat_ratio_p = self.ext_bscat_ratio.ctypes.data_as(c_double_p)
        self.ext_air_p = self.ext_air.ctypes.data_as(c_double_p)
        self.ssa_air_p = self.ssa_air.ctypes.data_as(c_double_p)
        self.droplet_fraction_p = self.droplet_fraction.ctypes.data_as(c_double_p)
        self.pristine_ice_fraction_p = self.pristine_ice_fraction.ctypes.data_as(c_double_p)
        #  #]
    def call(self):
        #  #[ do the simulation
        ''' call the libms.multiscatter ctypes function
        using all inputs defined in the class instance '''

        #status = self.libms.ms_print_stats_screen(config)
        #sys.exit(1)

        status = self.libms.multiscatter(
            # inputs
            self.n,
            self.m,
            c.byref(self.config),
            self.instrument,
            MS_SURFACE_DEFAULT,
            self.lidar_altitude_p,
            self.radius_p,
            self.extinction_p,
            self.ssa_p,
            self.g_p,
            self.ext_bscat_ratio_p,
            self.ext_air_p,
            self.ssa_air_p,
            self.droplet_fraction_p,
            self.pristine_ice_fraction_p,
            # results
            self.atb_mie_1d_p,
            self.atb_ray_1d_p)

        if status != 0:
            errtxt = ('ERROR: the libms.multiscatter() call returned with '+
                'non-zero status! probably something is very wrong... '+
                f'(return status was {status})')
            raise MultiScatterInterfaceError(errtxt)
        #  #]

if __name__ == '__main__': # pragma: no cover
    print('MS_TOP_HAT = ', MS_TOP_HAT)
    print('MS_GAUSSIAN = ', MS_GAUSSIAN)

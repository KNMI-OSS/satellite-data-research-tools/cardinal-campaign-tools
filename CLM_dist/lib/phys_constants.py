#!/usr/bin/env python3

#  #[ documentation
#   Copyright (C) 2022-2023  KNMI
#
#   Author: Dave Donovan <dave.donovan@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module defines some useful Physical constants
'''
#  #]

#----------------------------------------
# Define some useful Physical constants
#----------------------------------------

MW_AIR = 28.966        # g/mol
K_BOLTZ = 1.380658e-23 # Nm/K
AV_NUM = 6.0222e+23    # molecules/mole
CP = 1004              # J/Kg/K
CV = 717               # J/Kg/K
RG = 8.314             # J/mole/K Universal gass cnst
RHO_W = 1.0e+6         # g/m^3
RHO_I = 0.92e+6        # g/m^3
C_LIGHT = 3.0e+8       # m/s
H_PLANCK = 6.6226e-34  # J.s
DEG2RAD = 1.7453292519943295E-002

# some profiles are not defined on all array locations
# and usually NaN values are used to indicate this.
# To be able to continue processing we need to assume
# a value anyway (usually 0 or 1) on this locations.
# This value is different for different atmospheric properties
# and is defined here.
MISSING_REPLACEMENT_EXTINCTION = 0.0
MISSING_REPLACEMENT_BACKSCATTERRATIO = 1.0 # also indicated as S
MISSING_REPLACEMENT_DEPOLARISATION = 0.0
MISSING_REPLACEMENT_SPECIES_ID = 0
# even for meteo inputs NaN values occur (below the surface)
MISSING_REPLACEMENT_WIND_U = 0.0
MISSING_REPLACEMENT_WIND_V = 0.0

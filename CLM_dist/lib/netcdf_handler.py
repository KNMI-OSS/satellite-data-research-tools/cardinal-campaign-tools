#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 17-May-2023
#
#   Copyright (C) 2023-2024  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#
#  LAST_CHANGES:
#
# Jun 11, 2024 : D.D. : Added atb_mie_cr
#
#
'''
a module to collect the needed NetCDF file reading and writing code.
'''
#  #]
#  #[ imported modules
import datetime
import numpy as np
from numpy import dtype

# note that Dataset actually is defined in netCDF4._netCDF4
# so prefent pylint from compaining about this
# pylint: disable=no-name-in-module
from netCDF4 import Dataset
# pylint: enable=no-name-in-module

from .file_cache import InputFileCache
from .custom_exceptions import AtmosphereFormattingError
#  #]

class NetCDFfileCache(InputFileCache):
    #  #[ NetCDF specific file cache
    '''
    a little derived class to cache the currently open NetCDF file
    for reuse, and only open a new one if needed.
    '''
    def open_in_file(self):
        return Dataset(self.in_file)
    def close_in_file(self):
        self.in_ds.close()
    #  #]

class NetCdfFileHandler:
    #  #[ netcdf file creation handler
    ''' a class to assist in creating a new NetCDF file '''
    def __init__(self, list_of_species, outfile):
        #  #[ init variables for later use
        self.list_of_species = list_of_species
        self.outfile = outfile

        # open a new netcdf dataset
        self.ncfile = Dataset(self.outfile, 'w')

        # init variables to None
        self.num_x = None
        self.num_y = None
        self.num_z = None
        self.dims = None

        self.sattelite_range_mid_var = None
        self.altitude_mid_var = None
        self.time_var = None
        self.latitude_var = None
        self.longitude_var = None
        self.atb_mie_var = None
        self.atb_ray_var = None
        self.atb_mie_co_var = None
        self.atb_ray_co_var = None
        self.atb_cr_var = None
        self.atb_mie_cr_var = None
        self.ms_ss_ratio = None
        self.part_depol_var = None
        self.temperature_var = None
        self.pressure_var = None
        self.ray_ext_var = None
        self.ray_beta_var = None

        self.lat_of_sensor_var = None
        self.lon_of_sensor_var = None
        self.u_wind_var = None
        self.v_wind_var = None
        #  #]
    def new_var(self, name, unit, dims):
        #  #[ add a variable
        ''' adds a new variable to the netcdf Dataset '''
        var = self.ncfile.createVariable(
                name, dtype('float64').char, dims)
        var.setncattr_string('units', unit)
        return var
        #  #]
    def add_and_fill_new_var(self, nc_var_name, nc_var_unit, data):
        #  #[ add and fill a variable
        ''' adds a new variable to the netcdf Dataset
        and fills it with data.
        This routine is only intended to be used for the arrays on the
        full 2D or 3D grid, so not for the dimension arrays
        (lat, lon, alt or time).'''
        var = self.ncfile.createVariable(
                nc_var_name, dtype('float64').char, self.dims)
        var.setncattr_string('units', nc_var_unit)
        var[...] = data[...]
        # for now there is no need to return this var to the main function
        #  #]
    def define_netcdf_file(self, num_x, num_y, num_z):
        #  #[ define all variables
        ''' define all variables that we need to export '''

        self.num_x = num_x
        self.num_y = num_y
        self.num_z = num_z

        # create 2 or 3 dimensions
        if num_y==0:
            # 2D case, x=time, z=altitude
            self.ncfile.createDimension('t', num_x)
            self.ncfile.createDimension('z', num_z)
        else:
            # 3D casem x,y=lat,lon, z=altitude
            self.ncfile.createDimension('x', num_x)
            self.ncfile.createDimension('y', num_y)
            self.ncfile.createDimension('z', num_z)

        self.sattelite_range_mid_var = self.new_var('range_mid', 'm', ('z'))
        self.altitude_mid_var = self.new_var('altitude_mid', 'm', ('z'))

        if num_y==0:
            # 2D case, x=time, z=altitude
            time_unit = "hours since 2022-10-24 00:00:00 +00:00"
            self.time_var = self.new_var('Time', time_unit, ('t',))
            dims = ('z','t')
            self.lat_of_sensor_var = self.new_var(
                'latitude_of_sensor', 'deg', ('t',))
            self.lon_of_sensor_var = self.new_var(
                'longitude_of_sensor', 'deg', ('t',))
        else:
            # 3D casem x,y=lat,lon, z=altitude
            self.latitude_var = self.new_var('Latitude', 'DegN', ('y','x'))
            self.longitude_var = self.new_var('Longitude', 'DegW', ('y','x'))
            dims = ('z','y','x')

        self.dims = dims

        self.atb_mie_var = self.new_var('ATB_Mie', 'm-1 sr-1', dims)
        self.atb_ray_var = self.new_var('ATB_Ray', 'm-1 sr-1', dims)
        self.atb_mie_co_var = self.new_var('ATB_Mie_co','m-1 sr-1', dims)
        self.atb_mie_cr_var = self.new_var('ATB_Mie_cr','m-1 sr-1', dims)
        self.atb_ray_co_var = self.new_var('ATB_Ray_co','m-1 sr-1', dims)
        self.atb_cr_var = self.new_var('ATB_cr', 'm-1 sr-1', dims)
        self.ms_ss_ratio = self.new_var('MS_SS_ratio', '1', dims)
        self.part_depol_var = self.new_var(
            'linear_particulate_depolarization_ratio', '1', dims)
        self.temperature_var = self.new_var('temperature', 'K', dims)
        self.pressure_var = self.new_var('pressure', 'Pa', dims)
        self.u_wind_var = self.new_var('u_wind', 'm/s', dims)
        self.v_wind_var = self.new_var('v_wind', 'm/s', dims)
        self.ray_ext_var = self.new_var('molecular_extinction', 'm-1', dims)
        self.ray_beta_var = self.new_var(
            'molecular_rayleigh_backscatter', 'm-1 sr-1', dims)
        #  #]
    def fill_with_data(self, v_ax, input_data, time_key_to_use,
                       atb_mie, atb_ray, atb_co_mie, atb_co_ray,
                       atb_cr, atb_cr_mie, ms_ss_ratio, depol_tot,
                       temperature, pressure, u_wind, v_wind,
                       ext_ray, beta_ray,
                       latitude_of_sensor, longitude_of_sensor):
        #  #[ fill all variables
        ''' fill a given netcdf variable with data '''
        self.sattelite_range_mid_var[:] = v_ax.lid_range[:]
        self.altitude_mid_var[:] = v_ax.lid_alt[:]

        if self.num_y == 0:
            # 2D case, x=time, z=altitude
            if time_key_to_use is None:
                error_txt = ('Error: cannot find a time key to use. '+
                    'Please define a time variable for your data.')
                raise AtmosphereFormattingError(error_txt)

            first_dt = input_data[time_key_to_use][0]
            ref_dt = datetime.datetime(
                first_dt.year, first_dt.month, first_dt.day, 0, 0, 0)

            time = [(t - ref_dt).total_seconds()/3600.
                    for t in input_data[time_key_to_use]]
            self.time_var[:] = np.array(time)

            self.lat_of_sensor_var[...] = latitude_of_sensor[0,:]
            self.lon_of_sensor_var[...] = longitude_of_sensor[0,:]
        else:
            # 3D casem x,y=lat,lon, z=altitude
            # use geolocation as defined for the first species
            species_index = 1
            latitude_key_to_use = f'species_{species_index}_latitude'
            longitude_key_to_use = f'species_{species_index}_longitude'
            self.latitude_var[...] = input_data[latitude_key_to_use]
            self.longitude_var[...] = input_data[longitude_key_to_use]

        self.atb_mie_var[...] = atb_mie[...]
        self.atb_ray_var[...] = atb_ray[...]
        self.atb_mie_co_var[...] = atb_co_mie[...]
        self.atb_ray_co_var[...] = atb_co_ray[...]
        self.atb_cr_var[...] = atb_cr[...]
        self.atb_mie_cr_var[...] = atb_cr_mie[...]
        self.ms_ss_ratio[...] = ms_ss_ratio[...]
        self.part_depol_var[...] = depol_tot[...]
        self.temperature_var[...] = temperature[...]
        self.pressure_var[...] = pressure[...]
        self.u_wind_var[...] = u_wind[...]
        self.v_wind_var[...] = v_wind[...]
        self.ray_ext_var[...] = ext_ray[...]
        self.ray_beta_var[...] = beta_ray[...]

        for spc in self.list_of_species:
            if spc.species_used:

                nc_var_name = f'Extinction_{spc.species_name}'
                if spc.selected_species_id is not None:
                    nc_var_name += f'_id_{spc.selected_species_id}'

                self.add_and_fill_new_var(nc_var_name,'m-1', spc.extinction)

                nc_var_name = f'Reff_{spc.species_name}'
                if spc.selected_species_id is not None:
                    nc_var_name += f'_id_{spc.selected_species_id}'

                self.add_and_fill_new_var(nc_var_name,'microns', spc.reff)
        #  #]
    def close(self):
        #  #[ close the file
        ''' close the netcdf Dataset and write out to file '''
        self.ncfile.close()
        #  #]
    #  #]

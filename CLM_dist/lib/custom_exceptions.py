#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 17-May-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module implements some custom exceptions for this software.
'''
#  #]

class CctException(Exception):
    ''' a generic base exception for this project
    (should not be directly used) '''

class UntestedFeature(CctException):
    ''' raised for untested features '''

class NotYetImplementedFeature(CctException):
    ''' raised for not yet implemented features '''

class ProgrammingError(CctException):
    ''' raised in case a programming error seems present'''

class AtmosphereFormattingError(CctException):
    ''' raised if there is a problem in the user config file '''

class AtmosphereCalculationError(CctException):
    ''' raised in case a calculation does not have the needed inputs'''

class IterationError(CctException):
    ''' raised if the index class has incorrect inputs '''

class MultiScatterInterfaceError(CctException):
    ''' raised in case the multiscatter routine returns
    with a non zero status result '''

class FormatErrorXLSX(CctException):
    ''' a custom error for the XLSX handling '''

class PlotException(CctException):
    ''' a base class for plotting problems
    (should not be directly used) '''

class ColorMapException(PlotException):
    ''' raised in case the color map handling interface fails '''

class CommandLineInputError(CctException):
    ''' raised in case use input on the commandline is invalid. '''

class FileGenerationError(CctException):
    ''' raised in case generation of an output file fails. '''

class ToolUsageError(CctException):
    ''' raised in case a tool is used in an incorrect way. '''

class FilehandlingError(CctException):
    ''' raised in case a file could not be copied, renamed or moved. '''

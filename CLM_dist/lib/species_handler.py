#!/usr/bin/env python3
#
#  #[ documentation
#
#   Created on 01-May-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module implements specific calculations and data storage
for each simulated atmospheric species
'''
#  #]
#  #[ imported modules
import os
#import sys
import numpy as np
from .custom_exceptions import (
    # UntestedFeature,
    AtmosphereFormattingError,
    AtmosphereCalculationError)
from .input_file_loader import translate_ref_to_real_fn
from .data_loader import read_attribute, define_missing_replacement #, read_data
#from .custom_exceptions import NotYetImplementedFeature
#  #]

def clip_to_valid_range(all_inp, profile_not_interpolated, key):
    #  #[ clip to valid range
    '''
    It was found that the eVe lidar data had some unphysical numbers
    so we had to do some QC of our own to let the multiscatter code
    work on this data.
    Dave Donovan suggested to apply clipping of certain parameters
    to ensure they are in a sensible range:
    * extinction should be between 0. and 10. per km
    * lidar_ratio S should be between 10 and 200
    * lin_depol_ratio should be between 0 and 0.5
    This current function implements this clipping
    "in place" in the provided not interpolated profile.
    '''

    min_allowed_value = None
    max_allowed_value = None
    if key.endswith('_extinction'): # valid extinction range
        min_allowed_value = all_inp.dict_of_qcp['ext_min_allowed_value']
        max_allowed_value = all_inp.dict_of_qcp['ext_max_allowed_value']
    if key.endswith('_s'): # valid lidar ratio range
#        if not 'species_' in key:
        min_allowed_value = all_inp.dict_of_qcp['lidar_ratio_min_allowed_value']
        max_allowed_value = all_inp.dict_of_qcp['lidar_ratio_max_allowed_value']
    if key.endswith('_lin_depol'):
        min_allowed_value = all_inp.dict_of_qcp['lin_depol_min_allowed_value']
        max_allowed_value = all_inp.dict_of_qcp['lin_depol_max_allowed_value']

    # skip check if key is not one of *_extinction, *_s, *_lin_depol
    if (min_allowed_value is None) or (max_allowed_value is None):
        return

    # skip check if max < min
    if max_allowed_value < min_allowed_value:
        # skipping check
        return

    warning_issued = False
    if min_allowed_value is not None:
        selection = np.where(
            profile_not_interpolated < min_allowed_value)
        if len(selection[0]) > 0:
            profile_not_interpolated[selection] = min_allowed_value
            print(f'WARNING: clipped {len(selection[0])} out of '+
                  f'{len(profile_not_interpolated)} values '+
                  f'in the profile for {key} (values were too low, '+
                  f'i.e. below {min_allowed_value})')
            warning_issued = True

    if max_allowed_value is not None:
        selection = np.where(
            profile_not_interpolated > max_allowed_value)
        if len(selection[0]) > 0:
            profile_not_interpolated[selection] = max_allowed_value
            print(f'WARNING: clipped {len(selection[0])} out of '+
                  f'{len(profile_not_interpolated)} values '+
                  f'in the profile for {key} (values were too high, '+
                  f'i.e. above {max_allowed_value})')
            warning_issued = True

    if warning_issued:
        print('WARNING: please check your data and the config file to make '+
              'sure you have no mistake in the scaling and applied units!')

    #  #]

def calc_reff(input_data, species, v_ax, key_wc,
              z_inp, idx, ext_not_interp):
    #  #[ calc water reff or rain or ice effective radius
    ''' calculated effective radius '''

    # TODO: adapt this routine to the case that
    # wc_not_interp or ext_not_interp are scalars

    # could also allow a check to see if the size of the select
    # array matches the interpolated size or not, and decide from
    # that how to do the calculation.

    wc_available = True
    if key_wc is None:
        wc_available = False
    elif input_data[key_wc] is None:
        wc_available = False
    if not wc_available:
        errtxt = (
            'ERROR in calc_reff: water content is not defined for '+
            f'species {species}. Cannot calculate reff. Please adapt your '+
            'config settings to ensure this property is available, i.e. '+
            'make sure either reff or wc and ext are available')
        raise AtmosphereCalculationError(errtxt)

    wc_not_interp = input_data[key_wc][idx.ci]*1.0e-6
    #wc = v_ax.interpolate_to_lidar_range(z_inp, wc_not_interp,
    #                                     key='_wc')

    pos_ext = np.where(ext_not_interp > 0.0)

    # first do the calculation, then interpolate
    reff_not_interp = np.zeros(wc_not_interp.shape)
    reff_not_interp[pos_ext] = (wc_not_interp[pos_ext] /
                                ext_not_interp[pos_ext]) * 1.5/np.pi

    reff = v_ax.interpolate_to_lidar_range(z_inp, reff_not_interp,
                                           key='_reff')
    return (reff, reff_not_interp)
    #  #]

def calc_extinction(species_index, species_name, species_type,
                    input_data, key_wc, key_reff,
                    v_ax, z_inp, idx):
    #  #[ calc water or rain or ice extinction
    '''
    calculate extinction from wc and reff
    for now this function assumes wc and reff are model data
    on a 3D array.
    '''

    # construct a species text variable (only used in error prints)
    species_text = (f'species:{species_index} '+
        f'with type:{species_type} and name: {species_name}')

    if input_data[key_wc] is None:
        errtxt = ('ERROR in calc_extinction: '+
            'water content is not defined for '+
            f'{species_text}. Cannot calculate extinction. Please adapt '+
            'your config settings to ensure this property is available')
        raise AtmosphereCalculationError(errtxt)

    if input_data[key_reff] is None:
        errtxt = ('ERROR in calc_extinction: '+
            'eff. radius is not defined for '+
            f'{species_text}. Cannot calculate extinction. Please adapt '+
            'your config settings to ensure this property is available')
        raise AtmosphereCalculationError(errtxt)

    wc_is_array = False
    if isinstance(input_data[key_wc], np.ndarray):
        wc_is_array = True

    if wc_is_array:
        wc_not_interp = input_data[key_wc][idx.ci]
    else:
        # just a constant float in this case
        wc_not_interp = input_data[key_wc]

    reff_is_array = False
    if isinstance(input_data[key_reff], np.ndarray):
        reff_is_array = True

    if reff_is_array:
        reff_not_interp = input_data[key_reff][idx.ci]
    else:
        # just a constant float in this case
        reff_not_interp = input_data[key_reff]

    # sanity check
    if wc_is_array and reff_is_array:
        # check if they have the same shape
        if np.shape(wc_not_interp) != np.shape(reff_not_interp):
            errtxt = ('ERROR in calc_extinction: wc and reff arrays '+
                'should have the same size')
            raise AtmosphereCalculationError(errtxt)

    # sanity check
    if not reff_is_array:
        # check if the scalar is positive
        if reff_not_interp <= 0.:
            errtxt = ('ERROR in calc_extinction: reff should be positive '+
                f'but currently has the constant value of {reff_not_interp}.')
            raise AtmosphereCalculationError(errtxt)

    if reff_is_array:
        reff_positive = np.where(reff_not_interp > 0.0)

    # first do the calculation, then interpolate

    # case 1: both wc and reff are an array
    if wc_is_array and reff_is_array:
        ext_not_interp = np.zeros(reff_not_interp.shape)
        ext_not_interp[reff_positive] = \
            (wc_not_interp[reff_positive] /
            reff_not_interp[reff_positive] ) * 1.5/np.pi # 1/m

    # case 2: wc in a scalar, reff is an array
    if (not wc_is_array) and reff_is_array:
        ext_not_interp = np.zeros(reff_not_interp.shape)
        ext_not_interp[reff_positive] = \
            (wc_not_interp /
            reff_not_interp[reff_positive] ) * 1.5/np.pi # 1/m

    # case 3:reff is a (positive) scalar
    if not reff_is_array:
        ext_not_interp = (wc_not_interp / reff_not_interp ) * 1.5/np.pi # 1/m

    ext = v_ax.interpolate_to_lidar_range(z_inp, ext_not_interp,
                                          key='_extinction')

    return (ext, ext_not_interp) # returns calculated extinction
    #  #]

class SpeciesHandler:
    ''' a class to collect common functionality for all species '''
    def __init__(self, input_data, all_inp,
                 v_ax, size_def, species_index, species_by_id=None):
        #  #[ init
        '''
        implemented for these species:
        ice_1, ice_2, ice_3
        '''
        self.species_index = species_index

        self.selected_species_id = None
        if species_by_id is not None:
            # defines the atmosph. species to handle
            print('species_by_id = ', species_by_id)
            self.selected_species_id = species_by_id
            self.species = 'generic'

        self.input_data = input_data # holds all incoming data
        self.all_inp = all_inp # holds all configuration data

        # save the type/name setting
        self.species_type = self.all_inp.dict_of_sp_params[
            f'species_{species_index}_type']
        self.species_name = self.all_inp.dict_of_sp_params[
            f'species_{species_index}_name']

        self.v_ax = v_ax
        self.numz_orig = v_ax.nz_orig
        self.numz_interp = v_ax.nz_interp
        self.size_def = size_def

        # set up arrays to store interpolated profiles
        self.extinction = np.zeros(size_def)
        #self.backscatter = np.zeros(size_def) # currently not used
        self.reff = np.zeros(size_def)
        self.s = np.zeros(size_def)
        self.lin_depol = np.zeros(size_def)
        self.g = np.zeros(size_def)
        self.inv_s = np.zeros(size_def)

        #print('DEBUG: input_data.keys() = ', input_data.keys())

        # choose keys to extract the right data
        self.key_ext = f'species_{species_index}_extinction'
        self.key_wc = f'species_{species_index}_wc'
        self.key_reff = f'species_{species_index}_reff'
        self.key_s = f'species_{species_index}_s'
        self.key_lin_depol = f'species_{species_index}_lin_depol'
        self.key_g = f'species_{species_index}_g'

        if self.selected_species_id is not None:
            self.key_species_id = f'species_{species_index}_id'
            # ... 'species_{species_index}_ids_to_include'

        # check if any inputs at all are defined for this species
        any_input_defined = False
        if self.input_data[self.key_ext] is not None:
            any_input_defined = True
        # for aerosol key_wc is set to None, so check for that
        # before checking if we have data
        if self.key_wc is not None:
            if self.input_data[self.key_wc] is not None:
                any_input_defined = True
        if self.input_data[self.key_reff] is not None:
            any_input_defined = True
        if self.input_data[self.key_s] is not None:
            any_input_defined = True
        if self.input_data[self.key_lin_depol] is not None:
            any_input_defined = True
        if self.input_data[self.key_g] is not None:
            any_input_defined = True

        self.species_used = False
        if any_input_defined:
            self.species_used = True
        #  #]
    def select_profile(self, key, z_inp, idx, select=None, mask=None):
        #  #[ select and interpolate
        '''
        select a profile from either 1D, 2D or 3D data
        and interpolate it to the desired vertical grid
        or just use a constant for filling the profile.
        Returns both uninterpolated and interpolated array.

        Optional input is the boolean select array, intended to select
        only certain array indices for filling. Others will be set to zero.
        The indices in this array were generated by numpy.where
        and should address the target interpolated array size (numz_interp).

        The optional mask array selects which elements from the incoming
        original array should be used (based on species_id).
        Others will be set to a missing default value.
        The indices in this mask array are the result of a numpy.where() call
        and should address the original UNinterpolated array.
        '''
        #ix = idx.index_x
        #iy = idx.index_y

        debug = False
        if debug:
            print('DEBUG in select_profile:')
            print('DEBUG: key = ', key)

        missing_replacement = define_missing_replacement(key)

        values = self.input_data[key]
        profile_interpolated = np.zeros([self.numz_interp])
        profile_not_interpolated = np.zeros([self.numz_orig])
        if values is not None:
            we_have_an_array = False
            if isinstance(values, np.ndarray):
                we_have_an_array = True
            if we_have_an_array:
                if values.ndim == 3:
                    # typical case for 3D model data
                    profile_not_interpolated = values[idx.ci]
                elif values.ndim == 2:
                    profile_not_interpolated = values[idx.ci]
                elif values.ndim == 1:
                    print('DEBUG: values = ', values)

                    #errtxt = ('ERROR: using a 1D array is '+
                    #    'untested, please use this current data set to '+
                    #    'define a test case before allowing this to be used.')
                    #raise UntestedFeature(errtxt)

                    # possible case if a constant profile is provided
                    # independent of time,
                    # or if just 1 time is provided.
                    profile_not_interpolated = values[:]
                else:
                    errtxt = 'ERROR: unhandled number of dimensions'
                    raise AtmosphereFormattingError(errtxt)

                # apply the mask
                if mask is not None:
                    if debug:
                        print('DEBUG: applying mask')
                        print('DEBUG: self.numz_orig = ', self.numz_orig)
                    orig_profile_not_interpolated = profile_not_interpolated
                    profile_not_interpolated = np.zeros([self.numz_orig])
                    profile_not_interpolated[:] = missing_replacement
                    profile_not_interpolated[mask] = \
                    orig_profile_not_interpolated[mask]
                    if debug:
                        print('DEBUG: orig_profile_not_interpolated.shape = ',
                            orig_profile_not_interpolated.shape)
                        print('DEBUG: profile_not_interpolated.shape = ',
                            profile_not_interpolated.shape)

                # clip values to valid range if needed
                clip_to_valid_range(self.all_inp, profile_not_interpolated, key)

                # interpolate to the standard grid
                profile_interpolated[:] = \
                  self.v_ax.interpolate_to_lidar_range(z_inp,
                                   profile_not_interpolated, key)

                if select:
                    profile_interpolated[not select] = 0.
            else:
                # if we do not have an array, see if we can use a constant
                profile_interpolated[:] = values
                profile_not_interpolated[:] = values
        else:
            # values is None, so this input was not defined
            # signal this by returning None
            profile_interpolated = None
            profile_not_interpolated = None

        return (profile_interpolated, profile_not_interpolated)
        #  #]
    def calc_properties(self, z_inp, idx):
        #  #[ extract or calculate needed properties
        # idx: defines which profile to process
        ''' extract and interpolate needed properties from the
        input data, or calculate them if not available '''

        if not self.species_used:
            # nothing to do if species is not used
            self.extinction = None
            self.reff = None
            self.s = None
            self.lin_depol = None
            self.g = None
            self.inv_s = None
            return

        # select part of the profile depending on the selected species_id
        mask = None
        if self.selected_species_id is not None:
            id_values_not_interp = \
              self.select_profile(self.key_species_id, z_inp, idx)[1]
            mask = np.where(np.isclose(id_values_not_interp,
                                       self.selected_species_id))
            # debug
            #print('mask = ', mask)

        # TODO: split this in a select and an interpolate function!!!
        #ext_no_interp =
        #ext_interp =
        ext, ext_not_interp = \
          self.select_profile(self.key_ext, z_inp, idx, mask=mask)
        if ext is None:
            # calculate if not available
            ext, ext_not_interp = \
              calc_extinction(self.species_index,
                              self.species_name, self.species_type,
                              self.input_data, self.key_wc, self.key_reff,
                              self.v_ax, z_inp, idx)

        self.extinction[idx.ci] = ext

        # backscatter not yet calculated ?

        positive_ext = np.where(ext > 0.0)

        reff = \
          self.select_profile(self.key_reff, z_inp, idx, mask=mask)[0]
        if reff is None:
            # calculate if not available
            reff = \
              calc_reff(self.input_data, self.species,
                        self.v_ax, self.key_wc,
                        z_inp, idx, ext_not_interp)[0]
        self.reff[idx.ci] = reff

        s = \
          self.select_profile(self.key_s, z_inp,idx,
                              select=positive_ext, mask=mask)[0]
        self.s[idx.ci] = s
        #
        lin_depol = \
          self.select_profile(self.key_lin_depol, z_inp, idx,
                              select=positive_ext, mask=mask)[0]
        self.lin_depol[idx.ci] = lin_depol

        g = \
          self.select_profile(self.key_g, z_inp, idx, mask=mask)[0]
        self.g[idx.ci] = g

        inv_s = np.zeros(np.shape(s))
        inv_s[positive_ext] = 1./s[positive_ext]
        self.inv_s[idx.ci] = inv_s

        # some safety checks
        # don't check the full 3D arrayshere!
        # it will make the run time much longer...
        if np.max(ext) is np.nan:
            self.print_nan_warning('extinction')
        #if np.max(Backscatter) is np.nan:
        #    self.print_nan_warning('Backscatter')
        if np.max(reff) is np.nan:
            self.print_nan_warning('reff')
        if np.max(s) is np.nan:
            self.print_nan_warning('s')
        if np.max(lin_depol) is np.nan:
            self.print_nan_warning('lin_depol')
        if np.max(inv_s) is np.nan:
            self.print_nan_warning('inv_s')
        #  #]
    def print_nan_warning(self, variablename):
        #  #[ print a species specific warning
        ''' print warnings in case of NaN '''
        print(f'WARNING for species: {self.species}')
        print(f'WARNING: {variablename} contains NaN values !!!')
        #  #]
    def print_some_properties(self):
        #  #[ mainly for debugging
        ''' print species properties (for debugging) '''
        if self.species_used:
            print('   ', self.species_type, '==> ext      :',
                  np.mean(self.extinction), np.std(self.extinction))
            print('   ', self.species_type, '==> reff     :',
                  np.mean(self.reff), np.std(self.reff))
            print('   ', self.species_type, '==> s        :',
                  np.mean(self.s), np.std(self.s))
            print('   ', self.species_type, '==> inv_s    :',
                  np.mean(self.inv_s), np.std(self.inv_s))
            print('   ', self.species_type, '==> lin_depol:',
                  np.mean(self.lin_depol), np.std(self.lin_depol))
        #  #]

def add_generic_species(infc, all_inp, input_data, species_index,
                        v_ax, size_def):
    #  #[ define generic species
    ''' defines a series of species defined by the species_id field '''
    key_species_id = f'species_{species_index}_id'
    key_species_ids_to_include = f'species_{species_index}_ids_to_include'
    species_id = input_data[key_species_id]
    species_ids_to_include = input_data[key_species_ids_to_include]

    # print('DEBUG species_id: ', species_id)
    # print('DEBUG species_ids_to_include: ', species_ids_to_include)
    # species_to_include = [int(spe)
    #                       for spe in species_ids_to_include.split(',')]
    # print('DEBUG: species_to_include = ', species_to_include)

    # retrieve the comment attribute of the species_{index}_id field
    data_dir = all_inp.data_dir
    sim_var = all_inp.dict_of_sv[key_species_id]
    if sim_var.input_file is None:
        # no input defined for species_id, so skip the generic handling
        return []

    inp_file = translate_ref_to_real_fn(all_inp, sim_var.input_file)
    inp_file = os.path.join(data_dir, inp_file)
    # print('inp_file = ', inp_file)

    infc.update_to_new_fn(inp_file)
    #read_data(sim_var, dsp = infc.in_ds, ...

    descr = read_attribute(infc.in_ds, sim_var.file_variable, 'Description')
    # print('descr = ', descr)
    descr = descr.split(':')[1]
    descr_splitted = descr.split(',')
    # print('descr_splitted = ', descr_splitted)
    descr_dict = {txt.split('=')[0].strip():txt.split('=')[1].strip()
                  for txt in descr_splitted}
    # print('descr_dict = ', descr_dict)

    # TODO:
    # add the description for each id that is used for this species
    # to the name field, so that it can be used as variable name
    # in the generated netcdf output file.

    list_of_species = []
    if species_id is None:
        # nothing to do
        return list_of_species

    list_of_species_ids = species_ids_to_include.split(',')
    for species_id_txt in list_of_species_ids:
        species_id = int(species_id_txt)
        txt = descr_dict[species_id_txt]
        # print('handling species with index: ', species_id, 'and text: ', txt)

        this_sp = SpeciesHandler(input_data, all_inp, v_ax, size_def,
                                 species_index, species_by_id=species_id)
        list_of_species.append(this_sp)

    return list_of_species
    #  #]

def get_accumulated_properties(list_of_species, idx):
    #  #[ accumulate the species
    ''' calculated accumulated properties '''

    #print('DEBUG: list_of_species = ', list_of_species)
    #for spec in list_of_species:
    #    # DEBUG print:
    #    print('spec.species_type = ', spec.species_type)
    #    print('spec.species_name = ', spec.species_name)
    #    print('spec.species_used = ', spec.species_used)
    #    spec.print_some_properties()

    # get the shape of the arrays from the first available species
    shape = None
    for spec in list_of_species:
        if spec.species_used:
            # print('DEBUG: spec.extinction = ', spec.extinction)
            shape = np.shape(spec.extinction[idx.ci])
            break

    # Form the total particulate extinction array
    part_extinction = np.zeros(shape)
    for spec in list_of_species:
        if spec.species_used:
            part_extinction += spec.extinction[idx.ci]

    pos_ext = np.where(part_extinction > 0)

    # construct averaged effective radius
    part_reff = np.ones(shape)
    part_reff[pos_ext] = 0.0
    for spec in list_of_species:
        if spec.species_used:
            part_reff[pos_ext] += \
              spec.extinction[idx.ci][pos_ext]*spec.reff[idx.ci][pos_ext]
    part_reff[pos_ext] /= part_extinction[pos_ext]

    # construct averaged g
    part_g = np.zeros(shape)
    for spec in list_of_species:
        if spec.species_used:
            part_g += spec.extinction[idx.ci]*spec.g[idx.ci]
    part_g[pos_ext] /= part_extinction[pos_ext]

    # construct averaged water droplet fraction
    droplet_fraction_part = np.zeros(shape)
    for spec in list_of_species:
        if spec.species_used:
            if spec.species_type == 'water':
                droplet_fraction_part[pos_ext] += \
                  spec.extinction[idx.ci][pos_ext]
    droplet_fraction_part[pos_ext] /= part_extinction[pos_ext]

    # construct averaged ice fraction
    pristine_ice_fraction_part = np.zeros(shape)
    for spec in list_of_species:
        if spec.species_used:
            if spec.species_type == 'ice':
                pristine_ice_fraction_part[pos_ext] += \
                  spec.extinction[idx.ci][pos_ext]
    pristine_ice_fraction_part[pos_ext] /=  part_extinction[pos_ext]

    # construct averaged S (lidar_ratio)
    lidar_ratio = np.ones(shape)
    lidar_ratio[pos_ext] = 0.0
    for spec in list_of_species:
        if spec.species_used:
            lidar_ratio[pos_ext] += \
              spec.extinction[idx.ci][pos_ext]*spec.s[idx.ci][pos_ext]
    lidar_ratio[pos_ext] /= part_extinction[pos_ext]

    # construct averaged beta
    tot_beta = np.zeros(shape)
    for spec in list_of_species:
        if spec.species_used:
            tot_beta += spec.extinction[idx.ci]*spec.inv_s[idx.ci]

    # some safety checks
    if np.max(part_extinction) is np.nan:
        print('WARNING: part_extinction contains NaN values !!')
    if np.max(part_reff) is np.nan:
        print('WARNING: part_reff contains NaN values !!')
    if np.max(part_g) is np.nan:
        print('WARNING: part_g contains NaN values !!')
    if np.max(droplet_fraction_part) is np.nan:
        print('WARNING: droplet_fraction_part contains NaN values !!')
    if np.max(pristine_ice_fraction_part) is np.nan:
        print('WARNING: pristine_ice_fraction_part contains NaN values !!')
    if np.max(lidar_ratio) is np.nan:
        print('WARNING: lidar_ratio contains NaN values !!')
    if np.max(tot_beta) is np.nan:
        print('WARNING: tot_beta contains NaN values !!')

    return (part_extinction, part_reff, part_g,
            droplet_fraction_part, pristine_ice_fraction_part,
            lidar_ratio, tot_beta)
    #  #]

def get_accumulated_properties_after(tot_beta, ms_induced_depol,
                                     list_of_species):
    #  #[ accumulate the species
    ''' more calculated accumulated properties '''

    # get the shape of the arrays from the first species
    spec = list_of_species[0]
    # shape = np.shape(spec.extinction)

    pos_beta = np.where(tot_beta > 0.0)
    depol_tot = np.zeros(np.shape(tot_beta))
    for spec in list_of_species:
        if spec.species_used:
            if spec.species_type == 'water':
                cross = ((ms_induced_depol+spec.lin_depol)*
                         spec.extinction*spec.inv_s)
            else:
                cross = spec.lin_depol*spec.extinction*spec.inv_s
            depol_tot[pos_beta] += cross[pos_beta]
    depol_tot[pos_beta] /= tot_beta[pos_beta]

    # some safety checks
    if np.max(depol_tot) is np.nan:
        print('=========================================')
        print('WARNING: depol_tot contains NaN values !!')
        print('=========================================')

    return depol_tot
    #  #]

#!/usr/bin/env python3
#
#  #[ documentation
#
#   Created on 24-May-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Based on the function get_cloud_extinction_for_vns_channels()
#   in the json_input_file_generator.py software written by
#   Sebastian Bley and Nils Madenach, which is licensed with the MIT license
#   and which was provided Nils by by email to J. de kloe on 23-May-2023.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module implements calculation of ice and liquid water cloud
extinction profiles using fixed cloud droplet effective radii
following Baum and OPOT atlas.
'''
#  #]

# import numpy as np # not needed afteral

def calc_ext(wct, reff, rho): # z is not needed
    ''' generic extinction calculation '''
    #dz = np.abs(np.diff(z))
    #cot = (3 * wct * dz) / (reff * 1e-6 * rho * 2)
    #ext = cot / dz

    # simplify to
    ext = (3 * wct) / (reff * 1e-6 * rho * 2)

    return ext

def get_ice_ext(iwc):
    ''' calculate ice cloud extinction '''
    rho_ice = 916.7 # Kg/m3
    reff_ice = 60 # Effective radii in ym following Baum
    ext_ice = calc_ext(iwc, reff_ice, rho_ice)
    return ext_ice

def get_water_ext(lwc):
    ''' calculate water cloud extinction '''
    rho_water = 977 # kg/m3
    reff_water = 7.33 # Effective radii in ym following OPOT Atlas
    ext_water = calc_ext(lwc, reff_water, rho_water)
    return ext_water

def get_cloud_extinction(iwc, lwc): # z is not needed
    ''' calculate water and ice cloud extinction '''
    #### Calculate cloud extinction profiles
    ### LWP is in g/m2 computed by LWP = 2/3 * COT * reff * density_water
    ### height units: m
    ### IWC units: Kg/m3

    rho_water = 977 # kg/m3
    rho_ice = 916.7 # Kg/m3
    reff_ice = 60 # Effective radii in ym following Baum
    reff_water = 7.33 # Effective radii in ym following OPOT Atlas

    ### Calculate the COT for each layer
    #cot_ice = ( (3 * iwc_interp * np.abs(np.diff(z))) /
    #            (reff_ice * 1e-6 * rho_ice * 2)        )
    #cot_water = ( (3 * lwc_interp * np.abs(np.diff(z))) /
    #              (reff_water * 1e-6 * rho_water * 2)    )

    #ext_ice = cot_ice / np.abs(np.diff(z))
    #ext_water = cot_water / np.abs(np.diff(z))

    ext_ice = calc_ext(iwc, reff_ice, rho_ice)
    ext_water = calc_ext(lwc, reff_water, rho_water)

    return ext_ice, ext_water

#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 30-Aug-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
a module to implement the file cache to prevent opening
input files over and over again.
'''
#  #]
#  #[ imported modules
#import os
#  #]

class InputFileCache:
    '''
    a little class to cache the currently open input file
    for reuse, and only open a new one if needed.
    Could be extended to have multiple files open at the same
    time if that would speed things up.
    '''
    def __init__(self, in_file):
        #  #[ init
        self.in_file = in_file
        try:
            # pylint: disable=assignment-from-no-return
            self.in_ds = self.open_in_file() # opens self.in_file
            # pylint: enable=assignment-from-no-return
        except FileNotFoundError:
            print('Error File Not Found: '+self.in_file)
            raise
        #  #]
    def open_in_file(self):
        #  #[ place holder
        ''' to be redefined in each derived class '''
        errtxt = ('the open_in_file() method must be redefined '+
            'in a sub-class before this InputFileCache class '+
            'can be used!')
        raise NotImplementedError(errtxt)
        #  #]
    def close_in_file(self):
        #  #[ place holder
        ''' to be redefined in each derived class '''
        errtxt = ('the close_in_file() method must be redefined '+
            'in a sub-class before this InputFileCache class '+
            'can be used!')
        raise NotImplementedError(errtxt)
        #  #]
    def update_to_new_fn(self, new_in_file):
        #  #[ open a new file or use an existing open file
        ''' open a new input file, but only if needed '''
        if new_in_file == self.in_file:
            # nothing to do
            return

        # close the currently open file handle (self.in_ds)
        self.close_in_file()

        # switch to the new file
        self.in_file = new_in_file
        try:
            # pylint: disable=assignment-from-no-return
            self.in_ds = self.open_in_file() # opens self.in_file
            # pylint: enable=assignment-from-no-return
        except FileNotFoundError:
            print('Error File Not Found: '+new_in_file)
            raise
        #  #]
    def close(self):
        #  #[ close the cache
        ''' close the open file(s) and clean up '''
        self.close_in_file()
        self.in_file = None
        #  #]

#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 09-Oct-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

'''
A simple script to automatically download the data pack needed
for compiling and testing this software.
Also to enable easier development it first looks at some local
folders on the developers machine to copy more up to date versions
in case these have not yet been uploaded to gitlab.
Finally it provides on the developers machine some test data
that cannot be redistributed on gitlab since we have no permission
to do so (i.e. ACTIVATE data).
'''
#
#  #]
#  #[ imported modules
import os
import sys

# the next trick is needed to ensure we can import the local lib folder
d1=os.path.abspath('.')
d2=os.path.abspath('CLM_dist')
sys.path.insert(0, d2)
sys.path.insert(0, d1)

# note that importing .helpers does not work in this case
# since this script is run as standalone script and not imported
#
## pylint: disable=wrong-import-position
from lib.helpers import (
    DEV_MACHINE, check_in_sw_root, on_devel_machine, on_knmi_machine)
from lib.custom_exceptions import FilehandlingError
## pylint: enable=wrong-import-position

#  #]
#  #[ settings
REPOSITORY_URL = ('https://gitlab.com/KNMI-OSS/'+
                  'satellite-data-research-tools/cardinal-campaign-tools')

DATAPACKAGE_NAME = 'Cardinal_LIDAR_Module_Data.tar.gz'
DATAPACKAGE_NAME_TMP = 'Cardinal_LIDAR_Module_Data.tar.gz.tmp'
#DATAPACKAGE_ID = '109759934' # v0.17
#DATAPACKAGE_ID = '111488037' # v0.18
#DATAPACKAGE_ID = '113097718' # v0.19
#DATAPACKAGE_ID = '114707908' # v0.20
#DATAPACKAGE_ID = '115343450' # v0.21
#DATAPACKAGE_ID = '119370242' # v0.22
#DATAPACKAGE_ID = '132825730' # v0.23 for software v0001
DATAPACKAGE_ID = '152878579' # v0.24 for software v0002

DATAPACKAGE_URL = f'{REPOSITORY_URL}/-/package_files/{DATAPACKAGE_ID}/download'

ACTIVATE_DATAPACKAGE = 'CARDINAL_Campaign_Tools_ACTIVATE_DATA_extras.tar.gz'

GITLAB_DIR = '/nobackup/users/kloedej/versiebeheer/gitlab'
#  #]

def download_data_package():
    #  #[ download it
    ''' download the data package to a temporaty name '''

    if os.path.exists(DATAPACKAGE_NAME_TMP):
        print('data package was already downloaded')
    else:
        cmd = (f'wget {DATAPACKAGE_URL} -O {DATAPACKAGE_NAME_TMP} '+
              '--progress=dot:giga')
        print('executing cmd: ', cmd)
        os.system(cmd)
    #  #]

def copy_local_data_package_if_newer():
    #  #[ copy if needed
    ''' check if the local development datapackage differs
    from the downloaded package, and if so, use the local copy '''

    developers_copy = 'undefined'
    if on_devel_machine():
        developers_copy = os.path.join(GITLAB_DIR, DATAPACKAGE_NAME)
    elif on_knmi_machine():
        # ! dont use os.path.join here !
        # ! it does not work well if multiple items start with a slash!
        developers_copy = '/net/'+DEV_MACHINE+GITLAB_DIR+'/'+DATAPACKAGE_NAME

    if os.path.exists(developers_copy):
        print('developers copy found')
    else:
        return

    # compare downloaded and local copy
    cmd = f'diff {developers_copy} {DATAPACKAGE_NAME_TMP}'
    print('executing cmd: ', cmd)
    result = os.system(cmd)
    if result == 0:
        print('local developers copy is identical to downloaded copy')
        cmd = f'mv {DATAPACKAGE_NAME_TMP} {DATAPACKAGE_NAME}'
        print('executing cmd: ', cmd)
        result = os.system(cmd)
    else:
        print('local developers copy differs from downloaded copy')
        print('using local copy...')
        cmd = f'cp  {developers_copy} {DATAPACKAGE_NAME}'
        print('executing cmd: ', cmd)
        result = os.system(cmd)
    #  #]

def copy_activate_data():
    #  #[ copy activate data
    ''' copy the ACTIVATE extras datapackage that we cannot
    redistribute on gitlab'''

    # copy from the devel workstation
    developers_copy = 'undefined'
    if on_devel_machine():
        developers_copy = os.path.join(GITLAB_DIR, ACTIVATE_DATAPACKAGE)
    elif on_knmi_machine():
        # ! dont use os.path.join here !
        # ! it does not work well if multiple items start with a slash!
        developers_copy = ('/net/'+DEV_MACHINE+GITLAB_DIR+'/'+
                           ACTIVATE_DATAPACKAGE)

    if os.path.exists(ACTIVATE_DATAPACKAGE):
        print('ACTIVATE datapackage already present')
        return

    if os.path.exists(developers_copy):
        print('extra ACTIVATE datapackage found')
        cmd = f'cp {developers_copy} {ACTIVATE_DATAPACKAGE}'
        print('executing cmd: ', cmd)
        os.system(cmd)
    #  #]

def download_data_packages():
    #  #[ do the downloads
    ''' download one or both datapackages '''

    # 1 first check if we are in the software root
    check_in_sw_root()
    # 2 then download the data package from gitlab
    download_data_package()

    if on_knmi_machine():
        # 3 then compare it to the local copy on the developers machine
        #   and replace it if the local copy is newer.
        copy_local_data_package_if_newer()

        # 4 copy the local data package that cannot be redistributed.
        copy_activate_data()

    # rename tmp data package in case the above step did not yield a result
    if not os.path.exists(DATAPACKAGE_NAME):
        if os.path.exists(DATAPACKAGE_NAME_TMP):
            cmd = f'mv {DATAPACKAGE_NAME_TMP} {DATAPACKAGE_NAME}'
            print('executing cmd: ', cmd)
            result = os.system(cmd)
            if result != 0:
                errtxt = (f'ERROR: could not move {DATAPACKAGE_NAME_TMP} '+
                          f'to {DATAPACKAGE_NAME}.')
                raise FilehandlingError(errtxt)

    # 5 clean up the tmp file
    if os.path.exists(DATAPACKAGE_NAME_TMP):
        os.remove(DATAPACKAGE_NAME_TMP)

    #  #]

if __name__ == '__main__':
    download_data_packages()

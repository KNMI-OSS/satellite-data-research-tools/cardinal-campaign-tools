#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 31-Aug-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
a module to collect the needed HDF5 file reading code
'''
#  #]
#  #[ imported modules
import h5py
from .file_cache import InputFileCache
#  #]

class HDF5fileCache(InputFileCache):
    #  #[ HDF5 specific file cache
    '''
    a little derived class to cache the currently open HDF5 file
    for reuse, and only open a new one if needed.
    '''
    def open_in_file(self):
        return h5py.File(self.in_file, 'r')
    def close_in_file(self):
        self.in_ds.close()
    #  #]

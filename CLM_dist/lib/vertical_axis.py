#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 22-May-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#   based on the prototype by: Dave Donovan <dave.donovan@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module defines the CALIPSO color map to be used for the plotting script.
'''
#  #]
#  #[ imported modules
# import sys

# these next 3 are only used by the test plot option
# in the interpolate_pressure_to_lidar_range2 method
import os
import matplotlib
import matplotlib.pyplot

# these imports are used by the main code
import numpy as np
from .custom_exceptions import ProgrammingError, AtmosphereCalculationError
from .phys_constants import (
    MISSING_REPLACEMENT_EXTINCTION,
    MISSING_REPLACEMENT_BACKSCATTERRATIO,
    MISSING_REPLACEMENT_DEPOLARISATION)
#  #]

class VerticalAxis:
    ''' a class to collect the handling of the vertical axis '''
    def __init__(self, all_inp, nz_orig):
        #  #[ init
        ''' store the needed settings and calculate ranges '''
        #-----------------------------------------
        # copy the lidar simulation parameters.
        # we need to know the range grid now etc.
        #-----------------------------------------
        self.sat_alt = all_inp.lidar_params.sat_alt
        self.z1 = all_inp.lidar_params.z1
        self.z2 = all_inp.lidar_params.z2
        self.dz1 = all_inp.lidar_params.dz1
        self.z3 = all_inp.lidar_params.z3
        self.dz2 = all_inp.lidar_params.dz2

        debug = False
        if debug:
            print('-'*50)
            print('sat_alt = ', self.sat_alt, ' [km]')
            print('z1  = ', self.z1, 'm')
            print('z2  = ', self.z2, 'm')  # seems not used ?
            print('dz1 = ', self.dz1, 'm') # seems not used ?
            print('z3  = ', self.z3, 'm')
            print('dz2 = ', self.dz2, 'm')

        # To make things less awkward with range transitions, we will use
        # a uniform range grid and then re-bin later in the instrument model

        # This defines a vertical coordinate array starting at 40 km (z1)
        # and going down to -500 m (z3) in steps of 100 m (dz2).
        # It uses meters as unit.
        # This coordinate is constant relative
        # to the satellite and independent from the possibly varying
        # input height used by the input (met) data.
        # This one is currently exported to the netcdf output file.
        self.lid_alt = np.arange(self.z1, self.z3 - self. dz2, -self.dz2)
        # print('lid_alt[:10] = ', self.lid_alt[:10])
        # print('lid_alt[-10:] = ', self.lid_alt[-10:])

        # This defines the range from the satellite to the lidar range bin
        # starting at 360 km (sat_alt - z1) and going up
        # to 400.5 km (sat_alt - z3) in steps of 100 m (dz2)
        # This is the reference grid used by the multiscatter library.
        self.lid_range = self.sat_alt * 1.0e+3 - self.lid_alt
        # print('self.lid_range[:10] = ', self.lid_range[:10])
        # print('self.lid_range[-10:] = ', self.lid_range[-10:])

        # just store the input
        self.nz_orig = nz_orig # old name was nz_inp

        self.nz_interp = np.size(self.lid_alt)
        #print('nz_interp = ', self.nz_interp)
        #print('nz_orig   = ', self.nz_orig)

        # dz_half = self.dz2/2.0 # seems never used
        #  #]
    def define_z_array(self, height, idx):
        #  #[ calc z array
        ''' calculate z array that defines the atmospheric properties. '''
        # height holds the altitude of the input data
        # (either lidar data or meteo data) in km
        # GEM example starts from 41.7 km down to 0.02 km in irregular steps,
        # of close to 4 km on top and 100 m at the bottom.
        #
        # z_inp converts this to the satellite range
        # since the multiscatter library seems to need this as input.
        # GEM example starts at 358 km and goes down to 400 km
        if len(height.shape) == 1:
            z_inp = self.sat_alt*1.0e+3 - height[:]*1.0e+3
        else:
            z_inp = self.sat_alt*1.0e+3 - height[idx.ci]*1.0e+3

#        if len(height.shape) == 1:
#            print('height[:5] = ', height[:5])
#            print('height[-5:] = ', height[-5:])
#        else:
#            print('height[idx.ci][:5] = ', height[idx.ci][:5])
#            print('height[idx.ci][-5:] = ', height[idx.ci][-5:])

#        print('z_inp[:5] = ', z_inp[:5])
#        print('z_inp[-5:] = ', z_inp[-5:])

        return z_inp
        #  #]
    def interpolate_to_lidar_range(self, z_inp, profile, key):
        #  #[ interpolate to the lid_range array
        ''' interpolate and take in to account that the
        input z array may be in descending order in stead of ascending
        whihc need flipping before np.interp will work correctly '''

        if len(z_inp) != len(profile):
            errtxt = ('ERROR in interpolate_to_lidar_range: '+
                'inputs z_inp and profile must have the same length '+
                'but this seems not the case! '+
                f'len(z_inp) = {len(z_inp)} and '+
                f'len(profile) = {len(profile)}. '
                'Probably this is a programming error ...')
            raise ProgrammingError(errtxt)

        # prevent extrapolation with an edge value (which might
        # be just noise and give a nonsens profile)
        left = 0.0
        right = 0.0

        # a special handling is needed for pressure!
        if key == 'met_pressure':
            return self.interpolate_pressure_to_lidar_range(z_inp, profile)
            #                                                create_plot=True)

        if key == 'met_temperature':
            # temperature should be extended by using the edge value
            #
            # The temperature profile should NOT be extended with values
            # of 0 K out of the valid data range!
            # This would give nonse values or possibly NaN values
            # for the calculated Rayleigh extinction and backscatter.
            left = min(profile)
            right = max(profile)
        elif key.endswith('_extinction'):
            left = MISSING_REPLACEMENT_EXTINCTION
            right = MISSING_REPLACEMENT_EXTINCTION
        elif key.endswith('_s'):
            left = MISSING_REPLACEMENT_BACKSCATTERRATIO
            right = MISSING_REPLACEMENT_BACKSCATTERRATIO
        elif key.endswith('_lin_depol'):
            left = MISSING_REPLACEMENT_DEPOLARISATION
            right = MISSING_REPLACEMENT_DEPOLARISATION

        # ensure z_met_inp is monotonically increasing!
        # if it is not the np.interp() function only returns
        # edge values ...
        if z_inp[0] < z_inp[-1]:
            prof_interp = np.interp(self.lid_range, z_inp, profile,
                                    left=left, right=right)
        else:
            prof_interp = np.interp(self.lid_range, np.flip(z_inp),
                                    np.flip(profile),
                                    left=left, right=right)

        return prof_interp
        #  #]
    def interpolate_pressure_to_lidar_range(self, z_inp, log10_p_profile,
                                            create_plot=False):
        #  #[ wrapper to interpolate log10_p to the lid_range array
        '''
        a wrapper method to 
        interpolate the log of the pressure profile and take in to
        account that the input z array may be in descending order in
        stead of ascending which need flipping before np.interp will
        work correctly.
        The create_plot flag may be used during manual testing
        to create a plot of the interpolated result.
        '''
        if z_inp[0] < z_inp[-1]:
            #print('not flipping ...')
            prof_interp = self.interpolate_pressure_to_lidar_range2(
                z_inp, log10_p_profile, create_plot)
        else:
            #print('flipping ...')
            flipped_z_inp = np.flip(z_inp)
            flipped_log10_p_profile = np.flip(log10_p_profile)
            prof_interp = self.interpolate_pressure_to_lidar_range2(
                flipped_z_inp, flipped_log10_p_profile, create_plot)

        return prof_interp
        #  #]
    def interpolate_pressure_to_lidar_range2(self, z_inp, log10_p_profile,
                                             create_plot=False):
        #  #[ interpolate log10_p to the lid_range array
        ''' interpolate the log of the pressure profile
        to the self.lid_range array.
        The case of descending z_inp jas already been handled
        by the interpolate_pressure_to_lidar_range wrapper method.
        The create_plot flag may be used during manual testing
        to create a plot of the interpolated result.
        '''
        # The pressure profile should NOT be extended with values of 0 hPa.
        # This would create a vacuum without any Rayleigh
        # extinction and backscatter, it will confuse the
        # simulation and introduce NaN results.
        #
        # Also an extrapolation using the interp1d class
        # from scipy.interpolate import is not a good idea.
        # This will derive the extrapolation line from the 2 edge
        # datapoints only, which may lead to increasing pressure
        # with altitude in case the pressure profile is noisy
        # or inaccurate due to rounding issues!
        #
        # So do a proper 1 dimensional fit on the log of the pressure
        # and use that for extrapolations. At advice of Dave:
        #     use only the 10 edge points of the profile to prevent steps
        #     between the interpolated and extrapolated profile sections

        # eVe example
        # above: 360.0 km up to 382.0 km (top of atmosphere)
        # good:  382.1 km up to 399.9 km (lidar range)
        # below: 400.0 km up to 400.5 km (so subsurface)

        # hardcoded constant defining how many edge points should
        # be considered for doing the fit on which the extrapolation
        # wiill be based.
        num_points = 10

        # dummy value for extrapolating the edge upwards,
        # so to lower pressure values.
        # It will be replaced later by the fitted function.
        left = min(log10_p_profile) # towards the upper atmosphere

        # set pressure to the edge value in case z is out of range
        # so to higher pressure values.
        right = max(log10_p_profile) # towards the surface

        # interpolate in the region where we have data
        prof_interp = np.interp(self.lid_range, z_inp, log10_p_profile,
                                left=left, right=right)

        # fit the edge points to a linear function
        slope_above, offset_above = \
          np.polyfit(z_inp[:num_points], log10_p_profile[:num_points], deg=1)

        if slope_above <= 0.:
            errtxt = ('ERROR in interpolate_pressure_to_lidar_range2: '+
                'the fit of log10_p_profile against z_inp (i.e. range to '+
                'the satellite) yielded a negative slope: '+
                f'slope_above = {slope_above} '+
                'but this value should always be positive otherwise the '+
                'pressure will increase with altitude during extrapolation!!!')
            raise AtmosphereCalculationError(errtxt)

        # note: the eVe data has about 20 points close to (or below?)
        # the surface with identical pressure, so it makes no sence to
        # try and fit towards the surface and extrapolate this.
        # Therefore this downward extrapolation uses a constant
        # edge value for now.

        # finally handle the extrapolated regions
        min_z = z_inp[0] # upper edge of the data
        # max_z = z_inp[-1] # closest to the surface # currently not needed

        # define locations in the upper atmosphere
        # above the defined log10_p_profile
        # (remember a smaller range means closer to the satellite, so
        # higher in the atmosphere)
        points_above = np.where(self.lid_range < min_z)

        # use the linear fit to replace the extrapolated region
        # above the defined profile of log10_p_profile
        prof_interp[points_above] = \
          offset_above + slope_above*self.lid_range[points_above]

        if create_plot:

            # some prints used to create test data for the
            # unit tests
            # print('z_inp[::200] = ', z_inp[::200])
            # print('log10_p_profile[::200] = ', log10_p_profile[::200])

            fig = matplotlib.pyplot.figure()
            ax1 = fig.add_subplot(1, 2, 1) # rows, columns, count
            ax2 = fig.add_subplot(1, 2, 2) # rows, columns, count

            ax1.plot(log10_p_profile, 400. - z_inp*1.e-3,
                     color='blue', linewidth=3)
            ax1.plot(log10_p_profile[:num_points],
                     400. - z_inp[:num_points]*1.e-3,
                     color='red', linewidth=5)
            ax1.plot(prof_interp, 400. - self.lid_range*1.e-3,
                     linestyle='', marker='o', markersize=1, color='green')

            p_profile = (10**log10_p_profile)/100. # [Pa] to [hPa]
            p_interp = (10**prof_interp)/100. # [Pa] to [hPa]
            ax2.plot(p_profile, 400. - z_inp*1.e-3, color='blue', linewidth=3)
            ax2.plot(p_profile[:num_points], 400. - z_inp[:num_points]*1.e-3,
                     color='red', linewidth=5)
            ax2.plot(p_interp, 400. - self.lid_range*1.e-3, linestyle='',
                     marker='o', markersize=1, color='green')

            ax1.set_xlabel('10log(pressure [Pa])')
            ax2.set_xlabel('pressure [hPa]')
            ax1.set_ylabel('altitude [km]')
            #ax2.set_ylabel('altitude [km]')

            ax2_range = ax2.twinx()
            #ax2_range.plot(p_profile, z_inp*1.e-3, color='orange')
            alt_min, alt_max = ax2.get_ylim()
            range_min = 400. - alt_max
            range_max = 400. - alt_min
            ax2_range.set_ylim(range_max, range_min)
            ax2_range.spines['right'].set_color('orange')
            ax2_range.tick_params(axis='y', colors='orange')
            ax2_range.set_ylabel('range [km]', color='orange')

            plotfile = 'interp_test.png'
            fig.savefig(plotfile,dpi=300)
            show_cmd = 'eog '+plotfile
            os.system(show_cmd)

        return prof_interp
        #  #]

#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 17-May-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module implements reading multi dimensional arrays
from a given netcdf input file, and allows for swapping
of the order of input dimensions if desired.
'''
#  #]
#  #[ imported modules
import sys
import numpy as np
from .custom_exceptions import UntestedFeature, FormatErrorXLSX
from .phys_constants import (
    MISSING_REPLACEMENT_EXTINCTION,
    MISSING_REPLACEMENT_BACKSCATTERRATIO,
    MISSING_REPLACEMENT_DEPOLARISATION,
    MISSING_REPLACEMENT_SPECIES_ID,
    MISSING_REPLACEMENT_WIND_U,
    MISSING_REPLACEMENT_WIND_V)
#  #]

def apply_scale_and_shift(data, comment, scale, shift):
    #  #[ apply scale and shift
    ''' shift and scale if needed '''
    if scale is not None:
        data = data*scale
        comment += f' with scale {scale}'

    if shift is not None:
        data = data+shift
        comment += f' with shift {shift}'
    return (data, comment)
    #  #]

def read_attribute(dsp, para, attr_name):
    #  #[ read an attribute value
    ''' reads an attribute value, either for a NetCDF or for a HDF5 variable '''

    if hasattr(dsp[para], 'attrs'):
        # HDF5 syntax: (as for now untested)
        # it returns None if the attribute does not exist
        attr_value = dsp[para].attrs.get(attr_name, default=None)
    else:
        # NetCDF syntex:
        attr_value = getattr(dsp[para], attr_name, None)

    return attr_value
    #  #]

def define_missing_replacement(key):
    #  #[ select the right missing_replacement
    ''' selects the missing replacement based on the parameter name '''
    missing_replacement = None
    if key.endswith('extinction'):
        missing_replacement = MISSING_REPLACEMENT_EXTINCTION
    elif key.endswith('_s'):
        missing_replacement = MISSING_REPLACEMENT_BACKSCATTERRATIO
    elif key.endswith('_lin_depol'):
        missing_replacement = MISSING_REPLACEMENT_DEPOLARISATION
    elif key.endswith('_id'):
        missing_replacement = MISSING_REPLACEMENT_SPECIES_ID
    elif key == 'met_wind_u':
        missing_replacement = MISSING_REPLACEMENT_WIND_U
    elif key == 'met_wind_v':
        missing_replacement = MISSING_REPLACEMENT_WIND_V

    return missing_replacement
    #  #]

def read_data(sim_var, dsp, domain):
    #  #[ read routine
    '''
    simvar: holds the configuration choices for the parameter to be loaded.
    dsp (data set pointer): the input dataset (may be a NetCDF or HDF5 dataset).
    domain: defines the data selection to be applied.

    data(Height,x,y) or data(x,y), data(x), or data(y) cases
    are all handled here
    '''
    #  TBD
    #   -Impliment the dimension reordering

    #debug = True
    debug = False

    ix1,ix2,iy1,iy2 = domain

    para = sim_var.file_variable
    scale = sim_var.scale_factor
    shift = sim_var.shift
    fixed_value = sim_var.fixed_value
    var_name = sim_var.variable_name
    file_variable = sim_var.file_variable
    dim_order = sim_var.dimension_order
    key = sim_var.key

    dim_names = None
    if para is not None:
        try:
            dim_names = dsp[para].dimensions
        except AttributeError:
            # sometimes dimensions have no name ! This happpens
            # for example for some fields in the ACTIVATE test dataset
            dim_names = ()

    #if para is not None:
    #    if 'Pressure' in para:
    #        debug = True

    missing_replacement = None
    if para is not None:
        missing_replacement = define_missing_replacement(key)
        #print('DEBUG: missing_replacement = ', missing_replacement,
        #      'for key', key)

    ndims = 0
    shape = None
    if para is not None:
        shape = dsp[para].shape
        ndims = len(shape)

    if debug:
        print('')
        print('='*30)
        print('DEBUG output from read_data():')
        print('='*30)
        print('-'*20)
        print('var_name = ', var_name)
        print('para = ', para)
        print('scale = ', scale)
        print('shift = ', shift)
        print('fixed_value = ', fixed_value)
        print('file_variable = ', file_variable)
        print('dim_order ', dim_order)
        print('shape = ', shape)
        print('ndims = ', ndims)
        print('key = ', key)
        print('dim_names = ', dim_names)
        print('='*30)

    # init
    comment = 'undefined'
    data = None
    units = None

    # check this one first, since para will be None
    # if a fixed input is given.
    if fixed_value is not None:
        data = fixed_value
        comment = 'using fixed value'
        data, comment = apply_scale_and_shift(data, comment, scale, shift)
        return data, units, comment

    # if no para and no fixed value, no input is defined.
    if para is None:
        data = None
        comment = 'no input was defined'
        return data, units, comment

    # special case for longitude
    if 'longitude' in var_name.lower():

        if ndims == 2:
            # extract data from a given 2D-domain
            data = np.array(dsp[para][iy1:iy2,ix1:ix2])
            errtxt = ('ERROR: using a 2D domain for longitude is '+
                'untested, please use this current data set to '+
                'define a test case before allowing this to be used.')
            raise UntestedFeature(errtxt)

        # note 1: this is a workaround for the GEM case!
        # note 2: this is not useful at all for the other cases
        #         so for those the latitude values will be useless
        if ndims == 1:
            if iy1 != -1 and iy2 != -1:
                # extract data from a given 1D-data set
                # NOTE: this is a dummy procedure for now only useful
                # for the GEM test case!
                # this just copies the first iy2-iy1 elements from
                # the  1D lon input array
                # to each row of the 2D data array!
                data = np.zeros([iy2-iy1,ix2-ix1])
                for i in range(np.shape(data)[0]): # 0 up to 48
                    data[i,:]=np.array(dsp[para][iy1+i])
                comment = ('extraction from a 1D array mapped '+
                           'to each 2D row (GEM workaround)')
            else:
                # for the ACTIVATE case we have a proper 1D array
                data = np.array(dsp[para][ix1:ix2]) #
                comment ='from a 1D array (ACTIVATE case)'

        if ndims == 0:
            # it seems we have a scalar value
            data = np.array(dsp[para])
            comment = 'using fixed value'

    # special case for latitude
    elif 'latitude' in var_name.lower():
        if ndims == 2:
            # extract data from a given 2D-domain
            data = np.array(dsp[para][iy1:iy2,ix1:ix2])
            errtxt = ('ERROR: using a 2D domain for latitude is '+
                'untested, please use this current data set to '+
                'define a test case before allowing this to be used.')
            raise UntestedFeature(errtxt)

        # note 1: this is a workaround for the GEM case!
        # note 2: this is not useful at all for the other cases
        #         so for those the latitude values will be useless
        if ndims == 1:
            if iy1 != -1 and iy2 != -1:
                # extract data from a given 1D-data set
                # NOTE: this is a dummy procedure for now only useful
                # for the GEM test case!
                # this just copies the first iy2-iy1 elements from
                # the  1D lon input array
                # to each column of the 2D data array!
                data = np.zeros([iy2-iy1,ix2-ix1])
                for i in range(np.shape(data)[1]):
                    data[:,i] = np.array(dsp[para][ix1+i])
                comment = ('extraction from a 1D array mapped '+
                           'to each 2D column (GEM workaround)')
            else:
                # for the ACTIVATE case we have a proper 1D array
                data = np.array(dsp[para][ix1:ix2]) #
                comment ='from a 1D array (ACTIVATE case)'

        if ndims == 0:
            # it seems we have a scalar value (CLOUDNET case)
            data = np.array(dsp[para])
            comment = 'using fixed value (CLOUDNET case)'

    else:
# suggested addition by GJvZ (do we really need this?)
#        if ndims == 4:
#            # extract data from a given 4D-dataset
#            print(dim_order)
#            if dim_order != [0, 1, 2, 3]:
#                # undefined case
#                errtxt = ('Sorry, swapping dimensions is not yet '+
#                    'implemenmted for the case of a 4-D dataset. '+
#                    'Only [0,1,2,3] is allowed for now.')
#                raise UntestedFeature(errtxt)
#
#            # case for dim order [0,1,2,3]
#            # i.e. 1st dim is z, 2nd dim is y, 3rd dim is x, 4th dime is ???
#
# ?           if (dim_order == [1,0]):
#                print(iy1,iy2)
#                data = np.array(dsp[para][0,:,0,:]) #
# ?           elif (dim_order == [0,1]):
#                data = np.array(dsp[para][0,:,0,:]) # z,y,x data
#            else:
#                errtxt = ('Sorry, swapping dimensions is not yet '+
#                    'implemenmted for the case of a 4-D dataset. '+
#                    'Only [1,0]  and [0,1] is allowed for now.')
#                raise UntestedFeature(errtxt)
##            data = np.array(dsp[para][:,iy1:iy2,ix1:ix2])
#            comment = 'from a 4D array'
#            print(np.shape(data))

        if ndims == 3:
            # extract data from a given 3D-dataset
            if dim_order != [0, 1, 2]:
                # undefined case
                errtxt = ('Sorry, swapping dimensions is not yet '+
                    'implemented for the case of a 3-D dataset. '+
                    'Only [0,1,2] is allowed for now.')
                raise UntestedFeature(errtxt)

            # case for dim order [0,1,2]
            # i.e. 1st dim is z, 2nd dim is y, 3rd dim is x
            data = np.array(dsp[para][:,iy1:iy2,ix1:ix2]) # z,y,x data
            comment = 'from a 3D array'

        if ndims == 2:
            if shape[0] == 1:
                # this is a 2-day array for which only the 2nd dimension
                # is used, so in fact it is just a 1D array.
                # so remove the unused dimension
                # this typically occurs for hdf5 files (ACTIVATE case)
                # for altitude.
                data = np.array(dsp[para][0,:]) # z
                comment = 'from a 2D array, 1st dim dummy, 2nd dim along z'
            else:
                # extract data from a given 2D-dataset
                inp_data = np.array(dsp[para][ix1:ix2,:]) # t,z

                if dim_order == [0, 1]:
                    # case for dim_order [0,1]
                    # i.e. first dim is z, second dim is time
                    data = inp_data
                    comment = 'from a 2D array'
                elif dim_order == [1, 0]:
                    # case for dim_order [1,0]
                    # i.e. first dim is time, second dim is z
                    # swap the axes to ensure dim 1 refers to z
                    data = np.swapaxes(inp_data, 0, 1)
                    comment = 'from a 2D array (dimensions swapped)'
                else:
                    errtxt = ('incompatible dimension definition: '+
                              f'{dim_order}. For a 2 dimensional array '+
                              'only [0,1] and [1,0] are allowd.')
                    raise FormatErrorXLSX(errtxt)

        if ndims == 1:
            try:
                # extract data from a given 1D-dataset
                # (this usually is along the z-axis or the t-axis)
                along_z_axis = False
                if 'height' in var_name.lower():
                    along_z_axis = True
                if 'alt' in var_name.lower():
                    along_z_axis = True
                if 'met_' in var_name.lower():
                    if 'time' not in var_name.lower():
                        along_z_axis = True

                try:
                    if dim_names[0] == 'height':
                        along_z_axis = True
                except IndexError:
                    # allow for variables without dimension names
                    pass

                if along_z_axis:
                    # for altitude/height and meteo data
                    # we usually have vertical profiles
                    data = np.array(dsp[para][:]) # z
                    comment = 'from a 1D array along z'
                else:
                    # only apply x1:x2 selection for time axis!
                    if dsp[para].size == 1:
                        # special case if the parameter holds a scalar only
                        data = np.array([dsp[para][0], ])
                        comment = 'from a scalar value'
                    else:
                        data = np.array(dsp[para][ix1:ix2]) # t
                        comment = 'from a 1D array along t'
            except ValueError:
                data = None
                comment = 'extraction from a 1D array failed'

        if ndims == 0:
            print('unhandled case: ndims=0')
            sys.exit(1)

    units = read_attribute(dsp, para, 'units')
    if units is None:
        # as workaround try to read a "unit" attribute in stead
        units = read_attribute(dsp, para, 'unit')

    # check for datatype, since for some sources (i.e. eVe data)
    # the time input arrays are given as strings!
    if data.dtype in (np.float32, np.float64):
        # sanity check for nans
        select_nan = np.isnan(data)

        if missing_replacement is not None:
            data[select_nan] = missing_replacement
        else:
            num_nans = len(data[select_nan])
            if num_nans != 0:
                print('\n'+'='*50)
                print(f'{num_nans} NaN values detected in input data!!')
                print(f'var_name = {var_name}')
                print(f'file_variable = {file_variable}')
                print('='*50)
                sys.exit(1)

    fill_value = read_attribute(dsp, para, '_FillValue')

    if fill_value is not None:
        # note that both data and fill_value have been read from the same
        # input file, so there should not be a chance of having
        # numerical differences between them.
        # Therefore a comparison using == should be safe.
        select_filled_values = np.where(data == fill_value)
        data[select_filled_values] = 0.0

    (data, comment) = apply_scale_and_shift(data, comment, scale, shift)

    return data, units, comment
    #  #]

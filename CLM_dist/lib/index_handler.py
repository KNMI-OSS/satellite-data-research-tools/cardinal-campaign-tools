#!/usr/bin/env python3

#  #[ documentation
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
'''
This module defines a class for more flexible looping
over 1D or 2D indexes
'''
#  #]

from math import floor
from .custom_exceptions import IterationError

class IndexClass:
    '''
    This class holds the actual index values
    and does the 1D or 2D looping
    '''
    def __init__(self, num_x, num_y=None):
        #  #[ init
        self.num_x = num_x
        self.num_y = num_y
        self.index_x = None
        self.index_y = None

        if self.num_x is None:
            # this should never happen!
            errtxt = 'ERROR in IndexClass: num_x should never be None!'
            raise IterationError(errtxt)

        if self.num_y is not None:
            if self.num_y == 0:
                # this should never happen!
                errtxt = ('ERROR in IndexClass: if num_y is provided '+
                          'it should not be zero!')
                raise IterationError(errtxt)

        self.update_combined_index()

        if num_y is None:
            self.total_cases = num_x
        else:
            self.total_cases = num_x*num_y
        self.num_returned_cases = 0
        #  #]
    def reset(self):
        #  #[ reser
        ''' reset so the iteration can start all over again '''
        self.index_x = None
        self.index_y = None
        self.num_returned_cases = 0
        #  #]
    def step_to_next_index_pair(self):
        #  #[ step to next
        ''' a function to step to the next index pair'''
        if self.num_y is None:
            # we do 1D looping in this case
            if self.index_x is None:
                self.index_x = -1
            self.index_x += 1
            if self.index_x >= self.num_x:
                raise StopIteration
        else:
            # we do 2D looping in this case
            if self.index_x is None:
                self.index_x = 0
            if self.index_y is None:
                self.index_y = -1
            self.index_y += 1
            if self.index_y >= self.num_y:
                self.index_y = 0
                self.index_x += 1
                if self.index_x >= self.num_x:
                    raise StopIteration

        self.update_combined_index()
        self.num_returned_cases += 1
        #  #]
    def update_combined_index(self):
        #  #[ handle combined index
        ''' provide a tuple that can be used as generic index
        for both 1D and 2D numpy arrays '''
        # define a combined index that can be used as index
        # for both 1D and 2 D arrays, by combining the index
        # vcalues in a tuple.
        # see: Dealing with variable numbers of indices within programs
        # in: https://numpy.org/doc/stable/user/basics.indexing.html
        if self.index_x is None:
            # iteration is not yet started
            self.ci = None
        else:
            if self.num_y is None:
                # 1D case
                # the ellpsis covers the first z or t dimension
                self.ci = (Ellipsis, self.index_x, )
            else:
                # 2D case
                # the ellpsis covers the first z or t dimension
                self.ci = (slice(None), self.index_y, self.index_x)
        #  #]
    def get_progress_string(self):
        #  #[ return progress str
        ''' return a simple progress string'''
        progress_txt = (
            f'{self.num_returned_cases} '+
            f'out of {self.total_cases}')
        return progress_txt
        #  #]
    def get_progress_percentage(self):
        #  #[ return progress precentage
        ''' return a progress percentage '''
        perc = self.num_returned_cases / self.total_cases
        return floor(100*perc)
        #  #]
    def print_properties(self):
        #  #[ do some prints
        '''
        a small method to print internal instance veriables,
        useful during debugging and development.
        '''
        print('self.num_x = ', self.num_x)
        print('self.num_y = ', self.num_y)
        print('self.index_x = ', self.index_x)
        print('self.index_y = ', self.index_y)
        #  #]

class IndexIterClass:
    '''
    this class adds the iterator interface to the above IndexClass.
    '''
    def __init__(self, num_x, num_y=None):
        #  #[ init
        self.idx = IndexClass(num_x, num_y)
        self.num_x = num_x
        self.num_y = num_y
        self.previously_reported_perc = 0.
        #  #]
    def __iter__(self):
        return self
    def __next__(self):
        #  #[ next
        # this next call raises StopIteration
        # in case no more index pairs are available
        self.idx.step_to_next_index_pair()
        return self.idx
        #  #]
    def reset(self):
        #  #[ reset
        ''' reset so the iteration can start all over again '''
        self.idx.reset()
        self.previously_reported_perc = 0.
        #  #]
    def get_size_def(self, nz):
        #  #[ returns sizedef
        ''' returns a sizedef that can be used to
        allocate numpy arrays needed to store
        the looping results.
        '''
        if self.num_y is None:
            sizedef = [nz, self.num_x]
        else:
            sizedef = [nz, self.num_y, self.num_x]
        return sizedef
        #  #]
    def display_progress(self, in_steps_of=5):
        #  #[ progress in text
        ''' display a simple progress indicator '''
        perc = self.idx.get_progress_percentage()
        if perc >= self.previously_reported_perc+in_steps_of:
            print('progress:'+self.idx.get_progress_string()+
                f' ({perc}%)', flush=True)
            self.previously_reported_perc = perc
        #  #]
    def display_progress_hashes(self, width=40):
        #  #[ progress in # chars
        ''' display a simple progress indicator using hashes '''
        perc = self.idx.get_progress_percentage()
        left = width * perc // 100
        right = width - left
        tags = "#" * left
        spaces = " " * right
        percents = f"{perc}%"
        print("\r[", tags, spaces, "]", percents, sep="", end="", flush=True)
        #  #]

if __name__ == '__main__': # pragma: no cover
    #  #[ some test/demo code
    print('1D example:')
    NUM_X = 5
    index_iterator = IndexIterClass(NUM_X)
    for idx in index_iterator:
        print(f'x = {idx.index_x} y = {idx.index_y} ', end='')
        print(f'ci = {idx.ci} ', end='')
        index_iterator.display_progress()

    print('2D example:')
    NUM_X = 3
    NUM_Y = 4
    index_iterator = IndexIterClass(NUM_X, NUM_Y)
    for idx in index_iterator:
        print(f'x = {idx.index_x} y = {idx.index_y} ', end='')
        print(f'ci = {idx.ci} ', end='')
        index_iterator.display_progress()
    #  #]

#!/usr/bin/env python3

#  #[ documentation
#
#   Copyright (C) 2023-2024  KNMI
#
#   Author: Gerd-Jan van Zadelhoff <gerd-jan.van.zadelhoff@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#  LAST_CHANGES:
#
# Jun 11, 2024 : D.D. : atb_ray co replced by atb_ray
#                       atb_cr replaced by atb_mie_cr
'''
This module defines the lidar instrument functions
that define the simulated errors
and the effect these have on the error estimates
'''
#  #]
#  #[ imported modules
import os
import sys
import json
import numpy as np
import matplotlib.pyplot as plt

# from scipy.interpolate import griddata # not used

#  #]

def load_json(input_file):
    #  #[ load json file
    ''' generic function to load a json file
    in this module used to load noise properties
    stored in the'atlid_noise_charaterization.json file
    '''
    with open(input_file, 'r', encoding='ascii') as file:
        data = json.load(file)
    return data
    #  #]

# this is a dummy not yet implemented function
# pylint: disable=unused-argument
def add_error_atlid(atb_mie_co_int, atb_mie_cr_int, arb_ray_int):
    #  #[ this is a stub for now
    """
    define the errors using a realistc ATLID instrument description
    and return the error arrays

    Returns ...
    """

    dmie_out = None
    dcross_out = None
    dray_out = None
    mie = None
    cross = None
    ray = None

    print('Instrument Module does not yet exist, use the Random option')
    sys.exit(2)

    return dmie_out,dcross_out,dray_out,mie,cross,ray
    #  #]
# pylint: enable=unused-argument

# this function is never used for now
# pylint: disable=unused-argument
def add_error_lin_estimates(mie,cross,ray,xmie,xcross,xray,dmie,
                            dcross,dray,add_noise=True):
    #  #[ a first attempt, not yet used
    """
    define the errors and return the error arrays

    Returns ...
    """
    print(np.shape(dmie))
    print(np.shape(mie))

    dmie_out= np.full_like(mie, np.median(dmie))
    dray_out= np.full_like(ray, np.median(dray))
    dcross_out=np.full_like(cross, np.median(dcross))
    #
    dim2=np.size(mie[0,:])
    #plt.scatter(np.log10(mie),np.log10(dmie))
    #plt.ylim=(-9,-1)
    #plt.xlim=(-9,-1)
    xmie[np.isinf(xmie) | np.isnan(xmie)] = 1.e-10
    #mie[mie<1.e-10]=1.e-10
    xmie = np.where(xmie < 1.e-10, 1.e-10, xmie)

    dmie = np.where(dmie < 1.e-8, 1.e-8, dmie)
    print(np.shape(dmie))
    print(np.shape(xmie))
    bins = 50  # Number of bins for x and y axes
    density = np.histogram2d(xmie, dmie, bins=bins, density=True)[0]

    # Plot the 2D density plot
    extent = [1e-8,1e-1,1e-8,1e-1]
    plt.imshow(density.T, origin='lower', extent=extent, cmap='viridis')
    plt.colorbar(label='Density')
    plt.show()
    sys.exit(1)

    #
    # add a random component to tyour values, just because you can add .
    #
    print('add_error:',add_noise)
    if add_noise:
        for iprof in range(np.size(mie[:,0])):
            noise = np.random.normal(0.0, dmie,dim2)
            mie[iprof,:] += noise
            noise = np.random.normal(0.0, dray,dim2)
            ray[iprof,:] += noise
            noise = np.random.normal(0.0, dmie,dim2)
            cross[iprof,:] += noise

    return dmie_out,dcross_out,dray_out,mie,cross,ray
    #  #]
# pylint: enable=unused-argument

def add_error_estimates(mie,cross,ray,dmie,dcross,dray,
                        instr_data_folder, add_noise):
    #  #[ add random errors
    """

      define the errors and return the error arrays

    Returns
    -------
    None.

    """
    #  the noise is also related to the signal (poisson)
    #  for this the noise of the atlid L1 data was analyzed and
    #  a LUT created for each of the three channels
    #
    json_file = os.path.join(instr_data_folder,
                             'atlid_noise_charaterization.json')
    noise = load_json(json_file)

    dmie_out= np.full_like(mie, dmie)
    dray_out= np.full_like(ray, dray)
    dcross_out=np.full_like(cross, dcross)

    # Perform 2D interpolation for each individual value
    for i in range(mie.shape[0]):
        for j in range(mie.shape[1]):
            value = mie[i, j]
            noise_mie = noise["mie_error_0_20"]
            if j < 41:
                noise_mie = noise["mie_error_20_40"]
            interpolated_value=np.interp(value,noise["beta"], noise_mie)
            #xx_mesh, yy_mesh = np.meshgrid(noise["beta"], noise_mie)
            #interpolated_value = griddata((xx_mesh.flatten(),
            # yy_mesh.flatten()), value, (xx_mesh, yy_mesh), method='linear')
            dmie_out[i, j] = interpolated_value

            value = ray[i, j]
            noise_ray = noise["ray_error_0_20"]
            if j < 41:
                noise_ray = noise["ray_error_20_40"]
            interpolated_value=np.interp(value,noise["beta"], noise_ray)
            #xx_mesh, yy_mesh = np.meshgrid(noise["beta"], noise_ray)
            #interpolated_value = griddata((xx_mesh.flatten(),
            # yy_mesh.flatten()), value, (xx_mesh, yy_mesh), method='linear')
            dray_out[i, j] = interpolated_value


            value = cross[i, j]
            noise_cross = noise["cross_error_0_20"]
            if j < 41:
                noise_cross = noise["cross_error_20_40"]
            #xx_mesh, yy_mesh = np.meshgrid(noise["beta"], noise_cross)
            #interpolated_value = griddata((xx_mesh.flatten(),
            # yy_mesh.flatten()), value, (xx_mesh, yy_mesh), method='linear')
            interpolated_value=np.interp(value,noise["beta"], noise_cross)
            dcross_out[i, j] = interpolated_value


    #print('dmie:',dmie)
    #print(np.shape(mie))
    #
    #plt.show()
    #plt.scatter(mie[:,:],dmie_out[:,:])


    #mie_old=np.copy(mie)

    dim2=np.size(mie[0,:])
    #
    # add a random component to our values, just because you can add .
    #
    print('add_error:',add_noise)
    if add_noise:
        for iprof in range(np.size(mie[:,0])):
            noise = np.random.normal(0.0, dmie_out[iprof,:], dim2)

            mie[iprof,:] += noise
            noise = np.random.normal(0.0, dray_out[iprof,:], dim2)
            ray[iprof,:] += noise
            noise = np.random.normal(0.0, dmie_out[iprof,:], dim2)
            cross[iprof,:] += noise

    #plt.scatter(mie[:,:],dmie_out[:,:],color='red')
    #plt.scatter(mie_old[:,:],dmie_out[:,:])
    #plt.xscale('log')
    #plt.yscale('log')
    #plt.show()

    return dmie_out,dcross_out,dray_out,mie,cross,ray
    #  #]

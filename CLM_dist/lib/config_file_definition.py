#!/usr/bin/env python3

#  #[ documentation
#
#   Created on 23-Jun-2023
#
#   Copyright (C) 2023  KNMI
#
#   Author: Jos de Kloe <jos.de.kloe@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module implements the definitions needed to create and
read the xlsx config file.
'''
#  #]
#  #[ general settings
EXPECTED_CONFIG_FORMAT_VERSION = ('config format version', '0.6')
#  #]
#  #[ table position settings
# format definition table positions
TABLE_POS_HEADERS = [
    # cell_index,
    #       header name
    ('A3', 'Table Upper Left Corner'),
    ('B3', 'Cell Index'),
    ]

# pylint: disable=line-too-long
TABLE_POS_DEFINITIONS = [
    # cell index
    #      dict_key (as used in python)
    #                                    name (as used in xlsx config)
    #                                                                  TOML path
    ('A4',  'lidar_table',                 'Lidar Table',                'Table_Position_Definitions.lidar_table'),
    ('A5',  'domain_table',                'Domain Table',               'Table_Position_Definitions.domain_table'),
    ('A6',  'qc_table',                    'QC params Table',            'Table_Position_Definitions.qc_table'),
    ('A7',  'file_table',                  'File Table',                 'Table_Position_Definitions.file_table'),
    ('A8',  'meteo_simvar_table',          'Meteo SimVar Table',         'Table_Position_Definitions.meteo_simvar_table'),
    ('A9',  'species_count_table',         'Species Count Table',        'Table_Position_Definitions.species_count_table'),
    ('A10', 'generic_simvar_table',        'Generic SimVar Table',       'Table_Position_Definitions.generic_simvar_table'),
    ]
# pylint: enable=line-too-long
#  #]
#  #[ lidar parameters settings
# format definition for table of lidar params
LID_PARS_COL_HEADERS = [
    # header name
    'Lidar parameters',
    'Values',
    ]

LID_PARS_ROW_DEFINITIONS = [
    # dict_key (as used in python)
    #                    name+unit (as used in xlsx config)
    #                                            TOML path
    ('sat_alt',          'Sat_alt[km]',          'Lidar_params.Sat_alt'),
    ('z1',               'z1[m]',                'Lidar_params.z1'),
    ('z2',               'z2[m]',                'Lidar_params.z2'),
    ('dz1',              'dz1[m]',               'Lidar_params.dz1'),
    ('z3',               'z3[m]',                'Lidar_params.z3'),
    ('dz2',              'dz2[m]',               'Lidar_params.dz2'),
    ('fov_telescope',    'fov_telescope[mrads]', 'Lidar_params.fov_telescope'),
    ('div_laser',        'div_laser[mrads]',     'Lidar_params.div_laser'),
    ('laser_wavelength', 'laser_wavelength[nm]', 'Lidar_params.laser_wavelength'),
    ]
#  #]
#  #[ domain settings
# format definition for the domain
DOMAIN_POS_HEADERS = [
    # header name
    'Domain to convert',
    'Value',
    ]

DOMAIN_PARS_ROW_DEFINITIONS = [
    # dict_key (as used in python)
    #       name+unit (as used in xlsx config)
    #              TOML path
    ('ix1', 'ix1', 'Domain_to_convert.ix1'),
    ('ix2', 'ix2', 'Domain_to_convert.ix2'),
    ('iy1', 'iy1', 'Domain_to_convert.iy1'),
    ('iy2', 'iy2', 'Domain_to_convert.iy2'),
    ]
#  #]
#  #[ qc settings
# format definition for the QC settings
QC_PARS_HEADERS = [
    # header name
    'QC parameters',
    'Value',
    ]

QC_PARS_ROW_DEFINITIONS = [
    # dict_key (as used in python)
    #       name+unit (as used in xlsx config)
    #              TOML path
    ('ext_min_allowed_value', 'Extinction min. allowed value [m-1]',
     'QC_params.extinction.min_allowed_value'),
    ('ext_max_allowed_value', 'Extinction max. allowed value [m-1]',
     'QC_params.extinction.max_allowed_value'),
    ('lidar_ratio_min_allowed_value', 'Lidar_Ratio (S) min. allowed value [sr]',
     'QC_params.lidar_ratio.min_allowed_value'),
    ('lidar_ratio_max_allowed_value', 'Lidar Ratio (S) max. allowed value [sr]',
     'QC_params.lidar_ratio.max_allowed_value'),
    ('lin_depol_min_allowed_value', 'Lin_depol min. allowed value',
     'QC_params.lin_depol.min_allowed_value'),
    ('lin_depol_max_allowed_value', 'Lin_depol max. allowed value',
     'QC_params.lin_depol.max_allowed_value'),
    ]
#  #]
#  #[ file list settings
# a table to store the data directory and file names
FILE_LIST_COL_HEADERS = [
    'Variable',
    'Value',
    ]

FILE_LIST_ROW_DEFINITIONS = [
    # dict_key (as used in python)
    #                 name+unit (as used in xlsx config)
    #                                          TOML path
    ('data_dir',      'Data Dir', 'File_List.data_dir'),
    ('num_inp_files', 'Number of input files', 'File_List.number_of_input_files'),
    # the next entry is generated dynamically by create_config_file.py
    # and can be repeated a number of times if needed
    #('input_file_01', '{REFNAME}', 'input_file.1.filename'),
    ]
#  #]
#  #[ simulation variable settings

SPECIES_COUNT_HEADERS = [
    'Name',
    'Value'
    ]

SPECIES_COUNT_DEFINITIONS = [
    ('num_species',     'Number of Species', 'species_count.num_species'),
    ]

# format definition for table of simulation variables
MAIN_COL_HEADERS = [
    # cell_index,
    #       name
    'Simulation Variable',
    'Input File',
    'File variable',
    'Dimension order',
    'Scale Factor',
    'Shift', # often this is also called offset
    'Fixed value',
    ]

# note: variables marked with #???#
# are required in the original code but not present in sample config file...

# pylint: disable=line-too-long
SIMVAR_METEO_DEFINITIONS = [
    # dict_key (as used in python)
    #                   name+unit (as used in config)
    #                                         TOML path
    #
    ('met_height',      'Met_height [km]',      'met_info.height'),
    ('met_latitude',    'Met_latitude [degN]',  'met_info.latitude'),
    ('met_longitude',   'Met_longitude [degE]', 'met_info.longitude'),
    ('met_time',        'Met_time',             'met_info.time'),
    ('met_temperature', 'Met_temperature [K]',  'met_info.temperature'),
    ('met_pressure',    'Met_pressure [hPa]',   'met_info.pressure'),
    ('met_wind_u',      'Met_wind_u [m/s]',     'met_info.wind_u'),
    ('met_wind_v',      'Met_wind_v [m/s]',     'met_info.wind_v'),
    ]


# Define the type of species and their name
# Species type is a text string. It must be one of generic/water/ice/aerosol.
# name is the name that will be used in the output netcdf file.
SPECIES_TYPE_HEADERS = [
    'Name',
    'Value'
    ]

SPECIES_TYPE_DEFINITIONS = [
    ('species_<count>_type', 'Species Type', 'species_list.species.<count>.type'),
    ('species_<count>_name', 'Species Name', 'species_list.species.<count>.name'),
    ]

SIMVAR_GENERIC_DEFINITIONS = [
    ('species_<count>_height',     'Height [km]',        'species_list.species.<count>.geolocation.height'),
    ('species_<count>_latitude',   'Latitude [DegN]',    'species_list.species.<count>.geolocation.latitude'),
    ('species_<count>_longitude',  'Longitude [DegE]',   'species_list.species.<count>.geolocation.longitude'),
    ('species_<count>_time',       'Time [Sec]',         'species_list.species.<count>.geolocation.time'),
    #
    ('species_<count>_id',         'Species_ID',         'species_list.species.<count>.selection.species_id'),
    ('species_<count>_ids_to_include', 'Species_IDs_to_Include', 'species_list.species.<count>.selection.species_ids_to_include'), # ids to be included in the calculation
    #
    ('species_<count>_wc',         'WC [gm-3]',          'species_list.species.<count>.properties.wc'),   # only used if ext undefined
    ('species_<count>_reff',       'Reff [um]',          'species_list.species.<count>.properties.reff'), # only used if ext undefined
    ('species_<count>_extinction', 'Extinction [m-1]',   'species_list.species.<count>.properties.extinction'), # may be left undefined
    ('species_<count>_s',          'S [sr]',             'species_list.species.<count>.properties.s'), # lidarratio #
    ('species_<count>_lin_depol',  'Lin_depol',          'species_list.species.<count>.properties.lin_depol'),
    ('species_<count>_g',          'g',                  'species_list.species.<count>.properties.g'),
    ]

# pylint: enable=line-too-long

#  #]

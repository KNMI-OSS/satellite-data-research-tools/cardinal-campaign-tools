#!/usr/bin/env python3

#  #[ documentation
#   Copyright (C) 2022-2023  KNMI
#
#   Author: Dave Donovan <dave.donovan@knmi.nl>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
'''
This module defines the CALIPSO color map to be used for the plotting script.
'''
#  #]
#  #[ imported modules
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

from .helpers import get_cmap_wrapper

#try:
from .custom_exceptions import ColorMapException
#except ImportError:
#    # this allows running this file as standalone script
#    # useful for testing during development.
#    from custom_exceptions import ColorMapException

#  #]
#  #[ constants
CM_NAME = 'CalipsoATBcm'

CALIPSO_CM_RED = (
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255, 70, 70,
    70, 90, 90, 90,110,110,110,130,130,130,150,150,150,150,150,150,170,170,170,170,
    170,170,180,180,180,180,180,180,190,190,190,190,190,190,200,200,200,200,200,200,
    210,210,210,210,210,210,215,215,215,215,215,215,220,220,220,220,220,220,225,225,
    225,225,225,225,230,230,230,230,230,230,235,235,235,235,235,235,240,240,240,240,
    240,240,240,245,245,245,245,245,245,245,255,255,255,255,255,255,255)

CALIPSO_CM_GREEN = (
    42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42,
    42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42,127,127,127,127,127,127,
    127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,
    127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,255,255,255,127,
    127,127,170,170,170,170,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,230,230,230,230,230,230,230,230,230,212,212,212,212,212,212,212,212,212,170,
    170,170,170,170,170,170,170,170,127,127,127,127,127,127, 85, 85, 85, 85, 85, 85,
    0,  0,  0,  0,  0,  0, 42, 42, 42, 42, 42, 42, 85, 85, 85,127,127,127, 70, 70,
    70, 90, 90, 90,110,110,110,130,130,130,150,150,150,150,150,150,170,170,170,170,
    170,170,180,180,180,180,180,180,190,190,190,190,190,190,200,200,200,200,200,200,
    210,210,210,210,210,210,215,215,215,215,215,215,220,220,220,220,220,220,225,225,
    225,225,225,225,230,230,230,230,230,230,235,235,235,235,235,235,240,240,240,240,
    240,240,240,245,245,245,245,245,245,245,255,255,255,255,255,255,255)

CALIPSO_CM_BLUE = (
    170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,
    170,170,170,170,170,170,170,170,170,170,170,170,170,170,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,170,170,170,127,
    127,127, 85, 85, 85, 85,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0, 85, 85, 85, 85, 85, 85,127,127,127,170,170,170, 70, 70,
    70, 90, 90, 90,110,110,110,130,130,130,150,150,150,150,150,150,170,170,170,170,
    170,170,180,180,180,180,180,180,190,190,190,190,190,190,200,200,200,200,200,200,
    210,210,210,210,210,210,215,215,215,215,215,215,220,220,220,220,220,220,225,225,
    225,225,225,225,230,230,230,230,230,230,235,235,235,235,235,235,240,240,240,240,
    240,240,240,245,245,245,245,245,245,245,255,255,255,255,255,255)
#  #]

def define_and_register_calipso_cm() :
    #  #[ define
    ''' define and register '''
    red=tuple(z/255. for z in CALIPSO_CM_RED)
    green=tuple(z/255. for z in CALIPSO_CM_GREEN)
    blue=tuple(z/255. for z in CALIPSO_CM_BLUE)
    colors=np.zeros(shape=(len(red),4))

    for i in range (0,len(red)-1) :
        colors[i,0]=red[i]
        colors[i,1]=green[i]
        colors[i,2]=blue[i]
        colors[i,3]=1.0

    colors=tuple(colors)
    cmap = mpl.colors.ListedColormap(colors, CM_NAME)

    success = False
    if hasattr(mpl, 'colormaps'):
        # pylint: disable=no-member
        mpl.colormaps.register(cmap)
        # pylint: enable=no-member
        success = True

    if not success:
        if hasattr(plt, 'register_cmap'):
            plt.register_cmap(cmap=cmap)
            success = True

    if not success:
        errtxt = ('Error in define_and_register_calipso_cm: '+
            'Unknown colormaps interface for matplotlib. '+
            'It seems you are using a matplotlib version that is '+
            'not compatible with currently used interface. '+
            'Please report this to the CCT maintainers.')
        raise ColorMapException(errtxt)

    return cmap
    #  #]

def test_calipso_cm_is_registred():
    #  #[ test
    ''' a little helper function to see if the colormap
    is already registered. '''

    if hasattr(mpl, 'colormaps'):
        # the key based interface was only added
        # with matplotlib 3.5
        try:
            # pylint: disable=no-member
            cmap = mpl.colormaps[CM_NAME]
            # pylint: enable=no-member

            # delete the local variable again
            # (only to prevent pylint to complain about an unused variable)
            del cmap

            # the colormap was already registered, so nothing to do
            return True
        except KeyError:
            return False
    elif hasattr(mpl, 'cm'):
        # for older matplotlib versions
        # (debian on gitlab ci/cd currently has 3.4)
        # use the get_cmap() function
        # note that this function is deprecated since matplotlib 3.7
        # matplotlib.colormaps.get_cmap(obj) can be used as alternative.
        try:
            cmap = mpl.cm.get_cmap(CM_NAME)

            # delete the local variable again
            # (only to prevent pylint to complain about an unused variable)
            del cmap

            # the colormap was already registered, so nothing to do
            return True
        except ValueError:
            return False
    else:
        errtxt = ('Unknown colormaps interface for matplotlib. '+
            'It seems you are using a matplotlib version that is '+
            'not compatible with the test currently implemented in '+
            'test_calipso_cm_is_registred. '+
            'Please report this to the CCT maintainers.')
        raise ColorMapException(errtxt)
    #  #]

def unregister_calipso_cm():
    #  #[ unregister the colormap
    ''' a little helper function to unregister the colormap '''

    # mpl.colormaps.unregister works for matplotlib v3.5.3
    # but seems not yet available for matplotlib v3.5.1
    # even though mpl.colormaps was already added for the version,
    # so take this in to account.
    success = False
    if hasattr(mpl, 'colormaps'):
        if hasattr(mpl.colormaps, 'unregister'):
            # pylint: disable=no-member
            mpl.colormaps.unregister(CM_NAME)
            # pylint: enable=no-member
            success = True

    if not success:
        if hasattr(mpl, 'cm'):
            if hasattr(mpl.cm, 'unregister_cmap'):
                # pylint: disable=no-member
                mpl.cm.unregister_cmap(CM_NAME)
                # pylint: enable=no-member
                success = True
            elif hasattr(mpl.cm, '_cmap_registry'):
                # accessing this internal member of mpl.cm is the
                # only way to get this done for older matplotlib versions
                # pylint: disable=protected-access
                mpl.cm._cmap_registry.pop(CM_NAME)
                # pylint: enable=protected-access
                success = True

    #if hasattr(mpl, 'colormaps'):
    #    mpl.colormaps.unregister(CM_NAME)
    #
    # versions above 3.7 probably will need something like this:
    #matplotlib.colormaps.unregister_cmap(CM_NAME)
    #matplotlib.colormaps.unregister(CM_NAME)

    if not success:
        errtxt = ('Unknown colormaps interface for matplotlib. '+
            'It seems you are using a matplotlib version that is '+
            'not compatible with the test currently implemented in '+
            'unregister_calipso_cm. '+
            'Please report this to the CCT maintainers.')
        raise ColorMapException(errtxt)
    #  #]

def get_calipso_cm():
    #  #[ get the colormap
    ''' a function to register (if needed) and get the colormap'''
    cmap = None

    if not test_calipso_cm_is_registred():
        define_and_register_calipso_cm()

    cmap = get_cmap_wrapper(CM_NAME)

    return cmap
    #  #]

if __name__ == "__main__": # pragma: no cover
    #  #[ some testing code
    #
    x = np.arange(0, np.pi, 0.1)
    y = np.arange(0, 2 * np.pi, 0.1)
    X, Y = np.meshgrid(x, y)
    Z = np.cos(X) * np.sin(Y) * 100
    #
    cm = get_calipso_cm()

    test_calipso_cm_is_registred()
    #
    # These both work !
    #

    #CS = plt.contourf( X, Y , Z, 20,cmap=cm)
    CS = plt.contourf( X, Y , Z, 20,cmap=CM_NAME)

    plt.show()
    #  #]

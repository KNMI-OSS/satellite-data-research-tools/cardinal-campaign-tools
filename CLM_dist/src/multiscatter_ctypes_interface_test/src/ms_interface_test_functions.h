/*
  A header file to the functions to assist in testing the python
  interface to the multiscatter library.

  Written by: Jos de Kloe, 2023

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* For details of how to call the multiple scattering algorithms, see
   the comments in multiscatter.h, called from ms.h: */
#include "ms.h"

int open_log_file(char *filename);

int close_log_file();

int test_log_file(char *filename);

int test_log_stdout();

int test_2_ints(int n, int m);

ms_real test_2_reals(ms_real x, ms_real y);

ms_real test_10_ms_real_pointers(
    int n,
    const ms_real *range,
    const ms_real *radius,
    const ms_real *ext,
    const ms_real *ssa,
    const ms_real *g,
    const ms_real *ext_bscat_ratio,
    const ms_real *ext_air,
    const ms_real *ssa_air,
    const ms_real *droplet_fraction,
    const ms_real *pristine_ice_fraction);

ms_real test_ms_config(int n, ms_config *config);

ms_real test_ms_instrument(int nfov, ms_instrument instrument);

ms_real test_ms_surface(ms_surface surface);

ms_real test_output_real_arrays(ms_real *bscat_out,
                                ms_real *bscat_air_out, int m);

/* this is the full interface to the multiscatter function */
int
test_multiscatter(
    /* Input data */
    int n,                    /* Number of input gates */
    int m,                    /* Number of output gates (>= n) */
    ms_config *config,        /* Configuration information */
    ms_instrument instrument, /* Structure containing instrument variables */
    ms_surface surface,       /* Surface scattering variables */
    const ms_real *range,     /* Height of each range gate, metres */
    const ms_real *radius,    /* Cloud/aerosol equivalent radius, microns */
    const ms_real *ext,       /* Total extinction coefficient, m-1 */
    const ms_real *ssa,       /* Total single-scatter albedo */
    const ms_real *g,         /* Total asymmetry factor */
    const ms_real *ext_bscat_ratio,/* Cloud/aerosol ext./bscat. ratio, sr */
    const ms_real *ext_air,   /* Air ext. coefft., m-1 (NULL for vacuum) */
    const ms_real *ssa_air,   /* Air single-scatter albedo */
    const ms_real *droplet_fraction,/* Fraction of extinction from droplets */
    const ms_real *pristine_ice_fraction,/* Fraction of ext from pristine ice */
    /* Output data */
    ms_real *bscat_out,       /* Measured backscatter, m-1 sr-1 */
    ms_real *bscat_air_out);   /* Measured backscatter of air, m-1 sr-1 */

/*
  The main program to assist in testing the python
  interface to the multiscatter library.

  Written by: Jos de Kloe, 2023

*/

#include <stdio.h>
#include <stdlib.h>

/* For details of how to call the multiple scattering algorithms, see
   the comments in multiscatter.h, called from ms.h: */
#include "ms.h"

#include "ms_interface_test_functions.h"

int main()
{
  int result;
  ms_config config;
  ms_instrument instrument;
  ms_surface surface;
  const int n=10;
  const int m=8;
  const int nfov=3;
  int i;
  ms_real x,y, result_real;
  
  open_log_file("STDOUT");

  printf("--------------------------------------\n");
  result = test_2_ints(4,7);
  printf("test_2_ints: result = %i\n", result);

  printf("--------------------------------------\n");
  result_real = test_2_reals(4.56,7.89);
  printf("test_2_reals: result_real = %f\n", result_real);

  printf("--------------------------------------\n");
  int nn = 3;
  ms_real test_values_01[3] = {0.1, 1.1, 2.1};
  ms_real test_values_02[3] = {0.2, 1.2, 2.2};
  ms_real test_values_03[3] = {0.3, 1.3, 2.3};
  ms_real test_values_04[3] = {0.4, 1.4, 2.4};
  ms_real test_values_05[3] = {0.5, 1.5, 2.5};
  ms_real test_values_06[3] = {0.6, 1.6, 2.6};
  ms_real test_values_07[3] = {0.7, 1.7, 2.7};
  ms_real test_values_08[3] = {0.8, 1.8, 2.8};
  ms_real test_values_09[3] = {0.9, 1.9, 2.9};
  ms_real test_values_10[3] = {0.0, 1.0, 2.0};
  const ms_real *range = &test_values_01[0];
  const ms_real *radius = &test_values_02[0];
  const ms_real *ext = &test_values_03[0];
  const ms_real *ssa = &test_values_04[0];
  const ms_real *g = &test_values_05[0];
  const ms_real *ext_bscat_ratio = &test_values_06[0];
  const ms_real *ext_air = &test_values_07[0];
  const ms_real *ssa_air = &test_values_08[0];
  const ms_real *droplet_fraction = &test_values_09[0];
  const ms_real *pristine_ice_fraction = &test_values_10[0];

  /* test 10 pointers to real arrays */
  result_real = test_10_ms_real_pointers(nn,
			range, radius, ext, ssa, g,
			ext_bscat_ratio, ext_air, ssa_air,
			droplet_fraction, pristine_ice_fraction);
  printf("test_10_ms_real_pointers: result_real = %f\n", result_real);

  printf("--------------------------------------\n");
  config.small_angle_algorithm = MS_SINGLE_SCATTERING;
  config.wide_angle_algorithm  = MS_WIDE_ANGLE_TDTS_NO_FORWARD_LOBE;
  config.options               = MS_QUIET | MS_CHECK_FOR_NAN;
  config.max_scattering_order  = 2;
  config.max_theta             = 0.3;
  config.first_wide_angle_gate = 3;
  config.coherent_backscatter_enhancement = 2.5;
  config.small_angle_lag       = malloc(sizeof(ms_real)*n);
  for (i = 0; i < n; i++) {config.small_angle_lag[i] = i+37;}
  config.total_src             = 7.777;
  config.total_reflected       = 8.888;
  config.ss_multiplier         = 9.999;
  
  result_real = test_ms_config(n, &config);
  printf("test_ms_config: result_real = %f\n", result_real);

  printf("--------------------------------------\n");
  instrument.receiver_type = MS_GAUSSIAN;
  instrument.altitude = 456.78;
  instrument.wavelength = 354.8e-9;
  instrument.rho_transmitter = 1.234;
  instrument.rho_receiver = malloc(sizeof(ms_real)*nfov);
  for (i = 0; i < nfov; i++) {instrument.rho_receiver[i] = 0.3+i*0.0345;}
  instrument.nfov = nfov;
  
  result_real = test_ms_instrument(nfov, instrument);
  printf("test_ms_instrument: result_real = %f\n", result_real);

  printf("--------------------------------------\n");
  surface.sigma0 = 1.e-1;
  surface.diffuse_albedo = 1.e-2;
  surface.direct_to_diffuse_albedo = 1.e-3;
  surface.diffuse_to_direct_backscatter = 1.e-4;
  surface.range = 12.;
  
  result_real = test_ms_surface(surface);
  printf("test_ms_surface: result_real = %f\n", result_real);

  printf("--------------------------------------\n");
  
  ms_real bscat_out[m];
  ms_real bscat_air_out[m];
  result_real = test_output_real_arrays(bscat_out, bscat_air_out, m);
  printf("test_output_real_arrays: result_real = %f\n", result_real);
  
  printf("--------------------------------------\n");

  int status;
  status = test_multiscatter(/* Input data */
			     n, m,
			     &config,
			     instrument,
			     surface,
			     range,
			     radius,
			     ext,
			     ssa,
			     g,
			     ext_bscat_ratio,
			     ext_air,
			     ssa_air,
			     droplet_fraction,
			     pristine_ice_fraction,
			     /* Output data */
			     bscat_out,
			     bscat_air_out);

  printf("test_multiscatter: status = %i\n", status);
  for (i = 0; i < m; i++) {
    printf("bscat_out[%i] = %f bscat_air_out[%i] = %f\n",
	   i, bscat_out[i], i, bscat_air_out[i]);
  }
    
  printf("--------------------------------------\n");

  close_log_file();

  test_log_file("log_file.txt");
  test_log_stdout();
  printf("--------------------------------------\n");
}


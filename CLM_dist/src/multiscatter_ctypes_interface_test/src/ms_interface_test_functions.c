/*
  several functions to assist in testing the python
  interface to the multiscatter library.

  Written by: Jos de Kloe, 2023

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

/* For details of how to call the multiple scattering algorithms, see
   the comments in multiscatter.h, called from ms.h: */
#include "ms.h"

/* keep this file pointer global */
FILE *fptr;

/*
  idea: pass a fptr to each test routine to export the test prints
  to a file, and allow using stdout for the standalone test program.
  NOTE: the filename needs to be defined as a bytes string in the calling \
  python3 code! Otherwise only the first character is passed.
*/
int open_log_file(char *filename)
/* #[ */
{
  /* printf("inside open_log_file: filename = [%s]\n", filename); */
  if (strcmp(filename, "STDOUT") == 0)
    {
      fptr = stdout;
    }
  else
    {
      fptr = fopen(filename, "w");
    }
  return 0;
}
/* #] */

int close_log_file()
/* #[ */
{
  if (fptr != stdout)
    {
      fclose(fptr);
    }
  return 0;
}
/* #] */

int test_log_file(char *filename)
/* #[ */
{
  open_log_file(filename);
  fprintf(fptr, "This is just some dummy testing text\n");
  fprintf(fptr, "And this is a second line of text\n");
  close_log_file();
}
/* #] */

int test_log_stdout()
/* #[ */
{
  open_log_file("STDOUT");
  fprintf(fptr, "STDOUT: This is just some dummy testing text\n");
  fprintf(fptr, "STDOUT: And this is a second line of text\n");
  close_log_file();
}
/* #] */

int test_2_ints(int n, int m)
/* #[ */
{
  fprintf(fptr, "test_2_ints: n = %i\n", n);
  fprintf(fptr, "test_2_ints: m = %i\n", m);
  return n+m;
}
/* #] */

ms_real test_2_reals(ms_real x, ms_real y)
/* #[ */
{
  fprintf(fptr, "test_2_reals: x = %f\n", x);
  fprintf(fptr, "test_2_reals: y = %f\n", y);
  return x+y;
}
/* #] */

ms_real test_10_ms_real_pointers(
    int n, /* size of the next arrays */
    const ms_real *range,
    const ms_real *radius,
    const ms_real *ext,
    const ms_real *ssa,
    const ms_real *g,
    const ms_real *ext_bscat_ratio,
    const ms_real *ext_air,
    const ms_real *ssa_air,
    const ms_real *droplet_fraction,
    const ms_real *pristine_ice_fraction)
/* #[ */
{
  int i;
  ms_real rsum=0.0;

  for (i = 0; i < n; i++) {
    fprintf(fptr, "=============\n");
    fprintf(fptr, "=== i = %d ===\n", i);
    fprintf(fptr, "=============\n");
    fprintf(fptr, "test_10_ms_reals: range = %f\n", range[i]);
    rsum += range[i];
    fprintf(fptr, "test_10_ms_reals: radius = %f\n", radius[i]);
    rsum += radius[i];
    fprintf(fptr, "test_10_ms_reals: ext = %f\n", ext[i]);
    rsum += ext[i];
    fprintf(fptr, "test_10_ms_reals: ssa = %f\n", ssa[i]);
    rsum += ssa[i];
    fprintf(fptr, "test_10_ms_reals: g = %f\n", g[i]);
    rsum += g[i];
    fprintf(fptr, "test_10_ms_reals: ext_bscat_ratio = %f\n",
	    ext_bscat_ratio[i]);
    rsum += ext_bscat_ratio[i];
    fprintf(fptr, "test_10_ms_reals: ext_air = %f\n", ext_air[i]);
    rsum += ext_air[i];
    fprintf(fptr, "test_10_ms_reals: ssa_air = %f\n", ssa_air[i]);
    rsum += ssa_air[i];
    fprintf(fptr, "test_10_ms_reals: droplet_fraction = %f\n",
	    droplet_fraction[i]);
    rsum += droplet_fraction[i];
    fprintf(fptr, "test_10_ms_reals: pristine_ice_fraction = %f\n",
	    pristine_ice_fraction[i]);
    rsum += pristine_ice_fraction[i];
  }
  fprintf(fptr, "rsum = %f\n", rsum);
  
  return rsum;
}
/* #] */

ms_real test_ms_config(int n, ms_config *config)
/* #[ */
{
  int i;
  ms_real rsum=0.0;
  
  fprintf(fptr, "test_ms_config: config->small_angle_algorithm = %i\n",
	  config->small_angle_algorithm);
  rsum += config->small_angle_algorithm;
  fprintf(fptr, "test_ms_config: config->wide_angle_algorithm = %i\n",
	  config->wide_angle_algorithm);
  rsum += config->wide_angle_algorithm;
  fprintf(fptr, "test_ms_config: config->options = %i\n",
	  config->options);
  rsum += config->options;
  fprintf(fptr, "test_ms_config: config->max_scattering_order = %i\n",
	  config->max_scattering_order);
  rsum += config->max_scattering_order;
  fprintf(fptr, "test_ms_config: config->max_theta = %f\n",
	  config->max_theta);
  rsum += config->max_theta;
  fprintf(fptr, "test_ms_config: config->first_wide_angle_gate = %i\n",
	  config->first_wide_angle_gate);
  rsum += config->first_wide_angle_gate;
  fprintf(fptr, "test_ms_config: config->coherent_backscatter_enhancement = %f\n",
	  config->coherent_backscatter_enhancement);
  rsum += config->coherent_backscatter_enhancement;

  fprintf(fptr, "array size n = %i\n", n);
  for (i = 0; i < n; i++) {
    fprintf(fptr, "test_ms_config: config->small_angle_lag[%2i] = %f\n",
	   i, config->small_angle_lag[i]);
    rsum += config->small_angle_lag[i];
  }
	 
  fprintf(fptr, "test_ms_config: config->total_src = %f\n",
	  config->total_src);
  rsum += config->total_src;
  fprintf(fptr, "test_ms_config: config->total_reflected = %f\n",
	 config->total_reflected);
  rsum += config->total_reflected;
  fprintf(fptr, "test_ms_config: config->ss_multiplier = %f\n",
	 config->ss_multiplier);
  rsum += config->ss_multiplier;

  fprintf(fptr, "rsum = %f\n", rsum);
  
  return rsum;
}
/* #] */

ms_real test_ms_instrument(int nfov, ms_instrument instrument)
/* #[ */
{
  int i;
  ms_real rsum=0.0;
  fflush(fptr);
  fprintf(fptr, "test_ms_instrument: instrument.receiver_type = %i\n",
	  instrument.receiver_type);
  rsum += instrument.receiver_type;
  fprintf(fptr, "test_ms_instrument: instrument.altitude = %f\n",
	  instrument.altitude);
  rsum += instrument.altitude;
  fprintf(fptr, "test_ms_instrument: instrument.wavelength = %f\n",
	  instrument.wavelength);
  rsum += instrument.wavelength;
  fprintf(fptr, "test_ms_instrument: instrument.rho_transmitter = %f\n",
	  instrument.rho_transmitter);
  rsum += instrument.rho_transmitter;
  for (i = 0; i < nfov; i++) {
    fprintf(fptr, "test_ms_instrument: instrument.rho_receiver[%2i] = %f\n",
	    i, instrument.rho_receiver[i]);
    rsum += instrument.rho_receiver[i];
  }
  fprintf(fptr, "test_ms_instrument: instrument.nfov = %i\n",
	  instrument.nfov);
  rsum += instrument.nfov;

  fprintf(fptr, "rsum = %f\n", rsum);
  
  return rsum;
}
/* #] */

ms_real test_ms_surface(ms_surface surface)
/* #[ */
{
  ms_real rsum=0.0;
  fprintf(fptr, "test_ms_surface: surface.sigma0 = %f\n",
	  surface.sigma0);
  rsum += surface.sigma0;
  fprintf(fptr, "test_ms_surface: surface.diffuse_albedo = %f\n",
	  surface.diffuse_albedo);
  rsum += surface.diffuse_albedo;
  fprintf(fptr, "test_ms_surface: surface.direct_to_diffuse_albedo = %f\n",
	  surface.direct_to_diffuse_albedo);
  rsum += surface.direct_to_diffuse_albedo;
  fprintf(fptr, "test_ms_surface: surface.diffuse_to_direct_backscatter = %f\n",
	  surface.diffuse_to_direct_backscatter);
  rsum += surface.diffuse_to_direct_backscatter;
  fprintf(fptr, "test_ms_surface: surface.range = %f\n",
	  surface.range);
  rsum += surface.range;

  fprintf(fptr, "rsum = %f\n", rsum);
  
  return rsum;
}
/* #] */

ms_real test_output_real_arrays(ms_real *bscat_out,
				ms_real *bscat_air_out, int m)
/* #[ */
{
  int i;
  ms_real rsum=0.0;
  for (i = 0; i < m; i++) {
    bscat_out[i] = 2.3*i+0.78;
    fprintf(fptr, "test_output_real_arrays: %f\n", bscat_out[i]);
    rsum += bscat_out[i];
    bscat_air_out[i] = 3.4e-2*i+0.0089;
    fprintf(fptr, "test_output_real_arrays: %f\n", bscat_air_out[i]);
    rsum += bscat_air_out[i];

    fprintf(fptr, "rsum = %f\n", rsum);
  }
  return rsum;
}
/* #] */

/* this is the full interface to the multiscatter function */
int
test_multiscatter(
    /* Input data */
    int n,                    /* Number of input gates */
    int m,                    /* Number of output gates (>= n) */
    ms_config *config,        /* Configuration information */
    ms_instrument instrument, /* Structure containing instrument variables */
    ms_surface surface,       /* Surface scattering variables */
    const ms_real *range,     /* Height of each range gate, metres */
    const ms_real *radius,    /* Cloud/aerosol equivalent radius, microns */
    const ms_real *ext,       /* Total extinction coefficient, m-1 */
    const ms_real *ssa,       /* Total single-scatter albedo */
    const ms_real *g,         /* Total asymmetry factor */
    const ms_real *ext_bscat_ratio,/* Cloud/aerosol ext./bscat. ratio, sr */
    const ms_real *ext_air,   /* Air ext. coefft., m-1 (NULL for vacuum) */
    const ms_real *ssa_air,   /* Air single-scatter albedo */
    const ms_real *droplet_fraction,/* Fraction of extinction from droplets */
    const ms_real *pristine_ice_fraction,/* Fraction of ext from pristine ice */
    /* Output data */
    ms_real *bscat_out,       /* Measured backscatter, m-1 sr-1 */
    ms_real *bscat_air_out)   /* Measured backscatter of air, m-1 sr-1 */
/* #[ */
{
  int status;
  int i;
  ms_real rsum=0.0;

  rsum+= n;
  rsum += m;

  rsum += config->small_angle_algorithm;
  rsum += config->wide_angle_algorithm;
  rsum += config->options;
  rsum += config->max_scattering_order;
  rsum += config->max_theta;
  rsum += config->first_wide_angle_gate;
  rsum += config->coherent_backscatter_enhancement;
  for (i = 0; i < n; i++) { rsum += config->small_angle_lag[i]; }
  rsum += config->total_src;
  rsum += config->total_reflected;
  rsum += config->ss_multiplier;

  fprintf(fptr, "after adding config: rsum = %f\n", rsum);

  int nfov = instrument.nfov;
  rsum += instrument.receiver_type;
  rsum += instrument.altitude;
  rsum += instrument.wavelength;
  rsum += instrument.rho_transmitter;
  for (i = 0; i < nfov; i++) { rsum += instrument.rho_receiver[i]; }
  rsum += instrument.nfov;

  fprintf(fptr, "after adding instrument: rsum = %f\n", rsum);

  rsum += surface.sigma0;
  rsum += surface.diffuse_albedo;
  rsum += surface.direct_to_diffuse_albedo;
  rsum += surface.diffuse_to_direct_backscatter;
  rsum += surface.range;

  fprintf(fptr, "after adding surface: rsum = %f\n", rsum);

  for (i = 0; i < n; i++) {
    rsum += range[i];
    rsum += radius[i];
    rsum += ext[i];
    rsum += ssa[i];
    rsum += g[i];
    rsum += ext_bscat_ratio[i];
    rsum += ext_air[i];
    rsum += ssa_air[i];
    rsum += droplet_fraction[i];
    rsum += pristine_ice_fraction[i];
  }

  fprintf(fptr, "after adding the 10 arrays: rsum = %f\n", rsum);

  /* fill the output arrays with some dummy data to allow
     testing the interface */
  for (i = 0; i < m; i++) {
    bscat_out[i] = 1.23 * i;
    bscat_air_out[i] = 4.56 * i;
  }

  status = MS_SUCCESS;
  return status;
}
/* #] */
